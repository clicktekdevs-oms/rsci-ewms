#!/usr/bin/env bash
DBNAME=debs_ewms
DATE=`date +"%Y%m%d"`

SQLFILE=/var/log/mysql_bak/$DBNAME-${DATE}.sql
mysqldump  -uroot  $DBNAME > $SQLFILE
gzip $SQLFILE