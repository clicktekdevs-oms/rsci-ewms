<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('users')->insert(
            array(
                'username' => 'superadmin',
                'password' => bcrypt('superadmin'),
                'firstname' => 'superadmin',
                'lastname'  => 'superadmin',
                'role_id'   => 0,
                'store_code' => 8001

            )
        );

        DB::table('user_roles')->insert(
            array(
                'role_name' => 'superadmin',
                'permissions' => 'canViewPurchaseOrder,CanAccessPacking,CanAccessShipping,CanAccessSublocReceiving,CanAccessSublocPicking,CanAccessSublocShipping,CanAccessReturnToWh,CanAccessStoreOrders,CanAccessProductMasterList,CanAccessStoreMasterList,CanAccessUsers,CanAccessUserRoles,CanAccessAuditTrail'
            )
        );



    }
}
