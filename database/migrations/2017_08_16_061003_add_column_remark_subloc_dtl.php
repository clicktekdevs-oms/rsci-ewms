<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnRemarkSublocDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('subloc_rcv_dtl', function ( Blueprint $table ) {

            $table->string('remarks')->after('qty_rcv')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subloc_rcv_dtl', function ( Blueprint $table ) {

            $table->dropColumn('remarks');

        });
    }
}
