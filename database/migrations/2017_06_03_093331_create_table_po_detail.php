<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('po_detail', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('rcv_no');
            $table->integer('sku');
            $table->bigInteger('upc')->nullable();
            $table->integer('dept_code');
            $table->string('division');
            $table->string('slot_code')->nullable();
            $table->integer('qty_ord');
            $table->integer('qty_rcv');
            $table->integer('assigned_to');
            $table->tinyInteger('po_status')->default(4);
            $table->tinyInteger('is_jda_sync')->default(0);
            $table->unique(['rcv_no', 'sku', 'upc', 'dept_code']);
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('po_detail');
    }
}
