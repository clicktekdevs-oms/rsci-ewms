<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnJdaStatusRtnwh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('rtnwh_list', function ( Blueprint $table ) {

            $table->string('jda_status')->after('mts_status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('rtnwh_list', function ( Blueprint $table ) {

            $table->dropColumn('jda_status');

        });
    }
}
