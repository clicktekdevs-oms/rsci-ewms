<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSkuDescRemarkRtnwhDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('rtnwh_detail', function ( Blueprint $table ) {

            $table->bigInteger('sku')->after('mts_no');
            $table->string('item_desc')->after('upc')->nullable();
            $table->string('remarks')->after('qty_rcv')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('rtnwh_detail', function ( Blueprint $table ) {

            $table->dropColumn('sku');
            $table->dropColumn('item_desc');
            $table->dropColumn('remarks');

        });
    }
}
