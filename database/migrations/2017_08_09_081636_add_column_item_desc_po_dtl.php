<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnItemDescPoDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->string('short_name')->after('upc');
            $table->string('item_desc')->after('short_name');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->dropColumn('short_name');
            $table->dropColumn('item_desc');
        });
    }
}
