<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnBoxDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('box_detail', function ( Blueprint $table ) {

            $table->integer('tl_no')->after('id');
        });


    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('box_detail', function ( Blueprint $table ) {

            $table->dropColumn('tl_no');

        });
    }
}
