<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddTableShipDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->date('ship_date')->after('assigned_to')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->dropColumn('ship_date');
        });
    }
}
