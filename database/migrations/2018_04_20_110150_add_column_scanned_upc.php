<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnScannedUpc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->bigInteger('scanned_upc')->nullable()->after('upc');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->dropColumn('scanned_upc');

        });
    }
}
