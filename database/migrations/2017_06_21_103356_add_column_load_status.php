<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLoadStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->dropColumn('is_ship');
            $table->integer('load_status')->after('assigned_to')->default(3);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->dropColumn('load_status');
        });
    }
}
