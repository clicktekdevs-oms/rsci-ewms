<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create( 'load_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->string('load_code');
            $table->string('box_no');
            $table->integer('is_load')->default(0);
            $table->integer('is_sync')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('load_detail');
    }
}
