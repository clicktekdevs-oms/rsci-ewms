<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSublocRcvDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_rcv_dtl', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->unsignedInteger('tl_no');
            $table->unsignedBigInteger('sku');
            $table->bigInteger('upc');
            $table->string('short_name');
            $table->unsignedSmallInteger('qty_ord');
            $table->unsignedSmallInteger('qty_rcv');
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique(['tl_no', 'sku', 'upc']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_rcv_dtl');
    }
}
