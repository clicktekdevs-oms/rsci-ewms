<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductMasterlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('product_masterlist', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('sku');
            $table->bigInteger('upc');
            $table->string('short_name');
            $table->string('item_desc');
            $table->integer('dept_code');
            $table->integer('iset_code');
            $table->unique(['sku', 'upc']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('product_masterlist');
    }
}
