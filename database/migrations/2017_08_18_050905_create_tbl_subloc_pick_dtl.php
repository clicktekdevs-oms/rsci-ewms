<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSublocPickDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_pick_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('tl_no');
            $table->date('ship_date');
            $table->string('from_loc');
            $table->string('to_loc');
            $table->integer('dept_code');
            $table->bigInteger('sku');
            $table->bigInteger('upc');
            $table->string('short_desc');
            $table->string('item_desc');
            $table->string('item_size');
            $table->string('item_color');
            $table->float('rtl_price');
            $table->integer('qty_req');
            $table->integer('qty_rcv')->default(0);
            $table->string('remarks');
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique(['tl_no', 'sku', 'upc']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_pick_detail');
    }
}
