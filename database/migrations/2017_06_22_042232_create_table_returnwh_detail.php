<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturnwhDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('rtnwh_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('mts_no');
            $table->bigInteger('upc');
            $table->integer('qty_req');
            $table->integer('qty_rcv');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
            $table->unique( ['mts_no', 'upc'] );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('rtnwh_detail');
    }
}
