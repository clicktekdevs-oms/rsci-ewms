<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePickList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pick_list', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('tl_no');
            $table->integer('doc_no');
            $table->date('ship_date')->nullable();
            $table->integer('to_loc');
            $table->integer('assigned_to')->nullable();
            $table->tinyInteger('pick_status')->default(3);
            $table->tinyInteger('is_jda_sync')->default(0);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique( [ 'tl_no', 'doc_no', 'to_loc'] );


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pick_list');
    }
}
