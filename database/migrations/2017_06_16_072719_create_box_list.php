<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('box_list', function ( Blueprint $table ) {

                $table->bigIncrements('id');
                $table->bigInteger('box_no');
                $table->integer('tl_no');
                $table->integer('store_num');
                $table->integer('is_assign')->default(0);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('box_list');
    }
}
