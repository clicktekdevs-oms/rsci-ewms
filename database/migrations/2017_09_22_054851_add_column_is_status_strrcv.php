<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnIsStatusStrrcv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('store_rcv_list', function ( Blueprint $table ) {

            $table->string('jda_status')->after('tl_status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('store_rcv_list', function ( Blueprint $table ) {

            $table->dropColumn('jda_status')->nullable();

        });
    }
}
