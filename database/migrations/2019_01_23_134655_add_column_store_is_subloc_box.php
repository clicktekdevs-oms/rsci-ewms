<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStoreIsSublocBox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('store_rcv_dtl', function ( Blueprint $table ) {

            $table->tinyInteger('is_subloc')->default(0)->after('box_no');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('store_rcv_dtl', function ( Blueprint $table ) {

            $table->dropColumn('is_subloc');

        });
    }
}
