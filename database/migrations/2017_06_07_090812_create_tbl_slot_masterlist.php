<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSlotMasterlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('slot_masterlist', function ( Blueprint $table ) {

                $table->bigIncrements('id');
                $table->string('slot_name', 50);
                $table->string('slot_zone', 10);
                $table->integer('store_num');
                $table->unique(['slot_name', 'slot_zone']);
                $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('slot_masterlist');
    }
}
