<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStoreClertkAssignBoxList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_detail', function ( Blueprint $table ) {

            $table->integer('is_assign_store')->after('store_num')->nullable();
            $table->integer('is_box_status')->after('is_assign_store')->default(2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_detail', function ( Blueprint $table ) {

            $table->dropColumn('is_assign_store');
            $table->dropColumn('is_box_status');

        });
    }
}
