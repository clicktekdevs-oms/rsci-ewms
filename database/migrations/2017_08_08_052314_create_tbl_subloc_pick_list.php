<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSublocPickList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_pick_list', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('tl_no');
            $table->date('ship_date')->nullable();
            $table->integer('from_loc');
            $table->integer('to_loc');
            $table->integer('assigned_to')->nullable();
            $table->tinyInteger('pick_status')->default(3);
            $table->string('jda_status')->nullable();
            $table->tinyInteger('is_jda_sync')->default(0);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique( [ 'tl_no', 'to_loc', 'from_loc'] );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_pick_list');
    }
}
