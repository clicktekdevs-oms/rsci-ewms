<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLoadList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('load_list', function (Blueprint $table){

            $table->bigIncrements('id');
            $table->string('load_code');
            $table->integer('assigned_to')->nullable();
            $table->integer('is_ship')->default(0);
            $table->integer('is_sync')->default(0);
            $table->unique(['load_code']);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('load_list');
    }
}
