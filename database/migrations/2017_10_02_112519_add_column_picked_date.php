<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnPickedDate extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pick_list', function ( Blueprint $table ) {

            $table->date('picked_date')->after('assigned_to')->nullable();

        });

        Schema::table('subloc_pick_list', function ( Blueprint $table ) {

            $table->date('picked_date')->after('assigned_to')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pick_list', function ( Blueprint $table ) {

            $table->dropColumn('picked_date');

        });

        Schema::table('subloc_pick_list', function ( Blueprint $table ) {

            $table->dropColumn('picked_date');

        });

    }
}
