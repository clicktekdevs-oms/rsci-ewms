<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBoxDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('box_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('box_no');
            $table->bigInteger('upc');
            $table->integer('mov_qty');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('box_detail');
    }
}
