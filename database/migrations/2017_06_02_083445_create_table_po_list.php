<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTablePoList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('po_list', function (Blueprint $table) {

            $table->bigIncrements('id');
            $table->integer('rcv_no');
            $table->integer('po_no');
            $table->string('inv_no')->nullable();
            $table->integer('total_qty');
            $table->string('ship_ref_no')->nullable();
            $table->date('entry_date');
            $table->tinyInteger('po_status')->default(4);
            $table->tinyInteger('is_jda_sync')->default(0);
            $table->unique(['rcv_no', 'po_no']);
            $table->timestamps();



        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('po_list');
    }
}
