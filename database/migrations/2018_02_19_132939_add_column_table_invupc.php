<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTableInvupc extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('product_masterlist', function ( Blueprint $table ) {

            $table->bigInteger('new_upc')->nullable()->after('upc');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('product_masterlist', function ( Blueprint $table ) {

            $table->dropColumn('new_upc');

        });
    }
}
