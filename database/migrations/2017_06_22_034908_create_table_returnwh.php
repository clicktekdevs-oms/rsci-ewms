<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReturnwh extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('rtnwh_list', function ( Blueprint $table ) {

                $table->bigIncrements('id');
                $table->bigInteger('mts_no');
                $table->integer('from_loc');
                $table->integer('assigned_to')->nullable();
                $table->integer('mts_status')->default(3);
                $table->integer('is_sync')->default(0);
                $table->timestamp('created_at')->useCurrent();
                $table->timestamp('updated_at');
                $table->unique( ['mts_no', 'from_loc'] );

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('rtnwh_list');
    }
}
