<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblSublocRcv extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_rcv_list', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->unsignedInteger('tl_no');
            $table->date('ship_date');
            $table->unsignedInteger('from_loc');
            $table->unsignedInteger('to_loc');
            $table->unsignedSmallInteger('assigned_to')->nullable();
            $table->unsignedSmallInteger('tl_status')->default( 3 );
            $table->unsignedSmallInteger('is_jda_sync')->default(0);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique(['tl_no', 'ship_date', 'from_loc', 'to_loc']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_rcv_list');
    }
}
