<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStoreAssign extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->integer('assigned_to_store')->after('assigned_to')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->dropColumn('assigned_to_store');

        });
    }
}
