<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblJdaStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('po_list', function ( Blueprint $table ) {

            $table->string('jda_status')->after('po_status')->nullable();

        });

        Schema::table('pick_list', function ( Blueprint $table ) {

            $table->string('pick_jda_status')->after('pick_status')->nullable();
            $table->string('ship_jda_status')->after('pick_jda_status')->nullable();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('po_list', function ( Blueprint $table ) {

            $table->dropColumn('jda_status');

        });

        Schema::table('pick_list', function ( Blueprint $table ) {

            $table->dropColumn('pick_jda_status');
            $table->dropColumn('ship_jda_status');

        });
    }
}
