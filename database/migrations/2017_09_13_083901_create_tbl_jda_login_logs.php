<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblJdaLoginLogs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('jda_login_logs', function ( Blueprint $table ) {

            $table->bigIncrements('id' );
            $table->string('ref_no');
            $table->string('username');
            $table->string('password');
            $table->string('hash_password');
            $table->string('text_desc');
            $table->integer('is_status')->nullable();
            $table->timestamp('created_at')->useCurrent = true;

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('jda_login_logs');
    }
}
