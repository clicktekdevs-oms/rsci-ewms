<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SublocLoadDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create( 'subloc_load_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->string('load_code');
            $table->string('box_no');
            $table->integer('tl_no');
            $table->integer('from_store_num')->nullable();
            $table->integer('store_num')->nullable();
            $table->integer('is_load')->default(0);
            $table->integer('is_sync')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
            $table->index(['box_no', 'tl_no']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_load_detail');
    }
}
