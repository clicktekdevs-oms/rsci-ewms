<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class SublocLoadList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_load_list', function (Blueprint $table){

            $table->bigIncrements('id');
            $table->string('load_code');
            $table->integer('assigned_to')->nullable();
            $table->date('ship_date')->nullable();
            $table->integer('assigned_to_store')->nullable();
            $table->integer('is_sync')->default(0);
            $table->integer('load_status')->default(3);
            $table->integer('load_status_store')->default(2);
            $table->unique(['load_code']);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_load_list');
    }
}
