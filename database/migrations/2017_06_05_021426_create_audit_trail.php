<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAuditTrail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('audit_trail', function ( Blueprint $table ) {

            $table->bigIncrements('audit_id');
            $table->string('module');
            $table->string('reference');
            $table->string('action');
            $table->string('data_before');
            $table->string('data_after');
            $table->integer('user_id');
            $table->timestamps();
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('audit_trail');
    }
}
