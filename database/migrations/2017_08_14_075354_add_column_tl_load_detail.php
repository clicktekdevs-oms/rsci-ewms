<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnTlLoadDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_detail', function ( Blueprint $table ) {

            $table->integer('tl_no')->after('box_no');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_detail', function ( Blueprint $table ) {

            $table->dropColumn('tl_no');

        });
    }
}
