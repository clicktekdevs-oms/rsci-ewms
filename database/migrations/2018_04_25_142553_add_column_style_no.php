<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStyleNo extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->bigInteger('style_no')->nullable()->after('scanned_upc');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->dropColumn('style_no');

        });
    }
}
