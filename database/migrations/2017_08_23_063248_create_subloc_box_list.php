<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSublocBoxList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_box_list', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('box_no');
            $table->integer('tl_no');
            $table->integer('from_store_num');
            $table->integer('store_num');
            $table->integer('is_assign')->default(0);
            $table->string('is_open')->default(0);
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
            $table->index(['box_no', 'tl_no']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_box_list');
    }
}
