<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddBoxListColumn extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('box_list', function ( Blueprint $table ) {

            $table->string('is_open')->after('is_assign')->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('box_list', function ( Blueprint $table ) {

            $table->dropColumn('is_open');
        });
    }
}
