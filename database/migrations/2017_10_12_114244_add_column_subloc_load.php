<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSublocLoad extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('subloc_load_list', function ( Blueprint $table ) {

            $table->string('seal_no')->after('load_code')->nullable();
            $table->string('plate_no')->after('seal_no')->nullable();
            $table->string('delivery_helper')->after('plate_no')->nullable();
            $table->string('driver_name')->after('delivery_helper')->nullable();
            $table->string('witnessed_sealed')->after('driver_name')->nullable();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subloc_load_list', function ( Blueprint $table ) {

            $table->dropColumn('seal_no');
            $table->dropColumn('plate_no');
            $table->dropColumn('delivery_helper');
            $table->dropColumn('driver_name');
            $table->dropColumn('witnessed_sealed');


        });
    }
}
