<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePickDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('pick_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('tl_no');
            $table->bigInteger('doc_no');
            $table->date('ship_date');
            $table->integer('dept_code');
            $table->bigInteger('sku');
            $table->bigInteger('upc');
            $table->string('short_desc');
            $table->string('item_desc');
            $table->string('item_size');
            $table->string('item_color');
            $table->string('to_loc');
            $table->string('from_slot');
            $table->string('to_slot')->nullable();
            $table->float('rtl_price');
            $table->integer('qty_req');
            $table->integer('qty_rcv')->default(0);
            $table->string('remarks');
            $table->unique(['tl_no', 'doc_no', 'sku', 'upc', 'ship_date']);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('pick_detail');
    }

}
