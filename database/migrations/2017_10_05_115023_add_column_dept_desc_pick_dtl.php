<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnDeptDescPickDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('pick_detail', function ( Blueprint $table ) {

            $table->string('dept_desc')->after('dept_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('pick_detail', function ( Blueprint $table ) {

            $table->dropColumn('dept_desc');

        });
    }
}
