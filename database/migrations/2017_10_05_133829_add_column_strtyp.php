<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStrtyp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('store_masterlist', function ( Blueprint $table ) {

            $table->string('store_type')->after('store_nam');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('store_masterlist', function ( Blueprint $table ) {

            $table->dropColumn('store_type');

        });
    }
}
