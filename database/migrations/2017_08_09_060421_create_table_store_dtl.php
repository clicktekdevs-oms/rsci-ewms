<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStoreDtl extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('store_rcv_dtl', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('tl_no');
            $table->unsignedBigInteger('sku');
            $table->unsignedBigInteger('upc');
            $table->string('item_desc')->nullable();
            $table->integer('qty_req')->default(0);
            $table->integer('qty_rec')->default(0);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique( [ 'tl_no', 'sku', 'upc'] );


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('store_rcv_dtl');
    }
}
