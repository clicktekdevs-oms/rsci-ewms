<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSublocBoxDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('subloc_box_detail', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->bigInteger('tl_no');
            $table->bigInteger('box_no');
            $table->bigInteger('upc');
            $table->integer('mov_qty');
            $table->timestamp('created_at')->useCurrent();
            $table->timestamp('updated_at');
            $table->index(['tl_no', 'box_no', 'upc']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('subloc_box_detail');
    }
}
