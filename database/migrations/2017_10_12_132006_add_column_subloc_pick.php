<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnSublocPick extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('subloc_pick_detail', function ( Blueprint $table ) {

            $table->string('dept_desc')->after('dept_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('subloc_pick_detail', function ( Blueprint $table ) {

            $table->dropColumn('dept_desc');

        });
    }
}
