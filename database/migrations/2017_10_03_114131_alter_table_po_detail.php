<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTablePoDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->renameColumn('dept_code', 'division_code');

        });

        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->string('department')->after('division');


        });

        Schema::table('po_detail', function ( Blueprint $table ) {

            $table->integer('dept_code')->after('division_code');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('po_detail', function ( Blueprint $table ) {


            $table->dropColumn('dept_code');
            $table->dropColumn('department');


        });
    }
}
