<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnStrRcvDtlBox extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('store_rcv_dtl', function ( Blueprint $table ) {

            $table->string('box_no')->nullable()->after('qty_rec');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('store_rcv_dtl', function ( Blueprint $table ) {

            $table->dropColumn('box_no');

        });
    }
}
