<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddColumnLoadListStrStatus extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->integer('load_status_store')->after('load_status')->default(2);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::table('load_list', function ( Blueprint $table ) {

            $table->dropColumn('load_status_store');

        });
    }
}
