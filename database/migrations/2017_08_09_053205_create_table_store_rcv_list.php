<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStoreRcvList extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('store_rcv_list', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('tl_no');
            $table->date('ship_date')->nullable();
            $table->integer('from_loc');
            $table->integer('to_loc');
            $table->integer('assigned_to')->nullable();
            $table->tinyInteger('tl_status')->default(3);
            $table->tinyInteger('is_jda_sync')->default(0);
            $table->timestamp('created_at')->useCurrent = true;
            $table->timestamp('updated_at');
            $table->unique( [ 'tl_no', 'to_loc', 'from_loc'] );


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('store_rcv_list');
    }
}
