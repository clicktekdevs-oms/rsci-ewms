<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblStoreMasterlist extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('store_masterlist', function ( Blueprint $table ) {

            $table->bigIncrements('id');
            $table->integer('store_num');
            $table->string('store_nam');
            $table->string('store_add1');
            $table->string('store_add2');
            $table->unique(['store_num']);
            $table->timestamps();

        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::drop('store_masterlist');
    }
}
