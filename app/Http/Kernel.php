<?php

namespace App\Http;

use Illuminate\Foundation\Http\Kernel as HttpKernel;

class Kernel extends HttpKernel
{
    /**
     * The application's global HTTP middleware stack.
     *
     * @var array
     */
    protected $middleware = [
        \Illuminate\Foundation\Http\Middleware\CheckForMaintenanceMode::class,
        \App\Http\Middleware\EncryptCookies::class,
        \Illuminate\Cookie\Middleware\AddQueuedCookiesToResponse::class,
        \Illuminate\Session\Middleware\StartSession::class,
        \Illuminate\View\Middleware\ShareErrorsFromSession::class,
//        \App\Http\Middleware\VerifyCsrfToken::class,
    ];

    /**
     * The application's route middleware.
     *
     * @var array
     */
    protected $routeMiddleware = [
        'auth' => \App\Http\Middleware\Authenticate::class,
        'auth.basic' => \Illuminate\Auth\Middleware\AuthenticateWithBasicAuth::class,
        'guest' => \App\Http\Middleware\RedirectIfAuthenticated::class,
        'receiving_gate'  => \App\Http\Middleware\ReceivingGate::class,
        'str_receiving_gate'  => \App\Http\Middleware\GateStrReceiving::class,
        'picking_gate'  => \App\Http\Middleware\GatePicking::class,
        'shipping_gate'  => \App\Http\Middleware\GateShipping::class,
        'returnwh_gate'  => \App\Http\Middleware\GateReturnToWh::class,
        'subloc_rcv_gate'  => \App\Http\Middleware\GateSublocRcv::class,
        'subloc_picking_gate'  => \App\Http\Middleware\GateSublocPicking::class,
        'subloc_ship_gate'  => \App\Http\Middleware\GateSublocShipping::class,
    ];
}
