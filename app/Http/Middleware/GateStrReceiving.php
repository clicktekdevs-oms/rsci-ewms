<?php

namespace App\Http\Middleware;

use Closure;
use Gate;
class GateStrReceiving
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ( $request->user()->role_id ) {

            if (Gate::denies('role-access', 'CanAccessStoreOrders'))  {
                return redirect('/');
            }
        }
        return $next($request);
    }
}
