<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/


get('/', 'UserController@getLogin');
post('users/signin', 'UserController@postSignin');

/*
 * For API REST
 */
Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {

    get('login/{username}/{password}', 'ApiLoginController@index');
    get('login/checkrole/{username}/{password}', 'ApiLoginController@checkRoles');
    get('login/checkcanship/{username}/{password}', 'ApiLoginController@checkIfCanShip');
    get('slot-code',    'ApiLoginController@getSlot');

    /* * * * * * * * * * * * * * * * * * *  *
     *                                      *
     *  Receiving routes API                *
     *                                      *
     * * * * * * * * * * * * * * * * * * *  */

    get('po-list/{pilerid}', "ApiReceivingController@getPOList");
    get('po-detail/{rcv_no}/{division_code}/{deptcode}',    "ApiReceivingController@getPODetails");
    get('po-update/{rcv_no}/{division_code}/{dept_code}/{upc}/{qty_rcv}/{slot}',  "ApiReceivingController@updatePODetails");
    get('po-unlisted/{rcv_no}/{division_code}/{dept_code}/{upc}/{qty_rcv}/{slot}/{division}/{department}/{assignedto}', "ApiReceivingController@insertUnlisted");


    /* * * * * * * * * * * * * * * * * * *  *
     *                                      *
     *  Picking routes API                  *
     *                                      *
     * * * * * * * * * * * * * * * * * * *  */


    get('pick-list/{pilerid}', 'ApiPickingController@getPickingList');
    get('pick-detail/{tl_no}',  'ApiPickingController@getPickingDetail');
    get('pick-update/{tl_no}/{upc}/{qty}', 'ApiPickingController@updatePickingDetail');
    get('pick-status/{tlno}',   'ApiPickingController@updatePickListStatus');

    get('box-code/{store_num}', 'ApiPickingController@getBoxCode');
    get('box-code/validate/{boxcode}',  'ApiPickingController@validateBox');
    get('box-insert/{box_code}/{storeloc}/{tlno}',  'ApiPickingController@insertNewBox');
    get('box-list-open/{store}','ApiPickingController@selectAvailBox');
    get('box-insert-detail/{tl}/{upc}/{box_code}/{qty}', 'ApiPickingController@insertBoxDetail');

    /* * * * * * * * * * * * * * * * * * * * *
      *                                      *
      *  Loading routes API                  *
      *                                      *
      * * * * * * * * * * * * * * * * * * *  */

    get('load-list/{piler}', 'ApiLoadShipController@getLoad');
    get('load-detail/{load_code}', 'ApiLoadShipController@getLoadDetail');
    get('load-update-detail/{load_code}/{box_code}', 'ApiLoadShipController@updateDetail');
    get('load-update-status/{load_code}/{ship_date}',   'ApiLoadShipController@updateStatus');

    /* * * * * * * * * * * * * * * * * * *  *
     *                                      *
     *  Return to Warehouse routes API      *
     *                                      *
     * * * * * * * * * * * * * * * * * * *  */


    get('rtnwh-list/{pilerid}', 'ApiReturnWHController@getList');
    get('rtnwh-detail/{mtsno}', 'ApiReturnWHController@getDetails');
    get('rtnwh-update/{mts}/{upc}/{qty}/{slot}', 'ApiReturnWHController@updateDetails');
    get('rtnwh-status/{mts}',   'ApiReturnWHController@updateStatus');
    get('rtnwh-add-items/{mts}/{upc}/{qty}', 'ApiReturnWHController@insertItems');

    /* * * * * * * * * * * * * * * * * * *  *
     *                                      *
     *  Subloc Receiving routes API         *
     *                                      *
     * * * * * * * * * * * * * * * * * * *  */

    get('subloc-rcv/{pilerid}', 'ApiSublocReceivingController@getRcvList');
    get('subloc-rcv-dtl/{tl}',  'ApiSublocReceivingController@getRcvDtl');
    get('subloc-rcv-update-status/{mts}',   'ApiSublocReceivingController@UpdateStatus');
    get('subloc-rcv-update-dtl/{mts}/{upc}/{qty}', 'ApiSublocReceivingController@updateDetails');
    get('subloc-rcv-unlisted/{mts}/{upc}/{qty}',   'ApiSublocReceivingController@insertUnlisted');



    /* * * * * * * * * * * * * * * * * * *  *
     *                                      *
     *  Subloc Picking routes API           *
     *                                      *
     * * * * * * * * * * * * * * * * * * *  */


    get('subloc-pick-list/{pilerid}', 'ApiSublocPickingController@getPickingList');
    get('subloc-pick-detail/{tl_no}',  'ApiSublocPickingController@getPickingDetail');
    get('subloc-pick-update/{tl_no}/{upc}/{qty}', 'ApiSublocPickingController@updatePickingDetail');
    get('subloc-pick-status/{tlno}',   'ApiSublocPickingController@updatePickListStatus');

    get('subloc-box-code/{store_num}', 'ApiSublocPickingController@getBoxCode');
    get('subloc-box-code/validate/{boxcode}',  'ApiSublocPickingController@validateBox');
    get('subloc-box-insert/{box_code}/{storeloc}/{tlno}/{fromstore}',  'ApiSublocPickingController@insertNewBox');
    get('subloc-box-list-open/{store}','ApiSublocPickingController@selectAvailBox');
    get('subloc-box-insert-detail/{tl}/{upc}/{box_code}/{qty}', 'ApiSublocPickingController@insertBoxDetail');


    /* * * * * * * * * * * * * * * * * * * * *
      *                                      *
      *  Subloc Loading routes API           *
      *                                      *
      * * * * * * * * * * * * * * * * * * *  */

    get('subloc-load-list/{piler}', 'ApiSublocLoadController@getLoad');
    get('subloc-load-detail/{load_code}', 'ApiSublocLoadController@getLoadDetail');
    get('subloc-load-update-detail/{load_code}/{box_code}', 'ApiSublocLoadController@updateDetail');
    get('subloc-load-update-status/{load_code}/{ship_date}',   'ApiSublocLoadController@updateStatus');

    /* * * * * * * * * * * * * * * * * * *  *
   *                                      *
   *  Store Receiving routes API         *
   *                                      *
   * * * * * * * * * * * * * * * * * * *  */

    get('store-rcv/{user}', 'ApiStoreReceivingController@getPell'); //list of pell
    get('store-rcv/pell-details/{pell}/{clerk}',    'ApiStoreReceivingController@getPellDetails'); //details of pell
    get('store-rcv/box-details/{box}',  'ApiStoreReceivingController@getBoxDetails'); // box detail
    get('store-rcv/update-details/{tl}/{upc}/{qty}/{box}',    'ApiStoreReceivingController@updateMtsQuantity');
    get('store-rcv/box-status/{box}/{user}',   'ApiStoreReceivingController@updateStatus');
    get('store-rcv/insert-unlisted/{tl}/{box}/{upc}/{qty}/{user}',    'ApiStoreReceivingController@insertItems');


    get('subloc-store-rcv/{user}', 'ApiSublocStoreReceivingController@getPell'); //list of pell
    get('subloc-store-rcv/pell-details/{pell}/{clerk}',    'ApiSublocStoreReceivingController@getPellDetails'); //details of pell
    get('subloc-store-rcv/box-details/{box}',  'ApiSublocStoreReceivingController@getBoxDetails'); // box detail
    get('subloc-store-rcv/update-details/{tl}/{upc}/{qty}/{box}',    'ApiSublocStoreReceivingController@updateMtsQuantity');
    get('subloc-store-rcv/box-status/{box}',   'ApiSublocStoreReceivingController@updateStatus');
    get('subloc-store-rcv/insert-unlisted/{tl}/{box}/{upc}/{qty}/{user}',    'ApiSublocStoreReceivingController@insertItems');
});


Route::group(['middleware' => 'auth'], function () {

    get('users', 'UserController@showUsers');
    get('users/create', 'UserController@createUser');
    get('users/filter',     'UserController@filterUser');
    get('users/role',       'UserController@showRoles');
    get('user/role/add',            'UserController@addRoles');
    get('user/role/edit/{id}',      'UserController@editRoles');
    get('user/role/filter',     'UserController@filterRoles');
    get('users/logout', 'UserController@logout');
    get('users/update/{id}', 'UserController@updateUser');
    get('users/delete/{id}',        'UserController@deleteUser');
    get('users/password/{id}',  'UserController@changePassword');

    post('user/changepassword', 'UserController@updatePassword');
    post('users/insert', 'UserController@insertUsers');
    post('user/update/data',    'UserController@updateUserData');
    post('user/roles/insert',       'UserController@insertRoles');
    post('user/roles/update',           'UserController@updateRoles');

    get('po', 'PurchaseOrderController@getList');
    get('po/filter',  'PurchaseOrderController@filterPos');
    get('po/division/{po}', 'PurchaseOrderController@getDivision');
    get('po/division/{po}/{val}', 'PurchaseOrderController@getSpecificDivision');
    get('po/division/detail/{rcv_no}/{dept_code}', 'PurchaseOrderController@getPOdetail');
    get('po/close/{po}/{rcv_no}',     'PurchaseOrderController@closedPO');
    get('po/partial/{po}/{rcv_no}',       'PurchaseOrderController@partialPO');
    get('po/pulljda',   'PurchaseOrderController@pullDataJda');
    get('po/rpt/shortage/pdf/{pono}/{rcv}', 'PurchaseOrderController@shortageRptPdf');
    get('po/rpt/upc/pdf/{pono}/{rcv}', 'PurchaseOrderController@NewUpcRptPdf');
    get('po/rpt/upc/csv/{pono}/{rcv}', 'PurchaseOrderController@NewUpcRptCsv');

    get('po/unlisted',  'PurchaseOrderController@unlistedItemRpt');

    post('po-list-post',    'PurchaseOrderController@poListPost');
    post('po/assign-to-piler',  'PurchaseOrderController@assignToPiler');
    post('po/assign-piler',     'PurchaseOrderController@assignPiler');
    post('po/detail/update',    'PurchaseOrderController@updatePOdetail');
    post('po/update/shipref',   'PurchaseOrderController@updateShipRef');


    get('picking', 'PickingController@getPickingList' );
    get('picking/filter',   'PickingController@getPickingFilter');
    get('picking/detail/{mts}', 'PickingController@getPickingDetails');
    get('picking/close/{mts}',  'PickingController@closeMts');
    get('picking/pulldata', 'PickingController@pullData');
    get('picking/mtslist/{mts}',    'PickingController@getMtsList');
    get('picking/packageslip/{mts}',    'PickingController@getPackageSlip');

    post('picking-list-post','PickingController@updatePickingShipDate');
    post('picking/assign-piler',    'PickingController@assignPiler');
    post('picking/assign-to-piler',     'PickingController@assignToPiler');
    post('picking/updateshipdate',  'PickingController@updateShipDate');

    get('subloc-rcv',   'SublocReceivingController@index');
    get('subloc-rcv-dtl/{mts}', 'SublocReceivingController@getDetails');
    get('subloc-rcv/filter',    'SublocReceivingController@filterDetails');
    get('subloc-rcv/pulldata',  'SublocReceivingController@pullData');
    get('subloc-rcv/closemts/{mts}',  'SublocReceivingController@closeMts');


    get('subloc/picking', 'SublocPickingController@getPickingList' );
    get('subloc/picking/filter',   'SublocPickingController@getPickingFilter');
    get('subloc/picking/detail/{mts}', 'SublocPickingController@getPickingDetails');
    get('subloc/picking/close/{mts}',  'SublocPickingController@closeMts');
    get('subloc/picking/pulldata', 'SublocPickingController@pullData');
    get('subloc/picking/mtslist/{mts}',    'SublocPickingController@getMtsList');
    get('subloc/picking/packageslip/{mts}',    'SublocPickingController@getPackageSlip');

    post('subloc/picking-list-post','SublocPickingController@updatePickingShipDate');
    post('subloc/picking/assign-piler',    'SublocPickingController@assignPiler');
    post('subloc/picking/assign-to-piler',     'SublocPickingController@assignToPiler');
    post('subloc/picking/updateshipdate',  'SublocPickingController@updateShipDate');


    post('subloc-rcv/actionform',   'SublocReceivingController@chooseAction');
    post('subloc-rcv/assign-piler',     'SublocReceivingController@assignPiler');
    post('subloc-rcv/update/quantity',  'SublocReceivingController@updateQty');

    get('masterlist/products',  'ProductMasterlistController@index');
    get('masterlist/products/filter',   'ProductMasterlistController@filterProducts');
    get('masterlist/slots',  'SlotMasterlistController@index');
    get('masterlist/slots/filter',  'SlotMasterlistController@filterSlot');
    get('masterlist/stores',    'StoreMasterlistController@index');
    get('masterlist/stores/filter', 'StoreMasterlistController@filterStores');

    /**
     *
     *  Loading/Shipping route
     *
     **/

    get('loadship', 'LoadShipController@getLoadShip');
    get('loadship/detail/{pellno}', 'LoadShipController@getLoadShipDetails');
    get('loadship/assignbox/{pellno}', 'LoadShipController@assignBoxtoPell');
    get('loadship/assignbox/{pellno}/filter', 'LoadShipController@filterAssignBoxtoPell');
    get('loadship/filter',      'LoadShipController@filterLoad');
    get('loadship/ship/{load_code}',    'LoadShipController@shipLoad');
    get('loadship/loadsheet/{load_code}',   'LoadShipController@getLoadSheet');


    post('loadship/add','LoadShipController@addOthers');
    post('loadship/new', 'LoadShipController@createLoad');
    post('loadship/assign',     'LoadShipController@assignPiler');
    post('loadship/assigned_to',    'LoadShipController@assignTo');
    post('loadship/assign-to-pell', 'LoadShipController@assignToPell');
    post('loadship/removebox',  'LoadShipController@removeBox');


    /**
     * Subloc Loading
     *
     */

    get('subloc/loadship', 'SublocLoadingController@getLoadShip');
    get('subloc/loadship/detail/{pellno}', 'SublocLoadingController@getLoadShipDetails');
    get('subloc/loadship/assignbox/{pellno}', 'SublocLoadingController@assignBoxtoPell');
    get('subloc/loadship/assignbox/{pellno}/filter', 'SublocLoadingController@filterAssignBoxtoPell');
    get('subloc/loadship/filter',      'SublocLoadingController@filterLoad');
    get('subloc/loadship/ship/{load_code}',    'SublocLoadingController@shipLoad');
    get('subloc/loadship/loadsheet/{load_code}',   'SublocLoadingController@getLoadSheet');


    post('subloc/loadship/add', 'SublocLoadingController@addDetails');
    post('subloc/loadship/new', 'SublocLoadingController@createLoad');
    post('subloc/loadship/assign',     'SublocLoadingController@assignPiler');
    post('subloc/loadship/assigned_to',    'SublocLoadingController@assignTo');
    post('subloc/loadship/assign-to-pell', 'SublocLoadingController@assignToPell');
    post('subloc/loadship/removebox',  'SublocLoadingController@removeBox');

    /**
     *
     * Return to WH
     *
     **/

    get('returnwh', 'ReturnWHController@getList');
    get('returnwh/detail/{mtsno}', 'ReturnWHController@getDetail');
    get('returnwh/filter',  'ReturnWHController@filterList');
    get('returnwh/close/{mtsno}', 'ReturnWHController@closeMts');
    get('returnwh/pulldata',    'ReturnWHController@pullData');

    post('returnwh/update/qty', 'ReturnWHController@updateQty');
    post('returnwh/assign',  'ReturnWHController@assignPiler');
    post('returnwh/assign-to-piler',    'ReturnWHController@assignToPiler');

    get('audit-trail',      'AuditTrailController@index');
    get('audit-trail/filter',   'AuditTrailController@filterTrails');

    get('store-receiving',  'StoreReceivingController@index');
    get('store-receiving/filter',  'StoreReceivingController@filterStoreReceiving');
    get('store-receiving/pulldata',  'StoreReceivingController@pullData');
    get('store-receiving/{pellno}', 'StoreReceivingController@getPellDetails');
    get('store-receiving/box-detail/{box}', 'StoreReceivingController@getBoxDetails');

    post('store-receiving/updateqty',   'StoreReceivingController@updateQty');
    post('store-receiving/assign',  'StoreReceivingController@assignClerk');
    post('store-receiving/assign-clerk',  'StoreReceivingController@assignToClerk');
    post('store-receiving/receive', 'StoreReceivingController@receiveLoad');

    get('store-subloc-receiving',   'SublocStoreReceivingController@index');
    get('store-subloc-receiving/pulldata',   'SublocStoreReceivingController@pullData');
    get('store-subloc-receiving/filter',   'SublocStoreReceivingController@getSublocRcvListFilter');
    get('store-subloc-receiving/{pell}',   'SublocStoreReceivingController@getPellDetails');
    get('store-subloc-receiving/box-detail/{box}',  'SublocStoreReceivingController@getBoxDetails');

    post('store-subloc-receiving/receive', 'SublocStoreReceivingController@receiveLoad');
    post('store-subloc-receiving/updateqty',   'SublocStoreReceivingController@updateQty');
    post('store-subloc-receiving/assign',  'SublocStoreReceivingController@assignClerk');
    post('store-subloc-receiving/assign-clerk',  'SublocStoreReceivingController@assignToClerk');




});