<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
use App\Helper\CommonHelper;
use Auth;
use PDF;
class ReturnWHController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $db;

    public function __construct(db $databaseManager)
    {
        $this->db  = $databaseManager;
        $this->middleware('returnwh_gate');
    }

    public function getList( Request $request ) {
        //
        $request->flash();

        $mts_no         = $request->get('filter_doc_no');
        $store          = $request->get('filter_store');
        $piler          = $request->get('filter_stock_piler');
        $entry_date     = $request->get('filter_entry_date');
        $pick_status    = $request->get('filter_pick_status');

        $_getUser = $this->db->table('users')
                             ->select($this->db->raw("id, concat(firstname, ' ', lastname) as fullname"))
                             ->where('role_id', 6)
                             ->orderBy('id')
                             ->get();

        $_getStores = $this->db->table('store_masterlist')
                               ->select($this->db->raw('store_num, store_nam'))
                               ->orderBy('store_nam')
                               ->get();

        $_getList = $this->db->table('rtnwh_list')
                             ->select( $this->db->raw("id, 
                                                       mts_no,
                                                       (select store_nam from wms_store_masterlist where store_num = from_loc) as from_loc,
                                                       (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as fullname,
                                                        is_sync,
                                                        assigned_to,
                                                        mts_status,
                                                        created_at"));

                            if ( !empty( $mts_no ) ) {
                                $_getList->whereRaw("tl_no like '$mts_no%'");
                            }
                            //for store name
                            if ( !empty( $store ) ) {
                                $_getList->where('to_loc', $store);
                            }
                            //for piler
                            if ( !empty( $piler ) ) {
                                $_getList->where( 'assigned_to', $piler );
                            }

                            if ( !empty( $pick_status ) ) {
                                $_getList->where( 'mts_status', $pick_status );
                            }else {
                                $_getList->where( 'mts_status', 3 );
                            }

                            if ( ! empty( $entry_date ) ) {

                                $_getList->whereRaw("date(created_at) = '$entry_date'");
                            }

                             $_getList = $_getList->orderBy('id', 'desc')->paginate( 10 );

        $data = array();
        $data['mts_list'] = $_getList;
        $data['users']    = $_getUser;
        $data['stores']   = $_getStores;

        return view('return_wh.reverse_list', $data );

    }

    public function filterList( Request $request ) {

        $request->flash();

            $mts_no = trim($request->get('filter_doc_no'));
            $piler  = $request->get('filter_stock_piler');
            $store  = $request->get('filter_store');
            $date   = $request->get('filter_entry_date');
            $pick_status    = $request->get('filter_pick_status');

        $_getUser = $this->db->table('users')
            ->select($this->db->raw("id, concat(firstname, ' ', lastname) as fullname"))
            ->where('role_id', 6)
            ->orderBy('id')
            ->get();

        $_getStores = $this->db->table('store_masterlist')
            ->select($this->db->raw('store_num, store_nam'))
            ->orderBy('store_nam')
            ->get();

        $_getList = $this->db->table('rtnwh_list')
            ->select( $this->db->raw("id, 
                                     mts_no,
                                     (select store_nam from wms_store_masterlist where store_num = from_loc) as from_loc,
                                     (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as fullname,
                                     is_sync,
                                     assigned_to,
                                     mts_status,
                                     created_at"));

            if ( !empty( $mts_no ) ) {

                $_getList->whereRaw("mts_no like '$mts_no%'");
            }

            if ( !empty( $store ) ) {

                $_getList->whereRaw("from_loc = $store");

                $str_name = $this->db->table('store_masterlist')
                    ->select('store_nam')
                    ->where('store_num', $store)
                    ->first();

                $str_name = $str_name->store_nam;
            }

            if (!empty( $piler) ) {

                $_getList->whereRaw("assigned_to = $piler");

                $picker = $this->db->table('users')
                    ->select($this->db->raw("CONCAT(firstname, ' ',lastname) as name"))
                    ->where('id', $piler)
                    ->first();

                $picker = $picker->name;
            }

            if ( ! empty( $date ) ) {

                $_getList->whereRaw("date(created_at) = '$date'");
            }

            if ( !empty( $pick_status ) ) {
                $_getList->where( 'mts_status', $pick_status );
            }

            $_getList = $_getList->orderBy('id')->paginate( 10 );

        $data = array();
        $data['mts_list'] = $_getList;
        $data['users']    = $_getUser;
        $data['stores']   = $_getStores;
        $data['str_name'] = isset( $str_name ) ? $str_name : '';
        $data['picker']   = isset( $picker ) ? $picker : '';

        return view('return_wh.reverse_list', $data );

    }

    public function getDetail( $mts_no ) {

        $_getList    = $this->db->table($this->db->raw('wms_rtnwh_list'))
                                ->select($this->db->raw("mts_no, (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as assigned_to, mts_status "))
                                ->where('mts_no', $mts_no)
                                ->first();

        $_getDetails = $this->db->table($this->db->raw('wms_rtnwh_detail a'))
                                ->select($this->db->raw('
                                        mts_no, sku,
                                        upc,
                                        item_desc,
                                        qty_req,
                                        qty_rcv,
                                        remarks
                                        '))
                                ->where('mts_no', $mts_no )
                                ->get();

        $data = array();
        $data['mts_list']   = $_getList;
        $data['mts_detail'] = $_getDetails;

        return view( 'return_wh.detail', $data );
    }

    public function assignPiler( Request $request ) {

        $mts = $request->get('mts_no');
        $action_btn = $request->get('returhwh_post');

        if ( $action_btn == 'report' ) {

            $_getList = $this->db->table('rtnwh_list')
                ->select($this->db->raw("mts_no, (select store_nam from wms_store_masterlist where store_num = from_loc) as from_loc, (select concat(firstname,  ' ',lastname) from wms_users where id = assigned_to) as assigned_to"))
                ->whereIn('mts_no', $mts)
                ->get();


            $_getDetails = $this->db->table($this->db->raw('wms_rtnwh_list a'))
                            ->select($this->db->raw(
                                'DISTINCT a.mts_no, b.upc, c.short_name, b.qty_req, b.qty_rcv'
                            ))
                            ->join($this->db->raw('wms_rtnwh_detail b'), $this->db->raw('a.mts_no'), '=', $this->db->raw('b.mts_no'))
                            ->join($this->db->raw('wms_product_masterlist c'), $this->db->raw('b.upc'), '=', $this->db->raw('c.upc'))
                            ->whereRaw('b.qty_req <> b.qty_rcv')
                            ->whereIn($this->db->raw('a.mts_no'), $mts)
                            ->orderBy($this->db->raw('b.upc'))
                ->get();


            $data = array();
            $data['lists'] = $_getList;
            $data['details'] = $_getDetails;

            $pdf = PDF::loadView('return_wh.report_list', $data );
            $pdf->setPaper('a4', 'landscape');
            return $pdf->stream();
//
//

        } else {

            if ( isset( $mts ) ) {

                $picker = $this->db->table('users')
                    ->select($this->db->raw("id, CONCAT(firstname, ' ',lastname) as name"))
                    ->where('role_id', 6)
                    ->get();

                $mts = implode(',', $mts );

                $data = array();
                $data['mts_no'] = $mts;
                $data['pilers'] = $picker;

                return view('return_wh.assign_piler_form', $data );
            }else {

                return redirect('returnwh')->with('message', 'Please select piler!');
            }

        }

    }

    public function assignToPiler( Request $request ) {

        $mts    = $request->get('mts_no');
        $piler  = $request->get('piler');

        $mts_no = explode(',', $mts);

        if ( !empty( $piler ) ) {


                $this->db->table('rtnwh_list')
                    ->whereIn( 'mts_no', $mts_no )
                    ->update( ['assigned_to' => $piler, 'mts_status' => 2] );

        }else {

            $this->db->table('rtnwh_list')
                ->whereIn( 'mts_no', $mts_no )
                ->update( ['assigned_to' => null, 'mts_status' => 3] );
        }

        $arrParams = array(
            'module'		=> 'Return To Warehouse',
            'action'		=> 'Assign MTS',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Assign MTS no '.json_encode($mts_no).' to '.$piler,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('returnwh')->with('message', 'MTS Successfully assigned!');

    }

    public function closeMts( $mts_no ) {

        if ( isset( $mts_no ) ) {

            //close MTS No.
            $this->db->table('rtnwh_list')->where( 'mts_no', $mts_no)->update(array( 'mts_status' => 0));

            $arrParams = array(
                'module'		=> 'Return To Warehouse',
                'action'		=> 'Close MTS No. '.$mts_no,
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> '',
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

            return redirect()->back()->with( 'message', 'MTS Successfully Posted!');
        }
    }

    public function updateQty( Request $request ) {

        $mts_no     = $request->get('mts_no');
        $sku        = $request->get('sku');
        $qty_rcv    = $request->get('qty_rcv');


        $data = array();
        $data['qty_rcv'] = is_numeric($qty_rcv) ? $qty_rcv : 0;

        $this->db->table('rtnwh_detail')
            ->where( 'mts_no', $mts_no )
            ->where( 'sku', $sku )
            ->update( $data );

        $arrParams = array(
            'module'		=> 'Return to WHSE',
            'action'		=> 'Update Mts Details',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Update Quantity of sku='.$sku.'quantity='.$qty_rcv.';mts_no='.$mts_no,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', 'Quantity Successfully Updated!');

    }

    public function pullData() {

        $filename   = 'job_rtnwh.php';
        $mod        = 'rtnwh';

        CommonHelper::pullData( $filename, $mod );

        // AuditTrail
        $data_after = "";

        $arrParams = array(
            'module'		=> 'Return To Warehouse',
            'action'		=> 'Pull Data from JDA',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> '',
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('returnwh')->with('message', 'Successfully pulled from JDA!');

    }


}
