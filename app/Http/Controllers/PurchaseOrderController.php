<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Session;
use App\PurchaseOrder;
use App\Helper\CommonHelper;
use Illuminate\Database\DatabaseManager as DB;
use Auth;
use PDF;
use Gate;
class PurchaseOrderController extends Controller
{

    public function __construct(Request $request, DB $databaseManager)
    {

        $this->request  = $request;
        $this->db       = $databaseManager;
        $this->middleware('receiving_gate');

    }


    public function getList( Request $request )
    {

        if (Gate::denies('role-access', 'canViewPurchaseOrder')) {
            return redirect('/');
        }

        $request->flash();

        $po_no      = trim($request->get('filter_po_no'));
        $ship_ref   = trim($request->get('filter_shipment_reference_no'));
        $inv_no     = trim($request->get('filter_invoice_no'));
        $entry_date = trim($request->get('filter_entry_date'));
        $po_status  = trim($request->get('filter_po_status'));

        $query = $this->db->table('po_list');
        if ( !empty( $po_no ) ) {
            $query->whereRaw("po_no like '$po_no%'");
        }
        if ( !empty( $ship_ref ) ) {
            $query->whereRaw("ship_ref_no like '$ship_ref%'");
        }
        if ( !empty( $inv_no ) ) {
            $query->whereRaw("inv_no like '$inv_no%'");
        }
        if ( !empty( $entry_date ) ) {
            $query->whereRaw("entry_date like '$entry_date%'");
        }
        if ( !empty( $po_status ) ) {
            $query->whereRaw("po_status = $po_status");
        }else {
            $query->whereRaw("po_status = 4");
        }

        $query = $query->orderBy('updated_at', 'desc')->paginate(10);

        $data = array();
        $data['result'] = $query;

        return view('purchase_order.list', $data);


    }

    public function filterPos( Request $request ) {

        $request->flash();

        $po_no      = trim($request->get('filter_po_no'));
        $ship_ref   = trim($request->get('filter_shipment_reference_no'));
        $inv_no     = trim($request->get('filter_invoice_no'));
        $entry_date = trim($request->get('filter_entry_date'));
        $po_status  = trim($request->get('filter_po_status'));


        $query = $this->db->table('po_list');
                        if ( !empty( $po_no ) ) {
                            $query->whereRaw("po_no like '$po_no%'");
                        }
                        if ( !empty( $ship_ref ) ) {
                            $query->whereRaw("ship_ref_no like '$ship_ref%'");
                        }
                        if ( !empty( $inv_no ) ) {
                            $query->whereRaw("inv_no like '$inv_no%'");
                        }
                        if ( !empty( $entry_date ) ) {
                            $query->whereRaw("entry_date like '$entry_date%'");
                        }
                        if ( !empty( $po_status ) ) {
                            $query->where( "po_status", $po_status );
                        }


        $query = $query->paginate(10);

        $data = array();
        $data['result'] = $query;

        return view('purchase_order.list', $data);


    }

    public function getDivision( $rcv_no ) {


        $getList = $this->db->table('po_list')
                          ->select($this->db->raw('po_no, rcv_no, ship_ref_no, entry_date, total_qty'))
                          ->where('rcv_no', $rcv_no)
                          ->first();

        $getDivision = $this->db->table('po_detail')
                                ->select($this->db->raw('DISTINCT division_code, division'))
                                ->where('rcv_no', $rcv_no)
                                ->orderBy('division')
                                ->get();

        $query = $this->db->table('po_detail')
                    ->select($this->db->raw("rcv_no,
                                             division,
                                             department,
                                             sum(qty_ord) as qty_ord,
                                             division_code,
                                             dept_code,
                                             sum(qty_rcv) as qty_rcv,
                                             (select CONCAT(firstname, ' ', lastname) from wms_users where id = assigned_to) as assigned_to , po_status"))
                    ->where('rcv_no', $rcv_no)
                    ->groupBy('dept_code')
                    ->orderBy('division_code')
                    ->orderBy('dept_code')
                    ->get();


        $data = array();
        $data['list']       = $getList;
        $data['result']     = $query;
        $data['divisions']  = $getDivision;

        return view('purchase_order.division', $data );

    }

    public function getSpecificDivision($rcv_no, $div_code ) {




        $getList = $this->db->table('po_list')
            ->select($this->db->raw('po_no, rcv_no, ship_ref_no, entry_date, total_qty'))
            ->where('rcv_no', $rcv_no)
            ->first();

        $getDivision = $this->db->table('po_detail')
            ->select($this->db->raw('DISTINCT division_code, division'))
            ->where('rcv_no', $rcv_no)
            ->orderBy('division')
            ->get();

        $getDivisionName = $this->db->table('po_detail')
            ->select($this->db->raw('DISTINCT division_code, division'))
            ->where('division_code', $div_code)
            ->orderBy('division')
            ->first();

        $query = $this->db->table('po_detail')
            ->select($this->db->raw("rcv_no,
                                             division,
                                             department,
                                             sum(qty_ord) as qty_ord,
                                             division_code,
                                             dept_code,
                                             sum(qty_rcv) as qty_rcv,
                                             (select CONCAT(firstname, ' ', lastname) from wms_users where id = assigned_to) as assigned_to , po_status"))
            ->where('rcv_no', $rcv_no)
            ->where('division_code', $div_code)
            ->groupBy('dept_code')
            ->orderBy('division_code')
            ->get();


        $data = array();
        $data['list']       = $getList;
        $data['result']     = $query;
        $data['divisions']  = $getDivision;
        $data['division_name']  = $getDivisionName->division;

        return view('purchase_order.division', $data );

    }

    public function getPOdetail( $rcv_no, $dept_code ) {

        $_getList = $this->db->table($this->db->raw('wms_po_detail a'))
            ->select($this->db->raw('DISTINCT a.sku, c.po_no, a.upc, a.qty_ord, a.qty_rcv, a.rcv_no, a.po_status, a.short_name, a.remarks'))
            ->join($this->db->raw('wms_po_list c'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('c.rcv_no'))
            ->where($this->db->raw('a.rcv_no'), $rcv_no)
            ->where($this->db->raw('a.dept_code'), $dept_code)
            ->orderBy('qty_ord')
            ->orderBy('sku')
            ->get();

        $_getListFirst = $this->db->table($this->db->raw('wms_po_detail a'))
            ->select($this->db->raw("(select concat(firstname, ' ', lastname) from wms_users where id = a.assigned_to ) as assigned_to, b.po_no, a.rcv_no, a.division"))
            ->join($this->db->raw('wms_po_list b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
            ->where($this->db->raw('a.rcv_no'), $rcv_no)
            ->where($this->db->raw('a.dept_code'), $dept_code)
            ->first();


        $data = array();
        $data['po_details'] = $_getList;
        $data['po_detail']  = $_getListFirst;

        return view('purchase_order.detail', $data);

    }

    public function assignToPiler( Request $request ) {

        $checkbox       = $request->get('checkbox');
        $dept_code      = $request->get('dept_code');
        $division_code  = $request->get('division_code');
        $rcv_no         = $request->get('rcv_no');

        $div_code = array();
        $dpt_code = array();

        foreach ( $checkbox as $item ) {

            $data = array();

            $div_code[]       = $division_code[$item];
            $dpt_code[]     = $dept_code[$item];

        }

        $_getDeptCodeName = $this->db->table('po_detail')
            ->select('division_code', 'division', 'dept_code', 'department')->distinct()
            ->whereIn('division_code', $div_code )
            ->whereIn('dept_code', $dpt_code )
            ->get();


        $division = array();
        $department = array();
        foreach ( $_getDeptCodeName as $name ) {

            $division[] =  $name->division;
            $department[] = $name->department;
            $dpt_id[]     = $name->dept_code;
            $div_id[]     = $name->division_code;
        }

        $_getPiler = $this->db->table('users')
                              ->select( 'id', 'firstname', 'lastname', 'role_id')
                              ->where('role_id', 6)
                              ->get();



        $data['division']  = implode(',', array_unique($division) );
        $data['department']  = implode(',', $department );
        $data['dept_code'] = implode(',', $dept_code );
        $data['dpt_id']  = implode(',', $dpt_id );
        $data['div_id'] = implode(',', array_unique($div_id) );
        $data['rcv_no']    = $rcv_no;
        $data['pilers']    = $_getPiler;

        return view('purchase_order.assign_piler_form', $data );

    }

    public function assignPiler( Request $request ) {

        $dept_code  = $request->get('dept_code');
        $div_id     = $request->get('division_code');


        $recv_nos   = $request->get('receiver_num');
        $user       = $request->get('stock_piler');

        $dept_code = explode(',', $dept_code);

        foreach ( $dept_code as $dept ) {

            $this->db->table('po_detail')
                     ->where('rcv_no', $recv_nos)
                     ->where('dept_code', $dept)
                     ->where('division_code', $div_id)
                     ->update(['assigned_to' => $user, 'po_status' => 3]);

        }

        return redirect('po/division/'.$recv_nos)->with('message', 'Department Successfully assigned!');

    }

    public function closedPO( $po_no, $rcv_no ) {

        if ( isset( $po_no ) && isset( $rcv_no ) ) {
            /*
             * Update PO to 0 (Closed)
             */

            $data = array();
            $data['po_status'] = 0;

            // PO LIST TABLE
            $this->db->table('po_list')
                     ->where( 'po_no', $po_no )
                     ->where( 'rcv_no', $rcv_no )
                     ->update($data);

            // PO DETAIL TABLE

            $this->db->table('po_detail')
                ->where( 'rcv_no', $rcv_no )
                ->update($data);

            $arrParams = array(
                'module'		=> 'PO Receiving',
                'action'		=> 'Close PO',
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> 'Close PO # '.$po_no,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

            return redirect()->back()->with( 'message', 'PO Successfully Closed!' );
        }

    }

    public function partialPO( $po_no, $rcv_no ) {

        if ( isset( $po_no ) && isset( $rcv_no ) ) {
            /*
             * Update PO to 0 (Closed)
             */

            $data = array();
            $data['po_status'] = 1;

            // PO LIST TABLE
            $this->db->table('po_list')
                ->where( 'po_no', $po_no )
                ->where( 'rcv_no', $rcv_no )
                ->update($data);

            // PO DETAIL TABLE

            $this->db->table('po_detail')
                ->where( 'rcv_no', $rcv_no )
                ->update( $data );

            $arrParams = array(
                'module'		=> 'PO Receiving',
                'action'		=> 'Partial Close PO',
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> 'Partial Close PO # '.$po_no,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);


            return redirect()->back()->with( 'message', 'PO Status changed to Partial Received!' );
        }

    }

    public function updatePOdetail( Request $request ) {

        $upc = $request->get('upc');
        $sku = $request->get('sku');
        $rcv = $request->get('rcv_no');
        $qty = $request->get('qty_rcv');

        $data = array();
        $data['qty_rcv'] = is_numeric($qty) ? $qty : 0;

        $this->db->table('po_detail')
                 ->where( 'rcv_no', $rcv )
                 ->where( 'sku', $sku )
                 ->where( 'upc', $upc )
                 ->update( $data );

        $arrParams = array(
            'module'		=> 'PO Receiving',
            'action'		=> 'Update PO Details',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Update Quantity of sku='.$sku.';upc='.$upc.';quantity='.$qty.';rcv_no='.$rcv,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', 'Quantity Successfully Updated!');

    }

    public function poListPost( Request $request ) {

        $po_no          = $request->get('po_no');
        $rcv_no         = $request->get('rcv_no');
        $ship_ref_no    = $request->get('ship_ref');
        $submit_btn     = $request->get('submitButton');
        $rcvr_no        = $request->get('rcvr_no');


        if ( $submit_btn == 'ship_ref_no' ) {

            $this->db->table('po_list')
                ->where('po_no', $po_no)
                ->where('rcv_no', $rcv_no)
                ->update(['ship_ref_no' => $ship_ref_no]);

            $arrParams = array(
                'module' => 'PO Receiving',
                'action' => 'Update Shipment Reference number',
                'reference' => $ship_ref_no,
                'data_before' => '',
                'data_after' => 'ship_ref_no => ' . $ship_ref_no,
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);
            return redirect('po')->with( 'message', 'Successfully Update Shipment Reference number!' );


        } else {

            return $this->unlistedItemRpt( $rcvr_no );
        }


    }

    public function pullDataJda() {

        $filename   = 'job_porcv.php';
        $mod        = 'porcv';

        CommonHelper::pullData( $filename, $mod );

        // AuditTrail
        $data_after = "";

        $arrParams = array(
            'module'		=> 'PO Receiving',
            'action'		=> 'Pull Data from JDA',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> '',
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('po')->with('message', 'Successfully pulled from JDA!');
    }

    /**
     * Report Shortage/Overtage PDF
     * @param $pono
     * @param $rcv_no
     */

    public function shortageRptPdf( $pono, $rcv_no ) {

    $_getList = $this->db->table('po_list')
                         ->select('po_no', 'rcv_no', 'ship_ref_no', 'inv_no')
                         ->where('rcv_no', $rcv_no)
                         ->where('po_no', $pono)
                         ->first();


    $_getDetails = $this->db->table($this->db->raw('wms_po_list a'))
                            ->select($this->db->raw(
                                'DISTINCT a.po_no, a.rcv_no, a.ship_ref_no, a.inv_no, b.sku, b.upc, b.short_name,b.division_code,b.qty_ord,b.qty_rcv,b.remarks'
                            ))
                            ->join($this->db->raw('wms_po_detail b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
                            ->whereRaw('b.qty_ord != 0')
                            ->whereRaw('b.qty_ord <> b.qty_rcv')
                            ->whereRaw("a.po_no = $pono")
                            ->whereRaw("a.rcv_no = $rcv_no")
                            ->orderBy($this->db->raw('b.division_code'))
                            ->orderBy($this->db->raw('b.short_name'))
                            ->orderBy($this->db->raw('b.sku'))
                            ->get();

        $data = array();
        $data['details'] = $_getDetails;
        $data['list']    = $_getList;

        $pdf = PDF::loadView('purchase_order.report_list', $data );
        $pdf->setPaper('a4', 'landscape');
         return $pdf->stream();

    }

    public function unlistedItemRpt( $rcv_no ) {

        $_getList = $this->db->table( $this->db->raw('wms_po_detail a'))
                             ->select( $this->db->raw("b.po_no, c.short_name, a.sku, a.upc, a.qty_rcv, (select concat(firstname, ' ', lastname) from wms_users where id = a.assigned_to) as assigned_to,a.created_at"))
                             ->leftJoin( $this->db->raw('wms_po_list b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
                             ->leftJoin( $this->db->raw('wms_product_masterlist c'), $this->db->raw('a.sku'), '=', $this->db->raw('c.sku'))
                             ->whereRaw('a.qty_ord = 0')
                             ->whereIn($this->db->raw('a.rcv_no'), $rcv_no )
                             ->get();

        $data = array();
        $data['details'] = $_getList;

        $pdf = PDF::loadView('purchase_order.unlisted', $data );
        $pdf->setPaper('a4', 'landscape');
        return $pdf->stream();

    }

    public function updateShipRef( Request $request ) {

        $ship_ref   = $request->get('ship_ref');
        $po_no      = $request->get('pono');
        $rcv_no     = $request->get('rcv_no');

        $this->db->table('po_list')
                 ->where('po_no', $po_no )
                 ->where('rcv_no', $rcv_no )
                 ->update( ['ship_ref_no' => $ship_ref ] );

        return redirect()->back()->with('message', 'Shipment reference no. successfully updated!');
    }

    public function NewUpcRptPdf( $pono, $rcv_no ) {

        $_getList = $this->db->table('po_list')
            ->select('po_no', 'rcv_no', 'ship_ref_no', 'inv_no')
            ->where('rcv_no', $rcv_no)
            ->where('po_no', $pono)
            ->first();


        $_getDetails = $this->db->table($this->db->raw('wms_po_list a'))
            ->select($this->db->raw(
                "DISTINCT a.po_no, a.rcv_no, a.ship_ref_no, a.inv_no, b.sku, b.upc, b.scanned_upc, b.short_name,b.division_code,b.qty_ord,b.qty_rcv,b.remarks,(SELECT CONCAT(firstname, ' ', lastname)  from wms_users where id = b.assigned_to) as assigned_to,b.created_at"
            ))
            ->join($this->db->raw('wms_po_detail b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
            ->whereRaw("a.po_no = $pono")
            ->whereRaw("a.rcv_no = $rcv_no")
            ->whereRaw("b.scanned_upc is not null")
            ->orderBy($this->db->raw('b.division_code'))
            ->orderBy($this->db->raw('b.short_name'))
            ->orderBy($this->db->raw('b.sku'))
            ->get();

        $data = array();
        $data['details'] = $_getDetails;
        $data['list']    = $_getList;

        $pdf = PDF::loadView('purchase_order.newupc', $data );
        $pdf->setPaper('a4', 'landscape');
        return $pdf->stream();

    }

    public function NewUpcRptCsv( $pono, $rcv_no ) {

        $_getList = $this->db->table('po_list')
            ->select('po_no', 'rcv_no', 'ship_ref_no', 'inv_no')
            ->where('rcv_no', $rcv_no)
            ->where('po_no', $pono)
            ->first();


        $_getDetails = $this->db->table($this->db->raw('wms_po_list a'))
            ->select($this->db->raw(
                "DISTINCT b.sku, b.division, b.style_no, b.scanned_upc, b.upc, a.po_no, b.qty_rcv"
            ))
            ->join($this->db->raw('wms_po_detail b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
            ->whereRaw("a.po_no = $pono")
            ->whereRaw("a.rcv_no = $rcv_no")
            ->whereRaw("b.scanned_upc is not null")
            ->orderBy($this->db->raw('b.division_code'))
            ->orderBy($this->db->raw('b.short_name'))
            ->orderBy($this->db->raw('b.sku'))
            ->get();



        $getDetailsToCsv = array();
       foreach ( $_getDetails as $detail ) {

           $getDetailsToCsv[] = (array) $detail;
       }


        ob_start();

        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename=NUPC.csv');
        header('Expires: 0');
        header('Cache-Control: no-cache');
        header('Content-Length: '. ob_get_length());

        $fp = fopen("php://output", 'w');

        fputs($fp,"\"SKU\",\"DIV\",\"STYLE\",\"OUPC\",\"NUPC\",\"PO\",\"QTY\"".PHP_EOL);

        foreach ( $getDetailsToCsv as $fields ) {

            $sku = $fields['sku'];
            $division = $fields['division'];
            $style  = $fields['style_no'];
            $scanned_upc = $fields['scanned_upc'];
            $upc =  $fields['upc'];
            $pono = $fields['po_no'];
            $qty = $fields['qty_rcv'];

            fputs($fp,"$sku,\"$division\",\"$style\",\"$upc\",\"$scanned_upc\",$pono,$qty".PHP_EOL);

        }

        $streamSize = ob_get_length();

        fclose($fp);

        header('Content-Length: '.$streamSize);

        ob_end_flush();

    }



}
