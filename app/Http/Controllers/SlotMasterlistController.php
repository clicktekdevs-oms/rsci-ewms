<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
class SlotMasterlistController extends Controller
{


    public function __construct(DB $databaseManager)
    {
        $this->db = $databaseManager;
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        //
        $request->flash();


        $_getList = $this->db->table('slot_masterlist')
                         ->select('id', 'slot_name', 'slot_zone', 'store_num')
                         ->orderBy('id')
                         ->paginate(10);

        $data =array();

        $data['slots'] = $_getList;

        return view('slots.list', $data );
    }

    public function filterSlot( Request $request ) {

        $request->flash();

        $slot_no = $request->get('filter_slot_no');

        $_getList = $this->db->table('slot_masterlist')
            ->select('id', 'slot_name', 'slot_zone', 'store_num')
            ->whereRaw("slot_name like '$slot_no%'")
            ->orderBy('id')
            ->paginate(10);

        $data =array();

        $data['slots'] = $_getList;

        return view('slots.list', $data );
    }


}
