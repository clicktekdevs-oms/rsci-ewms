<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth, Session, Validator;
use Illuminate\Database\DatabaseManager as DB;
use App\UserRoles;
class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    
    protected $db;
    
    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function getLogin()
    {
        //

        if ( Auth::check() ) {


            if ( Auth::user()->role_id == 5 ) {

                return redirect('loadship');
            }if ( Auth::user()->role_id == 7 ) {

                return redirect('store-receiving');
            }
            else {

                return redirect('po');

            }
        }

         return view('users.login');

    }

    public function showUsers(Request $request)
    {
        $request->flash();

        if ( Auth::user()->role_id == 2 || Auth::user()->role_id == 3 ) {

            $query = $this->db->table('users')
                ->select($this->db->raw('id, username, firstname, lastname, (select role_name from wms_user_roles where id = role_id) as role_id, created_at, store_code'))
                ->where('store_code', 8001)
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        }elseif( Auth::user()->role_id == 7 ) {

            $query = $this->db->table('users')
                ->select($this->db->raw('id, username, firstname, lastname, (select role_name from wms_user_roles where id = role_id) as role_id, created_at, store_code'))
                ->where('store_code', Auth::user()->store_code)
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        }else {
            $query = $this->db->table('users')
                ->select($this->db->raw('id, username, firstname, lastname, (select role_name from wms_user_roles where id = role_id) as role_id, created_at, store_code'))
                ->orderBy('created_at', 'desc')
                ->paginate(10);
        }

        $role = $this->db->table('user_roles')
                         ->orderBy('id')
                         ->get();


        $data = array();
        $data['result'] = $query;
        $data['roles'] = $role;

        return view('users.list', $data);

    }

    public function filterUser( Request $request ) {

        $request->flash();
        $username = $request->get('username');


        $query = $this->db->table('users')
            ->select($this->db->raw('id, username, firstname, lastname, (select role_name from wms_user_roles where id = role_id) as role_id, created_at, store_code'));
        if ( !empty( $username) ) {

            $query->whereRaw("username LIKE '$username%'");
        }
        $query = $query->orderBy('created_at', 'desc')
            ->paginate(10);

        $data = array();
        $data['result'] = $query;

        return view('users.list', $data);

    }

    public function createUser() {

        $getRoles = $this->db->table('user_roles');

        $wh_admin = array('2', '3');

        if ( in_array(Auth::user()->role_id, $wh_admin ) ){

            $getRoles->whereIn('id', [2, 3, 4, 5, 6]);
        }

        $getRoles = $getRoles->orderBy('id')->get();

        $getStores = $this->db->table('store_masterlist')
                              ->select('store_num', 'store_nam')
                              ->orderBy('store_nam')
                              ->get();
        $data = array();
        $data['roles'] = $getRoles;
        $data['stores'] = $getStores;


        return view('users.insert', $data );
    }

    public function logout() {

        Auth::logout();
        return redirect('/');

    }

    public function postSignin( Request $request ) {

        $username = $request->get('username');
        $password = $request->get('password');

        if (Auth::attempt(array('username'=>$username, 'password'=>$password))) {


            if ( Auth::user()->role_id == 5 ) {

                return redirect('loadship');

            }elseif ( Auth::user()->role_id == 6 || Auth::user()->role_id == 8 ) {

                Auth::logout();

                return redirect()->back()->with('message', 'Unauthorized Access!');

            }elseif ( Auth::user()->role_id == 7 ) {

                return redirect('store-receiving');
            }

            return redirect('po')->with('success', trans('users.text_success_login'));

        }



        return redirect('/')
            ->with('message', trans('users.error_login'))
            ->withInput();
    }

    public function showRoles( Request $request ) {

        $request->flash();

        $query = $this->db->table('user_roles')
                      ->select('id', 'role_name', 'permissions', 'created_at')
                      ->get();

        $data = array();
        $data['roles'] = $query;

        return view('user_roles.list', $data);
    }

    public function addRoles() {

        return view('user_roles.insert');
    }


    public function editRoles( $role_id ) {

        $query = $this->db->table('user_roles')
                      ->where('id', $role_id )
                      ->first();

        $permissions = explode(',', $query->permissions);

        $data = array();
        $data['role_id']   = $query->id;
        $data['role_name'] = $query->role_name;
        $data['permission'] = $permissions;


        return view('user_roles.update', $data);
    }

    public function filterRoles( Request $request ) {

        $request->flash();

        $query = $this->db->table('user_roles')
            ->select('id', 'role_name', 'permissions', 'created_at')
            ->where('role_name', $request->get('role_name'))
            ->get();

        $data = array();
        $data['roles'] = $query;

        return view('user_roles.list', $data);

    }

    public function updateRoles( Request $request ) {

        $rules = array(
            'role_name'		=> 'required|unique:user_roles,role_name,'.$request->get('role_id').'id',
            'permissions'	=> 'required'
        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('user/role/edit/' . $request->get('role_id'))
                ->withErrors($validator)
                ->withInput();
        } else {

            $role = $this->db->table('user_roles')->where('id', $request->get('role_id'))->first();

            $data_before = 'Role Name: ' . $role->role_name . '<br />' .
                'Permissions: ' . $role->permissions;
            // AuditTrail

            // Update User Role Details
            $arrParams = array(
                'role_name'		=> $request->get('role_name'),
                'permissions'	=> implode(',', $request->get('permissions')),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('user_roles')->where('id', $request->get('role_id'))->update($arrParams);

            // AuditTrail
            $data_after = 'Role Name: ' . $request->get('role_name') . '<br />' .
                'Permissions: ' . implode(', ', $request->get('permissions'));

            $arrParams = array(
                'module'		=> 'User Roles',
                'action'		=> 'Modified User Role',
                'reference'		=> $request->get('role_name'),
                'data_before'	=> $data_before,
                'data_after'	=> $data_after,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

            return redirect('user/role/edit/'.$request->get('role_id'))->with('message', 'Successfully updated the role!');

        }
    }

    public function updateUser( $user_id ) {

        $query = $this->db->table('users')
                      ->select($this->db->raw("id, username, firstname, lastname, role_id,(select role_name from wms_user_roles where id = role_id) as role_name, store_code, (select store_nam from wms_store_masterlist where store_num = store_code) as store_nam"))
                      ->where('id', $user_id)
                      ->first();

        $getRoles = $this->db->table('user_roles');

        $wh_admin = array('2', '3');

        if ( in_array(Auth::user()->role_id, $wh_admin ) ){

            $getRoles->whereIn('id', [2, 3, 4, 5, 6]);
        }

        $getRoles = $getRoles->orderBy('id')->get();

        $getStores = $this->db->table('store_masterlist')
                              ->select('store_num', 'store_nam')
                              ->orderBy('store_nam')
                              ->get();


        $data = array();
        $data['user']       = $query;
        $data['roles']      = $getRoles;
        $data['stores']     = $getStores;

        return view('users.update', $data );
    }

    public function changePassword( $id ) {

        $data = array();
        $data['user_id'] = $id;

        return view('users.password', $data );

    }

    /**
     * @param Request $request
     */

    public function updatePassword( Request $request ) {

        $user_id    = $request->get('user_id');
        $password   = $request->get('password');

        $rules = array(
            'password'				            => 'required|confirmed',
            'password_confirmation'				=> 'required',

        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return redirect()->back()
                ->withErrors($validator)
                ->withInput();

        }else {

            $this->db->table('users')->where('id', $user_id)->update(['password' => bcrypt( $password ) ]);

            return redirect( 'users/password/'.$user_id )
                ->with('message', 'Password Successfully Changed');

        }


    }

    public function updateUserData( Request $request ) {

        $user_id = $request->get('user_id');
        $rules = array(
            'firstname'				=> 'required|min:2',
            'lastname'				=> 'required|min:2',
            'role_id'				=> 'required',

        );
        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {

            return redirect( 'users/update/'.$user_id )
                        ->withErrors($validator)
                        ->withInput();

        } else {
            // AuditTrail
            $user = $this->db->table('users')->where('id', $user_id)->first();

            $data_before = 'Username: ' . $user->username . '<br />' .
                'First Name: ' . $user->firstname . '<br />' .
                'Last Name: ' . $user->lastname . '<br />';

            if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3 ) {

                $store_code = 8001;

            }elseif ( Auth::user()->role_id == 7 ) {

                $store_code = Auth::user()->store_code;

            } else {

                $store_code = $request->get('filter_store');

            }


            // Update User Details
            $arrParams = array(
                'firstname'     => $request->get('firstname'),
                'lastname'      => $request->get('lastname'),
                'store_code'    => $store_code,
                'role_id'       => $request->get('role_id'),
                'updated_at'    => date('Y-m-d H:i:s')
            );

            $this->db->table('users')->where('id', $user_id)->update($arrParams);

            // AuditTrail

            $data_after = 'Username: ' . $request->get('username') . '<br />' .
                'First Name: ' . $request->get('firstname') . '<br />' .
                'Last Name: ' . $request->get('lastname') . '<br />' .
                /*  'Barcode: ' . $request->get('barcode') . '<br />' .*/
                'Role: ' . '' . '<br />' .
                'Brand: ' . '';

            $arrParams = array(
                'module'        => 'Users',
                'action'        => 'Modified User',
                'reference'     => $request->get('username'),
                'data_before'   => $data_before,
                'data_after'    => $data_after,
                'user_id'       => Auth::user()->id,
                'created_at'    => date('Y-m-d H:i:s'),
                'updated_at'    => date('Y-m-d H:i:s')
            );
            $this->db->table('audit_trail')->insert($arrParams);

            // AuditTrail

            return redirect('users/update/'.$user_id)->with('message', 'Successfully updated the user profile!');

        }
    }

    public function deleteUser( $user_id ) {


        $this->db->table('users')->where('id', $user_id)->delete();

        $data_after = "Deleted: $user_id";

        $arrParams = array(
            'module'		=> 'Users',
            'action'		=> 'Deleted Users',
            'reference'		=> $user_id,
            'data_before'	=> '',
            'data_after'	=> $data_after,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );
        $this->db->table('audit_trail')->insert($arrParams);
        // AuditTrail

        return redirect('users')->with('message', 'Successfully deleted the selected users!');

    }
    
    
    
    public function insertUsers( Request $request ) {

        $rules = array(
            'username'				=> 'required|unique:users,username,id',
            'password'				=> 'required|alpha_num|between:6,12|confirmed',
            'password_confirmation'	=> 'required|alpha_num|between:6,12',
            'firstname'				=> 'required|min:2',
            'lastname'				=> 'required|min:2',
            'role_id'				=> 'required',
            'filter_store'			=> ''
        );
        
        $validator = Validator::make($request->all(), $rules);


        if ($validator->fails()) {
            return redirect('users/create')
                ->withErrors($validator)
                ->withInput();
        } else {



            if (Auth::user()->role_id == 2 || Auth::user()->role_id == 3 ) {

                $store_code = 8001;

            }elseif ( Auth::user()->role_id == 7 ) {

                $store_code = Auth::user()->store_code;
            }
            else {

                $store_code = $request->get('filter_store');

            }
            $arrParams = array(
                'username'		=> $request->get('username'),
                'password'		=> bcrypt($request->get('password')),
                'firstname'		=> $request->get('firstname'),
                'lastname'		=> $request->get('lastname'),
                'role_id'		=> $request->get('role_id'),
                'store_code'	=> $store_code,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('users')->insert($arrParams);

            $data_before = '';

            $data_after = json_encode('Username: ' . $request->get('username').
                'First Name:' . $request->get('firstname').
                'Last Name:' . $request->get('lastname').
                'Role: ' . '');

            $arrParams = array(
                'module'		=> 'Users',
                'action'		=> 'Added New User',
                'reference'		=> $request->get('username'),
                'data_before'	=> $data_before,
                'data_after'	=> $data_after,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);
            

            return redirect('users/create')->with('message', 'Successfully created a new user!');
            
        }
    }

    public function insertRoles( Request $request ) {

        $rules = array(
            'role_name'		=> 'required|unique:user_roles,role_name',
            'permissions'	=> 'required'
        );

        $validator = Validator::make($request->all(), $rules);

        if ($validator->fails()) {
            return redirect('user/role/add')
                ->withErrors($validator)
                ->withInput();
        } else {
            $arrParams = array(
                'role_name' => $request->get('role_name'),
                'permissions' => implode(',', $request->get('permissions')),
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

            $this->db->table('user_roles')->insert($arrParams);

            // AuditTrail
            $data_before = '';
            $data_after = 'Role Name: ' . $request->get('role_name') . '<br />' .
                'Permissions: ' . implode(', ', $request->get('permissions'));

            $arrParams = array(
                'module' => 'User Roles',
                'action' => 'Added New User Role',
                'reference' => $request->get('role_name'),
                'data_before' => $data_before,
                'data_after' => $data_after,
                'user_id' => Auth::user()->id,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s')
            );

                $this->db->table('audit_trail')->insert($arrParams);
            // AuditTrail

            return redirect('user/role/add')->with('message', 'Successfully created a new role!');

        }
    }
}
