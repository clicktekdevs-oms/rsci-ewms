<?php

namespace App\Http\Controllers;

use App\Helper\CommonHelper;
use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
use Auth;
class LoadShipController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct( DB $databaseManager )
    {
        $this->db = $databaseManager;
        $this->middleware('shipping_gate');
    }

    public function getLoadShip( Request $request ) {

        $request->flash();

        $_getLoad = $this->db->table( $this->db->raw('wms_load_list a'))
                             ->select($this->db->raw("DISTINCT
                                                        a.id,
                                                        a.load_code,
                                                        (select DISTINCT store_nam from wms_store_masterlist where store_num = b.store_num) as store_nam,
                                                        COUNT(distinct b.box_no) as total_box,
                                                        (select concat(firstname, ' ', lastname) from wms_users where id = a.assigned_to) as assigned_to,
                                                        a.created_at,
                                                        a.ship_date,
                                                        a.load_status"))
                             ->leftJoin($this->db->raw('wms_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                             ->groupBy('load_code')
                             ->orderBy('id', 'desc')
                             ->get();


        $getLoads = CommonHelper::customPaginate(10, $_getLoad);
        $getLoads->setPath('');


        $data = array();
        $data['load_list'] = $getLoads;

        return view('loadship.shipping', $data);
    }

    public function filterLoad( Request $request ) {

        $request->flash();

        $load_code = trim($request->get('filter_load_code'));
        $ship_date = $request->get('filter_entry_date');


        $_getLoad = $this->db->table( $this->db->raw('wms_load_list a'))
            ->select($this->db->raw("DISTINCT
                                                        a.id,
                                                        a.load_code,
                                                        (select DISTINCT store_nam from wms_store_masterlist where store_num = b.store_num) as store_nam,
                                                        COUNT(distinct b.box_no) as total_box,
                                                        (select concat(firstname, ' ', lastname) from wms_users where id = a.assigned_to) as assigned_to,
                                                        a.created_at,
                                                        a.ship_date,
                                                        a.load_status"))
            ->leftJoin($this->db->raw('wms_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'));

            if ( !empty( $load_code ) ) {

                $_getLoad->whereRaw("a.load_code LIKE '$load_code%'");
            }
            if ( !empty( $ship_date ) ) {

                $_getLoad->whereRaw("a.ship_date LIKE '$ship_date%'");

            }

            $_getLoad = $_getLoad->groupBy('load_code')->orderBy('id', 'desc')->get();

        $getLoads = CommonHelper::customPaginate(10, $_getLoad);
        $getLoads->setPath('');


        $data = array();
        $data['load_list'] = $getLoads;

        return view('loadship.shipping', $data);

    }

    public function getLoadShipDetails( $pell_no ) {

        $getPiler = $this->db->table('load_list')
                             ->select($this->db->raw("id, (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as fullname, seal_no, plate_no, delivery_helper, driver_name, witnessed_sealed, load_status"))
                             ->where('load_code', $pell_no)
                             ->first();

        $_getBoxes = $this->db->table($this->db->raw('wms_box_list a'))
                            ->select($this->db->raw('DISTINCT a.id, a.box_no, a.tl_no, (select store_nam from wms_store_masterlist where store_num = a.store_num) as store_num, (select store_add1 from wms_store_masterlist where store_num = a.store_num) as store_add'))
                            ->join($this->db->raw('wms_load_detail b'), $this->db->raw('a.box_no'), '=', $this->db->raw('b.box_no'))
                            ->whereRaw("b.load_code = '$pell_no'")
                            ->where($this->db->raw('a.is_assign'), 1)
                            ->where($this->db->raw('a.is_open'), 1)
                            ->get();

        $data = array();
        $data['box_details']    = $_getBoxes;
        $data['load_code']      = $pell_no;
        $data['piler']          = $getPiler;

        return view('loadship.boxdetails', $data );
    }

    public function removeBox( Request $request ) {

        $box_no     = $request->get('box_no');
        $load_code  = $request->get('load_code');
        $tl         = $request->get('tl_no');

        if ( is_array( $box_no ) ) {

            //delete from load detail
            $this->db->table('load_detail')
                     ->whereIn('box_no', $box_no)
                     ->whereIn('tl_no', $tl)
                     ->where('load_code', $load_code)
                        ->delete();

            // update statuses from box_list

            $this->db->table('box_list')
                     ->whereIn('box_no', $box_no)
                     ->whereIn('tl_no', $tl)
                     ->update([ 'is_assign' => 0,
                                'is_open'   => 0]);
        }

        $arrParams = array(
            'module'		=> 'Loading/Shipping',
            'action'		=> 'Remove Box',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Remove Box no '.json_encode($box_no).'; Pell No. '.json_encode($load_code),
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', 'Successfully removed box from Pell no. '.$load_code);


    }

    public function assignPiler( Request $request ) {

        $pell = $request->get('pell');

        if ( isset( $pell ) ) {

            $pell = implode(',', $pell);

            $get_user = $this->db->table('users')
                ->select($this->db->raw("id, concat(firstname, ' ', lastname) as fullname"))
                ->where('role_id', 6)
                ->get();

            $data = array();
            $data['pell']  = $pell;
            $data['users'] = $get_user;

            return view('loadship.shipping_assign_piler', $data );

        }

        return redirect()->back()->with('message', 'Please select Pell no!');
    }

    public function assignTo( Request $request ) {

        $_pell      = $request->get('pell_list');
        $piler      = $request->get('stockpiler');
        $pell_no    = explode(',', $_pell );


        $this->db->table('load_list')
                 ->whereIn( 'load_code', $pell_no )
                 ->update( ['assigned_to' => $piler, 'load_status' => 2] );

        if ( empty( $piler ) ) {

            $this->db->table('load_list')
                ->whereIn( 'load_code', $pell_no )
                ->update( ['load_status' => 3, 'assigned_to' => '' ] );

        }

        $arrParams = array(
            'module'		=> 'Loading/Shipping',
            'action'		=> 'Assign Piler',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Assign Pell no '.json_encode($_pell).'; to. '.$piler,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('loadship')->with('message', 'Load assigned successfully!');

    }

    public function assignBoxtoPell( $load_code ) {


        $getStores = $this->db->table('store_masterlist')
                              ->select('store_num', 'store_nam')
                              ->where('store_type', 'S')
                              ->orderBy('store_nam')
                              ->get();


        $_getBoxes = $this->db->table($this->db->raw('wms_box_list a'))
                              ->select($this->db->raw('DISTINCT a.id, a.box_no, a.tl_no, a.store_num, (select store_nam from wms_store_masterlist where store_num = a.store_num) as store_nam, (select store_add1 from wms_store_masterlist where store_num = a.store_num) as store_add'))
                              ->join( $this->db->raw('wms_box_detail b'), function ( $join ) {

                                  $join->on( $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                                  $join->on( $this->db->raw('a.box_no'), '=', $this->db->raw('b.box_no'));

                              })
                              ->where($this->db->raw('a.is_assign'), 0)
                              ->where($this->db->raw('a.is_open'), 0);
                               if( !empty( $store_num ) ) {

                                    $_getBoxes->where($this->db->raw('a.store_num'), $store_num);

                               }
                              $_getBoxes = $_getBoxes->get();

        $data = array();
        $data['load_code']  = $load_code;
        $data['box_list']   = $_getBoxes;
        $data['stores']     = $getStores;

        return view('loadship.assign_pell', $data );
    }

    public function filterAssignBoxtoPell( Request $request ) {

        $request->flash();

        $store = $request->get('store');
        $load_code = $request->get('load_code');

        $getStores = $this->db->table('store_masterlist')
            ->select('store_num', 'store_nam')
            ->where('store_type', 'S')
            ->orderBy('store_nam')
            ->get();

        $_getBoxes = $this->db->table($this->db->raw('wms_box_list a'))
            ->select($this->db->raw('DISTINCT a.id, a.box_no, a.tl_no, a.store_num, (select store_nam from wms_store_masterlist where store_num = a.store_num) as store_nam, (select store_add1 from wms_store_masterlist where store_num = a.store_num) as store_add'))
            ->join( $this->db->raw('wms_box_detail b'), function ( $join ) {

                $join->on( $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                $join->on( $this->db->raw('a.box_no'), '=', $this->db->raw('b.box_no'));

            })
            ->where($this->db->raw('a.is_assign'), 0)
            ->where($this->db->raw('a.is_open'), 0);
        if( !empty( $store ) ) {

            //get storename
            $getStoreDesc = $this->db->table('store_masterlist')
                ->select('store_num', 'store_nam')
                ->where( 'store_num', $store )
                ->first();

            $store_name = $getStoreDesc->store_nam;

            $_getBoxes->where($this->db->raw('a.store_num'), $store);
        }
        $_getBoxes = $_getBoxes->get();

        $data = array();
        $data['load_code']  = $load_code;
        $data['box_list']   = $_getBoxes;
        $data['stores']     = $getStores;
        $data['store_name'] = isset( $store_name ) ? ucwords( $store_name ) : '';

        return view('loadship.assign_pell', $data );

    }

    public function assignToPell( Request $request ) {

        $box_no         = $request->get('box_no');
        $load_code      = $request->get('load_code');
        $store_num      = $request->get('store_num');
        $tl_num         = $request->get('tl_no');
        $checkbox       = $request->get('checkbox');

        if ( is_array( $box_no ) && !is_null( $load_code ) ) {

            foreach ( $checkbox as $box ) {

                $data = array();
                $data['load_code']  = $load_code;
                $data['box_no']     = $box_no[$box];
                $data['tl_no']      = $tl_num[$box];
                $data['is_load']    = 1;
                $data['store_num']  = $store_num[$box];
                $data['updated_at'] = Carbon::now();

                //insert to load detail
                $this->db->table('load_detail')->insert( $data );
//
//                //for updating box statuses
                $this->db->table('box_list')
                         ->where( 'box_no', $box_no[$box] )
                         ->where( 'tl_no', $tl_num[$box] )
                         ->update( [ 'is_assign' => 1, 'is_open' => 1 ] );
            }

            $arrParams = array(
                'module'		=> 'Loading/Shipping',
                'action'		=> 'Assign Box to Pell',
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> 'Assign Box no '.json_encode($box_no).'; to Pell No. '.json_encode($load_code),
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

        }

        return redirect()->back()->with('message', 'Box Successfully assigned to Pell No '.$load_code);

    }

    public function shipLoad( $load_code ) {

        if ( isset( $load_code ) ) {

            $this->db->table('load_list')->where('load_code', $load_code)->update( ['load_status' => 0] );

            $arrParams = array(
                'module'		=> 'Loading/Shipping',
                'action'		=> 'Ship Pell',
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> 'Ship Pell No. '.$load_code,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

            return redirect()->back()->with('message', "$load_code Successfully shipped!");
        }
    }

    public function createLoad() {

        $loadmax = $this->db->table('load_list')
                            ->select($this->db->raw('max(id) as max_created, max(load_code) as load_code'))
                            ->first();




        if( empty( $loadmax->max_created) ) {
            $loadCode = 'LD0000001';
        } else {
            $loadCode = substr($loadmax->max_created, -7);
            $loadCode = (int) $loadCode + 1;
            $loadCode = 'LD' . sprintf("%07s", (int)$loadCode);
        }

        $this->db->table('load_list')->insert(['load_code' => $loadCode]);

        $load = $this->db->table('load_list')->select('load_code')->where('load_code', '=',$loadCode)->first();


        $arrParams = array(
            'module'		=> 'Loading/Shipping',
            'action'		=> 'Create Pell',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Create Pell No. '.$loadCode,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return response()->json(array('result' => $load), 200);

    }

    /**
     * @param $load_code
     */

    public function getLoadSheet( $load_code ) {

        $getPiler = $this->db->table('load_list')
                        ->select($this->db->raw("id, (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as fullname, seal_no, plate_no, delivery_helper, driver_name, witnessed_sealed, load_status"))
                        ->where('load_code', $load_code)
                        ->first();

        $_getList = $this->db->table( $this->db->raw('wms_load_detail a'))
                         ->select( $this->db->raw(' DISTINCT a.load_code, c.box_no, GROUP_CONCAT(DISTINCT c.tl_no) as tl_no,(SELECT distinct assigned_to from wms_load_list where load_code = a.load_code) as assigned_to,
                                    (SELECT distinct ship_date from wms_load_list where load_code = a.load_code) as ship_date,    
                                    (select DISTINCT store_nam from wms_store_masterlist where store_num = b.store_num) as store_num,
                                    (select sum(mov_qty) from wms_box_detail where box_no = c.box_no)  as mov_qty'))
                         ->join($this->db->raw('wms_box_list b'), $this->db->raw('a.box_no'), '=', $this->db->raw('b.box_no'))
                         ->join( $this->db->raw('wms_box_detail c'), function ( $join ) {

                             $join->on($this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'));
                             $join->on($this->db->raw('b.tl_no'), '=', $this->db->raw('c.tl_no'));

                         })
                         ->whereRaw("a.load_code =  '$load_code'")
                         ->groupBy($this->db->raw('c.box_no'))
                         ->orderBy($this->db->raw('c.box_no'))
                         ->get();

        foreach ( $_getList as $list ) {

            $store_name[] = $list->store_num;
            $load_no[]    = $list->load_code;
            $assigned_to  = $list->assigned_to;
            $ship_date    = $list->ship_date;

        }


        $load_no = array_unique( $load_no );
        $store_name = array_unique( $store_name );
        //$assigned_to = array_unique( $assigned_to );



        $getName = $this->db->table('users')
                        ->select($this->db->raw("concat(firstname, ' ', lastname) as name"))
                        ->where('id', $assigned_to)
                        ->first();

        $data = array();
        $data['details']    = $_getList;
        $data['load_code']  = $load_code;
        $data['store_name'] = $store_name;
        $data['piler']      = isset($getName) ? ucwords( $getName->name ) : '';
        $data['ship_date']  = $ship_date;
        $data['load_list']  = $getPiler;

        return view('loadship.print_loading_sheet', $data);
    }

    /**
     * @param Request $request
     *
     **/

    public function addOthers( Request $request ) {

        $seal_no        = $request->get('seal_no');
        $plate_no       = $request->get('plate_no');
        $helper         = $request->get('helper');
        $driver         = $request->get('driver');
        $witness_sealed = $request->get('witness_sealed');
        $pell_no        = $request->get('pell_no');

        $data = array();
        $data['seal_no']    = $seal_no;
        $data['plate_no']   = $plate_no;
        $data['delivery_helper']    = $helper;
        $data['driver_name']        = $driver;
        $data['witnessed_sealed']   = $witness_sealed;


        $this->db->table('load_list')
                 ->where('load_code', $pell_no )
                 ->update( $data );

        return redirect()->back()->with('message', 'Successfully Submitted!');
    }
}
