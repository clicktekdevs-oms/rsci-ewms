<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
use App\Helper\CommonHelper;
use Auth;
class SublocReceivingController extends Controller
{

    public function __construct( DB $databaseManager)
    {
        $this->db = $databaseManager;
        $this->middleware('subloc_rcv_gate');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request ) {

        $request->flash();

        $mts_no     = $request->get('mts_no');
        $ship_date  = $request->get('ship_date');
        $from_store = $request->get('filter_from_store');
        $to_store   = $request->get('filter_to_store');
        $filter_tl_status = $request->get('filter_tl_status');



        $_getStores = $this->db->table('store_masterlist')
                               ->select()
                               ->get();


        $_getSublocRcvList = $this->db->table('subloc_rcv_list')
            ->select($this->db->raw("tl_no, ship_date, (select store_nam from wms_store_masterlist where store_num = from_loc) as from_loc, (select store_nam from wms_store_masterlist where store_num = to_loc) as to_loc, (select concat(firstname, ' ', lastname) as name from wms_users where id = assigned_to) as assigned_to, tl_status"));

        if ( !empty( $mts_no ) ) {

            $_getSublocRcvList->whereRaw("tl_no like '$mts_no%'");
        }
        if ( !empty( $ship_date ) ) {

            $_getSublocRcvList->where('ship_date', $ship_date );

        }
        if ( !empty( $from_store ) ) {

            $_getSublocRcvList->where('from_loc', $from_store );

        }
        if ( !empty( $to_store ) ) {

            $_getSublocRcvList->where('to_loc', $to_store );

        }
        if ( !empty( $filter_tl_status ) ) {

            $_getSublocRcvList->where('tl_status', $filter_tl_status );

        } else {

            $_getSublocRcvList->where('tl_status', 3 );

        }

        $_getSublocRcvList = $_getSublocRcvList->orderBy('ship_date', 'desc')->paginate( 10 );

        $param = array();
        $param['subloc_lists']  = $_getSublocRcvList;
        $param['stores']        = $_getStores;

        return view('subloc_rcv.list', $param );

    }

    public function getDetails( $mts ) {

        if ( isset( $mts ) ) {

            $_getSublocRcvList = $this->db->table('subloc_rcv_list')
                                          ->select($this->db->raw("tl_no, ship_date, (select concat(firstname, ' ', lastname) from wms_users where id = assigned_to) as assigned_to, tl_status"))
                                          ->where('tl_no', $mts)
                                          ->first();

            $_getSublocRcvDtl = $this->db->table( $this->db->raw( "wms_subloc_rcv_dtl a" ))
                                     ->select( $this->db->raw( "DISTINCT a.tl_no, a.sku, a.short_name, a.upc, a.qty_ord, a.qty_rcv, a.remarks" ) )
                                     ->where( $this->db->raw("a.tl_no"), $mts )
                                     ->orderBy('upc')
                                     ->get();
        }

        $param = array();
        $param['subloc_list']    = $_getSublocRcvList;
        $param['subloc_details'] = $_getSublocRcvDtl;

        return view('subloc_rcv.detail', $param );

    }

    public function filterDetails( Request $request ) {

        $request->flash();

        $mts_no     = $request->get('mts_no');
        $ship_date  = $request->get('ship_date');
        $from_store = $request->get('filter_from_store');
        $to_store   = $request->get('filter_to_store');
        $filter_tl_status = $request->get('filter_tl_status');

        $_getStores = $this->db->table('store_masterlist')
            ->select()
            ->get();


        $_getSublocRcvList = $this->db->table('subloc_rcv_list')
                                      ->select($this->db->raw("tl_no, ship_date, (select store_nam from wms_store_masterlist where store_num = from_loc) as from_loc, (select store_nam from wms_store_masterlist where store_num = to_loc) as to_loc, (select concat(firstname, ' ', lastname) as name from wms_users where id = assigned_to) as assigned_to, tl_status"));

        if ( !empty( $mts_no ) ) {

            $_getSublocRcvList->whereRaw("tl_no like '$mts_no%'");
        }
        if ( !empty( $ship_date ) ) {

            $_getSublocRcvList->where('ship_date', $ship_date );

        }
        if ( !empty( $from_store ) ) {

            $_getSublocRcvList->where('from_loc', $from_store );

        }
        if ( !empty( $to_store ) ) {

            $_getSublocRcvList->where('to_loc', $to_store );

        }
        if ( !empty( $filter_tl_status ) ) {

            $_getSublocRcvList->where('tl_status', $filter_tl_status );

        }

            $_getSublocRcvList = $_getSublocRcvList->orderBy('ship_date', 'desc')->paginate( 10 );

        $param = array();
        $param['subloc_lists']  = $_getSublocRcvList;
        $param['stores']        = $_getStores;

        return view('subloc_rcv.list', $param );


    }

    public function chooseAction( Request $request ) {

        $mts_no = $request->get('mts_no');

        $_getStockPiler = $this->db->table('users')
                               ->select($this->db->raw("id, concat(firstname, ' ', lastname) as name "))
                               ->where('role_id', 6)
                               ->get();

        $param = array();
        $param['mts_no']    = implode(',', $mts_no );
        $param['pilers']    = $_getStockPiler;

        return view('subloc_rcv.assign_piler_form', $param );

    }

    public function assignPiler( Request $request ) {

        $mts_no = $request->get('mts_no');
        $piler = $request->get('piler');

        $mts_no = explode(',', $mts_no );

        if ( !empty( $piler ) ) {
            foreach ( $mts_no as $mts ) {

                $this->db->table('subloc_rcv_list')
                    ->where( 'tl_no', $mts )
                    ->update( ['assigned_to' => $piler, 'tl_status' => 2] );
            }
        }else {

            $this->db->table('subloc_rcv_list')
                ->whereIn( 'tl_no', $mts_no )
                ->update( ['assigned_to' => null, 'tl_status' => 3] );
        }

        return redirect('subloc-rcv')->with('message', 'MTS No. Successfully assigned');

    }

    public function pullData() {

        $filename   = 'job_subloc_rcv.php';
        $mod        = 'subloc_rcv';

        CommonHelper::pullData( $filename, $mod );

        // AuditTrail
        $data_after = "";

        $arrParams = array(
            'module'		=> 'Subloc Receiving',
            'action'		=> 'Pull Data from JDA',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> '',
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('subloc-rcv')->with('message', 'Successfully pulled from JDA!');

    }

    public function updateQty( Request $request ) {

        $mts_no     = $request->get('tl_no');
        $sku        = $request->get('sku');
        $qty_rcv    = $request->get('qty_rcv');


        $data = array();
        $data['qty_rcv'] = is_numeric($qty_rcv) ? $qty_rcv : 0;

        $this->db->table('subloc_rcv_dtl')
            ->where( 'tl_no', $mts_no )
            ->where( 'sku', $sku )
            ->update( $data );

        $arrParams = array(
            'module'		=> 'Subloc Receiving',
            'action'		=> 'Update Mts Details',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Update Quantity of sku='.$sku.'quantity='.$qty_rcv.';Transfer #='.$mts_no,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', 'Quantity Successfully Updated!');

    }

    public function closeMts( $mts_no ) {

        if ( isset( $mts_no ) ) {

            $this->db->table('subloc_rcv_list')->where('tl_no', $mts_no)->update(['tl_status' => 0]);

        }

        return redirect()->back()->with('message', "MTS $mts_no Successfully Posted!");

    }

}
