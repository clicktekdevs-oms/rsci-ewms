<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
use App\Helper\CommonHelper;
use Auth;
use PDF;
class PickingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private  $db;

    public function __construct(DB $databaseManager)
    {
        $this->db = $databaseManager;
        $this->middleware('picking_gate');
    }

    public function getPickingList( Request $request ) {

        $request->flash();
        $mts_no         = $request->get('filter_doc_no');
        $store          = $request->get('filter_store');
        $piler          = $request->get('filter_stock_piler');
        $pick_status    = $request->get('filter_pick_status');
        $ship_date      = $request->get('filter_entry_date');


        $getStores = $this->db->table('store_masterlist')
                    ->select('store_num', 'store_nam')
                    ->where('store_type', 'S')
                    ->orderBy('store_nam')
                    ->get();

        $_getUser = $this->db->table('users')
                             ->select($this->db->raw("id, concat(firstname, ' ',lastname) as name"))
                             ->where('role_id', 6)
                             ->orderBy('id')
                             ->get();

        $_getPickList = $this->db->table('pick_list')
            ->select($this->db->raw("id, tl_no, ship_date, (select store_nam from wms_store_masterlist where store_num = to_loc) as to_loc, (select concat(firstname, ' ',lastname) from wms_users where id = assigned_to ) as assigned_to, picked_date, pick_status"));
        //for tl_no
        if ( !empty( $mts_no ) ) {
            $_getPickList->whereRaw("tl_no like '$mts_no%'");
        }
        //for store name
        if ( !empty( $store ) ) {
            $_getPickList->where('to_loc', $store);
        }
        //for piler
        if ( !empty( $piler ) ) {
            $_getPickList->where( 'assigned_to', $piler );
        }

        if ( !empty( $ship_date ) ) {

            $_getPickList->where( 'ship_date', $ship_date );

        }

        if ( !empty( $pick_status ) ) {

            $_getPickList->where( 'pick_status', $pick_status );

        } else {

            $_getPickList->where( 'pick_status', 3 );
        }

        $_getPickList = $_getPickList->orderBy('id', 'desc')->paginate( 10 );

        $data = array();
        $data['user']       = $_getUser;
        $data['store_list'] = $getStores;
        $data['pick_list']  = $_getPickList;

        return view('picking.list', $data );

    }

    public function getPickingFilter( Request $request ) {

        $request->flash();

        $mts_no = $request->get('filter_doc_no');
        $store  = $request->get('filter_store');
        $piler = $request->get('filter_stock_piler');
        $pick_status= $request->get('filter_pick_status');
        $ship_date      = $request->get('filter_entry_date');

        $getStores = $this->db->table('store_masterlist')
            ->select('store_num', 'store_nam')
            ->where('store_type', 'S')
            ->orderBy('store_nam')
            ->get();

        $_getUser = $this->db->table('users')
            ->select($this->db->raw("id, concat(firstname, ' ',lastname) as name"))
            ->where('role_id', 6)
            ->orderBy('id')
            ->get();


        $_getPickList = $this->db->table('pick_list')
            ->select($this->db->raw("id, tl_no, ship_date, (select store_nam from wms_store_masterlist where store_num = to_loc) as to_loc, (select concat(firstname, ' ',lastname) from wms_users where id = assigned_to ) as assigned_to, picked_date, pick_status"));
        //for tl_no
        if ( !empty( $mts_no ) ) {

                $_getPickList->whereRaw("tl_no like '$mts_no%'");

            }
            //for store name
            if ( !empty( $store ) ) {

                $str_name = $this->db->table('store_masterlist')
                                 ->select('store_nam')
                                 ->where('store_num', $store)
                                 ->first();

                $str_name = $str_name->store_nam;

                $_getPickList->where('to_loc', $store);
            }
            //for piler
            if ( !empty( $piler ) ) {

                $picker = $this->db->table('users')
                    ->select($this->db->raw("CONCAT(firstname, ' ',lastname) as name"))
                    ->where('id', $piler)
                    ->first();

                $picker = $picker->name;

                $_getPickList->where( 'assigned_to', $piler );

            }

            if ( !empty( $ship_date ) ) {

                $_getPickList->where( 'ship_date', $ship_date );

            }

            if ( !empty( $pick_status ) ) {

                $_getPickList->where( 'pick_status', $pick_status );

            }

            $_getPickList = $_getPickList->orderBy('id') ->paginate( 10 );


        $data = array();
        $data['user']       = $_getUser;
        $data['store_list'] = $getStores;
        $data['store_name'] = isset( $str_name ) ? $str_name : '';
        $data['picker']     = isset( $picker ) ? ucfirst( $picker ) : '';
        $data['pick_list']  = $_getPickList;

        return view('picking.list', $data );

    }

    public function assignPiler( Request $request ) {

        $id    = $request->get('tl_id');
        $tl_no = $request->get('mts_no' );
        $action_btn = $request->get('action_btn');

        if ($action_btn == "shipment_date$id") {
            return $tl_no;
        }

        if ( empty( $tl_no ) ) {
            return redirect()->back()->with('message', 'Please Select Piler!');
        }

        $tl_no = implode(', ', $tl_no);

        $_getPiler = $this->db->table('users')
                          ->select($this->db->raw("id, CONCAT(firstname, ' ',lastname) as name"))
                          ->where('role_id', 6 )
                          ->get();

        $data = array();
        $data['tl_no'] = $tl_no;
        $data['users'] = $_getPiler;

       return view('picking.assign_piler_form', $data );

    }

    public function assignToPiler( Request $request ) {

        $mts_no = $request->get('mts_no');
        $piler  = $request->get('piler');

        $mts_no = explode(', ', $mts_no);

       if ( !empty( $piler ) ) {
           foreach ( $mts_no as $mts ) {

               $this->db->table('pick_list')
                   ->where( 'tl_no', $mts )
                   ->update( ['assigned_to' => $piler, 'pick_status' => 2] );
           }
       }else {

           $this->db->table('pick_list')
               ->whereIn( 'tl_no', $mts_no )
               ->update( ['assigned_to' => null, 'pick_status' => 3] );
       }

        $arrParams = array(
            'module'		=> 'Picking',
            'action'		=> 'Assign MTS',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> 'Assign MTS no '.json_encode($mts_no).' to '.$piler,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

       return redirect('picking')->with('message', 'MTS No. Successfully assigned');

    }

    public function getPickingDetails( $mts_no ) {

        $_getDetail = $this->db->table($this->db->raw('wms_pick_detail a'))
                            ->select($this->db->raw('DISTINCT a.sku, a.upc, a.short_desc, a.item_desc, a.qty_req, a.qty_rcv, a.remarks'))
                            ->where( 'tl_no', $mts_no )
                            ->get();

        $_getFirst = $this->db->table('pick_list')
                              ->select($this->db->raw("tl_no, ship_date, (select concat(firstname, ' ', lastname)  from wms_users where id = assigned_to) as name" ))
                              ->where( 'tl_no', $mts_no )
                              ->first();


        $data['mts_detail'] = $_getFirst;
        $data['details'] = $_getDetail;

        return view('picking.detail', $data );
    }

    public function updatePickingShipDate( Request $request ) {




        $ship_date  = $request->get('filter_entry_date');
        $mts_no     = $request->get('mts_no');

        print_r($ship_date);

        $this->db->table('pick_list')->where('tl_no', $mts_no)->update(['ship_date' => $ship_date]);

        return redirect()->back()->with('message', 'Successfully updated shipment battle');

    }

    public function closeMts( $mts ) {

        if ( isset( $mts ) ) {

            $this->db->table( 'pick_list')->where( 'tl_no', $mts)->update(['pick_status' => 0]);

            $arrParams = array(
                'module'		=> 'Picking',
                'action'		=> 'Close MTS',
                'reference'		=> Auth::user()->id,
                'data_before'	=> '',
                'data_after'	=> 'Close MTS No.' . $mts,
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);

            return redirect()->back()->with('message', "MTS $mts successfully closed!");
        }
    }

    public function pullData() {

        $filename   = 'job_pick.php';
        $mod        = 'picklist';

        CommonHelper::pullData( $filename, $mod );

        // AuditTrail
        $data_after = "";

        $arrParams = array(
            'module'		=> 'Picking',
            'action'		=> 'Pull Data from JDA',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> '',
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('picking')->with('message', 'Successfully pulled from JDA!');

    }

    public function getMtsList( $mts ) {

        $_getList = $this->db->table($this->db->raw('wms_pick_list a'))
                             ->select($this->db->raw("distinct a.tl_no, 
                                                      a.to_loc,
                                                      (select store_nam from wms_store_masterlist where store_num = a.to_loc) as to_loc_desc,
                                                      b.dept_code,
                                                      b.dept_desc,
                                                      a.ship_date"))
                             ->join( $this->db->raw('wms_pick_detail b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                             ->where($this->db->raw('a.tl_no'), $mts )
                             ->first();

        $_getDetail = $this->db->table($this->db->raw('wms_pick_list a'))
                               ->select($this->db->raw('DISTINCT a.tl_no, c.box_no'))
                               ->join( $this->db->raw('wms_pick_detail b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                               ->join( $this->db->raw('wms_box_detail c'), $this->db->raw('a.tl_no'), '=', $this->db->raw('c.tl_no'))
                               ->where( $this->db->raw('a.tl_no'), $mts)
                               ->orderBy( $this->db->raw('c.box_no'))
                               ->get();

        $_getDetail = collect( $_getDetail );
        $collect_dtl = $_getDetail->groupBy('box_no');



        $data = array();
        $data['lists']      = $_getList;
        $data['details']    = $collect_dtl;

        return view('picking.print_packing_list', $data );

    }

    public function getPackageSlip( $mts ) {

        $data = array();

        $_getBox = $this->db->table( $this->db->raw('wms_box_list a'))
                        ->select( $this->db->raw('distinct a.box_no,
                                                    (select group_concat(tl_no) from wms_box_list where box_no = a.box_no) as tl_no,
                                                     b.ship_date,
                                                     (select DISTINCT store_nam from wms_store_masterlist where store_num = b.to_loc) as to_loc,
                                                     (select sum(mov_qty) from wms_box_detail where box_no = a.box_no) as total_qty '))
                        ->join($this->db->raw('wms_pick_list b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                        ->where($this->db->raw('a.tl_no'), $mts)
                        ->orderBy($this->db->raw('a.box_no'))
                        ->get();

        $data['get_box'] = $_getBox;

        $pdf = PDF::loadView('picking.package_slip', $data)->setPaper('a6')->setOrientation('portrait');

        return $pdf->stream("package-slip.pdf");

    }

    public function updateShipDate( Request $request ) {

        $tl_no      = $request->get('mts_no');
        $ship_date  = $request->get('ship_date');

        $ship_date = date('Y-m-d', strtotime( $ship_date ));

        $this->db->table( 'pick_list' )->where( 'tl_no', $tl_no )->update( [ 'ship_date' => $ship_date ] );

        return redirect()->back()->with('message', 'Shipment date successfully updated!');

    }

}
