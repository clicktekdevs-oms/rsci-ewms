<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
class StoreMasterlistController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct( DB $databaseManager)
    {
        $this->db = $databaseManager;
    }

    public function index( Request $request )
    {
        //
        $request->flash();

        $_getList = $this->db->table('store_masterlist')
                         ->select('id', 'store_num', 'store_nam', 'store_add1', 'store_add2')
                         ->orderBy('id')
                         ->paginate(10);

        $data = array();

        $data['stores'] = $_getList;

        return view('stores.list', $data );

    }

    public function filterStores( Request $request ) {

        $request->flash();

        $store_code = $request->get('filter_store_code');
        $store_name = $request->get('filter_store_name');

        $_getList = $this->db->table('store_masterlist')
            ->select('id', 'store_num', 'store_nam', 'store_add1', 'store_add2')
            ->whereRaw("store_num like '$store_code%'")
            ->whereRaw("store_nam like '$store_name%'")
            ->orderBy('id')
            ->paginate(10);


        $data = array();

        $data['stores'] = $_getList;

        return view('stores.list', $data );

    }


}
