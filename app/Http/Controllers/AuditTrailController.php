<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
class AuditTrailController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(DB $databaseManager)
    {
        $this->db = $databaseManager;
    }

    public function index(Request $request)
    {
        $request->flash();
        //
        $getList = $this->db->table('audit_trail')
                            ->select($this->db->raw("audit_id, module, reference, action, data_after, (select concat(firstname, ' ', lastname) from wms_users where id = user_id) as user_id, created_at, updated_at"))
                            ->orderBy('audit_id', 'desc')
                            ->paginate(10);

        $data = array();
        $data['result'] = $getList;


        return view('audit_trail.list', $data);
    }

    public function filterTrails( Request $request ) {

        $request->flash();

        $datefrom   = $request->get('filter_date_from');
        $dateto     = $request->get('filter_date_to');

        $getList = $this->db->table('audit_trail')
            ->select($this->db->raw("audit_id, module, reference, action, data_after, (select concat(firstname, ' ', lastname) from wms_users where id = user_id) as user_id, created_at, updated_at"))
            ->whereBetween('created_at', array($datefrom . ' 00:00:00', $dateto . ' 23:59:59'))
            ->orderBy('audit_id', 'desc')
            ->paginate(10);

        $data = array();
        $data['result'] = $getList;

        return view('audit_trail.list', $data);
    }

}
