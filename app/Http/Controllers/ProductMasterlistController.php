<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
class ProductMasterlistController extends Controller
{

    public function __construct(DB $databaseManager)
    {
        $this->db = $databaseManager;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index( Request $request )
    {
        $request->flash();
        //

        $_getList = $this->db->table('product_masterlist')
                         ->select('sku', 'upc', 'short_name', 'item_desc', 'dept_code')
                         ->orderBy('id')
                         ->paginate(10);

        $data = array();
        $data['products'] = $_getList;

        return view('products.list', $data );
    }

    public function filterProducts( Request $request ) {

        $request->flash();

        $sku = $request->get('filter_prod_sku');
        $upc = $request->get('filter_prod_upc');


        $_getList = $this->db->table('product_masterlist')
            ->select('sku', 'upc', 'short_name', 'item_desc', 'dept_code')
            ->whereRaw("sku like '$sku%'")
            ->whereRaw("upc like '$upc%'")
            ->orderBy('id')
            ->paginate(10);

        $data = array();
        $data['products'] = $_getList;

        return view('products.list', $data );

    }


}
