<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
use Validator;
class ApiSublocPickingController extends Controller
{

    private $db;

    public function __construct( DB $databaseManager ) {

        $this->db = $databaseManager;

    }

    public function getPickingList( $piler_id ) {

        $_getPickList = $this->db->table('subloc_pick_list')
            ->select( $this->db->raw('tl_no, from_loc, (select store_nam from wms_store_masterlist where store_num = from_loc) as from_store_name,  to_loc, (select store_nam from wms_store_masterlist where store_num = to_loc) as to_store_name') )
            ->where( 'assigned_to', $piler_id )
            ->where('pick_status', 2)
            ->get();

        return response()->json(['result' => $_getPickList ], 200);
    }

    public function getPickingDetail( $tl_no ) {

        $_getPickDetail = $this->db->table($this->db->raw('wms_subloc_pick_detail a'))
            ->select($this->db->raw("DISTINCT a.tl_no,
                                                        a.id,
                                                        a.upc,
                                                        a.sku,
                                                        ifnull(a.dept_code, '') as dept_code,
                                                        ifnull(a.short_desc, '') as short_name,
                                                        ifnull(a.item_desc, '') as item_desc,
                                                        ifnull(a.rtl_price, '') as rtl_price,
                                                        a.qty_req,
                                                        a.qty_rcv,
                                                        a.item_color,
                                                        a.item_size"))
            ->where($this->db->raw('a.tl_no'), $tl_no )
            ->get();

        return response()->json(['result' => $_getPickDetail ], 200);

    }

    public function updatePickingDetail( $tl_no, $upc, $qty ) {


        $this->db->table('subloc_pick_detail')
            ->where( 'tl_no', $tl_no )
            ->where( 'upc', $upc )
            ->update( ['qty_rcv' => $qty ] );

        return response()->json(['result' => 'success'], 200);

    }

    public function updatePickListStatus( $tl_number ) {

        $this->db->table('subloc_pick_list')
            ->where( 'tl_no', $tl_number )
            ->update( ['pick_status' => 1, 'picked_date' => date('Y-m-d') ] );

        return response()->json(['result' => 'success'], 200);
    }

    public function insertNewBox( $box_no, $storeloc, $tlno, $from_store ) {

        $data = array();

        $data['box_no']     = $box_no;
        $data['tl_no']      = $tlno;
        $data['from_store_num'] = $from_store;
        $data['store_num']  = $storeloc;


        //check if exist
        $_ifExist = $this->db->table('subloc_box_list')
            ->select('box_no')
            ->where('box_no', $box_no)
            ->where('tl_no', $tlno)
            ->first();

        if ( !count( $_ifExist ) ) {

            $this->db->table('subloc_box_list')->insert( $data );

        }


        return response()->json(['result' => 'success'], 200 );

    }

    public function getBoxCode( $store_num ) {

        $_getBox = $this->db->table( 'subloc_box_list' )
            ->select($this->db->raw('MAX(box_no) as box_no'))
            ->where( 'store_num', $store_num )
            ->first();


        if ( empty( $_getBox->box_no ) ) {

            $getBoxNo = 0;

        } else {

            $getBoxNo = $_getBox;

        }

        return response()->json( [ 'result' => [$getBoxNo] ], 200 );

    }

    public function validateBox( $boxcode ) {

        $_valBox= $this->db->table('subloc_box_list')
            ->select($this->db->raw('count(box_no) as boxes_count'))
            ->where('box_no', $boxcode )
            ->first();

        return response()->json( [ 'result' => [$_valBox] ], 200 );

    }

    public function selectAvailBox( $store ) {

        $_availableBox = $this->db->table('subloc_box_list')
            ->select($this->db->raw('box_no, store_num, group_concat(tl_no) as tl_no, box_no'))
            ->where('store_num', $store )
            ->where('is_assign', 0)
            ->groupBy('box_no')
            ->get();

        return response()->json( [ 'result' => $_availableBox ], 200 );

    }

    public function insertBoxDetail($tl_no, $upc, $box_code, $qty ) {

        $_ifExist = $this->db->table('subloc_box_detail')
            ->select('box_no')
            ->where('box_no', $box_code)
            ->where('tl_no', $tl_no)
            ->where('upc', $upc)
            ->first();

        //check if detail is duplicated
        if ( !count( $_ifExist ) ) {

            $data = array();
            $data['tl_no']      = $tl_no;
            $data['box_no']     = $box_code;
            $data['upc']        = $upc;
            $data['mov_qty']    = $qty;

            $this->db->table('subloc_box_detail')->insert( $data );

        }

        return response()->json(['result' => 'success'], 200 );

    }



}
