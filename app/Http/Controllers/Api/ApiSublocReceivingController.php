<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
class ApiSublocReceivingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $db;

    public function __construct( db $databaseManager) {

        $this->db = $databaseManager;

    }

    public function getRcvList( $piler ) {

        if ( isset( $piler ) ) {

            $_getList = $this->db->table('subloc_rcv_list')
                ->select($this->db->raw('tl_no, (select distinct store_nam from wms_store_masterlist where store_num = from_loc) as from_loc, (select distinct store_nam from wms_store_masterlist where store_num = to_loc) as to_loc'))
                ->where('assigned_to', $piler)
                ->where('tl_status', 2)
                ->where('is_jda_sync', 0)
                ->get();

            return response()->json(array('result' => $_getList), 200);

        }
    }

    public function getRcvDtl( $mts_no ) {

        if ( isset( $mts_no ) ) {

            $_getDetail = $this->db->table( $this->db->raw('wms_subloc_rcv_dtl a') )
                               ->select( $this->db->raw("DISTINCT a.tl_no, a.upc, b.item_desc, a.qty_ord"))
                               ->join( $this->db->raw('wms_product_masterlist b'), $this->db->raw('a.upc'), '=', $this->db->raw('b.upc'))
                               ->where( $this->db->raw('a.tl_no'), $mts_no )
                               ->get();

            return response()->json(array('result' => $_getDetail), 200);

        }
    }

    public function updateStatus( $mts_no ) {

        if ( isset( $mts_no ) ) {

            $this->db->table('subloc_rcv_list')->where('tl_no', $mts_no )->update(['tl_status' => 1]);

            return response()->json(array('result' => 'success'), 200 );

        }
    }

    public function updateDetails( $mts, $upc, $rcv_qty ) {

        $data = array();
        $data['qty_rcv']    = $rcv_qty;

        $this->db->table('subloc_rcv_dtl')
                 ->where('tl_no', $mts)
                 ->where('upc', $upc)
                 ->update( $data );

        return response()->json(array('result' => 'success'), 200 );

    }

    public function insertUnlisted( $mts, $upc, $qty ) {

        $checkMtsInfo = $this->db->table('product_masterlist')
                                 ->where('upc', $upc)
                                 ->first();

        $data = array();

        $data['tl_no']          = $mts;
        $data['sku']            = $checkMtsInfo->sku;
        $data['short_name']     = $checkMtsInfo->short_name;
        $data['upc']            = $upc;
        $data['qty_rcv']        = $qty;
        $data['created_at']     = date('Y-m-d H:i:s');
        $data['remarks']        = 'NOT IN MTS';

        $this->db->table('subloc_rcv_dtl')->insert( $data );

        return response()->json(['result' => 'success'], 200);
    }


}
