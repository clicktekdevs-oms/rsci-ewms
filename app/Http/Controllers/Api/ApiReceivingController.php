<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as DB;
class ApiReceivingController extends Controller
{

    public function __construct(DB $databaseManager)
    {
        $this->db = $databaseManager;
    }

    public function getPOList( $piler_id ) {

        $_getList =  $this->db->table($this->db->raw('wms_po_list a'))
                        ->select($this->db->raw( 'DISTINCT a.po_no, a.rcv_no, b.division_code, b.division, b.dept_code, b.department, b.po_status' ))
                        ->join($this->db->raw('wms_po_detail b'), $this->db->raw('a.rcv_no'), '=', $this->db->raw('b.rcv_no'))
                        ->where($this->db->raw('b.assigned_to'), $piler_id)
                        ->where($this->db->raw('b.po_status'), 3)
                        ->groupBy($this->db->raw('a.rcv_no'))
                        ->groupBy($this->db->raw('b.dept_code'))
                        ->get();

        return response()->json(['result' => $_getList], 200);
    }

    public function getPODetails( $rcv_no, $division_code, $dept_code ) {

        $_getDetail = $this->db->table($this->db->raw('wms_po_detail a'))
                               ->select($this->db->raw("
                                        DISTINCT a.rcv_no,
                                        a.dept_code as division,
                                        a.sku,
                                        a.upc,
                                        ifnull(a.item_desc, '')as description,
                                        a.qty_ord,
                                        a.qty_rcv,
                                        a.po_status
                                        "))
                                ->where($this->db->raw('a.rcv_no'), $rcv_no )
                                ->where($this->db->raw('a.division_code'), $division_code )
                                ->where($this->db->raw('a.dept_code'), $dept_code )
                                ->orderBy($this->db->raw('a.sku'))
                                ->get();

        return response()->json(['result' => $_getDetail], 200);

    }

    public function updatePODetails( $rcv_no, $division_code, $dept_code, $upc, $rcvd_qty, $slot ) {



        // Update Quantity received

        $data = array();

        $data['slot_code']  = $slot;
        $data['qty_rcv']    = $rcvd_qty;
        $data['po_status']  = 2;

        $this->db->table('po_detail')
                 ->where( 'rcv_no', $rcv_no )
                 ->where( 'upc', $upc )
                 ->where( 'division_code', $division_code )
                 ->where( 'dept_code', $dept_code )
                 ->update( $data );

        /*
         * Check condition for PO Status
         * Select all POS where Status = 3 (Assigned) and Status = 4 (Open)
         */

        $_checkStatus = $this->db->table('po_detail')
                                 ->where( 'rcv_no', $rcv_no )
                                 ->whereIn('po_status', [3, 4] )
                                 ->get();

        /*
         * If no records found update PO list to 2 (done) status
         */

        if ( !count( $_checkStatus ) ) {

            $this->db->table('po_list')->where( 'rcv_no', $rcv_no )->update(['po_status' => 2, 'updated_at' => Carbon::now() ]);
        }

        return response()->json(['result' => 'success'], 200);

    }

    public function insertUnlisted( $rcv_no, $division_code, $dept_code, $upc, $rcvd_qty, $slot, $division, $department, $assigned_to ) {

        $checkItems = $this->db->table('product_masterlist')
            ->select('sku', 'short_name')
            ->where('upc', $upc)
            ->orWhere('new_upc', $upc)
            ->first();

        $data = array();
        $data['rcv_no']         = $rcv_no;
        $data['division_code']  = $division_code;
        $data['dept_code']      = $dept_code;
        $data['upc']            = $upc;
        $data['qty_rcv']        = $rcvd_qty;
        $data['slot_code']      = $slot;
        $data['division']       = $division;
        $data['department']     = $department;
        $data['assigned_to']    = $assigned_to;
        $data['po_status']      = 2;
        $data['created_at']     = Carbon::now();


        if ( empty( $checkItems ) ) {

            $data['remarks'] = 'NOT IN PO';
            $this->db->table('po_detail')->insert( $data );

        }else {

            $check_rcv = $this->db->table('po_detail')
                            ->select('rcv_no')
                            ->where( 'rcv_no', $rcv_no )
                            ->where( 'sku', $checkItems->sku )
                            ->where('division_code', $division_code)
                            ->where('dept_code', $dept_code)
                            ->first();

                if ( empty( $check_rcv ) ) {

                    $data['sku']            = count( $checkItems ) ? $checkItems->sku : '';
                    $data['short_name']     = count( $checkItems ) ? $checkItems->short_name : '';
                    $data['remarks']        = 'NOT IN DIVISION';
                    $this->db->table('po_detail')->insert( $data );

                }else {

                    $checkIfNewUpc = $this->db->table('product_masterlist')
                        ->select('sku', 'short_name')
                        ->where('new_upc', $upc)
                        ->first();

                    if ( !empty( $checkIfNewUpc ) ) {

                        $this->db->table('po_detail')
                            ->where( 'rcv_no', $rcv_no )
                            ->where( 'sku', $checkIfNewUpc->sku )
                            ->where( 'division_code', $division_code )
                            ->where( 'dept_code', $dept_code )
                            ->update( ['scanned_upc' => $upc, 'remarks' => 'Scanned UPC='.$upc,'qty_rcv' => $rcvd_qty, 'created_at' => Carbon::now(), 'updated_at' => Carbon::now() ] );

                    }
                }
        }

        return response()->json(['result' => 'success'], 200);

    }
}
