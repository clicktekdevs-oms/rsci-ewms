<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
class ApiLoadShipController extends Controller
{
    //
    protected $db;

    public function __construct( db $databaseManager )
    {
        $this->db = $databaseManager;
    }

    public function getLoad( $piler ) {

        $_getLoad = $this->db->table( $this->db->raw('wms_load_list a'))
                             ->select($this->db->raw('DISTINCT a.load_code, (select store_nam from wms_store_masterlist where store_num = b.store_num) as str_name,COUNT(distinct b.box_no) as total_box, CURDATE() as date_now'))
                             ->join( $this->db->raw('wms_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                             ->where($this->db->raw('a.assigned_to'), $piler)
                             ->where( $this->db->raw('a.load_status'), 2)
                             ->where( $this->db->raw('a.is_sync'), 0)
                             ->groupBy( $this->db->raw('a.load_code'))
                             ->get();

        return response()->json( ['result' => $_getLoad], 200 );

    }

    public function getLoadDetail( $load_code ) {

        $_getLoadDetail = $this->db->table('load_detail')
                                   ->select($this->db->raw('DISTINCT load_code, box_no'))
                                   ->where('load_code', $load_code)
                                   ->get();

        return response()->json( ['result' => $_getLoadDetail], 200 );

    }

    public function updateDetail( $load_code, $box_code ) {

        if ( isset( $load_code ) || isset( $box_code ) ) {

            $this->db->table('load_detail')
                ->where('box_no', $box_code)
                ->where('load_code', $load_code)
                ->delete();

            // update statuses from box_list

            $this->db->table('box_list')
                     ->where('box_no', $box_code)
                     ->update([
                                'is_assign' => 0,
                                'is_open'   => 0
                              ]);

        }

        return response()->json( ['result' => 'success'], 200 );

    }

    public function updateStatus( $load_code, $ship_date ) {

        if ( isset( $load_code ) || isset( $ship_date ) ) {

            $data = array();
            $data['ship_date']      = $ship_date;
            $data['load_status']    = 1;

            $this->db->table('load_list')
                     ->where('load_code', $load_code)
                     ->update( $data );

            return response()->json( ['result' => 'success'], 200 );

        }
    }
}
