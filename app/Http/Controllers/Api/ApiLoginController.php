<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\DatabaseManager as DB;
use Gate;
class ApiLoginController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct(DB $db)
    {
        $this->db = $db;
    }

    public function index( $username, $password )
    {
        //
        if (Auth::attempt(array('username' => $username, 'password' => $password )) ) {

            $data = array();
            $data['id']        = Auth::user()->id;
            $data['fname'] = Auth::user()->firstname;
            $data['lname']  = Auth::user()->lastname;

            return response()->json(array('result' => [$data]), 200 );
        } else {

            return response()->json('', 200);
        }

    }

    public function getSlot() {

        $_getSlot = $this->db->table('slot_masterlist')
                             ->select('slot_name')
                             ->get();

        return response()->json(array('result' => $_getSlot), 200 );

    }

    public function checkRoles( $username, $password ) {

        if (Auth::attempt(array('username' => $username, 'password' => $password )) ) {

            $data = array();
            $data['id']        = Auth::user()->id;

            return response()->json(array('result' => [$data]), 200 );
        } else {

            return response()->json(array('result' => 'failed'), 200);
        }

    }


    public function checkIfCanShip( $username, $password ) {

        if (Auth::attempt(array('username' => $username, 'password' => $password )) ) {

            if (Gate::check('role-access', 'CanAccessShipping') || Gate::check('role-access', 'CanAccessSublocShipping')) {

                return response()->json(array('result' => [['id' => Auth::user()->id]]), 200 );

            } else {

                return response()->json(array('result' => 'failed'), 500);
            }


        } else {

            return response()->json(array('result' => 'failed'), 200);
        }

    }




}
