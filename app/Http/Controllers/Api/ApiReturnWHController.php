<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
class ApiReturnWHController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct( db $databaseManager)
    {
        $this->db = $databaseManager;
    }

    public function getList( $piler_id )
    {
        //
        if ( isset( $piler_id ) ) {

            $_getList = $this->db->table('rtnwh_list')
                ->select($this->db->raw('distinct mts_no,
                                                     from_loc,
                                                     (select store_nam from wms_store_masterlist where store_num = from_loc) as store_desc'))
                ->where('assigned_to', $piler_id)
                ->where('mts_status', 2)
                ->where('is_sync', 0)
                ->get();

            return response()->json(array('result' => $_getList), 200);

        }
    }

    public function getDetails( $mts_no ) {

        $_getDetail = $this->db->table($this->db->raw('wms_rtnwh_detail a'))
                                ->select($this->db->raw('mts_no, upc, qty_req, (select short_name from wms_product_masterlist where upc = a.upc limit 1) as short_name'))
                                ->where('mts_no', $mts_no)
                                ->get();

        return response()->json(array( 'result' => $_getDetail ), 200 );

    }

    public function updateDetails( $mts_no, $upc, $qty, $slot ) {

        if ( isset( $mts_no ) || isset( $upc ) || isset( $qty ) ) {

            $this->db->table('rtnwh_detail')
                     ->where('mts_no', $mts_no )
                     ->where('upc', $upc )
                     ->update([ 'qty_rcv' => $qty, 'updated_at' => Carbon::now() ]);

            return response()->json(array('result' => 'success'), 200 );

        }
    }

    public function insertItems( $mts, $upc, $qty ) {

        $checkMtsInfo = $this->db->table('product_masterlist')
                                ->where('upc', $upc)
                                ->first();

        $data = array();
        $data['mts_no']         = $mts;
        $data['sku']            = count($checkMtsInfo) ? $checkMtsInfo->sku : '';
        $data['upc']            = $upc;
        $data['item_desc']      = count( $checkMtsInfo ) ? $checkMtsInfo->item_desc : '';
        $data['qty_rcv']        = $qty;
        $data['remarks']        = 'NOT IN MTS';

        $data['updated_at']     = Carbon::now();

        $this->db->table('rtnwh_detail')->insert( $data );

        return response()->json(array('result' => 'success'), 200 );

    }

    public function updateStatus( $mts ) {

        if ( isset( $mts ) ) {

            $this->db->table('rtnwh_list')->where( 'mts_no', $mts )->update(['mts_status' => 1 ]);

            return response()->json(array('result' => 'success'), 200 );

        }
    }


}
