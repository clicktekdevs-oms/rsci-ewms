<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
class ApiSublocStoreReceivingController extends Controller
{
    //
    private  $db;

    public function __construct( db $databaseManager ) {

        $this->db = $databaseManager;

    }

    public function getPell( $user_id ) {

        $query = $this->db->table($this->db->raw('wms_subloc_load_list a'))
            ->select($this->db->raw('DISTINCT a.load_code, b.store_num'))
            ->join( $this->db->raw('wms_subloc_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
            ->where( $this->db->raw('b.is_assign_store'), $user_id )
            ->where( $this->db->raw('a.load_status_store'), 2 )
            ->get();

        return response()->json(array('result' => $query), 200);

    }

    public function getPellDetails( $load_code, $clerk ) {

        $query = $this->db->table('subloc_load_detail')
            ->select('box_no', 'load_code')
            ->where('load_code', $load_code)
            ->where('is_assign_store', $clerk)
            ->where('is_box_status', 1)
            ->orderBy('box_no')
            ->get();

        return response()->json(array('result' => $query), 200);

    }

    public function getBoxDetails( $box ) {

        $query = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
            ->select( $this->db->raw('DISTINCT b.box_no, a.tl_no, a.sku, a.upc, a.item_desc, a.qty_req'))
            ->join( $this->db->raw('wms_subloc_box_detail b'), function ( $join ){
                $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                $join->on($this->db->raw('a.upc'), '=', $this->db->raw('b.upc'));
            })                          ->where( $this->db->raw('b.box_no'), $box )
            ->orderBy( $this->db->raw('a.upc'))
            ->get();

        return response()->json(array('result' => $query), 200);

    }

    public function updateMtsQuantity( $tl, $upc, $quantity, $box ) {

        $this->db->table('store_rcv_dtl')
            ->where('tl_no', $tl)
            ->where('upc', $upc)
            ->update(['qty_rec' => $quantity, 'box_no' => $box, 'is_subloc' => 1, 'updated_at' => date('Y-m-d H:i:s') ]);

        return response()->json(array('success' => 'true'), 200);
    }

    public function updateStatus( $box ) {

        $this->db->table('subloc_load_detail')
            ->where('box_no', $box)
            ->update(['is_box_status' => 0 ]);

        $checkPell = $this->db->table('subloc_load_detail')
            ->select('load_code')
            ->where('box_no', $box )
            ->first();

        $checkIfDone = $this->db->table('subloc_load_detail')
            ->select('box_no')
            ->where('load_code', $checkPell->load_code )
            ->whereIn('is_box_status', [1,2])
            ->get();


        //check if boxes are all done

        if ( !count( $checkIfDone ) ) {

            $this->db->table('subloc_load_list')->where('load_code', $checkPell->load_code)->update(['load_status_store' => 1 ]);

        }

        return response()->json(array('success' => 'true'), 200);

    }

    public function insertItems( $tl, $box, $upc, $qty_rec, $user ) {

        $data = array();

        $data['tl_no']      = $tl;
        $data['upc']        = $upc;
        $data['box_no']     = $box;
        $data['mov_qty']    = $qty_rec;

        $this->db->table('subloc_box_detail')->insert( $data );

        $param = array();
        $param['tl_no']         = $tl;
        $param['upc']           = $upc;
        $param['qty_rec']       = $qty_rec;
        $param['is_subloc']     = 1;
        $param['box_no']        = $box;
        $param['updated_at']    = Carbon::now();

        $this->db->table('store_rcv_dtl')->insert( $param );

        $arrParams = array(
            'module'		=> 'Store Receiving',
            'action'		=> 'Add items in Box',
            'reference'		=> $user,
            'data_before'	=> '',
            'data_after'	=> 'Add items in Box #'.$box,
            'user_id'		=> $user,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return response()->json(array('success' => 'true'), 200);
    }

}
