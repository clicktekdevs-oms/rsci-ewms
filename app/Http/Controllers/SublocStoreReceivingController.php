<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Database\DatabaseManager as db;
use Auth;
use PDF;
use App\Helper\CommonHelper;

class SublocStoreReceivingController extends Controller
{

    private $db;

    public function __construct( db $databaseManager ) {

        $this->db = $databaseManager;
        $this->middleware('str_receiving_gate');

    }

    public function index( Request $request ) {
        //

        $request->flash();

        if ( Auth::user()->role_id == 7 ) {

            $getList = $this->db->table($this->db->raw('wms_subloc_load_list a'))
                ->select( $this->db->raw('DISTINCT a.id, a.load_code, a.ship_date, a.load_status_store'))
                ->join( $this->db->raw('wms_subloc_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                ->where($this->db->raw('a.load_status'), 1)
                ->where($this->db->raw('b.store_num'), Auth::user()->store_code)
                ->orderBy('load_code', 'desc')
                ->paginate( 10 );

        } else {

            $getList = $this->db->table($this->db->raw('wms_subloc_load_list a'))
                ->select( $this->db->raw('DISTINCT a.id, a.load_code, a.ship_date, a.load_status_store'))
                ->join( $this->db->raw('wms_subloc_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                ->where($this->db->raw('a.load_status'), 1)
                ->orderBy('load_code', 'desc')
                ->paginate( 10 );

        }

        $getAuthStore = $this->db->table('store_masterlist')
            ->select('store_nam')
            ->where('store_num', Auth::user()->store_code)
            ->first();

        $data = array();
        $data['load_list'] = $getList;
        $data['store_name'] = $getAuthStore->store_nam;


        return view('subloc_store_rcv.list', $data );

    }

    public function getSublocRcvListFilter( Request $request ) {

        $request->flash();

        $pell = trim($request->get('pell'));
        $ship_date = trim($request->get('ship_date') );

        if ( Auth::user()->role_id == 7 ) {

            $getList = $this->db->table($this->db->raw('wms_subloc_load_list a'))
                ->select( $this->db->raw('DISTINCT a.id, a.load_code, a.ship_date, a.load_status_store'))
                ->join( $this->db->raw('wms_subloc_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                ->where($this->db->raw('a.load_status'), 1)
                ->where($this->db->raw('b.store_num'), Auth::user()->store_code);

            if (!empty( $pell ) ) {

                $getList->whereRaw("a.load_code LIKE '$pell%'");
            }

            if (!empty( $ship_date ) ) {

                $getList->where($this->db->raw("a.ship_date"), $ship_date);
            }

            $getList = $getList->orderBy('load_code', 'desc')->paginate( 10 );

        } else {

            $getList = $this->db->table($this->db->raw('wms_subloc_load_list a'))
                ->select( $this->db->raw('DISTINCT a.id, a.load_code, a.ship_date, a.load_status_store'))
                ->join( $this->db->raw('wms_subloc_load_detail b'), $this->db->raw('a.load_code'), '=', $this->db->raw('b.load_code'))
                ->where($this->db->raw('a.load_status'), 1);

            if (!empty( $pell ) ) {

                $getList->where($this->db->raw("a.load_code LIKE '$pell%'"));
            }

            if (!empty( $ship_date ) ) {

                $getList->where($this->db->raw("a.ship_date"), $ship_date);
            }

            $getList = $getList->orderBy('load_code', 'desc')->paginate( 10 );


        }

        $getAuthStore = $this->db->table('store_masterlist')
            ->select('store_nam')
            ->where('store_num', Auth::user()->store_code)
            ->first();

        $data = array();
        $data['load_list'] = $getList;
        $data['store_name'] = $getAuthStore->store_nam;

        return view('subloc_store_rcv.list', $data );

    }

    /**
     * @param $pell
     */

    public function getPellDetails( $pell ) {

        $getList = $this->db->table('subloc_load_list')
            ->select('id', 'load_code', 'ship_date')
            ->where('load_code', $pell)
            ->first();

        $getPellDetails = $this->db->table($this->db->raw('wms_subloc_load_detail a'))
            ->select($this->db->raw("a.box_no, 
            group_concat(DISTINCT b.tl_no) as tl_no,
             (select concat(firstname, ' ', lastname) from wms_users where id = is_assign_store ) as store_clerk,
              (SELECT sum(mov_qty) from wms_subloc_box_detail where box_no = b.box_no) as total_qty_rcv,
              (SELECT sum(qty_rec) from wms_store_rcv_dtl where box_no = b.box_no and is_subloc = 1) as total_qty_scan,
               is_box_status"))
            ->join($this->db->raw('wms_subloc_box_list b'), $this->db->raw('a.box_no'), '=', $this->db->raw('b.box_no'))
            ->where($this->db->raw('a.load_code'), $pell)
            ->where($this->db->raw('b.is_assign'), 1)
            ->where($this->db->raw('b.is_open'), 1)
            ->groupBy( $this->db->raw('a.box_no'))
            ->orderBy('box_no')
            ->get();



        $data = array();
        $data['box_lists']   = $getPellDetails;
        $data['pell']        = $pell;
        $data['ship_date']   = $getList->ship_date;


        return view('subloc_store_rcv.detail', $data);
    }

    public function getBoxDetails( $box_no ) {

        $getBoxList = $this->db->table('subloc_load_detail')
            ->where('box_no', $box_no)
            ->first();

        $getBoxDetails = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
            ->select( $this->db->raw('DISTINCT b.box_no, a.tl_no, a.sku, a.upc, a.item_desc, a.qty_req, a.qty_rec'))
            ->leftJoin( $this->db->raw('wms_subloc_box_detail b'), function ( $join ){
                $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                $join->on($this->db->raw('a.upc'), '=', $this->db->raw('b.upc'));
            })
            ->where( $this->db->raw('b.box_no'), $box_no)
            ->orWhere( $this->db->raw('a.box_no'), $box_no)
            ->orderBy($this->db->raw('a.tl_no, a.upc'))
            ->get();



        $data = array();
        $data['box_info']  = $getBoxList;
        $data['box_lists'] = $getBoxDetails;

        return view('subloc_store_rcv.box_number', $data);

    }

    /**
     * @param Request $request
     */

    public function updateQty( Request $request ) {

        $tl     = $request->get('tl_no');
        $sku    = $request->get('sku');
        $upc    = $request->get('upc');
        $qty    = $request->get('qty_rec');
        $box    = $request->get('box_no');

        $this->db->table('store_rcv_dtl')
            ->where('tl_no', $tl)
            ->where('sku', $sku)
            ->where('upc', $upc)
            ->update(['qty_rec' => $qty, 'box_no'  => $box, 'updated_at' => date('Y-m-d H:i:s') ]);

        $arrParams = array(
            'module'		=> 'Subloc Store Receiving',
            'action'		=> 'Update Items in Box',
            'reference'		=>  '',
            'data_before'	=> '',
            'data_after'	=> 'Update Box# ='.$box .'sku#'.$sku.';upc#'.$upc.";mts#".$tl,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', 'Quantity successfully updated!');
    }

    /**
     * @param Request $request
     */

    public function assignClerk( Request $request ) {

        $pell   = $request->get('pell');
        $box_no = $request->get('box_no');
        $btn    = $request->get('action_button');
        $date   = $request->get('ship_date');

        if ( $btn == 'assign' ) {

            $box_no = implode(',', $box_no );

            $getClerk = $this->db->table('users')
                ->select($this->db->raw("id, concat(firstname, ' ', lastname) as name"))
                ->whereIn('role_id', [7, 8])
                ->where('store_code', Auth::user()->store_code)
                ->orderBy('id')
                ->get();


            $data = array();
            $data['pell']       = $pell;
            $data['box_no']     = $box_no;
            $data['clerks']     = $getClerk;

            return view('subloc_store_rcv.assign_strclerk_form', $data);

        }elseif( $btn == 'unlisted' ) {

            $getUnlistedDtl = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
                ->select( $this->db->raw("DISTINCT b.box_no, (select concat(firstname, ' ', lastname) from wms_users where id = c.is_assign_store) as name"))
                ->join( $this->db->raw('wms_subloc_box_list b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                ->join( $this->db->raw('wms_subloc_load_detail c'), $this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'))
                ->whereRaw("c.load_code =  '$pell' ")
                ->where( $this->db->raw('a.qty_req'), 0)
                ->first();

            $getUnlisted = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
                ->select( $this->db->raw('DISTINCT b.box_no, a.tl_no, a.sku, a.upc, a.item_desc, a.qty_req, a.qty_rec, a.created_at'))
                ->join( $this->db->raw('wms_subloc_box_list b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                ->join( $this->db->raw('wms_subloc_load_detail c'), $this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'))
                ->whereRaw("c.load_code =  '$pell' ")
                ->where( $this->db->raw('a.qty_req'), 0)
                ->get();

            $getStoreName = $this->db->table('store_masterlist')
                ->select($this->db->raw("store_nam"))
                ->where('store_num', Auth::user()->store_code)
                ->first();

            $data = array();
            $data['pell']       = $pell;
            $data['box']        = !empty( $getUnlistedDtl->box_no ) ? $getUnlistedDtl->box_no : '';
            $data['unlisteds']  = $getUnlisted;
            $data['scanner']    = !empty( $getUnlistedDtl->name ) ? $getUnlistedDtl->name : '' ;
            $data['store_name'] = $getStoreName->store_nam;

            return view('subloc_store_rcv.unlisted', $data );

        } elseif ( $btn == 'discrepancy' ) {

            $getStoreName = $this->db->table('store_masterlist')
                ->select($this->db->raw("store_nam"))
                ->where('store_num', Auth::user()->store_code)
                ->first();

            $getDiscrepancyFirst = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
                ->select( $this->db->raw("DISTINCT c.load_code, (select concat(firstname, ' ', lastname) from wms_users where id = c.is_assign_store) as name"))
                ->join( $this->db->raw('wms_subloc_box_list b'), $this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'))
                ->join( $this->db->raw('wms_subloc_load_detail c'), $this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'))
                ->whereRaw("c.load_code =  '$pell' ")
                ->whereRaw('a.qty_req != 0')
                ->whereRaw('a.qty_req <> a.qty_rec')
                ->first();

            $getDiscrepancy = $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
                ->select( $this->db->raw('DISTINCT b.box_no, a.tl_no, a.sku, a.upc, a.item_desc, b.mov_qty as qty_req, a.qty_rec, a.updated_at'))
                ->join( $this->db->raw('wms_subloc_box_detail b'), function ( $join ) {
                    $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                    $join->on($this->db->raw('a.upc'), '=', $this->db->raw('b.upc'));
                })
                ->join( $this->db->raw('wms_subloc_load_detail c'), function ( $join ) {
                    $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('c.tl_no'));
                    $join->on($this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'));
                })
                ->whereRaw("c.load_code =  '$pell' ")
                ->whereRaw('a.qty_req != 0')
                ->whereRaw('a.qty_req <> a.qty_rec')
                ->get();



            $data = array();
            $data['pell']       = $pell;
            $data['box']        = !empty( $getDiscrepancyFirst->box_no ) ? $getDiscrepancyFirst->box_no : '';
            $data['discrepancies']  = $getDiscrepancy;
            $data['scanner']    = !empty( $getDiscrepancyFirst->name ) ? $getDiscrepancyFirst->name : '' ;
            $data['store_name'] = $getStoreName->store_nam;

            return view('subloc_store_rcv.discrepancy', $data );
        }

        elseif ( $btn == 'summary' ) {

            $getStoreName = $this->db->table('store_masterlist')
                ->select($this->db->raw("store_nam"))
                ->where('store_num', Auth::user()->store_code)
                ->first();

            $getSummary =  $this->db->table( $this->db->raw('wms_store_rcv_dtl a'))
                ->select( $this->db->raw('DISTINCT b.box_no, a.tl_no, a.sku, a.upc, a.item_desc, b.mov_qty as qty_req, c.is_assign_store as name, a.qty_rec, a.created_at'))
                ->join( $this->db->raw('wms_subloc_box_detail b'), function ( $join ) {
                    $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('b.tl_no'));
                    $join->on($this->db->raw('a.upc'), '=', $this->db->raw('b.upc'));
                })
                ->join( $this->db->raw('wms_subloc_load_detail c'), function ( $join ) {
                    $join->on($this->db->raw('a.tl_no'), '=', $this->db->raw('c.tl_no'));
                    $join->on($this->db->raw('b.box_no'), '=', $this->db->raw('c.box_no'));
                })
                ->whereRaw("c.load_code =  '$pell' ")
                ->orderBy($this->db->raw('b.box_no,a.tl_no'))
                ->get();
            $data = array();
            $data['summaries']  = $getSummary;
            $data['pell']       = $pell;
            $data['ship_date']  = $date;
            $data['store_name'] = $getStoreName->store_nam;


            return view('subloc_store_rcv.store_report_list', $data);
        }


    }

    public function assignToClerk( Request $request ) {

        $pell   = $request->get('pell_no');
        $box    = $request->get('box_no');
        $clerk  = $request->get('clerk');

        $boxes = explode(',', $box );

        array_map('trim', $boxes);

        foreach ( $boxes as $box ) {

            $this->db->table('subloc_load_detail')
                ->where( 'load_code', $pell )
                ->where( 'box_no', $box )
                ->update( ['is_assign_store' => $clerk, 'is_box_status' => 1] );

            $arrParams = array(
                'module'		=> 'Subloc Store Receiving',
                'action'		=> 'Assign Box to Store Clerk',
                'reference'		=>  '',
                'data_before'	=> '',
                'data_after'	=> 'Assign Box#'.json_encode($box).';to '.json_encode(User::getUsersName(ucwords($clerk))),
                'user_id'		=> Auth::user()->id,
                'created_at'	=> date('Y-m-d H:i:s'),
                'updated_at'	=> date('Y-m-d H:i:s')
            );

            $this->db->table('audit_trail')->insert($arrParams);


        }



        return redirect('store-subloc-receiving/'.$pell )->with('message', 'Successfully assigned to Store Clerk!');

    }

    /**
     * @param Request $request
     */

    public function receiveLoad( Request $request ) {

        $jda_username   = trim( strtoupper( $request->get('jda_username') ) );
        $jda_password   = trim( strtoupper( $request->get('jda_password') ) );
        $pell_no        = trim( $request->get('pell_code') );

        CommonHelper::jdaLogin( $jda_username, $jda_password, $pell_no, 'login_run');

        $checkLoginStatus = $this->db->table('jda_login_logs')
            ->select('text_desc', 'is_status')
            ->where('ref_no', $pell_no)
            ->orderBy('created_at', 'desc')
            ->first();

        if ( $checkLoginStatus->is_status == 0 ) {

            //update pell to received
            $this->db->table('subloc_load_list')->where('load_code', $pell_no )->update( ['load_status_store' => 0] );

            $message = 'Pell Successfully Received!';


        } else {

            $message = 'An error occured.Please see logs.';
        }

        $arrParams = array(
            'module'		=> 'Subloc Store Receiving',
            'action'		=> 'Received Pell in JDA',
            'reference'		=>  '',
            'data_before'	=> '',
            'data_after'	=> 'Receive Pell#'.$pell_no,
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect()->back()->with('message', $message );

    }

    public function pullData() {

        $filename   = 'job_subloc_rcv.php';
        $mod        = 'subloc_rcv';

        CommonHelper::pullData( $filename, $mod );

        // AuditTrail
        $data_after = "";

        $arrParams = array(
            'module'		=> 'Subloc Store Receiving',
            'action'		=> 'Pull Data from JDA',
            'reference'		=> Auth::user()->id,
            'data_before'	=> '',
            'data_after'	=> '',
            'user_id'		=> Auth::user()->id,
            'created_at'	=> date('Y-m-d H:i:s'),
            'updated_at'	=> date('Y-m-d H:i:s')
        );

        $this->db->table('audit_trail')->insert($arrParams);

        return redirect('store-subloc-receiving')->with('message', 'Successfully pulled from JDA!');

    }


}
