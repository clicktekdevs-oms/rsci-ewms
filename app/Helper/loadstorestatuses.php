<?php
namespace App\Helper;
/**
 * Created by PhpStorm.
 * User: dhap
 * Date: 6/2/17
 * Time: 7:51 PM
 */

function load_store_status() {

    return array(

        '2' => 'OPEN',
        '1' => 'DONE',
        '0' => 'RECEIVED'

    );
}