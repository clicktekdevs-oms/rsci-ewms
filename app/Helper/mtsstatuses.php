<?php
namespace App\Helper;

function mts_status() {

    return array(

        '3' => 'OPEN',
        '2' => 'ASSIGNED',
        '1' => 'DONE',
        '0' => 'POSTED'

    );
}