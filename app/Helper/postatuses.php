<?php
namespace App\Helper;
/**
 * Created by PhpStorm.
 * User: dhap
 * Date: 6/2/17
 * Time: 7:51 PM
 */

function po_status() {

    return array(

        '4' => 'OPEN',
        '3' => 'ASSIGNED',
        '2' => 'DONE',
        '1' => 'PARTIAL RECEIVED',
        '0' => 'POSTED'

    );
}
