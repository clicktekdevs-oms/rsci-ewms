<?php
namespace App\Helper;
/**
 * Created by PhpStorm.
 * User: dhap
 * Date: 6/2/17
 * Time: 7:51 PM
 */

function picklist_status() {

    return array(

        '3' => 'OPEN',
        '2' => 'ASSIGNED',
        '1' => 'DONE',
        '0' => 'POSTED'

    );
}