<?php
namespace App\Helper;
use App\Helper\postatuses;
use App\Helper\pickliststatus;
use App\Helper\mtsstatuses;
use App\Helper\loadstorestatuses;
use DB;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Pagination\LengthAwarePaginator;
/**
 * Common functions such as random numbers, string manipulations/validations
 *
 * @package 		SSI-WMS
 * @subpackage 	Common
 * @category    	Helpers
 * @author 		Dean Francis Casili | fcasili2stratpoint.com | dean.casili@gmail.com
 * @version 		Version 1.0
 *
 */
class CommonHelper {
    /**
     * check variable is empty
     *
     * @param 	$var		variable to be analyzed
     * @return 	(boolean)
     **/
    public static function hasValue($var)
    {
        if(!isset($var))
        {
            return FALSE;
        } else
        {
            if(is_null($var) || empty($var))
            {
                return FALSE;
            } else
            {
                return TRUE;
            }
        }
    }



    public static function filternator($query,$arrparam=array(),$limit,$getcount=false)
    {
        $x=0;
        foreach ($arrparam as $key => $filcol)
        {
            $subkey= substr($key,7);
            if ($x<$limit)
            {
                if( CommonHelper::hasValue($filcol) && CommonHelper::hasValue($key) ) $query->where(''.$subkey.'', 'LIKE', '%'. $filcol .'%');
            }
            if ($key=='sort')$sort=$filcol;
            if ($key=='order')$order=$filcol;
            if ($key=='page')$page=$filcol;
            $x++;
        }
        if($getcount) return count($query);

        if( CommonHelper::hasValue($sort) && CommonHelper::hasValue($order))
        {

        }
        if(CommonHelper::hasValue($page))
        {
            $query->skip(30 * ($page - 1))
                ->take(30);
        }

        return $query;
    }

    public static function pagenator($query,$page)
    {
        if(CommonHelper::hasValue($page))
        {
            $query->skip(30 * ($page - 1))
                ->take(30);
        }
        return $query;
    }

    /**
     * checks if variable is a valid array and not empty
     *
     * @param    $arr        array variable to be analyzed
     * @return   (boolean)
     **/
    public static function arrayHasValue($arr)
    {
        if(!isset($arr) && !is_array($arr))
        {
            return FALSE;
        } else
        {
            if(count($arr) === 0 || empty($arr))
            {
                return FALSE;
            } else
            {
                return TRUE;
            }
        }
    }

    /**
     * returns null if the variable is empty
     *
     * @param    $var        variable to be analyzed
     * @return   (mixed)
     **/
    public static function assess_variable_value($var)
    {
        return (has_value($var) ? $var : NULL);
    }

    /**
     * check variable is numeric and has value
     *
     * @param    $var        variable to be analyzed
     * @return   (boolean)
     **/
    public static function numericHasValue($var)
    {
        if(isset($var) && is_numeric($var))
        {
            return TRUE;
        } else
        {
            return FALSE;
        }
    }

    /**
     * recursively converts object to array
     *
     * @param (object)   $data   object to be converted to array
     * @return (object)
     **/
    public static function object_to_array($data)
    {
        if(is_array($data) || is_object($data)) {
            $result = array();
            foreach($data as $key => $value) {
                $result[$key] = CommonHelper::object_to_array($value);
            }
            return $result;
        }
        return $data;
    }

    /**
     * searches a specific array item based on key
     *
     * @param (string)   $item           the needle
     * @param (string)   $array_key      key of the array to be compared with the needle
     * @param (string)   $array_items    the haystak
     * @return (array)
     **/
    public static function get_item_from_array($item,$array_key,$array_items)
    {
        if(! CommonHelper::arrayHasValue($array_items)) return array();
        if(! CommonHelper::hasValue($item)) return array();
        $array_val = array();
        foreach($array_items as $array_item)
        {
            if($array_item[$array_key] == $item)
            {
                $array_val = $array_item;
                break;
            }
        }
        return $array_val;
    }

    /**
     * checks if the value is in the array
     *
     * @param (string)   $item           the needle
     * @param (string)   $array_items    the haystak
     * @return (boolean)
     **/
    public static function valueInArray($item, $array_items)
    {
        // print_r()
        if(! CommonHelper::arrayHasValue($array_items)) return FALSE;

        if (in_array($item, $array_items)) {
            return TRUE;
        } else {
            return FALSE;
        }
    }


    public static function setRequiredFields($required_fields = array()) {
        if(self::arrayHasValue($required_fields))
        {
            foreach($required_fields as $value)
            {
                $tmp_val = Input::get($value);
                if(!self::hasValue($tmp_val)) throw new Exception("Missing {$value} parameter");
            }
        }
    }

    public static function return_success_message($message)
    {
        return Response::json(array(
            'error' => false,
            'message' => 'Success',
            'result' => $message),
            200
        );
    }

    public static function return_success()
    {
        return Response::json(array(
            'error' => false,
            'message' => 'Success'),
            200
        );
    }


    public static function return_fail($message)
    {
        return Response::json(array(
            "error" => true,
            "result" => $message),
            400
        );
    }

    /**
     * Execute command in the background without PHP waiting for it to finish for Unix
     *
     * @example  Commonhelper::execInBackground();
     *
     * @param  $cmd       string command to execute
     * @return
     */
    /*  public static function execInBackground($cmd,$source)
     {
         $cmd = 'php -q ' . __DIR__.'/../../app/cron/jda/' . $cmd;
         $filename=$source . "_" . date('m_d_y');
         $outputfile = __DIR__.'/../../app/cron/jda/logs/'.$filename.'.log';
         $pidfile = __DIR__.'/../../app/cron/jda/logs/pidfile.log';

         exec(sprintf("%s >> %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
         // exec($cmd . " >/dev/null 2> /dev/null & echo $!");

         // exec($cmd . " > /dev/null &");
     }*/
    public static function execInBackgroundDEB($cmd,$source)
    {
        $cmd = 'php -q ' . __DIR__.'/../../app/cron/jda/' . $cmd;
        $filename=$source . "_" . date('m_d_y');
        $outputfile = __DIR__.'/../../app/cron/jda/logs/'.$filename.'.log';
        $pidfile = __DIR__.'/../../app/cron/jda/logs/pidfile.log';

        exec(sprintf("%s >> %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
        // exec($cmd . " >/dev/null 2> /dev/null & echo $!");

        // exec($cmd . " > /dev/null &");
    }

    /**
     * Execute command in the background without PHP waiting for it to finish for Unix
     *
     * @example  Commonhelper::execInBackground();
     *
     * @param  $cmd       string command to execute
     * @return
     */
    public static function execInBackground()
    {
        $cmd = 'php -q ' . __DIR__.'/../../app/cron/jda/';

       print_r($cmd);
//        $filename=$source . "_" . date('m_d_y');
//        $outputfile = __DIR__.'/../../app/cron/jda/logs/'.$filename.'.log';
//        $pidfile = __DIR__.'/../../app/cron/jda/logs/pidfile.log';
//
//        exec(sprintf("%s >> %s 2>&1 & echo $! >> %s", $cmd, $outputfile, $pidfile));
//        // exec($cmd . " >/dev/null 2> /dev/null & echo $!");
//
//        // exec($cmd . " > /dev/null &");
    }

    /**
     * Execute command in the background without PHP waiting for it to finish for Unix
     *
     * @example  Commonhelper::execInBackground();
     *
     * @param  $cmd       string command to execute
     * @return
     */
    public static function archiveLogs()
    {
        $file       = __DIR__.'/../../archive_logs/audit_trail_'.time().'.sql';
        $cmd        = 'mysqldump -uroot -proot ccri wms_audit_trail > ' . $file;
        $outputfile = __DIR__.'/../../archive_logs/output.log';
        $pidfile    = __DIR__.'/../../archive_logs/pidfile.log';

        exec($cmd, $outputfile, $pidfile);

        if($pidfile) {
            return FALSE;
        }

        return TRUE;
    }

    /*
     * Method to strip tags globally.
     */
    public static function globalXssClean()
    {
        // Recursive cleaning for array [] inputs, not just strings.
        $sanitized = static::arrayStripTags(Input::get());
        Input::merge($sanitized);
    }

    public static function arrayStripTags($array)
    {
        $result = array();

        foreach ($array as $key => $value) {
            // Don't allow tags on key either, maybe useful for dynamic forms.
            $key = strip_tags($key);

            // If the value is an array, we will just recurse back into the
            // function to keep stripping the tags out of the array,
            // otherwise we will set the stripped value.
            if (is_array($value)) {
                $result[$key] = static::arrayStripTags($value);
            } else {
                // I am using strip_tags(), you may use htmlentities(),
                // also I am doing trim() here, you may remove it, if you wish.
                $result[$key] = trim(strip_tags($value));
            }
        }

        return $result;
    }

    public static function getPOStatus( $status_id ) {

        $po_status = po_status();

       return $po_status[$status_id];

    }

    public static function getPickStatus( $status_id ) {

        $pick_status = picklist_status();

        return $pick_status[$status_id];

    }

    public static function getMtsStatus( $status_id ) {

        $mts_status = mts_status();

        return $mts_status[$status_id];
    }

    public static function getStoreStatus( $status_id ) {

        $str_status = store_status();

        return $str_status[$status_id];

    }

    public static function getLoadStrStatus( $status_id ) {

        $str_status = load_store_status();

        return $str_status[$status_id];
    }

    public static function pullData($cmd, $source) {
        //pull data using shell command
        if(!file_exists(app_path('Jda4R4/JdaInput/Jobs/').$cmd) || $source == null) {
            print_r('Please Check your Module filename.');
        }else{
            $cmd = 'nohup php -q '.dirname(getcwd()).'/app/Jda4R4/JdaInput/Jobs/'.$cmd;
            $filename = $source."_".date('m_d_y');
            $logs = dirname(getcwd()).'/app/Jda4R4/JdaInput/Logs/'.$filename.'.log';
            shell_exec(sprintf('%s >> %s 2>&1',$cmd, $logs));
            return 'Successfully pulled from JDA';
        }
    }

    public static function jdaLogin($jda_username, $jda_password, $pell, $command)
    {
        $path = app_path("Jda4R4/JdaOutput/db2Jobs/".$command.".php $jda_username $jda_password $pell");
        $cmd = 'php -q '.$path;
        $filename = $command."_".date('m_d_y');
        $logs = app_path('Jda4R4/JdaOutput/db2Logs/'.$filename.'.logs');

        return shell_exec(sprintf('%s >> %s 2>&1',$cmd, $logs));

    }
    
    public static function getBoxDetails( $tl_no, $box_no ) {


        $get_boxes = DB::table(DB::raw('wms_pick_list a'))
            ->select(DB::raw('DISTINCT a.tl_no, c.box_no, b.dept_code,  a.ship_date, b.short_desc, b.sku, b.upc, b.item_desc, b.qty_req, c.mov_qty, b.rtl_price'))
            ->join( DB::raw('wms_pick_detail b'), DB::raw('a.tl_no'), '=', DB::raw('b.tl_no'))
            ->join( DB::raw('wms_box_detail c'), function ( $join) {
                      $join->on( DB::raw('a.tl_no'), '=', DB::raw('c.tl_no'));
                      $join->on( DB::raw('b.upc'), '=', DB::raw('c.upc'));
            })
            ->where( DB::raw('a.tl_no'), $tl_no )
            ->where( DB::raw('c.box_no'), $box_no )
            ->orderBy( DB::raw('b.short_desc'))
            ->orderBy( DB::raw('b.sku'))
            ->get();
        
        return $get_boxes;
    }

    public static function getSublocBoxDetails( $tl_no, $box_no ) {


        $get_boxes = DB::table(DB::raw('wms_subloc_pick_list a'))
            ->select(DB::raw('DISTINCT a.tl_no, c.box_no, b.dept_code,  a.ship_date, b.dept_code, b.short_desc, b.sku, b.upc, b.item_desc, b.qty_req, c.mov_qty'))
            ->join( DB::raw('wms_subloc_pick_detail b'), DB::raw('a.tl_no'), '=', DB::raw('b.tl_no'))
            ->join( DB::raw('wms_subloc_box_detail c'), function ( $join) {
                $join->on( DB::raw('a.tl_no'), '=', DB::raw('c.tl_no'));
                $join->on( DB::raw('b.upc'), '=', DB::raw('c.upc'));
            })
            ->where( DB::raw('a.tl_no'), $tl_no )
            ->where( DB::raw('c.box_no'), $box_no )
            ->orderBy( DB::raw('b.short_desc'))
            ->orderBy( DB::raw('b.sku'))
            ->get();

        return $get_boxes;
    }

    public static function getPellStatus( $pell ) {

        $get_boxes = DB::table('load_list')
            ->where('load_code', $pell )
            ->first();

        return $get_boxes->load_status_store;
    }

    public static function getValueFromArray($array)
    {
        $array_item = array();

        foreach ( $array as $item ) {

            $array_item = explode(',', $item->permissions);

        }

        return $array_item;
    }

    public static function customPaginate($perPage, $result)
    {
        $currentPage = LengthAwarePaginator::resolveCurrentPage();

        //Create a new Laravel collection from the array data
        $collection = collect($result);

        //Slice the collection to get the items to display in current page
        $currentPageSearchResults = $collection->slice(($currentPage -1) * $perPage, $perPage)->all();

        //Create our paginator and pass it to the view
        $paginatedSearchResults = new LengthAwarePaginator($currentPageSearchResults, count($collection), $perPage);

        return $paginatedSearchResults;

    }

}
