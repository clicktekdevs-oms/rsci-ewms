<?php
require_once(__DIR__.'/../mysqlInstance/queries.php');
require_once(__DIR__.'/../db2Instance/queries.php');

/**
 *
 * Get PO available in db2/Jda
 */

$db2_connect = new db2Queries();
$db2_connect->Connect();
$db2_connect->getSublocPick();
$db2_connect->getSublocPickDetail();

$sql_connect = new MysqlInstances();
$sql_connect->Connect();
$sql_connect->getSublocPickList();
$sql_connect->getSublocPickDetail();