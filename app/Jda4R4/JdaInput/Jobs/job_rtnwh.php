<?php
require_once(__DIR__.'/../mysqlInstance/queries.php');
require_once(__DIR__.'/../db2Instance/queries.php');

/**
 *
 * Get PO available in db2/Jda
 */
$db2_connect = new db2Queries();
$db2_connect->Connect();
$db2_connect->getReturnToWH();
$db2_connect->getReturnToWHDetails();

/**
 *
 * Get PO's from CSV to mysql DB
 *
 */
$mysql_connect = new MysqlInstances();
$mysql_connect->Connect();
$mysql_connect->getReturnToWHList();
$mysql_connect->getReturnWHDetails();