<?php

/**
 * @property null dbh
 */
class MysqlConnect
{

    function __construct($system, $database, $username, $password)
    {
        $this->system   = $system;
        $this->database = $database;
        $this->username = $username;
        $this->password = $password;
    }

    public function Connect()
    {
        try {
            $cnString = "mysql:host={$this->system};dbname={$this->database}; --local-infile";

            echo "Connecting to MYSQL(FSRI)... \n";
            echo "HOST: {$this->system}\t";
            echo "DBNAME: {$this->database}\n";
            echo "USERNAME: {$this->username}\t";
            echo "Password: ".str_repeat('*', strlen($this->password) + rand(1,20)). "\n";

            $this->dbh = new PDO($cnString, $this->username, $this->password,array(
                PDO::MYSQL_ATTR_LOCAL_INFILE => true,
                PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_PERSISTENT => true
            ));

            echo "\nConnected Successfully! \n ";
        } catch (PDOException $e) {
            echo "\nCan't Connect \n {$e->getMessage()} \n";
            exit(str_repeat('-', 100)."\n");
        }
    }

    public function sanitize_columns($columns)
    {
        return isset($columns) ? implode(',', $columns) : 'Error!empty columns';
    }


    public function importToMysqlDB($file, $table, $columns)
    {
        if($file != '' && $table != '' && $columns != '') {
            $columns = $this->sanitize_columns($columns);
            $query = "LOAD DATA LOCAL INFILE '$file' IGNORE INTO TABLE {$table} FIELDS TERMINATED BY ',' ENCLOSED BY '\"' LINES TERMINATED BY '\n' IGNORE 1 LINES ($columns)";
            echo "\n Importing $file to Table=$table";
            $this->dbh->exec($query);
            echo "\n Imported Successfully! (Table=$table). \n";
        }else {
            exit('Please check your parameters!');
        }
    }

    public function close() {
        echo "Closing PDO Connection... \n";
        echo str_repeat('-',100)."\n";
        $this->dbh = null;
    }
}
