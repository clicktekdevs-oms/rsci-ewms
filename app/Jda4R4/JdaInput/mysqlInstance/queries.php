<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/filelist.php');
class MysqlInstances
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
                                          $mysql['DATABASE'],
                                          $mysql['USERNAME'],
                                          $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function _fileExist($file)
    {
        if(file_exists($file)) {
            return $file;
        }else {
            exit("Unable to locate the file ($file)! \n");
        }
    }

    public function getReceivingList()
    {
        $tbl_purchase_order_list = 'wms_po_list';
        $file = $this->_fileExist(PURCHASE_ORDER_LIST);
        $csv_columns = array('rcv_no', 'inv_no', 'po_no', 'total_qty', 'entry_date');
        return $this->sqlconn->importToMysqlDB($file, $tbl_purchase_order_list, $csv_columns);
    }

    public function getReceivingDetail()
    {
        $tbl_name = 'wms_po_detail';
        $file = $this->_fileExist(PURCHASE_ORDER_DETAILS);
        $csv_columns = array('sku','upc','rcv_no','division_code', 'dept_code', 'style_no', 'qty_ord', 'division', 'department', 'item_desc', 'short_name');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getPickList() {

        $tbl_name = 'wms_pick_list';
        $file = $this->_fileExist(PICK_LIST);
        $csv_columns = array('tl_no', 'doc_no', 'ship_date','to_loc');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getPickDetail() {

        $tbl_name = 'wms_pick_detail';
        $file = $this->_fileExist(PICK_DETAIL);
        $csv_columns = array('tl_no', 'doc_no', 'to_loc', 'ship_date', 'short_desc', 'item_desc', 'sku', 'upc', 'from_slot', 'rtl_price', 'qty_req', 'dept_code', 'dept_desc', 'item_size', 'item_color');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);

    }

    public function getReturnToWHList() {

        $tbl_name   = 'wms_rtnwh_list';
        $file       = $this->_fileExist(RTN_WH_LIST );
        $csv_columns = array( 'mts_no', 'from_loc' );
        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_columns);

    }

    public function getReturnWHDetails() {

        $tbl_name    = 'wms_rtnwh_detail';
        $file        = $this->_fileExist( RTN_WH_DTL );
        $csv_column  = array( 'mts_no', 'sku', 'upc', 'item_desc', 'qty_req' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );

    }

    public function getSublocRcvList() {

        $tbl_name    = 'wms_subloc_rcv_list';
        $file        = $this->_fileExist( SUBLOC_RCV_LIST );
        $csv_column  = array( 'tl_no', 'ship_date', 'from_loc', 'to_loc' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );

    }

    public function getSublocRcvDtl() {

        $tbl_name    = 'wms_subloc_rcv_dtl';
        $file        = $this->_fileExist( SUBLOC_RCV_DTL );
        $csv_column  = array( 'tl_no', 'sku', 'upc', 'short_name', 'qty_ord' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );

    }

    public function getSublocPickList() {

        $tbl_name   = 'wms_subloc_pick_list';
        $file       = $this->_fileExist(SUBLOC_PICK_LIST);
        $csv_column  = array( 'tl_no', 'ship_date', 'from_loc', 'to_loc');

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );
    }

    public function getSublocPickDetail() {

        $tbl_name   = 'wms_subloc_pick_detail';
        $file       = $this->_fileExist(SUBLOC_PICK_DTL);
        $csv_column  = array( 'tl_no', 'ship_date', 'from_loc', 'to_loc', 'dept_code', 'dept_desc', 'sku', 'upc', 'short_desc', 'item_desc', 'item_size', 'item_color', 'rtl_price', 'qty_req' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );
    }

    public function getStoreReceivingList() {

        $tbl_name    = 'wms_store_rcv_list';
        $file        = $this->_fileExist( STORE_RCV_LIST );
        $csv_column  = array( 'tl_no', 'from_loc', 'to_loc', 'ship_date' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );

    }

    public function getStoreReceivingDetail() {

        $tbl_name    = 'wms_store_rcv_dtl';
        $file        = $this->_fileExist( STORE_RCV_DTL );
        $csv_column  = array( 'tl_no', 'sku', 'upc', 'item_desc', 'qty_req' );

        return $this->sqlconn->importToMysqlDB( $file, $tbl_name, $csv_column );

    }


    public function getProductMasterlist()
    {
        $tbl_name = 'wms_product_masterlist';
        $file = $this->_fileExist(PRODUCT_MASTERLIST);
        $csv_columns = array('sku', 'upc', 'new_upc', 'short_name', 'item_desc', 'dept_code', 'iset_code');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getSlotMasterlist()
    {
        $tbl_name = 'wms_slot_masterlist';
        $file = $this->_fileExist(SLOT_MASTERLIST);
        $csv_columns = array('slot_name','slot_zone','store_num');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getStoreMasterlist()
    {
        $tbl_name = 'wms_store_masterlist';
        $file = $this->_fileExist(STORE_MASTERLIST);
        $csv_columns = array('store_num','store_nam', 'store_type', 'store_add1', 'store_add2');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getUserRoles()
    {
        $tbl_name = 'wms_users_roles';
        $file = $this->_fileExist(USER_ROLES);
        $csv_columns = array('role_number','role_desc');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function getUserPermission()
    {
        $tbl_name = "wms_user_role_permission";
        $file = $this->_fileExist(USER_PERMISSION);
        $csv_columns = array('role_id', 'role_permission');
        return $this->sqlconn->importToMysqlDB($file, $tbl_name, $csv_columns);
    }

    public function closeTransaction()
    {
        return $this->sqlconn->close();
    }

}
