<?php
define('PURCHASE_ORDER_LIST', __DIR__ . '/../csv/po_list.csv');
define('PURCHASE_ORDER_DETAILS', __DIR__ . '/../csv/po_detail.csv');
define('PICK_LIST',       __DIR__.'/../csv/pick_list.csv');
define('PICK_DETAIL',       __DIR__.'/../csv/pick_detail.csv');
define('RTN_WH_LIST',       __DIR__.'/../csv/returnwh_list.csv');
define('RTN_WH_DTL',        __DIR__.'/../csv/returnwh_detail.csv');
define('SUBLOC_RCV_LIST',   __DIR__.'/../csv/subloc_rcv_list.csv');
define('SUBLOC_RCV_DTL',    __DIR__.'/../csv/subloc_rcv_detail.csv');
define('STORE_RCV_LIST',    __DIR__.'/../csv/store_rcv_list.csv');
define('STORE_RCV_DTL',    __DIR__.'/../csv/store_rcv_dtl.csv');
define('SUBLOC_PICK_LIST',  __DIR__.'/../csv/subloc_picklist.csv');
define('SUBLOC_PICK_DTL',   __DIR__.'/../csv/subloc_picklist_detail.csv');
define('PRODUCT_MASTERLIST',     __DIR__ .'/../csv/product_masterlist.csv');
define('SLOT_MASTERLIST',        __DIR__ .'/../csv/slot_masterlist.csv');
define('STORE_MASTERLIST',       __DIR__ .'/../csv/store_masterlist.csv');
define('USER_ROLES',             __DIR__ .'/csv/user_roles.csv');
define('USER_PERMISSION',       __DIR__.'/csv/role_permission.csv');
define('COMPONENT_TOTAL',       __DIR__.'/csv/component_total.csv');