<?php
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../db2Instance/connection.php');


class  db2Queries {


    public function __construct()
    {
        $db2_credential = db2Credentials();
        $this->db2conn  = new db2Connection( $db2_credential['HOSTNAME'],
                                             $db2_credential['DATABASE'],
                                             $db2_credential['USERNAME'],
                                             $db2_credential['PASSWORD'],
                                             $db2_credential['CONN_DRV']);

        $this->lib = $db2_credential['DATABASE'];


    }

    public function Connect()
    {
        return $this->db2conn->connect();
    }

    public function getPurchaseOrderList() {

        $lib = $this->lib;
        $file_name = 'po_list';
        $csv_header = array( 'POMRCV', 'POSHP1', 'PONUMB', 'POUNTS', 'POEDAT');

        $query = "SELECT $lib.POMRCH.POMRCV,  $lib.POMHDR.poshp1,  $lib.POMRCH.PONUMB, $lib.POMRCH.POUNTS, $lib.POMHDR.POEDAT 
                FROM $lib.POMRCH 
                LEFT JOIN $lib.POMHDR ON $lib.POMHDR.PONUMB = $lib.POMRCH.PONUMB 
                WHERE $lib.POMRCH.POSTAT = 3";


        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getPurchaseOrderDetail() {

        $lib = $this->lib;
        $file_name = 'po_detail';
        $csv_header = array( 'INUMBR', 'IUPC', 'POMRCV', 'IDEPT', 'ISDEPT', 'IMFGNO', 'POMQTY', 'DIVISION', 'DEPARTMENT', 'IDESCR', 'ISORT');

        $query = "SELECT DISTINCT $lib.POMRCD.INUMBR,
                  $lib.INVUPC.IUPC,
                  $lib.POMRCD.POMRCV,
                  $lib.INVDPT.IDEPT,
                  $lib.INVMST.ISDEPT,
                  $lib.INVMST.IMFGNO,
                  $lib.pomrcd.pomqty,
                  RTRIM((SELECT $lib.INVDPT.DPTNAM FROM $lib.INVDPT WHERE $lib.INVDPT.IDEPT = $lib.INVMST.IDEPT AND $lib.INVDPT.ISDEPT = 0 AND $lib.INVDPT.ICLAS = 0 AND $lib.INVDPT.ISCLAS = 0)) as DIVISION,
                  RTRIM((SELECT $lib.INVDPT.DPTNAM FROM $lib.INVDPT WHERE $lib.INVDPT.IDEPT = $lib.INVMST.IDEPT AND $lib.INVDPT.ISDEPT = $lib.INVMST.ISDEPT AND $lib.INVDPT.ICLAS = 0 AND $lib.INVDPT.ISCLAS = 0)) as DEPARTMENT,
                  $lib.INVMST.IDESCR,
                  $lib.INVMST.ISORT
                FROM $lib.POMRCD
                INNER JOIN $lib.INVUPC ON $lib.POMRCD.INUMBR = $lib.INVUPC.INUMBR
                INNER JOIN $lib.INVMST ON $lib.POMRCD.INUMBR = $lib.INVMST.INUMBR
                INNER JOIN $lib.INVDPT ON $lib.INVMST.IDEPT = $lib.INVDPT.IDEPT
                INNER JOIN $lib.POMRCH ON $lib.POMRCD.POMRCV = $lib.POMRCH.POMRCV
                WHERE $lib.POMRCH.POSTAT = 3 AND $lib.INVDPT.ISDEPT=0  AND $lib.INVDPT.ICLAS=0 AND $lib.INVDPT.ISCLAS=0";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);
    }

    public function getPickingList() {

        $lib = $this->lib;
        $file_name = 'pick_list';
        $csv_header = array( 'WHMVSR', 'WHMOVE', 'TRFBDT', 'TRFTLC');

        $query = "SELECT DISTINCT $lib.whsmvd.whmvsr, $lib.whsmvh.whmove, $lib.trfhdr.trfbdt, $lib.trfhdr.trftlc FROM  $lib.whsmvh
				left join $lib.whsmvd on $lib.whsmvh.whmove = $lib.whsmvd.whmove
			 	left join $lib.trfhdr on $lib.whsmvd.whmvsr = $lib.trfhdr.trfbch
				where $lib.whsmvh.whmvtp = 2  AND $lib.whsmvh.WHMVST = '1'
				";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getPickingDetail() {

        $lib = $this->lib;
        $file_name = 'pick_detail';
        $csv_header = array( 'TRFBCH', 'WHMOVE', 'TRFTLC', 'TRFBDT', 'ISORT', 'IDESCR', 'INUMBR', 'IUPC', 'WHMFSL', 'TRFROU', 'TRFREQ', 'IDEPT', 'DPTNAM', 'ISSIZE', 'ISCOLR');

        $query = "SELECT  DISTINCT $lib.whsmvd.whmvsr, $lib.whsmvd.whmove, $lib.trfhdr.TRFTLC, $lib.trfhdr.trfbdt, $lib.INVMST.ISORT, $lib.INVMST.IDESCR, $lib.whsmvd.inumbr, $lib.invupc.IUPC, $lib.whsmvd.Whmfsl, $lib.TRFDTL.TRFROU, $lib.whsmvd.WHMVQR, $lib.INVDPT.IDEPT,
                    $lib.INVDPT.DPTNAM, $lib.INVMST.ISSIZE, $lib.NPEXTPF.NPCOLR
                    from $lib.whsmvh
                    INNER join $lib.whsmvd on $lib.whsmvh.whmove  = $lib.whsmvd.whmove
                    INNER join $lib.invupc on $lib.whsmvd.inumbr  = $lib.invupc.inumbr
                    INNER JOIN $lib.INVMST ON $lib.whsmvd.inumbr  = $lib.INVMST.INUMBR
                    INNER JOIN $lib.INVDPT ON $lib.INVMST.IDEPT   = $lib.INVDPT.IDEPT
                    INNER JOIN $lib.TRFHDR ON $lib.WHSMVD.WHMVSR  = $lib.TRFHDR.TRFBCH
                    INNER JOIN $lib.TRFDTL ON $lib.TRFHDR.TRFBCH  = $lib.TRFDTL.TRFBCH
                    and $lib.whsmvd.inumbr = $lib.trfdtl.inumbr
                    INNER JOIN $lib.NPEXTPF ON $lib.whsmvd.inumbr = $lib.NPEXTPF.INUMBR
                    where $lib.whsmvh.whmvtp = 2  AND $lib.whsmvh.WHMVST = '1'
                    AND $lib.INVDPT.ISDEPT = 0
                    AND $lib.INVDPT.ICLAS = 0
                    AND $lib.INVDPT.ISCLAS = 0";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getSublocReceiving() {

        $lib = $this->lib;
        $file_name = 'subloc_rcv_list';
        $csv_header = array( 'TRFBCH', 'TRFBDT', 'TRFFLC', 'TRFTLC');

        $query = "SELECT $lib.trfhdr.trfbch, $lib.trfhdr.trfbdt, $lib.trfhdr.trfflc, $lib.trfhdr.trftlc
				FROM $lib.trfhdr
				INNER join $lib.tblstr on $lib.tblstr.strnum = $lib.trfhdr.trftlc 
				WHERE $lib.trfhdr.trfsts = 'S' and $lib.trfhdr.trftyp = 1 and $lib.tblstr.strtyp='U'";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getSublocReceivingDetail() {

        $lib = $this->lib;
        $file_name = 'subloc_rcv_detail';
        $csv_header = array( 'TRFBCH', 'INUMBR', 'IUPC', 'ISORT', 'TRFSHP');

        $query  = "SELECT $lib.trfdtl.trfbch, $lib.invmst.inumbr, $lib.INVUPC.IUPC, $lib.invmst.isort, $lib.trfdtl.trfshp
                    from $lib.TRFHDR
                    INNER join $lib.trfdtl on $lib.trfhdr.trfbch = $lib.trfdtl.trfbch
                    INNER join $lib.INVUPC ON $lib.TRFDTL.INUMBR = $lib.INVUPC.INUMBR
                    INNER join $lib.tblstr on $lib.tblstr.strnum = $lib.trfhdr.trftlc
                    INNER JOIN $lib.INVMST on $lib.trfdtl.inumbr = $lib.invmst.inumbr
                    WHERE $lib.trfhdr.trfsts = 'S' and $lib.tblstr.strtyp='U' ";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);
    }

    public function getSublocPick() {

        $lib = $this->lib;
        $file_name = 'subloc_picklist';
        $csv_header = array( 'TRFBCH', 'TRFBDT', 'TRFFLC', 'TRFTLC');

        $query = "SELECT $lib.trfhdr.trfbch, $lib.trfhdr.trfbdt, $lib.trfhdr.trfflc, $lib.trfhdr.trftlc
				from $lib.trfhdr
				left join $lib.tblstr on $lib.tblstr.strnum = $lib.trfhdr.trfflc
				where $lib.trfhdr.trfsts = 'W' and $lib.tblstr.strtyp = 'U' and $lib.trfhdr.trfbdt >= 171001";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);
    }

    public function getSublocPickDetail() {

        $lib = $this->lib;
        $file_name = 'subloc_picklist_detail';
        $csv_header = array( 'TRFBCH', 'TRFFLC','TRFTLC', 'IDEPT', 'DPTNAM', 'INUMBR', 'IUPC', 'ISORT', 'IDESCR', 'ISSIZE', 'NPCOLR', 'TRFROU', 'TRFREQ');

        $query = "SELECT $lib.TRFHDR.TRFBCH,
                        $lib.TRFHDR.TRFBDT,
                        $lib.TRFDTL.TRFFLC,
                        $lib.TRFDTL.TRFTLC,
                        $lib.INVDPT.IDEPT,
                        $lib.INVDPT.DPTNAM,
                        $lib.TRFDTL.INUMBR,
                        $lib.INVUPC.IUPC,
                        $lib.INVMST.ISORT,
                        $lib.INVMST.IDESCR,
                        $lib.INVMST.ISSIZE,
                        $lib.NPEXTPF.NPCOLR,
                        $lib.TRFDTL.TRFROU,
                        $lib.TRFDTL.TRFREQ
                        from  $lib.trfhdr
                        INNER join  $lib.trfdtl on  $lib.trfhdr.trfbch  =  $lib.trfdtl.trfbch
                        INNER join  $lib.tblstr on  $lib.trfhdr.trfflc  =  $lib.tblstr.strnum
                        INNER JOIN  $lib.INVUPC ON  $lib.TRFDTL.INUMBR  =  $lib.INVUPC.INUMBR
                        INNER JOIN  $lib.INVMST ON  $lib.TRFDTL.INUMBR  =  $lib.INVMST.INUMBR
                        INNER JOIN  $lib.NPEXTPF ON $lib.TRFDTL.INUMBR =  $lib.NPEXTPF.INUMBR
                        INNER JOIN  $lib.INVDPT ON  $lib.INVMST.IDEPT   =  $lib.INVDPT.IDEPT
                        where  $lib.TRFHDR.TRFSTS = 'W' and  $lib.TBLSTR.STRTYP = 'U'
                        AND $lib.INVDPT.ISDEPT = 0
                        AND $lib.INVDPT.ICLAS = 0
                        AND $lib.INVDPT.ISCLAS = 0
                        and $lib.trfhdr.trfbdt >= 171001";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);
    }

    public function getReturnToWH() {

        $lib = $this->lib;
        $file_name = 'returnwh_list';
        $csv_header = array( 'TRFBCH', 'TRFFLC');

        $query = "SELECT $lib.trfhdr.trfbch, $lib.trfhdr.trfflc
				from $lib.trfhdr
				where $lib.trfhdr.trfsts = 'S' and $lib.trfhdr.trftlc = 8001";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getReturnToWHDetails() {

        $lib = $this->lib;
        $file_name = 'returnwh_detail';
        $csv_header = array( 'TRFBCH', 'INUMBR', 'IUPC', 'IDESCR', 'TRFSHP');

        $query = "SELECT $lib.trfhdr.trfbch, $lib.trfdtl.inumbr, $lib.invupc.iupc, $lib.invmst.idescr, $lib.trfdtl.trfshp
				from $lib.trfhdr
				inner join $lib.trfdtl on $lib.trfhdr.trfbch = $lib.trfdtl.trfbch
				inner join $lib.invupc ON $lib.TRFDTL.INUMBR = $lib.INVUPC.INUMBR 
				inner join $lib.invmst on $lib.trfdtl.inumbr = $lib.invmst.inumbr
				where $lib.trfhdr.trfsts = 'S' and $lib.trfhdr.trftlc = 8001";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function storeReceivingList() {

        $lib = $this->lib;
        $file_name = 'store_rcv_list';
        $csv_header = array( 'TRFBCH', 'TRFFLC', 'TRFTLC', 'TRFBDT');

        $query = "SELECT $lib.trfhdr.trfbch, $lib.trfhdr.trfflc, $lib.trfhdr.trftlc, $lib.trfhdr.trfbdt from $lib.trfhdr where $lib.trfhdr.trfsts = 'S' and $lib.trfhdr.trfflc in (8001, 80011)";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function storeReceivingDetail() {

        $lib = $this->lib;
        $file_name = 'store_rcv_dtl';
        $csv_header = array( 'TRFBCH', 'INUMBR', 'IUPC', 'IDESCR', 'TRFREQ');

        $query = "select $lib.trfdtl.trfbch, $lib.trfdtl.inumbr, $lib.invupc.iupc, $lib.invmst.idescr, $lib.trfdtl.trfshp from $lib.trfdtl
                  inner join $lib.trfhdr on $lib.trfdtl.trfbch = $lib.trfhdr.trfbch
                  inner join $lib.invupc on $lib.trfdtl.inumbr = $lib.invupc.inumbr
                  inner join $lib.invmst on $lib.trfdtl.inumbr = $lib.invmst.inumbr
                  where $lib.trfhdr.trfsts = 'S' and $lib.trfhdr.trfflc in (8001, 80011) and $lib.trfdtl.trfshp <> 0";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }



    public function getProductMasterlist() {

        $lib = $this->lib;
        $file_name = 'product_masterlist';
        $csv_header = array( 'INUMBR', 'IUPC', 'IUPC#', 'ISORT', 'IDESCR', 'IDEPT', 'ISET');

        $query = "SELECT  DISTINCT  $lib.INVMST.INUMBR, 
                          $lib.INVUPC.IUPC,
                          $lib.INVMST.IVNDP#, 
                          $lib.INVMST.ISORT,
                          TRANSLATE(
                            $lib.INVMST.IDESCR,
                            ' ',
                            '£ú') as IDESCR,
                          $lib.INVMST.ISET,
                          $lib.INVMST.IDEPT
				FROM $lib.INVMST
				INNER JOIN $lib.INVUPC ON $lib.INVMST.INUMBR = $lib.INVUPC.INUMBR";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getSLotMasterlist() {

        $lib = $this->lib;
        $file_name = 'slot_masterlist';
        $csv_header = array( 'WHSLOT', 'WHZONE', 'STRNUM');

        $query = "SELECT $lib.WHSLOC.WHSLOT, $lib.WHSLOC.WHZONE, $lib.WHSLOC.STRNUM FROM $lib.WHSLOC";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }

    public function getStoreMasterlist() {

        $lib = $this->lib;
        $file_name = 'store_masterlist';
        $csv_header = array( 'STRNUM', 'STRNAM', 'STRTYP', 'STADD1', 'STADD2');

        $query = "SELECT $lib.TBLSTR.STRNUM, $lib.TBLSTR.STRNAM, $lib.TBLSTR.STRTYP, $lib.TBLSTR.STADD1, $lib.TBLSTR.STADD2 FROM $lib.TBLSTR";

        return $this->db2conn->exportToCSV( $file_name, $query, $csv_header);

    }




}