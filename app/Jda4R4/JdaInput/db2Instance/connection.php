<?php
require_once __DIR__.'/../config/config.php';

class db2Connection {

    private $hostname;
    private $database;
    private $username;
    private $password;
    private $conn_drv;


    public function __construct( $hostname, $database, $username, $password, $conn_drv )
    {
         $this->hostname = $hostname;
         $this->database = $database;
         $this->username = $username;
         $this->password = $password;
         $this->conn_drv = $conn_drv;
    }


    public function connect() {

       try {

           $connString = "odbc:DRIVER={$this->conn_drv}; ".
                         "SYSTEM={$this->hostname}; ".
                         "DATABASE={$this->database}; ".
                         "UID={$this->username}; ".
                         "PWD={$this->password};";

           echo "Connecting to DB2\n";
           echo "SYSTEM : {$this->hostname}\t";
           echo "LIBRARY : {$this->database}\n";
           echo "UID : {$this->username}\t";
           echo "PWD : ".str_repeat('*', strlen($this->password) + rand(1,20)). "\n";

           $this->dbh = new PDO($connString,"","");
           $this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           echo "\nConnected Successfully! \n ";


       } catch ( PDOException $e ) {

           echo "\nCan't Connect \n {$e->getMessage()} \n";
           exit();

       }
    }

    public function exportToCSV($file, $query, $csv_header)
    {
        # For Receiving | Purchase Orders
        echo "Exporting file=$file ...";
        $start_time = microtime(true);
        $statement = $this->dbh->prepare($query);
        $statement->execute();
        $result  = $statement->fetchAll(PDO::FETCH_ASSOC);

        $fp = fopen(dirname(__DIR__).'/csv/'.$file.'.csv', 'w');
        fputcsv($fp, $csv_header);


        foreach ($result as $fields) {
            $fields = array_map('trim', $fields);

            fputcsv($fp, $fields);
        }
        $end_time = microtime(true);
        $time_diff = $end_time - $start_time;
        $time_diff = self::timeElapsed($time_diff);
        fclose($fp);
        echo "\nExporting finished! file=($file) timeElapsed=$time_diff \n";
    }

    public function timeElapsed($s) {
        $h = floor($s / 3600);
        $s -= $h * 3600;
        $m = floor($s / 60);
        $s -= $m * 60;
        //return $h.':'.sprintf('%02d', $m).':'.sprintf('%02d', $s.(($s >1) ? 'seconds' : 'second'));
        return $h.($h >1 ? ' hours' : ' hour') .", " .$m.($m >1 ?' minutes' : ' minute') .', '.number_format($s, 2).($s>1 ? ' seconds':' second');
    }

    public function close() {
        echo 'Closing DB2 Connection.....'."\n";
        echo str_repeat('-', 100)."\n";
        $this->dbh = null;
    }

}
