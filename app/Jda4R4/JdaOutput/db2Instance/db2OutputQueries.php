<?php
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../db2Instance/connection.php');

class  db2OutputQueries {


    public function __construct()
    {
        $db2_credential = db2Credentials();
        $this->db2conn  = new db2Connection( $db2_credential['HOSTNAME'],
                                             $db2_credential['DATABASE'],
                                             $db2_credential['USERNAME'],
                                             $db2_credential['PASSWORD'],
                                             $db2_credential['CONN_DRV']);

        $this->lib = $db2_credential['DATABASE'];


    }

    public function Connect()
    {
        return $this->db2conn->connect();
    }

    /**
     * PO RECEIVING CONTROLLER
     * @param $receiver_no
     * @param $sku
     * @param $qty
     * @return mixed
     */

    public function JDAUpdatePOQty( $receiver_no, $sku, $qty ) {

        $lib = $this->lib;

        $query = "UPDATE $lib.POMRCD 
                  SET $lib.POMRCD.POMCUR = $qty
                  WHERE $lib.POMRCD.POMRCV = $receiver_no
                  AND $lib.POMRCD.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $query );

    }

    public function getPoStatus( $receiver ) {

        $query = "SELECT $this->lib.POMRCH.PONUMB, $this->lib.POMRCH.POSTAT FROM $this->lib.POMRCH WHERE $this->lib.POMRCH.POMRCV = $receiver FETCH FIRST ROW ONLY";

        return $this->db2conn->runDb2FetchFirst( $query );

    }

    /**
     * @param $tl_no integer
     * @param $sku integer
     * @param $qty integer
     */
    public function updateTransferQtyToAlloc( $tl_no, $sku, $qty ) {

        $lib = $this->lib;

        $query = "UPDATE $lib.TRFDTL 
                  SET $lib.TRFDTL.TRFALC = $qty
                  WHERE $lib.TRFDTL.TRFBCH = '$tl_no'
                  AND $lib.TRFDTL.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $query );
    }

    public function updateTransferShipQty( $tl_no, $sku, $qty_rcv ) {

        $lib = $this->lib;

        $query = "UPDATE $lib.TRFDTL 
                  SET $lib.TRFDTL.TRFSHP = $qty_rcv
                  WHERE $lib.TRFDTL.TRFBCH = '$tl_no'
                  AND $lib.TRFDTL.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $query );

    }

    /**
     * Get POs to RA
     *
     */
    public function getRAPOs() {

        $lib = $this->lib;

        $_getPos = "SELECT $lib.POMHDR.PONUMB
                  FROM $lib.POMHDR 
                  WHERE $lib.POMHDR.PONUMB 
                  NOT IN(SELECT $lib.POMRCH.PONUMB FROM $lib.POMRCH) 
                  AND $lib.POMHDR.POSTAT = '3'";

        return $this->db2conn->runDb2QueryFetch( $_getPos );

    }

    public function checkPickStatus( $mts_no ) {

        $lib = $this->lib;

        $getPick = "SELECT $lib.WHSMVX.WHMVST FROM $lib.WHSMVX WHERE $lib.WHSMVX.WHMVSR = $mts_no";

        return $this->db2conn->runDb2FetchFirst( $getPick );

    }

    public function checkShipStatus( $mts_no ) {

        $lib = $this->lib;

        $checkShip = "SELECT $lib.trfhdr.trfsts from $lib.trfhdr where $lib.trfhdr.trfbch = '$mts_no'";

        return $this->db2conn->runDb2FetchFirst( $checkShip );

    }

    public function checkSublocRcvStatus( $mts_no ) {

        $lib = $this->lib;

        $checkShip = "SELECT $lib.trfhdr.trfsts from $lib.trfhdr where $lib.trfhdr.trfbch = '$mts_no'";

        return $this->db2conn->runDb2FetchFirst( $checkShip );

    }

    public function checkJdaAllocStatus( $mts_no ) {

        $lib = $this->lib;

        $checkShip = "SELECT $lib.trfhdr.trfsts from $lib.trfhdr where $lib.trfhdr.trfbch = '$mts_no'";

        return $this->db2conn->runDb2FetchFirst( $checkShip );

    }

    public function checkJdaPickStatus( $mts_no ) {

        $lib = $this->lib;

        $checkShip = "SELECT $lib.trfhdr.trfsts from $lib.trfhdr where $lib.trfhdr.trfbch = '$mts_no'";

        return $this->db2conn->runDb2FetchFirst( $checkShip );

    }

    public function checkJdaReceiveStatus( $mts_no ) {

        $lib = $this->lib;

        $checkShip = "SELECT $lib.trfhdr.trfsts from $lib.trfhdr where $lib.trfhdr.trfbch = '$mts_no'";

        return $this->db2conn->runDb2FetchFirst( $checkShip );

    }




    /**
     *
     * Picking Controller
     *
     **/


    /**
     * @param $tl_no
     * @param $sku
     * @param $qty
     */

    public function mvdUpdate( $tl_no, $doc_no, $sku, $qty ) {

        $lib = $this->lib;

        $query = "UPDATE $lib.WHSMVD 
                  SET $lib.WHSMVD.WHMVQM = $qty
                  WHERE $lib.WHSMVD.WHMVSR = '$tl_no'
                  AND $lib.WHSMVD.WHMOVE = $doc_no
                  AND $lib.WHSMVD.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $query );

    }

    public function trfUpdate( $tl_no, $sku, $qty) {

        $lib = $this->lib;

        $_query = "UPDATE $lib.TRFDTL
                   SET $lib.TRFDTL.TRFREC = $qty
                   WHERE $lib.TRFDTL.TRFBCH = '$tl_no'
                   AND $lib.TRFDTL.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $_query );

    }

    public function updateSublocRcvQty( $mts, $sku, $qty ) {

        $lib = $this->lib;

        $_query = "UPDATE $lib.TRFDTL
                   SET $lib.TRFDTL.TRFREC = $qty
                   WHERE $lib.TRFDTL.TRFBCH = '$mts'
                   AND $lib.TRFDTL.INUMBR = $sku";

        return $this->db2conn->runDb2Query( $_query );

    }

    public function Closed() {

        return $this->db2conn->close();
    }

}