<?php
require_once __DIR__.'/../config/config.php';

class db2Connection {

    private $hostname;
    private $database;
    private $username;
    private $password;
    private $conn_drv;


    public function __construct( $hostname, $database, $username, $password, $conn_drv )
    {
         $this->hostname = $hostname;
         $this->database = $database;
         $this->username = $username;
         $this->password = $password;
         $this->conn_drv = $conn_drv;
    }


    public function connect() {

       try {

           $connString = "odbc:DRIVER={$this->conn_drv}; ".
                         "SYSTEM={$this->hostname}; ".
                         "DATABASE={$this->database}; ".
                         "UID={$this->username}; ".
                         "PWD={$this->password};";

           echo "Connecting to DB2\n";
           echo "SYSTEM : {$this->hostname}\t";
           echo "LIBRARY : {$this->database}\n";
           echo "UID : {$this->username}\t";
           echo "PWD : ".str_repeat('*', strlen($this->password) + rand(1,20)). "\n";

           $this->dbh = new PDO($connString,"","");

           echo "\nConnected Successfully! \n ";


       } catch ( PDOException $e ) {

           echo "\nCan't Connect \n {$e->getMessage()} \n";
           exit();

       }
    }

    public function runDb2Query( $query ) {
        /**
         * @param $query = statement/query
         **/
        try{

            $statement = $this->dbh->prepare($query);

            $statement->execute();

            $result  = $statement->fetchAll(PDO::FETCH_ASSOC);

            echo $statement->rowCount() ." row affected.Updated Successfully! \n";

            return $result;

        }catch (Exception $e ) {

            echo $e;

        }

    }

    public function runDb2QueryFetch( $query ) {

        $statement = $this->dbh->prepare( $query );
        $statement->execute();
        $result  = $statement->fetchAll(PDO::FETCH_ASSOC);
        return $result;
    }


    public function runDb2FetchFirst( $query ) {
        $statement = $this->dbh->prepare($query);
        $statement->execute();
        $result  = $statement->fetch(PDO::FETCH_ASSOC);
        return $result;
    }

    public function runDb2QueryFirst($query)
    {
        $statement = $this->dbh->prepare($query);
        $statement->execute();
        $result  = $statement->fetch();
        return $result;
    }

    public function close() {
        echo 'Closing DB2 Connection.....'."\n";
        echo str_repeat('-', 100)."\n";
        $this->dbh = null;
    }





}
