<?php
require_once(__DIR__.'/../mysqlInstance/PickingFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/Picking.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    echo "date/time :".date("Y-m-d H:i:s")."\n";

    $sql_connect = new PickingFunctions();
    $sql_connect->Connect();

    $db2_queries = new db2OutputQueries();
    $db2_queries->Connect();


    $_getPickToProcess = $sql_connect->getPickList();



    foreach ( $_getPickToProcess as $pickToProcess ) {

        $_getPickToUpdate = $sql_connect->getPickQty( $pickToProcess['tl_no'] );

        foreach ($_getPickToUpdate as $item) {

            //update the quantity
            echo 'Updating moved qty; Transfer no =' . $item['tl_no'] . ', sku =' . $item['sku'] . ', quantity received ' . $item['qty_rcv'] . "\n";
            $db2_queries->mvdUpdate($item['tl_no'], $item['doc_no'], $item['sku'], $item['qty_rcv']);

        }

        $checkStatus = $db2_queries->checkPickStatus( $pickToProcess['tl_no']);

        $sql_connect->updateJdaPickStatus( $pickToProcess['tl_no'], $checkStatus['WHMVST'] );

        $jda_connect = new Picking();
        $jda_connect->Login();
        $jda_connect->Initiate();
        $tl = $jda_connect->getTL($pickToProcess['doc_no']);

        //Check if document number has an error

        if (!empty ($tl)) {

            $data = array();
            $data['module'] = 'Picking';
            $data['ref_num'] = $pickToProcess['tl_no'];
            $data['ref_desc'] = $tl;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);
            continue;
        }

        // check if document number has been successful updated in JDA

        $_checkIfSuccess = $jda_connect->closePick($pickToProcess['doc_no']);

        if (!empty ($_checkIfSuccess)) {

            $data = array();
            $data['module'] = 'Picking';
            $data['ref_num'] = $pickToProcess['tl_no'];
            $data['ref_desc'] = $_checkIfSuccess;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);

            // if error is like this then dont update is_sync column
            if ($_checkIfSuccess == "No Documents Accepted, Job will not be Submitted") {
                continue;
            }
        }

        $sql_connect->updateIsSynced($pickToProcess['doc_no'], $pickToProcess['tl_no']);

        $checkStatus = $db2_queries->checkPickStatus( $pickToProcess['tl_no']);

        $sql_connect->updateJdaPickStatus( $pickToProcess['tl_no'], $checkStatus['WHMVST'] );

        sleep(3);
    }
    cronHelper::unlock();

}