<?php
require_once(__DIR__.'/../mysqlInstance/PurchaseOrderFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/POReceiving.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $sql_connect = new PurchaseOrderFunctions();
    $sql_connect->Connect();



    $db2_queries = new db2OutputQueries();
    $db2_queries->Connect();


    $getPOqty = $sql_connect->getPOQty();
    $getPOlist = $sql_connect->getPurchaseOrderList();
    $getNotInPo = $sql_connect;


    /**
     *
     * Update PO Quantity
     *
     */
    foreach ($getPOqty as $qty) {

        echo 'Updating receiver_no ' . $qty['rcv_no'] . ', sku ' . $qty['sku'] . ', quantity received ' . $qty['qty_rcv'] . "\n";
        $db2_queries->JDAUpdatePOQty($qty['rcv_no'], $qty['sku'], $qty['qty_rcv']);
        echo 'Updated Successfully!' . "\n";

    }


    foreach ($getPOlist as $po) {

        $checkPoStat = $db2_queries->getPoStatus( $po['rcv_no'] );
        $updateStatus = $sql_connect->updateJdaStatus( $po['rcv_no'], $checkPoStat['POSTAT'] );


        $db2_connect = new POReceiving();
        $db2_connect->Login();
        $db2_connect->Initiate();
        // enter Receiver No.
        $rcv_no = $db2_connect->enterReceiverNo($po['rcv_no']);
        if ( !empty( $rcv_no )) {

            $data = array();
            $data['module'] = 'Receiving PO';
            $data['ref_num'] = $po['rcv_no'];
            $data['ref_desc'] = $rcv_no;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);
            continue;
        }
        $db2_connect->enterTransferNo($po['inv_no']);

        $getNotInPo = $sql_connect->getItemNotInPo( $po['rcv_no'] );


        if ( count( $getNotInPo ) ) {
            $enterSlot = $db2_connect->enterSlot($po['po_status'], $getNotInPo);
        }else {
            $enterSlot = $db2_connect->enterSlot($po['po_status'], '');
        }


        if (!empty($enterSlot)) {

            $data = array();
            $data['module'] = 'Receiving PO';
            $data['ref_num'] = $po['rcv_no'];
            $data['ref_desc'] = $enterSlot;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);

        }

        /**
         * Update is sync status to mysql db
         */

        $sql_connect->updatePOStatus($po['po_no'], $po['rcv_no']);


        $sql_connect->updatePOdetails($po['rcv_no']);

        $checkPoStat = $db2_queries->getPoStatus( $po['rcv_no'] );
        $updateStatus = $sql_connect->updateJdaStatus( $po['rcv_no'], $checkPoStat['POSTAT'] );


    }

    cronHelper::unlock();
}







