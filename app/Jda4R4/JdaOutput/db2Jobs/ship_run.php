<?php
require_once(__DIR__.'/../mysqlInstance/ShippingFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/Shipping.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');

if(($pid = cronHelper::lock()) !== FALSE) {


    $sql_connect = new ShippingFunctions();
    $sql_connect->Connect();

    $db2_queries = new db2OutputQueries();
    $db2_queries->Connect();

    $_getShipToUpdate = $sql_connect->getShipToProcess();

    foreach ($_getShipToUpdate as $ship) {

        $checkJdaStatus = $db2_queries->checkShipStatus( $ship['tl_no'] );

        $sql_connect->updateJdaShip( $ship['tl_no'], $checkJdaStatus['TRFSTS'] );

        $jda_connect = new Shipping();
        $jda_connect->Login();

        $jda_connect->Initiate();

        $_checkResult = $jda_connect->doShip($ship['tl_no']);

        if (!empty ($_checkResult)) {

            $data = array();
            $data['module'] = 'Shipping';
            $data['ref_num'] = $ship['tl_no'];
            $data['ref_desc'] = $_checkResult;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);

            // if error is like this then dont update is_sync column
            if ($_checkResult != "Has been placed on the JOBQ") {
                continue;
            }

            /**
             *
             * WMS_PICK_LIST COLUMN IS_JDA_SYNC
             * 1 = SUCCESSFULLY PICKED
             * 2 = SUCCESSFULLY SHIPPED
             *
             *
             **/

            $sql_connect->updateIsSynced($ship['tl_no']);

            $checkJdaStatus = $db2_queries->checkShipStatus( $ship['tl_no'] );

            sleep( 3 );

            $sql_connect->updateJdaShip( $ship['tl_no'], $checkJdaStatus['TRFSTS'] );

        }
    }
    cronHelper::unlock();
}