<?php
require_once(__DIR__.'/../mysqlInstance/SublocPickingFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/SublocPick.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $sql_connect = new SublocPickingFunctions();
    $sql_connect->Connect();


    $checkTLtoPick = $sql_connect->getTransferToPick();

    $db2Connect = new db2OutputQueries();
    $db2Connect->Connect();

    foreach ( $checkTLtoPick as $tl ) {

        $checkJdaStatus = $db2Connect->checkJdaPickStatus( $tl['tl_no'] );

        $sql_connect->updatePickStatus( $tl['tl_no'], $checkJdaStatus['TRFSTS'] );

        $jda_connect = new SublocPick();
        $jda_connect->Login();

        $jda_connect->Initiate();

        $jda_connect->doPick( $tl['from_loc'], $tl['tl_no'] );


        $sql_connect->updateIsSyncedAlloc( $tl['tl_no'] );

        $checkJdaStatus = $db2Connect->checkJdaAllocStatus( $tl['tl_no'] );

        sleep( 3 );

        $sql_connect->updatePickStatus( $tl['tl_no'], $checkJdaStatus['TRFSTS'] );
    }




}