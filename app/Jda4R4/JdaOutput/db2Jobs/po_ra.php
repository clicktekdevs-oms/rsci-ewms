<?php
require_once(__DIR__.'/../mysqlInstance/POReceivingAuthFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/POReceivingRA.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $db2_queries = new db2OutputQueries();
    $db2_queries->Connect();

    $sql_connect = new POReceivingAuthFunctions();
    $sql_connect->Connect();


    $poToRa = $db2_queries->getRAPOs();


    foreach ($poToRa as $pos) {

        $db2_connect = new POReceivingRA();
        $db2_connect->Login();

        $db2_connect->Initiate();
        $enterPo = $db2_connect->enterPo($pos['PONUMB']);

        if (!empty($enterPo)) {

            $data = array();
            $data['module'] = 'PO Receiving Authorization';
            $data['ref_num'] = $pos['PONUMB'];
            $data['ref_desc'] = $enterPo;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);

        }
    }
    cronHelper::unlock();
}