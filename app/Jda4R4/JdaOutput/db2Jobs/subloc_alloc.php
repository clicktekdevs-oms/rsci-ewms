<?php
require_once(__DIR__.'/../mysqlInstance/SublocPickingFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/SublocAlloc.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $sql_connect = new SublocPickingFunctions();
    $sql_connect->Connect();


    $checkTLtoAlloc = $sql_connect->getTransferToAllocate();

    $db2Connect = new db2OutputQueries();
    $db2Connect->Connect();

    foreach ( $checkTLtoAlloc as $tl ) {

        $getQuantity = $sql_connect->getTransferQuantity( $tl['tl_no'] );

        foreach ( $getQuantity as $quantity ) {

            $db2Connect->updateTransferQtyToAlloc( $quantity['tl_no'], $quantity['sku'], $quantity['qty_req'] );
            $db2Connect->updateTransferShipQty( $quantity['tl_no'], $quantity['sku'], $quantity['qty_rcv'] );

        }


        $checkJdaStatus = $db2Connect->checkJdaAllocStatus( $tl['tl_no'] );

        $sql_connect->updateAllocateStatus( $tl['tl_no'], $checkJdaStatus['TRFSTS'] );

        $jda_connect = new SublocAlloc();
        $jda_connect->Login();

        $jda_connect->Initiate();

        $jda_connect->doAlloc( $tl['tl_no'] );

        $sql_connect->updateIsSyncedAlloc( $tl['tl_no'] );

        $checkJdaStatus = $db2Connect->checkJdaAllocStatus( $tl['tl_no'] );

        sleep( 3 );

        $sql_connect->updateAllocateStatus( $tl['tl_no'], $checkJdaStatus['TRFSTS'] );
    }




}