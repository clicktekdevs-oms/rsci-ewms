<?php
require_once(__DIR__.'/../mysqlInstance/SublocReceivingFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/SublocReceiving.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');
require_once(__DIR__.'/../db2Keystrokes/StoreReceiving.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $sql_connect = new SublocReceivingFunctions();
    $sql_connect->Connect();

    $db2Connect = new db2OutputQueries();
    $db2Connect->Connect();


    $checkPending = $sql_connect->getPendingStoreReceive();

    foreach ( $checkPending as $pending ) {

        $tlToProcess = $sql_connect->getTLtoProcess( $pending['ref_no'] );

        foreach ( $tlToProcess as $toProcess ) {


            //check JDA Status
            $checkTransferSts = $db2Connect->checkJdaPickStatus( $toProcess['tl_no'] );
            $sql_connect->updateJdaRcvStatus( $toProcess['tl_no'],  $checkTransferSts['TRFSTS'] );


            $getUpdateTLQty = $sql_connect->getUpdateTLQuantity( $toProcess['tl_no'] );

            // update jda table quantity

            foreach ( $getUpdateTLQty as $item ) {

                echo "Update Transfer no.=".$item['tl_no'].";sku=".$item['sku'].";qty_rec=".$item['qty_rec']."\n";
                $db2Connect->trfUpdate( $item['tl_no'], $item['sku'], $item['qty_rec'] );
                echo "Updated successfully! \n";
            }

            //DO screenscrape

            $jda_connect = new StoreReceiving();
            $jda_connect->Login( $pending['username'], base64_decode($pending['password']) );
            $jda_connect->initiate();
            $jda_connect->enterMts( $toProcess['tl_no'] );

            $getAddedSku = $sql_connect->getAddedUpc( $toProcess['tl_no'] );

            if ( count( $getAddedSku ) ) {

                foreach ( $getAddedSku as $sku ) {

                    echo 'add items in transfer...';
                    $jda_connect->addItems( $sku['sku'], $sku['qty_rec'] );

                }
            }

            $jda_connect->closeProcess();

            //check JDA transfer Status
            $checkTransferSts = $db2Connect->checkJdaPickStatus( $toProcess['tl_no'] );
            $sql_connect->updateJdaRcvStatus( $toProcess['tl_no'],  $checkTransferSts['TRFSTS'] );

            //update is_jda_sync to 1
            $sql_connect->updateIsSyncSts( $toProcess['tl_no'] );

        }
    }

    cronHelper::unlock();
}