<?php
require_once(__DIR__.'/../db2Keystrokes/StoreReceivingLogin.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');
require_once(__DIR__.'/../mysqlInstance/LoginFunctions.php');
if(($pid = cronHelper::lock()) !== FALSE) {

    echo "date/time :" . date("Y-m-d H:i:s") . "\n";

    $jda_username =  $argv[1] == "" ? "" : $argv[1];
    $jda_password  =  $argv[2] == "" ? "" : $argv[2];
    $refnum = $argv[1] == "" ? "" : $argv[3];

    $sql_connect = new StoreLogin();
    $sql_connect->Connect();

    $db2_connect  = new StoreReceivingLogin();
    $checkStatus = $db2_connect->Login( $jda_username, $jda_password );

    if ( $checkStatus == "User $jda_username does not exist." ) {

        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "User $jda_username does not exist", 5);

    }
    elseif ($checkStatus == "Password not correct for user profile.")
    {
        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "Password not correct for user profile.", 4);

    }elseif ($checkStatus  == "Sign-on information required.")
    {
        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum,  "Sign-on information required.", 3);

    }elseif ($checkStatus == "User profile $jda_username cannot sign on.")
    {
        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "User profile $jda_username cannot sign on.", 2);

    }elseif ($checkStatus == "Next not valid sign-on disables user profile.")
    {
        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "Next not valid sign-on disables user profile.", 1);
    }
    elseif ($checkStatus == "Merchandise Management System")
    {
        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "Merchandise Management System", 0);
    }else {

        $sql_connect->insertToLogs( $jda_username, base64_encode($jda_password), password_hash($jda_password,PASSWORD_DEFAULT), $refnum, "Request Timeout.Please check connection", 6);

    }

    cronHelper::unlock();

}