<?php
require_once(__DIR__.'/../mysqlInstance/ReturnToWHFunctions.php');
require_once(__DIR__.'/../db2Keystrokes/ReturnWh.php');
require_once(__DIR__ . '/../db2Instance/db2OutputQueries.php');
require_once(__DIR__.'/../config/config.php');
require_once(__DIR__.'/../cron_helper/cronhelper.php');

if(($pid = cronHelper::lock()) !== FALSE) {

    $sql_connect = new ReturnToWHFunctions();
    $sql_connect->Connect();


    $_getMtsToProcess = $sql_connect->getMtsToProcess();
    $db2_queries = new db2OutputQueries();
    $db2_queries->Connect();


    foreach ($_getMtsToProcess as $toProcess) {

        //update the quantity to jda
//            echo $toProcess['mts_no'];
        $_getQty = $sql_connect->getMtsQuantity($toProcess['mts_no']);



        foreach ($_getQty as $item) {

            echo "Updating Transfer detail; Transfer no= " . $item['mts_no'] . " SKU= " . $item['sku'] . " Qty Received= " . $item['qty_rcv'] . "\n";
            $db2_queries->trfUpdate($item['mts_no'], $item['sku'], $item['qty_rcv']);
            echo "Successfully Updated!" . "\n";

        }

        $checkRcvSts = $db2_queries->checkJdaReceiveStatus( $toProcess['mts_no'] );
        $sql_connect->updateJdaRcvStatus( $toProcess['mts_no'], $checkRcvSts['TRFSTS']);

        $jda_connect = new ReturnWh();
        $jda_connect->Login();
        $jda_connect->initiate();

        $checkMts = $jda_connect->enterMts($toProcess['mts_no']);
        $checkUnlisted = $sql_connect->getUnlistedSkus( $toProcess['mts_no'] );

        if (!empty ($checkMts)) {

            $data = array();
            $data['module'] = 'Return to Warehouse';
            $data['ref_num'] = $toProcess['mts_no'];
            $data['ref_desc'] = $checkMts;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);
            continue;
        }

        if ( count( $checkUnlisted ) ) {

            foreach ( $checkUnlisted as $unlisted ) {

                $jda_connect->addItems( $unlisted['sku'], $unlisted['qty_rcv'] );

            }

        }

        $_checkClose = $jda_connect->closeProcess();

        if (!empty ($_checkClose)) {

            $data = array();
            $data['module'] = 'Return to Warehouse';
            $data['ref_num'] = $toProcess['mts_no'];
            $data['ref_desc'] = $_checkClose;
            $data['created_at'] = date("Y-m-d H:i:s");
            $data['updated_at'] = date("Y-m-d H:i:s");

            // insert logs
            $sql_connect->insertLogs($data);

            // if error is like this then dont update is_sync column
            if ($_checkClose != "Has been placed on the JOBQ") {
                echo 'continue';
                continue;
            }
        }

        $checkRcvSts = $db2_queries->checkJdaReceiveStatus( $toProcess['mts_no'] );
        $sql_connect->updateJdaRcvStatus( $toProcess['mts_no'], $checkRcvSts['TRFSTS']);
        $sql_connect->updateIsSynced($toProcess['mts_no']);

    }
    cronHelper::unlock();
}
