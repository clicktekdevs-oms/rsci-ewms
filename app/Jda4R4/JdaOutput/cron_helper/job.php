<?php

require 'cronhelper.php';

if(($pid = cronHelper::lock()) !== FALSE) {

    /*
     * Cron job code goes here
    */
    sleep(10); // Cron job code for demonstration

    cronHelper::unlock();
}
