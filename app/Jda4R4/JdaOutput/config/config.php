<?php

function db2Credentials() {

    $mode = 'TEST';

    switch ($mode) {

        case 'TEST' :

            return array(

                'HOSTNAME' => 'jdaprod2.rgoc.com.ph',
                'DATABASE' => 'MMRSTLIB',
                'USERNAME' => 'WMSSYS',
                'PASSWORD' => 'PASSWORD',
                'CONN_DRV' => 'IBM i Access ODBC Driver 64-bit'
            );

        case 'PROD' :

            return array(

                'HOSTNAME' => '',
                'DATABASE' => '',
                'USERNAME' => '',
                'PASSWORD' => '',
                'CONN_DRV'   => ''
            );

    }

}


function mysqlCredentials() {

    return array(

        'HOSTNAME' => 'localhost',
        'DATABASE' => 'debs_ewms',
        'USERNAME' => 'root',
        'PASSWORD' => 'mysql'
    );
}