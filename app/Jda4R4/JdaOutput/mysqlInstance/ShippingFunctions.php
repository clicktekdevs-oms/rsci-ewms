<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class ShippingFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getShipToProcess() {

        $_getList = "SELECT DISTINCT a.tl_no, a.doc_no ,a.ship_jda_status
                    from wms_pick_list a
                    inner join wms_load_detail b on a.tl_no = b.tl_no
                    inner join wms_load_list c on b.load_code = c.load_code
                     where a.pick_status = 0 and a.pick_jda_status = 2
                     and c.load_status = 0
                     and NOT (a.ship_jda_status <=> 'S')";

        return $this->sqlconn->runQuery( $_getList );

    }

    public function updateIsSynced( $tl_no ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_pick_list
                   SET is_jda_sync = 2, updated_at = '$now'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function updateJdaShip( $tl_no, $status ) {

        $_query = "UPDATE wms_pick_list
                   SET ship_jda_status = '$status'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );

    }


    public function insertLogs( $data = array() ) {

        $keys = implode(',', array_keys($data));
        $values = array_values($data);

        $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );

    }

}