<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class PurchaseOrderFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getPurchaseOrderList() {

        $_getPOList = "SELECT * FROM wms_po_list where po_status in(1,0) and is_jda_sync = 0 or jda_status !=6 order by created_at desc";

        return $this->sqlconn->runQuery($_getPOList);

    }

    public function getItemNotInPo( $rcv_no ) {

        $getItems = "SELECT sku, qty_rcv from wms_po_detail where qty_ord = 0 and rcv_no = $rcv_no and sku != 0 and qty_rcv != 0";

        return $this->sqlconn->runQuery($getItems);

    }

    public function getPOQty() {

        $_getPOQty = "SELECT a.rcv_no, a.sku, a.upc, a.qty_rcv FROM wms_po_detail a
                      inner join wms_po_list b 
                      on a.rcv_no = b.rcv_no
                      where b.po_status in( 1, 0 ) and b.is_jda_sync = 0 and a.qty_ord != 0";

        return $this->sqlconn->runQuery($_getPOQty);

    }

    public function updatePOStatus( $po_no, $receiver_no ) {

        $now = date("Y-m-d H:i:s");
        $_query = "UPDATE wms_po_list
                   SET is_jda_sync = 1, updated_at = '$now'
                   WHERE po_no = $po_no
                   AND rcv_no = $receiver_no";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function updateJdaStatus( $receiver, $status ) {

        $_query = "UPDATE wms_po_list
                   SET jda_status = $status
                   WHERE rcv_no = $receiver";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function updatePOdetails( $receiver_no ) {

        $now = date("Y-m-d H:i:s");
        $_query = "UPDATE wms_po_detail
                   SET is_jda_sync = 1, updated_at = '$now'
                   WHERE rcv_no = $receiver_no";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function insertLogs( $data = array() ) {

       $keys = implode(',', array_keys($data));
       $values = array_values($data);

       $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );
    }
}