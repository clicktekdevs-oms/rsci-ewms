<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class SublocReceivingFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getMtstoProcess() {

        $_getList = "SELECT DISTINCT tl_no from wms_subloc_rcv_list where tl_status = 0 and is_jda_sync = 0";

        return $this->sqlconn->runQuery( $_getList );

    }

    public function getPickQty() {

        $_getPickDtl = "SELECT DISTINCT a.tl_no, b.sku, b.qty_rcv from wms_subloc_rcv_list a 
                        INNER JOIN wms_subloc_rcv_dtl b
                        ON a.tl_no = b.tl_no
                        where a.tl_status = 0 and a.is_jda_sync = 0 and b.qty_rcv <> 0";

        return $this->sqlconn->runQuery( $_getPickDtl );

    }

    public function updateIsSynced( $mts ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_subloc_rcv_list
                   SET is_jda_sync = 1, updated_at = '$now'
                   WHERE tl_no = $mts";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function updateJdaShip( $mts, $status ) {

        $_query = "UPDATE wms_subloc_rcv_list
                   SET jda_status = '$status'
                   WHERE tl_no = $mts";

        return $this->sqlconn->updateQuery( $_query );

    }


    public function insertLogs( $data = array() ) {

        $keys = implode(',', array_keys($data));
        $values = array_values($data);

        $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function getPendingStoreReceive()
    {

        $_getList = "SELECT ref_no, username, password, hash_password, is_status from wms_jda_login_logs where is_status = 0 and ref_no LIKE 'SL%' ";

        return $this->sqlconn->runQuery($_getList);
    }

    public function getTLtoProcess( $pell_no ) {

        $_getList = "SELECT DISTINCT a.tl_no from wms_subloc_load_detail a 
                     inner join wms_store_rcv_list b on a.tl_no = b.tl_no 
                     where a.load_code = '$pell_no' and b.is_jda_sync = 0 and NOT (b.jda_status <=> 'C')";

        return $this->sqlconn->runQuery( $_getList );
    }

    public function updateJdaRcvStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_store_rcv_list
                   SET jda_status = '$status'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function getUpdateTLQuantity( $tl ) {

        $_getList = "SELECT tl_no,sku, qty_rec from wms_store_rcv_dtl where tl_no = $tl";

        return $this->sqlconn->runQuery($_getList);

    }

    public function getAddedUpc( $tl_no ) {

        $_getList = "SELECT tl_no, sku, qty_rec from wms_store_rcv_dtl where qty_req = 0 and tl_no = $tl_no";

        return $this->sqlconn->runQuery( $_getList );
    }

    public function updateIsSyncSts( $tl_no ) {

        $date_now = date('Y-m-d H:i:s');

        $_query = "UPDATE wms_store_rcv_list
                   SET is_jda_sync = 1, updated_at = '$date_now'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

}