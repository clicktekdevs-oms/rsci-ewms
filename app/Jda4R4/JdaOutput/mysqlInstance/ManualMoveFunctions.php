<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class ManualMoveFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getRcvNoToProcess() {

        $_getRcvList = "select DISTINCT rcv_no from wms_po_detail where is_jda_sync = 1";

        return $this->sqlconn->runQuery( $_getRcvList );

    }

    public function getSkusQty( $rcv_no ) {

        $_getSkus = "SELECT sku, slot_code, qty_rcv from wms_po_detail where is_jda_sync = 1 and  rcv_no = $rcv_no ";

        return $this->sqlconn->runQuery( $_getSkus );

    }

}
