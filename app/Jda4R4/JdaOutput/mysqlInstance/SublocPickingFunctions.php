<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');

class SublocPickingFunctions {

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getTransferToAllocate() {

        $query = "SELECT tl_no FROM wms_subloc_pick_list where pick_status = 0 and is_jda_sync = 0";

        return $this->sqlconn->runQuery( $query );

    }

    public function getTransferQuantity( $transfer ) {

        $query = "SELECT tl_no,sku, qty_req, qty_rcv FROM wms_subloc_pick_detail where tl_no = $transfer";

        return $this->sqlconn->runQuery( $query );

    }

    public function updateAllocateStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_subloc_pick_list
                   SET jda_status = '$status'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function updatePickStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_subloc_pick_list
                   SET jda_status = '$status'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function updateIsSyncedAlloc( $mts ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_subloc_pick_list
                   SET is_jda_sync = 1, updated_at = '$now'
                   WHERE tl_no = $mts";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function updateIsSyncedPick( $mts ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_subloc_pick_list
                   SET is_jda_sync = 2, updated_at = '$now'
                   WHERE tl_no = $mts";

        return $this->sqlconn->updateQuery( $_query );

    }

    public function getTransferToPick() {

        $query = "SELECT tl_no, from_loc FROM wms_subloc_pick_list where pick_status = 0 and is_jda_sync = 1 and jda_status = 'A'";

        return $this->sqlconn->runQuery( $query );

    }
}