<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class PickingFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getPickList() {

        $_getPickList = "select tl_no, doc_no from wms_pick_list where is_jda_sync = 0 and pick_status = 0 or pick_jda_status = 1 order by updated_at desc";

        return $this->sqlconn->runQuery( $_getPickList );

    }

    public function getPickQty( $tl_no ) {

        $_getPickDtl = "SELECT DISTINCT a.tl_no, a.doc_no, b.sku, b.qty_rcv from wms_pick_list a 
                        INNER JOIN wms_pick_detail b
                        ON a.tl_no = b.tl_no
                        WHERE a.tl_no = $tl_no";

        return $this->sqlconn->runQuery( $_getPickDtl );

    }

    public function updateIsSynced( $doc_no, $tl_no ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_pick_list
                   SET is_jda_sync = 1, updated_at = '$now'
                   WHERE tl_no = $tl_no
                   AND doc_no = $doc_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function updateJdaPickStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_pick_list
                   SET pick_jda_status = $status
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function insertLogs( $data = array() ) {

        $keys = implode(',', array_keys($data));
        $values = array_values($data);

        $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );
    }

}