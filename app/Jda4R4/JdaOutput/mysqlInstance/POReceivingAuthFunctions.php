<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class POReceivingAuthFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function insertLogs( $data = array() ) {

        $keys = implode(',', array_keys($data));
        $values = array_values($data);

        $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );
    }
}