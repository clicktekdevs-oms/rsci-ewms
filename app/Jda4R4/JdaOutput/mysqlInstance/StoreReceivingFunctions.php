<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class StoreReceivingFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getPendingStoreReceive()
    {

        $_getList = "SELECT ref_no, username, password, hash_password, is_status from wms_jda_login_logs where is_status = 0";

        return $this->sqlconn->runQuery($_getList);

    }

    public function getTLtoProcess( $pell_no ) {

        $_getList = "SELECT DISTINCT a.tl_no from wms_load_detail a 
                     inner join wms_store_rcv_list b on a.tl_no = b.tl_no 
                     where a.load_code = '$pell_no' and b.is_jda_sync = 0 and NOT (b.jda_status <=> 'C')";

        return $this->sqlconn->runQuery( $_getList );

    }

    public function getAddedUpc( $tl_no ) {

        $_getList = "SELECT tl_no, sku, qty_rec from wms_store_rcv_dtl where qty_req = 0 and tl_no = $tl_no";

        return $this->sqlconn->runQuery( $_getList );
    }

    public function updateJdaRcvStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_store_rcv_list
                   SET jda_status = '$status'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function updateIsSyncSts( $tl_no ) {

        $date_now = date('Y-m-d H:i:s');

        $_query = "UPDATE wms_store_rcv_list
                   SET is_jda_sync = 1, updated_at = '$date_now'
                   WHERE tl_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }

    /**
     * @param $tl integer
     */
    public function getUpdateTLQuantity( $tl ) {

        $_getList = "SELECT tl_no,sku, qty_rec from wms_store_rcv_dtl where tl_no = $tl";

        return $this->sqlconn->runQuery($_getList);

    }


}