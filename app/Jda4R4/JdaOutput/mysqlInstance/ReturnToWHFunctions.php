<?php
require_once(__DIR__.'/connection.php');
require_once(__DIR__.'/../config/config.php');
class ReturnToWHFunctions
{

    function __construct()
    {
        $mysql = mysqlCredentials();
        $this->sqlconn = new MysqlConnect($mysql['HOSTNAME'],
            $mysql['DATABASE'],
            $mysql['USERNAME'],
            $mysql['PASSWORD']);
    }

    public function Connect()
    {
        $this->sqlconn->Connect();
    }

    public function getMtsToProcess() {

        $_getList = "select mts_no from wms_rtnwh_list where mts_status = 0 and is_sync = 0 or jda_status = 'S'";

        return $this->sqlconn->runQuery( $_getList );

    }

    public function getMtsQuantity( $mts_no ) {

        $get_qty = "SELECT DISTINCT a.mts_no, b.upc, c.sku, b.qty_rcv from wms_rtnwh_list a 
                    INNER JOIN wms_rtnwh_detail b
                    ON a.mts_no = b.mts_no
                    INNER JOIN wms_product_masterlist c
                    ON b.upc = c.upc
                    WHERE  a.mts_no = $mts_no AND a.mts_status = 0 AND a.is_sync = 0";

        return $this->sqlconn->runQuery( $get_qty );

    }

    public function getUnlistedSkus( $mts_no ) {

        $getQty = "select mts_no, sku, qty_rcv from wms_rtnwh_detail where qty_req = 0 and sku != 0 and mts_no = $mts_no";

        return $this->sqlconn->runQuery( $getQty );

    }

    public function updateJdaRcvStatus( $tl_no, $status ) {

        $_query = "UPDATE wms_rtnwh_list
                   SET jda_status = '$status'
                   WHERE mts_no = $tl_no";

        return $this->sqlconn->updateQuery( $_query );
    }



    public function updateIsSynced( $mts ) {

        $now = date("Y-m-d H:i:s");

        $_query = "UPDATE wms_rtnwh_list
                   SET is_sync = 1, updated_at = '$now'
                   WHERE mts_no = $mts";

        return $this->sqlconn->updateQuery( $_query );
    }

    public function insertLogs( $data = array() ) {

        $keys = implode(',', array_keys($data));
        $values = array_values($data);

        $_query = "INSERT wms_jda_log ($keys) VALUES ('$values[0]', $values[1], '$values[2]', '$values[3]', '$values[4]' )";

        return $this->sqlconn->updateQuery( $_query );
    }
}