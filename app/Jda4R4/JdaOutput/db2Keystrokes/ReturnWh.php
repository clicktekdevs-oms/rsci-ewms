<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class ReturnWh
{

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {
        $config = db2Credentials();
        $this->db2_host = $config['HOSTNAME'];
        $this->db2_username = $config['USERNAME'];
        $this->db2_password = $config['PASSWORD'];
        $this->db2_database = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login()
    {
        $jda = $this->jda;
        $jda->login($this->db2_username, $this->db2_password);
        $result = $jda->screenCheck('Merchandise Management System');
        if ($result) {
            return true;
        } else {
            $result = $jda->screenCheck('is allocated to another job.');
            if ($result) {
                $jda->write(ENTER, true);
                $jda->show();
                return true;
            } else {
                return false;
            }
        }
    }

    public function initiate() {

        $jda = $this->jda;
        $jda->write('09', true); $jda->show();
        $jda->write('01', true); $jda->show();
        $jda->write('17', true); $jda->show();

    }

    public function enterMts( $mts ) {

        $jda = $this->jda;
        $jda->write( $mts, true); $jda->show();
        $jda->write(ENTER, true); $jda->show();

        if ( $jda->screenCheck("Transfer Batch Invalid") ) {

            return "Transfer Batch Invalid";

        }elseif ( $jda->screenCheck("Transfer Batch Not In SHIPPED Status") ) {

            return "Transfer Batch Not In SHIPPED Status";

        }

        $jda->write('1', true); $jda->show();


    }

    public function addItems( $sku, $qty ) {

        $jda = $this->jda;
        $jda->write(F9, true ); $jda->show();
        $jda->write($sku, true ); $jda->show();
        $jda->write(TAB, true ); $jda->show();
        $jda->write( $qty, true ); $jda->show();
        $jda->write(ENTER, true ); $jda->show();
        $jda->write(F7, true ); $jda->show();
        $jda->write(F1, true ); $jda->show();

    }

    public function closeProcess() {

        $jda = $this->jda;
        $jda->write(F10, true); $jda->show();
        $jda->write(F10, true); $jda->show();
        if ( $jda->screenCheck("Has been placed on the JOBQ") ) {

            return  "Has been placed on the JOBQ";

        }

    }
}