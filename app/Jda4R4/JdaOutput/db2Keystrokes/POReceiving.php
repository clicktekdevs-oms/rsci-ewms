<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class POReceiving
{

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {
        $config = db2Credentials();
        $this->db2_host         = $config['HOSTNAME'];
        $this->db2_username     = $config['USERNAME'];
        $this->db2_password     = $config['PASSWORD'];
        $this->db2_database     = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login()
    {
        $jda = $this->jda;
        $jda->login( $this->db2_username, $this->db2_password );
        $result = $jda->screenCheck('Merchandise Management System');
        if($result)
        {
            return true;
        }
        else
        {
            $result = $jda->screenCheck('is allocated to another job.');
            if($result)
            {
                $jda->write(ENTER, true); $jda->show();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function Initiate()
    {
        $jda = $this->jda;
        $jda->write('08', true); $jda->show();
        $jda->write('02', true); $jda->show();
        $jda->write('02', true); $jda->show();
    }

    public function enterReceiverNo( $receiver_no ) {

        $jda = $this->jda;
        $jda->write( $receiver_no, true); $jda->show();
        $jda->write(ENTER, true); $jda->show();
        if ( $jda->screenCheck('This receiver has been detail received, cannot dock receive') ) {
            return 'This receiver has been detail received, cannot dock receive';
        }
        $jda->write(TAB, true); $jda->show();
        $jda->write('', true); $jda->show();
        $jda->write(TAB, true); $jda->show();
        $jda->write(TAB, true); $jda->show();
        $jda->write('SYS', true); $jda->show();

    }

    public function enterTransferNo( $transfer_no ) {

        $jda = $this->jda;
        $jda->write( $transfer_no, true); $jda->show();
        $jda->write(END, true); $jda->show();
        $jda->write('1', true); $jda->show();
        $jda->write(END, true); $jda->show();
        $jda->write('1', true); $jda->show();
        $jda->write(END, true); $jda->show();
        $jda->write('1', true); $jda->show();
        $jda->write(END, true); $jda->show();
        $jda->write('1', true); $jda->show();
        $jda->write(END, true); $jda->show();
        $jda->write(TAB, true); $jda->show();
        $jda->write(TAB, true); $jda->show();
        $jda->write(TAB, true); $jda->show();

    }

    public function enterSlot( $postat, $getNotInPOQty ) {

        $jda = $this->jda;
        $jda->write( 'PZ000001', true); $jda->show();
        $jda->write(TAB, true); $jda->show();
        $jda->write( 'SYS', true); $jda->show();
        $jda->write(F5, true); $jda->show();
        $jda->write('1', true); $jda->show();

        foreach ($getNotInPOQty as $po)
        {
            $jda->write(F10, true); $jda->show();
            $jda->write($po['sku'], true); $jda->show();
            $jda->write(TAB, true); $jda->show();
            $jda->write($po['qty_rcv'], true); $jda->show();
            $jda->write(TAB, true); $jda->show();
            $jda->write('PZ000001', true); $jda->show();
            $jda->write(ENTER, true); $jda->show();
            $jda->write(F9, true); $jda->show();
            $jda->write(F1, true); $jda->show();
        }

        $jda->write(F7, true); $jda->show();
        $jda->write(TAB,true);

        if ($jda->screenCheck('Do You Wish To Close The Remaining P/O')) {
            $jda->write('N', true); $jda->show();
            $jda->write(ENTER, true);
        }

        $jda->write(F7, true); $jda->show();

        if ( $jda->screenCheck( 'Has been placed on the JOBQ.') ) {
            return 'Has been placed on the JOBQ.Processing will begin when machine resources are available.';
        }

    }


}
