<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class Shipping
{

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {
        $config = db2Credentials();
        $this->db2_host = $config['HOSTNAME'];
        $this->db2_username = $config['USERNAME'];
        $this->db2_password = $config['PASSWORD'];
        $this->db2_database = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login()
    {
        $jda = $this->jda;
        $jda->login($this->db2_username, $this->db2_password);
        $result = $jda->screenCheck('Merchandise Management System');
        if ($result) {
            return true;
        } else {
            $result = $jda->screenCheck('is allocated to another job.');
            if ($result) {
                $jda->write(ENTER, true);
                $jda->show();
                return true;
            } else {
                return false;
            }
        }
    }

    public function initiate() {

        $jda = $this->jda;
        $jda->write('09', true); $jda->show();
        $jda->write('01', true); $jda->show();
        $jda->write('11', true); $jda->show();
        $jda->write('03', true); $jda->show();

    }

    public function doShip( $tl_number ) {

        $jda = $this->jda;

        $jda->write( $tl_number, true); $jda->show();
        $jda->write(ENTER, true); $jda->show();

        if ( $jda->screenCheck("Transfer Number Not Found" ) ) {

            return "Transfer Number Not Found";

        }
        elseif ( $jda->screenCheck( 'Transfer Does Not Have "Picked" Status') ) {

            return 'Transfer Does Not Have "Picked" Status';

        }
        elseif ( $jda->screenCheck("All Transfer Details Have Not Been Picked") ) {

            return "All Transfer Details Have Not Been Picked";

        }

        $jda->write(F7, true); $jda->show();
        $jda->write('01', true); $jda->show();

        if ( $jda->screenCheck( "Has been placed on the JOBQ") ) {

            return "Has been placed on the JOBQ";

        }

    }

    public function closeShip() {

        $jda = $this->jda;

        $jda->write(ENTER, true); $jda->show();
        $jda->write(F7, true); $jda->show();
    }

}