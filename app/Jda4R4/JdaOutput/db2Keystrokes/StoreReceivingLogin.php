<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class StoreReceivingLogin {

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {
        $config = db2Credentials();
        $this->db2_host = $config['HOSTNAME'];
        $this->db2_username = $config['USERNAME'];
        $this->db2_password = $config['PASSWORD'];
        $this->db2_database = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login( $jda_username, $jda_password )
    {
        $jda = $this->jda;

        $jda->login( $jda_username, $jda_password ); $jda->show();

        if ( $jda->screenCheck("User $jda_username does not exist.")) {

            return "User $jda_username does not exist.";

        }

        if ( $jda->screenCheck("Password not correct for user profile.") ) {

            return "Password not correct for user profile.";

        }

        if ( $jda->screenCheck("Next not valid sign-on disables user profile.") ) {

            return "Next not valid sign-on disables user profile.";

        }

        if ( $jda->screenCheck("User profile $jda_username cannot sign on.") ) {

            return "User profile $jda_username cannot sign on.";

        }

        if ( $jda->screenCheck("Merchandise Management System") ) {

            return "Merchandise Management System";

        }

        if ( $jda->screenCheck("is allocated to another job.") ) {

            return "Merchandise Management System";

        }else {

            return "Request Timeout.Please check connection";
        }

    }

}