<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class SublocReceiving
{

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {
        $config = db2Credentials();
        $this->db2_host = $config['HOSTNAME'];
        $this->db2_username = $config['USERNAME'];
        $this->db2_password = $config['PASSWORD'];
        $this->db2_database = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login()
    {
        $jda = $this->jda;
        $jda->login($this->db2_username, $this->db2_password);
        $result = $jda->screenCheck('Merchandise Management System');
        if ($result) {
            return true;
        } else {
            $result = $jda->screenCheck('is allocated to another job.');
            if ($result) {
                $jda->write(ENTER, true);
                $jda->show();
                return true;
            } else {
                return false;
            }
        }
    }

    public function initiate() {

        $jda = $this->jda;
        $jda->write('09', true); $jda->show();
        $jda->write('01', true); $jda->show();
        $jda->write('17', true); $jda->show();

    }

    public function doSublocRcv( $tl_number ) {

        $jda = $this->jda;

        $jda->write($tl_number, true); $jda->show();
        $jda->write(ENTER, true); $jda->show();
        $jda->write('1', true); $jda->show();
        $jda->write(F10, true); $jda->show();
        $jda->write(F10, true); $jda->show();
        $jda->write(ENTER, true); $jda->show();

    }

}