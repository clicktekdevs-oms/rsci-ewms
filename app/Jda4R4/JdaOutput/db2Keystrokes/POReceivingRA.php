<?php
require_once(__DIR__.'/../db2library/jdatelnet.php');
require_once(__DIR__.'/../config/config.php');


class POReceivingRA
{

    protected $jda;

    protected $db2 = null;
    protected $db2_host;
    protected $db2_username;
    protected $db2_password;
    protected $db2_database;

    public function __construct($debug_level = 1)
    {

        $config = db2Credentials();
        $this->db2_host         = $config['HOSTNAME'];
        $this->db2_username     = $config['USERNAME'];
        $this->db2_password     = $config['PASSWORD'];
        $this->db2_database     = $config['DATABASE'];

        $this->jda = new jdatelnet($this->db2_host);
        $this->jda->debugLvl = $debug_level;
    }

    public function Login()
    {
        $jda = $this->jda;
        $jda->login( $this->db2_username, $this->db2_password );
        $result = $jda->screenCheck('Merchandise Management System');
        if($result)
        {
            return true;
        }
        else
        {
            $result = $jda->screenCheck('is allocated to another job.');
            if($result)
            {
                $jda->write(ENTER, true); $jda->show();
                return true;
            }
            else
            {
                return false;
            }
        }
    }

    public function Initiate()
    {
        $jda = $this->jda;
        $jda->write('08', true); $jda->show();
        $jda->write('02', true); $jda->show();
        $jda->write('01', true); $jda->show();
        $jda->write('01', true); $jda->show();
    }

    public function enterPo( $po_number ) {

        $jda = $this->jda;
        $jda->write( trim( $po_number ), true); $jda->show();
        $jda->write(F7, true); $jda->show();
        if ( $jda->screenCheck( 'This P.O./B.O.') ) {
            return 'This P.O./B.O. already has an open receiver - #';
        }
        $jda->write('1', true); $jda->show();

        if ( $jda->screenCheck( 'Has been placed on the JOBQ.') ) {
            $jda->write(ENTER, true); $jda->show();
            return 'Has been placed on the JOBQ.Processing will begin when machine resources are available.';
        }


    }

}
