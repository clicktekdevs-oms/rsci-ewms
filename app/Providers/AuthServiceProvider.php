<?php

namespace App\Providers;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use App\Helper\CommonHelper;
use DB;
use Auth;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any application authentication / authorization services.
     *
     * @param  \Illuminate\Contracts\Auth\Access\Gate  $gate
     * @return void
     */
    public function boot(GateContract $gate)
    {
        $this->registerPolicies($gate);

        $gate->define('role-access', function($user, $module) {
            $getRoles = DB::table('user_roles')->select('id', 'permissions')->where('id', $user->role_id )->get();
            $getPermission = CommonHelper::getValueFromArray($getRoles);

            $checkRoles = in_array($module, $getPermission) ? true : false;
            return $checkRoles;
        });

        //
    }
}
