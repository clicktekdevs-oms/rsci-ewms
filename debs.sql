-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               5.7.18-0ubuntu0.16.04.1 - (Ubuntu)
-- Server OS:                    Linux
-- HeidiSQL Version:             9.5.0.5196
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;

-- Dumping structure for table debs_ewms.wms_audit_trail
CREATE TABLE IF NOT EXISTS `wms_audit_trail` (
  `audit_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `reference` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `action` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_before` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `data_after` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`audit_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_audit_trail: ~8,890 rows (approximately)
DELETE FROM `wms_audit_trail`;
/*!40000 ALTER TABLE `wms_audit_trail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_audit_trail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_box_detail
CREATE TABLE IF NOT EXISTS `wms_box_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(11) NOT NULL,
  `box_no` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `mov_qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `box_no` (`box_no`),
  KEY `Index 3` (`tl_no`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_box_detail: ~77,441 rows (approximately)
DELETE FROM `wms_box_detail`;
/*!40000 ALTER TABLE `wms_box_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_box_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_box_list
CREATE TABLE IF NOT EXISTS `wms_box_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `box_no` bigint(20) NOT NULL,
  `tl_no` int(11) NOT NULL,
  `store_num` int(11) NOT NULL,
  `is_assign` int(11) NOT NULL DEFAULT '0',
  `is_open` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_box_list: ~4,934 rows (approximately)
DELETE FROM `wms_box_list`;
/*!40000 ALTER TABLE `wms_box_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_box_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_jda_log
CREATE TABLE IF NOT EXISTS `wms_jda_log` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `module` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `ref_num` bigint(20) NOT NULL,
  `ref_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_jda_log: ~105,022 rows (approximately)
DELETE FROM `wms_jda_log`;
/*!40000 ALTER TABLE `wms_jda_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_jda_log` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_jda_login_logs
CREATE TABLE IF NOT EXISTS `wms_jda_login_logs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `ref_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `hash_password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `text_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_jda_login_logs: ~65 rows (approximately)
DELETE FROM `wms_jda_login_logs`;
/*!40000 ALTER TABLE `wms_jda_login_logs` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_jda_login_logs` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_load_detail
CREATE TABLE IF NOT EXISTS `wms_load_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `load_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tl_no` int(11) NOT NULL,
  `store_num` int(11) DEFAULT NULL,
  `is_assign_store` int(11) DEFAULT NULL,
  `is_box_status` int(11) NOT NULL DEFAULT '2',
  `is_load` int(11) NOT NULL DEFAULT '0',
  `is_sync` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_load_detail: ~5,102 rows (approximately)
DELETE FROM `wms_load_detail`;
/*!40000 ALTER TABLE `wms_load_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_load_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_load_list
CREATE TABLE IF NOT EXISTS `wms_load_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `load_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seal_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plate_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_helper` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessed_sealed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `assigned_to_store` int(11) DEFAULT NULL,
  `ship_date` date DEFAULT NULL,
  `load_status` int(11) NOT NULL DEFAULT '3',
  `load_status_store` int(11) NOT NULL DEFAULT '2',
  `is_sync` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `load_list_load_code_unique` (`load_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_load_list: ~256 rows (approximately)
DELETE FROM `wms_load_list`;
/*!40000 ALTER TABLE `wms_load_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_load_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_migrations
CREATE TABLE IF NOT EXISTS `wms_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_migrations: ~54 rows (approximately)
DELETE FROM `wms_migrations`;
/*!40000 ALTER TABLE `wms_migrations` DISABLE KEYS */;
INSERT INTO `wms_migrations` (`migration`, `batch`) VALUES
	('2014_10_12_000000_create_users_table', 1),
	('2017_06_02_083445_create_table_po_list', 1),
	('2017_06_03_093331_create_table_po_detail', 1),
	('2017_06_05_021426_create_audit_trail', 1),
	('2017_06_05_055825_create_user_role', 1),
	('2017_06_06_044353_create_product_masterlist', 1),
	('2017_06_07_090812_create_tbl_slot_masterlist', 1),
	('2017_06_07_091711_create_tbl_store_masterlist', 1),
	('2017_06_09_090604_add_column_remarks_po_list', 1),
	('2017_06_14_061848_create_table_jda_logs', 1),
	('2017_06_14_101631_create_table_pick_list', 1),
	('2017_06_15_023624_create_pick_detail', 1),
	('2017_06_16_072719_create_box_list', 1),
	('2017_06_16_084011_create_box_detail', 1),
	('2017_06_19_075820_add_box_list_column', 1),
	('2017_06_20_072038_create_load_list', 1),
	('2017_06_21_062903_create_load_detail', 1),
	('2017_06_21_103356_add_column_load_status', 1),
	('2017_06_22_021911_add_table_ship_date', 1),
	('2017_06_22_034908_create_table_returnwh', 1),
	('2017_06_22_042232_create_table_returnwh_detail', 1),
	('2017_07_26_025715_create_tbl_subloc_rcv', 1),
	('2017_07_26_031850_create_tbl_subloc_rcv_dtl', 1),
	('2017_08_04_030658_add_column_box_detail', 1),
	('2017_08_08_052314_create_tbl_subloc_pick_list', 1),
	('2017_08_09_053205_create_table_store_rcv_list', 1),
	('2017_08_09_060421_create_table_store_dtl', 1),
	('2017_08_09_081636_add_column_item_desc_po_dtl', 1),
	('2017_08_12_021856_add_column_store_assign', 1),
	('2017_08_12_023252_add_column_store_num_load_detail', 1),
	('2017_08_12_074638_add_column_store_clertk_assign_box_list', 1),
	('2017_08_13_044827_add_column_load_list_str_status', 1),
	('2017_08_14_075354_add_column_tl_load_detail', 1),
	('2017_08_15_024230_create_tbl_jda_status', 1),
	('2017_08_16_061003_add_column_remark_subloc_dtl', 1),
	('2017_08_16_081814_add_column_jda_status_tbl_subloc_list', 1),
	('2017_08_18_050905_create_tbl_subloc_pick_dtl', 1),
	('2017_08_23_063248_create_subloc_box_list', 1),
	('2017_08_23_063852_create_subloc_box_detail', 1),
	('2017_08_30_120858_subloc_load_list', 1),
	('2017_08_30_121310_subloc_load_detail', 1),
	('2017_09_07_072751_add_column_sku_desc_remark_rtnwh_detail', 1),
	('2017_09_11_034802_add_column_is_subloc_load_dtl', 1),
	('2017_09_13_083901_create_tbl_jda_login_logs', 1),
	('2017_09_22_054851_add_column_is_status_strrcv', 1),
	('2017_10_02_112519_add_column_picked_date', 1),
	('2017_10_02_173633_add_column_jda_status_rtnwh', 1),
	('2017_10_05_115023_add_column_dept_desc_pick_dtl', 1),
	('2017_10_05_133829_add_column_strtyp', 1),
	('2017_10_11_135723_add_column_load_list', 2),
	('2017_10_12_114244_add_column_subloc_load', 3),
	('2017_10_12_132006_add_column_subloc_pick', 3),
	('2017_10_03_114131_alter_table_po_detail', 4),
	('2017_11_07_111702_add_column_str_rcv_dtl_box', 5),
	('2018_02_19_132939_add_column_table_invupc', 6),
	('2018_04_20_110150_add_column_scanned_upc', 7),
	('2018_04_25_142553_add_column_style_no', 8);
/*!40000 ALTER TABLE `wms_migrations` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_pick_detail
CREATE TABLE IF NOT EXISTS `wms_pick_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` bigint(20) NOT NULL,
  `doc_no` bigint(20) NOT NULL,
  `ship_date` date NOT NULL,
  `dept_code` int(11) NOT NULL,
  `dept_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_loc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `from_slot` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_slot` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `rtl_price` double(8,2) NOT NULL,
  `qty_req` int(11) NOT NULL,
  `qty_rcv` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pick_detail_tl_no_doc_no_sku_upc_ship_date_unique` (`tl_no`,`doc_no`,`sku`,`upc`,`ship_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_pick_detail: ~79,557 rows (approximately)
DELETE FROM `wms_pick_detail`;
/*!40000 ALTER TABLE `wms_pick_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_pick_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_pick_list
CREATE TABLE IF NOT EXISTS `wms_pick_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(11) NOT NULL,
  `doc_no` int(11) NOT NULL,
  `ship_date` date DEFAULT NULL,
  `to_loc` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `picked_date` date DEFAULT NULL,
  `pick_status` tinyint(4) NOT NULL DEFAULT '3',
  `pick_jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ship_jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_jda_sync` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `pick_list_tl_no_doc_no_to_loc_unique` (`tl_no`,`doc_no`,`to_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_pick_list: ~3,692 rows (approximately)
DELETE FROM `wms_pick_list`;
/*!40000 ALTER TABLE `wms_pick_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_pick_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_po_detail
CREATE TABLE IF NOT EXISTS `wms_po_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rcv_no` int(11) NOT NULL,
  `sku` int(11) NOT NULL,
  `upc` bigint(20) DEFAULT NULL,
  `scanned_upc` bigint(20) DEFAULT NULL,
  `style_no` bigint(20) DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `division_code` int(11) NOT NULL,
  `dept_code` int(11) NOT NULL,
  `division` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `department` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `slot_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_ord` int(11) NOT NULL,
  `qty_rcv` int(11) NOT NULL,
  `assigned_to` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `po_status` tinyint(4) NOT NULL DEFAULT '4',
  `is_jda_sync` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `po_detail_rcv_no_sku_upc_dept_code_unique` (`rcv_no`,`sku`,`upc`,`division_code`,`dept_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_po_detail: ~1,270 rows (approximately)
DELETE FROM `wms_po_detail`;
/*!40000 ALTER TABLE `wms_po_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_po_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_po_list
CREATE TABLE IF NOT EXISTS `wms_po_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `rcv_no` int(11) NOT NULL,
  `po_no` int(11) NOT NULL,
  `inv_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `total_qty` int(11) NOT NULL,
  `ship_ref_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `entry_date` date NOT NULL,
  `po_status` tinyint(4) NOT NULL DEFAULT '4',
  `jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_jda_sync` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `po_list_rcv_no_po_no_unique` (`rcv_no`,`po_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_po_list: ~10 rows (approximately)
DELETE FROM `wms_po_list`;
/*!40000 ALTER TABLE `wms_po_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_po_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_product_masterlist
CREATE TABLE IF NOT EXISTS `wms_product_masterlist` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `sku` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `new_upc` bigint(20) DEFAULT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dept_code` int(11) NOT NULL,
  `iset_code` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `product_masterlist_sku_upc_unique` (`sku`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_product_masterlist: ~597,755 rows (approximately)
DELETE FROM `wms_product_masterlist`;
/*!40000 ALTER TABLE `wms_product_masterlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_product_masterlist` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_rtnwh_detail
CREATE TABLE IF NOT EXISTS `wms_rtnwh_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mts_no` int(11) NOT NULL,
  `sku` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_req` int(11) NOT NULL,
  `qty_rcv` int(11) NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rtnwh_detail_mts_no_upc_unique` (`mts_no`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_rtnwh_detail: ~960 rows (approximately)
DELETE FROM `wms_rtnwh_detail`;
/*!40000 ALTER TABLE `wms_rtnwh_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_rtnwh_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_rtnwh_list
CREATE TABLE IF NOT EXISTS `wms_rtnwh_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `mts_no` bigint(20) NOT NULL,
  `from_loc` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `mts_status` int(11) NOT NULL DEFAULT '3',
  `jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_sync` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `rtnwh_list_mts_no_from_loc_unique` (`mts_no`,`from_loc`)
) ENGINE=InnoDB AUTO_INCREMENT=11556 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_rtnwh_list: ~122 rows (approximately)
DELETE FROM `wms_rtnwh_list`;
/*!40000 ALTER TABLE `wms_rtnwh_list` DISABLE KEYS */;
INSERT INTO `wms_rtnwh_list` (`id`, `mts_no`, `from_loc`, `assigned_to`, `mts_status`, `jda_status`, `is_sync`, `created_at`, `updated_at`) VALUES
	(1, 332459, 893, NULL, 3, NULL, 0, '2017-10-11 06:00:04', '0000-00-00 00:00:00'),
	(113, 332511, 884, NULL, 3, NULL, 0, '2017-10-12 06:00:03', '0000-00-00 00:00:00'),
	(327, 332887, 893, 21, 1, NULL, 0, '2017-10-20 06:00:02', '0000-00-00 00:00:00'),
	(950, 333146, 801, 9, 0, 'C', 1, '2017-10-25 13:56:02', '2017-11-02 09:02:17'),
	(951, 333147, 801, 9, 0, 'C', 1, '2017-10-25 13:56:02', '2017-11-02 09:02:28'),
	(994, 333152, 884, 9, 1, NULL, 0, '2017-10-25 19:08:02', '0000-00-00 00:00:00'),
	(1107, 333206, 893, 9, 1, NULL, 0, '2017-10-26 19:16:02', '0000-00-00 00:00:00'),
	(1108, 333210, 893, 9, 0, 'C', 1, '2017-10-26 19:16:02', '2017-11-02 14:48:15'),
	(1160, 333253, 820, 9, 0, 'C', 1, '2017-10-27 11:40:03', '2017-11-02 14:48:27'),
	(1563, 333401, 820, NULL, 3, NULL, 0, '2017-10-31 06:00:04', '0000-00-00 00:00:00'),
	(1564, 333411, 893, 9, 0, 'C', 1, '2017-10-31 06:00:04', '2017-11-02 14:47:23'),
	(1910, 333783, 801, 9, 0, 'C', 1, '2017-11-08 13:56:01', '2017-11-16 15:49:13'),
	(1954, 333791, 884, 9, 0, 'C', 1, '2017-11-08 19:24:02', '2017-11-16 15:48:20'),
	(2049, 333830, 893, 9, 0, 'C', 1, '2017-11-09 17:16:01', '2017-11-16 15:48:35'),
	(2207, 333924, 884, 32, 0, 'C', 1, '2017-11-11 09:08:02', '2017-11-17 13:51:18'),
	(2208, 333925, 884, 32, 0, 'C', 1, '2017-11-11 09:08:02', '2017-11-17 13:51:28'),
	(2209, 334036, 884, NULL, 3, NULL, 0, '2017-11-14 06:00:04', '0000-00-00 00:00:00'),
	(2210, 334037, 884, NULL, 3, NULL, 0, '2017-11-14 06:00:04', '0000-00-00 00:00:00'),
	(2211, 334038, 884, NULL, 3, NULL, 0, '2017-11-14 06:00:04', '0000-00-00 00:00:00'),
	(2435, 334137, 884, NULL, 3, NULL, 0, '2017-11-16 06:00:05', '0000-00-00 00:00:00'),
	(2610, 334333, 884, 13, 0, 'C', 1, '2017-11-21 06:00:04', '2017-11-28 10:00:14'),
	(2611, 334338, 884, 13, 0, 'C', 1, '2017-11-21 06:00:04', '2017-11-28 09:59:19'),
	(2785, 334386, 801, 13, 0, 'C', 1, '2017-11-22 13:40:02', '2017-11-28 13:28:11'),
	(2790, 334390, 80011, NULL, 3, NULL, 0, '2017-11-22 14:16:02', '0000-00-00 00:00:00'),
	(2938, 334473, 893, 6, 0, 'C', 1, '2017-11-23 18:48:03', '2017-11-29 10:12:38'),
	(2947, 334477, 893, 6, 0, 'C', 1, '2017-11-23 19:56:02', '2017-11-29 10:12:50'),
	(2948, 334478, 893, 6, 0, 'C', 1, '2017-11-24 06:00:04', '2017-12-01 12:43:18'),
	(2998, 334527, 820, NULL, 3, NULL, 0, '2017-11-24 12:16:02', '0000-00-00 00:00:00'),
	(3006, 334533, 801, 13, 0, 'C', 1, '2017-11-24 13:16:02', '2017-11-28 13:27:17'),
	(3330, 334660, 820, 13, 0, 'C', 1, '2017-11-27 11:48:03', '2017-11-28 14:41:17'),
	(3564, 334781, 801, 6, 0, 'C', 1, '2017-11-29 12:56:03', '2017-12-01 10:55:14'),
	(3565, 334782, 801, 6, 0, 'C', 1, '2017-11-29 12:56:03', '2017-12-01 10:54:22'),
	(3566, 334783, 801, 6, 0, 'C', 1, '2017-11-29 12:56:03', '2017-12-01 10:54:33'),
	(3789, 334903, 884, 9, 0, 'C', 1, '2017-12-02 06:00:04', '2017-12-04 09:35:17'),
	(3883, 334949, 893, 32, 0, 'C', 1, '2017-12-02 17:48:02', '2017-12-08 08:03:21'),
	(4057, 335029, 820, 32, 0, 'C', 1, '2017-12-04 11:32:02', '2017-12-08 08:03:31'),
	(4125, 335054, 884, 6, 0, 'C', 1, '2017-12-05 06:00:03', '2017-12-08 08:23:14'),
	(4451, 335168, 893, 6, 0, 'C', 1, '2017-12-07 19:40:01', '2017-12-08 08:23:25'),
	(4454, 335177, 893, 6, 0, 'C', 1, '2017-12-08 06:00:04', '2017-12-08 08:22:17'),
	(4473, 335218, 820, 6, 0, 'C', 1, '2017-12-08 11:40:03', '2017-12-13 08:13:18'),
	(4478, 335224, 820, 6, 0, 'C', 1, '2017-12-08 12:16:02', '2017-12-13 08:13:28'),
	(4481, 335228, 820, 6, 0, 'C', 1, '2017-12-08 12:40:02', '2017-12-13 08:13:39'),
	(4822, 335387, 801, 6, 0, 'C', 1, '2017-12-11 13:16:01', '2017-12-13 08:13:50'),
	(4876, 335415, 884, 6, 0, 'C', 1, '2017-12-12 06:00:04', '2017-12-18 07:55:13'),
	(5051, 335503, 801, 6, 0, 'C', 1, '2017-12-13 13:56:01', '2017-12-18 07:55:23'),
	(5563, 336040, 884, NULL, 3, NULL, 0, '2017-12-21 13:48:02', '0000-00-00 00:00:00'),
	(5597, 336862, 80012, NULL, 3, NULL, 0, '2018-01-05 14:56:02', '0000-00-00 00:00:00'),
	(5618, 336866, 975, NULL, 3, NULL, 0, '2018-01-05 17:32:02', '0000-00-00 00:00:00'),
	(5930, 337063, 884, 9, 0, 'C', 1, '2018-01-15 16:00:05', '2018-01-22 08:38:13'),
	(5993, 337099, 893, 9, 0, 'C', 1, '2018-01-16 09:56:02', '2018-01-22 08:37:12'),
	(6074, 337101, 801, 9, 0, 'C', 1, '2018-01-17 06:00:03', '2018-01-22 08:37:23'),
	(6117, 337141, 820, 9, 0, 'C', 1, '2018-01-17 11:24:01', '2018-01-22 08:37:33'),
	(6211, 337185, 884, 9, 0, 'C', 1, '2018-01-18 09:08:03', '2018-01-22 08:36:18'),
	(6249, 337189, 975, 6, 0, 'C', 1, '2018-01-18 13:56:01', '2018-01-26 09:25:17'),
	(6641, 337356, 820, NULL, 3, NULL, 0, '2018-01-22 12:16:02', '0000-00-00 00:00:00'),
	(6650, 337357, 820, 21, 0, 'C', 1, '2018-01-22 13:08:02', '2018-01-23 14:38:17'),
	(6706, 337382, 884, NULL, 3, NULL, 0, '2018-01-22 19:56:02', '0000-00-00 00:00:00'),
	(6738, 337410, 893, NULL, 3, NULL, 0, '2018-01-23 09:40:02', '0000-00-00 00:00:00'),
	(6741, 337411, 893, 6, 0, 'C', 1, '2018-01-23 09:48:03', '2018-01-26 09:25:27'),
	(6814, 337429, 801, 6, 0, 'C', 1, '2018-01-23 18:40:02', '2018-01-26 09:25:37'),
	(6889, 337471, 820, 6, 0, 'C', 1, '2018-01-24 13:48:02', '2018-01-29 15:04:17'),
	(7436, 337661, 820, NULL, 3, NULL, 0, '2018-01-29 11:56:02', '0000-00-00 00:00:00'),
	(7499, 337670, 893, NULL, 3, NULL, 0, '2018-01-29 19:48:02', '0000-00-00 00:00:00'),
	(7725, 337756, 884, 32, 0, 'C', 1, '2018-02-01 06:00:03', '2018-02-07 15:00:17'),
	(8216, 337925, 801, NULL, 3, NULL, 0, '2018-02-05 11:24:02', '0000-00-00 00:00:00'),
	(8217, 337927, 801, NULL, 3, NULL, 0, '2018-02-05 11:24:02', '0000-00-00 00:00:00'),
	(8280, 337948, 884, 32, 0, 'C', 1, '2018-02-05 19:08:02', '2018-02-07 08:57:13'),
	(8282, 337952, 893, 32, 0, 'C', 1, '2018-02-05 19:24:01', '2018-02-07 15:00:28'),
	(8364, 338006, 801, 9, 0, 'C', 1, '2018-02-06 15:40:02', '2018-02-12 15:52:18'),
	(8365, 338007, 801, 9, 0, 'C', 1, '2018-02-06 15:40:02', '2018-02-12 15:52:28'),
	(8401, 338014, 893, NULL, 3, NULL, 0, '2018-02-07 06:00:04', '0000-00-00 00:00:00'),
	(8402, 338015, 893, NULL, 3, NULL, 0, '2018-02-07 06:00:04', '0000-00-00 00:00:00'),
	(8436, 338048, 820, NULL, 3, NULL, 0, '2018-02-07 10:08:03', '0000-00-00 00:00:00'),
	(8473, 338063, 893, NULL, 3, NULL, 0, '2018-02-07 14:32:02', '0000-00-00 00:00:00'),
	(8474, 338065, 893, NULL, 3, NULL, 0, '2018-02-07 14:40:02', '0000-00-00 00:00:00'),
	(8476, 338066, 893, NULL, 3, NULL, 0, '2018-02-07 14:56:02', '0000-00-00 00:00:00'),
	(8614, 338118, 820, 6, 0, 'C', 1, '2018-02-08 18:08:02', '2018-02-13 09:12:14'),
	(8715, 338168, 884, NULL, 3, NULL, 0, '2018-02-09 16:48:01', '0000-00-00 00:00:00'),
	(9012, 338305, 820, 6, 0, 'C', 1, '2018-02-12 11:56:02', '2018-02-13 10:39:12'),
	(9013, 338306, 820, 6, 0, 'C', 1, '2018-02-12 11:56:02', '2018-02-13 10:39:22'),
	(9033, 338310, 884, NULL, 3, NULL, 0, '2018-02-12 14:16:02', '0000-00-00 00:00:00'),
	(9071, 338327, 884, 9, 0, 'C', 1, '2018-02-12 19:00:01', '2018-02-19 10:43:13'),
	(9079, 338340, 893, 9, 0, 'C', 1, '2018-02-13 06:00:06', '2018-02-15 16:37:11'),
	(9232, 338404, 820, 9, 0, 'C', 1, '2018-02-14 11:08:02', '2018-02-19 10:40:15'),
	(9233, 338406, 820, 9, 0, 'C', 1, '2018-02-14 11:16:09', '2018-02-15 14:11:18'),
	(9293, 338422, 884, 9, 0, 'C', 1, '2018-02-14 18:48:02', '2018-02-19 10:40:25'),
	(9299, 338423, 893, 6, 0, 'C', 1, '2018-02-14 19:32:01', '2018-02-22 15:05:13'),
	(9333, 338459, 893, 6, 2, NULL, 0, '2018-02-15 09:48:02', '0000-00-00 00:00:00'),
	(9392, 338469, 820, 9, 0, 'C', 1, '2018-02-15 17:08:03', '2018-02-19 10:40:36'),
	(9415, 338481, 801, 9, 0, 'C', 1, '2018-02-16 06:00:02', '2018-02-19 10:39:14'),
	(9748, 338580, 820, 6, 0, 'C', 1, '2018-02-18 19:40:03', '2018-02-20 09:43:13'),
	(9812, 338651, 820, 6, 0, 'C', 1, '2018-02-19 13:40:02', '2018-02-22 15:06:14'),
	(9846, 338658, 884, 6, 0, 'C', 1, '2018-02-19 17:56:01', '2018-02-22 15:06:25'),
	(9847, 338661, 884, 6, 0, 'C', 1, '2018-02-19 17:56:01', '2018-02-22 15:06:35'),
	(9852, 338663, 884, 9, 0, 'C', 1, '2018-02-19 18:24:02', '2018-02-26 08:46:13'),
	(9853, 338668, 884, 9, 0, 'C', 1, '2018-02-19 18:32:03', '2018-02-26 08:46:23'),
	(9854, 338669, 884, 9, 0, 'C', 1, '2018-02-19 18:40:01', '2018-02-26 08:45:12'),
	(9855, 338670, 884, 9, 0, 'C', 1, '2018-02-19 18:40:01', '2018-02-26 08:45:22'),
	(9857, 338671, 884, 9, 0, 'C', 1, '2018-02-19 18:48:02', '2018-02-26 08:45:32'),
	(9858, 338672, 884, 9, 0, 'C', 1, '2018-02-19 18:56:02', '2018-02-26 10:46:13'),
	(9866, 338673, 884, 6, 0, 'C', 1, '2018-02-19 19:56:01', '2018-02-22 15:20:18'),
	(9867, 338674, 893, 9, 0, 'C', 1, '2018-02-19 19:56:01', '2018-02-27 08:03:14'),
	(9931, 338703, 820, NULL, 3, NULL, 0, '2018-02-20 13:48:01', '0000-00-00 00:00:00'),
	(10016, 338738, 801, 6, 0, 'C', 1, '2018-02-21 10:24:01', '2018-02-22 15:44:13'),
	(10017, 338739, 801, 6, 0, 'C', 1, '2018-02-21 10:32:03', '2018-02-22 15:44:24'),
	(10018, 338740, 801, 6, 0, 'C', 1, '2018-02-21 10:32:03', '2018-02-22 15:44:35'),
	(10158, 338789, 820, 6, 0, 'C', 1, '2018-02-22 13:56:02', '2018-02-27 08:25:13'),
	(10629, 338942, 893, 9, 0, 'C', 1, '2018-02-26 16:48:01', '2018-03-02 13:07:13'),
	(10742, 338985, 975, 6, 0, 'C', 1, '2018-02-27 16:56:02', '2018-03-02 10:29:13'),
	(10758, 338995, 975, 6, 0, 'C', 1, '2018-02-27 18:56:03', '2018-03-05 16:54:22'),
	(10762, 338990, 975, 6, 2, NULL, 0, '2018-02-27 19:24:02', '0000-00-00 00:00:00'),
	(10769, 338996, 975, 6, 2, NULL, 0, '2018-02-28 06:00:03', '0000-00-00 00:00:00'),
	(10770, 338997, 975, 6, 0, 'C', 1, '2018-02-28 06:00:03', '2018-03-02 10:29:24'),
	(10881, 339033, 975, 6, 2, NULL, 0, '2018-02-28 19:48:01', '0000-00-00 00:00:00'),
	(10883, 339044, 893, 6, 0, 'C', 1, '2018-03-01 06:00:04', '2018-03-06 14:59:12'),
	(11035, 339103, 8002, NULL, 3, NULL, 0, '2018-03-02 10:48:01', '0000-00-00 00:00:00'),
	(11044, 339104, 820, 6, 0, 'C', 1, '2018-03-02 11:56:03', '2018-03-06 14:59:23'),
	(11134, 339141, 884, 6, 0, 'C', 1, '2018-03-03 09:08:02', '2018-03-06 14:59:33'),
	(11324, 339144, 801, 6, 1, NULL, 0, '2018-03-04 18:56:02', '0000-00-00 00:00:00'),
	(11376, 339217, 820, NULL, 3, NULL, 0, '2018-03-05 11:24:02', '0000-00-00 00:00:00'),
	(11419, 339239, 884, NULL, 3, NULL, 0, '2018-03-05 16:48:02', '0000-00-00 00:00:00'),
	(11555, 339299, 801, NULL, 3, NULL, 0, '2018-03-06 19:48:01', '0000-00-00 00:00:00');
/*!40000 ALTER TABLE `wms_rtnwh_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_slot_masterlist
CREATE TABLE IF NOT EXISTS `wms_slot_masterlist` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `slot_name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `slot_zone` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `store_num` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `slot_masterlist_slot_name_slot_zone_unique` (`slot_name`,`slot_zone`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_slot_masterlist: ~18 rows (approximately)
DELETE FROM `wms_slot_masterlist`;
/*!40000 ALTER TABLE `wms_slot_masterlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_slot_masterlist` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_store_masterlist
CREATE TABLE IF NOT EXISTS `wms_store_masterlist` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `store_num` int(11) NOT NULL,
  `store_nam` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `store_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `store_add1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `store_add2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `store_masterlist_store_num_unique` (`store_num`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_store_masterlist: ~56 rows (approximately)
DELETE FROM `wms_store_masterlist`;
/*!40000 ALTER TABLE `wms_store_masterlist` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_store_masterlist` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_store_rcv_dtl
CREATE TABLE IF NOT EXISTS `wms_store_rcv_dtl` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(11) NOT NULL,
  `sku` bigint(20) unsigned NOT NULL,
  `upc` bigint(20) unsigned NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_req` int(11) NOT NULL DEFAULT '0',
  `qty_rec` int(11) NOT NULL DEFAULT '0',
  `box_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `store_rcv_dtl_tl_no_sku_upc_unique` (`tl_no`,`sku`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_store_rcv_dtl: ~66,035 rows (approximately)
DELETE FROM `wms_store_rcv_dtl`;
/*!40000 ALTER TABLE `wms_store_rcv_dtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_store_rcv_dtl` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_store_rcv_list
CREATE TABLE IF NOT EXISTS `wms_store_rcv_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(11) NOT NULL,
  `ship_date` date DEFAULT NULL,
  `from_loc` int(11) NOT NULL,
  `to_loc` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `tl_status` tinyint(4) NOT NULL DEFAULT '3',
  `jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_jda_sync` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `store_rcv_list_tl_no_to_loc_from_loc_unique` (`tl_no`,`to_loc`,`from_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_store_rcv_list: ~3,184 rows (approximately)
DELETE FROM `wms_store_rcv_list`;
/*!40000 ALTER TABLE `wms_store_rcv_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_store_rcv_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_box_detail
CREATE TABLE IF NOT EXISTS `wms_subloc_box_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` bigint(20) NOT NULL,
  `box_no` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `mov_qty` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subloc_box_detail_tl_no_box_no_upc_index` (`tl_no`,`box_no`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_box_detail: ~1,061 rows (approximately)
DELETE FROM `wms_subloc_box_detail`;
/*!40000 ALTER TABLE `wms_subloc_box_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_box_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_box_list
CREATE TABLE IF NOT EXISTS `wms_subloc_box_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `box_no` bigint(20) NOT NULL,
  `tl_no` int(11) NOT NULL,
  `from_store_num` int(11) NOT NULL,
  `store_num` int(11) NOT NULL,
  `is_assign` int(11) NOT NULL DEFAULT '0',
  `is_open` varchar(255) COLLATE utf8_unicode_ci NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subloc_box_list_box_no_tl_no_index` (`box_no`,`tl_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_box_list: ~73 rows (approximately)
DELETE FROM `wms_subloc_box_list`;
/*!40000 ALTER TABLE `wms_subloc_box_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_box_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_load_detail
CREATE TABLE IF NOT EXISTS `wms_subloc_load_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `load_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `box_no` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `tl_no` int(11) NOT NULL,
  `from_store_num` int(11) DEFAULT NULL,
  `store_num` int(11) DEFAULT NULL,
  `is_assign_store` int(11) DEFAULT NULL,
  `is_box_status` int(11) NOT NULL DEFAULT '2',
  `is_load` int(11) NOT NULL DEFAULT '0',
  `is_sync` int(11) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `subloc_load_detail_box_no_tl_no_index` (`box_no`,`tl_no`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_load_detail: ~28 rows (approximately)
DELETE FROM `wms_subloc_load_detail`;
/*!40000 ALTER TABLE `wms_subloc_load_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_load_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_load_list
CREATE TABLE IF NOT EXISTS `wms_subloc_load_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `load_code` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `seal_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `plate_no` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `delivery_helper` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `driver_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `witnessed_sealed` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `ship_date` date DEFAULT NULL,
  `assigned_to_store` int(11) DEFAULT NULL,
  `is_sync` int(11) NOT NULL DEFAULT '0',
  `load_status` int(11) NOT NULL DEFAULT '3',
  `load_status_store` int(11) NOT NULL DEFAULT '2',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subloc_load_list_load_code_unique` (`load_code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_load_list: ~9 rows (approximately)
DELETE FROM `wms_subloc_load_list`;
/*!40000 ALTER TABLE `wms_subloc_load_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_load_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_pick_detail
CREATE TABLE IF NOT EXISTS `wms_subloc_pick_detail` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` bigint(20) NOT NULL,
  `ship_date` date NOT NULL,
  `from_loc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `to_loc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `dept_code` int(11) NOT NULL,
  `dept_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sku` bigint(20) NOT NULL,
  `upc` bigint(20) NOT NULL,
  `short_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_desc` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_size` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_color` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `rtl_price` double(8,2) NOT NULL,
  `qty_req` int(11) NOT NULL,
  `qty_rcv` int(11) NOT NULL DEFAULT '0',
  `remarks` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subloc_pick_detail_tl_no_sku_upc_unique` (`tl_no`,`sku`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_pick_detail: ~1,371 rows (approximately)
DELETE FROM `wms_subloc_pick_detail`;
/*!40000 ALTER TABLE `wms_subloc_pick_detail` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_pick_detail` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_pick_list
CREATE TABLE IF NOT EXISTS `wms_subloc_pick_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(11) NOT NULL,
  `ship_date` date DEFAULT NULL,
  `from_loc` int(11) NOT NULL,
  `to_loc` int(11) NOT NULL,
  `assigned_to` int(11) DEFAULT NULL,
  `picked_date` date DEFAULT NULL,
  `pick_status` tinyint(4) NOT NULL DEFAULT '3',
  `jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_jda_sync` tinyint(4) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subloc_pick_list_tl_no_to_loc_from_loc_unique` (`tl_no`,`to_loc`,`from_loc`)
) ENGINE=InnoDB AUTO_INCREMENT=112 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_pick_list: ~54 rows (approximately)
DELETE FROM `wms_subloc_pick_list`;
/*!40000 ALTER TABLE `wms_subloc_pick_list` DISABLE KEYS */;
INSERT INTO `wms_subloc_pick_list` (`id`, `tl_no`, `ship_date`, `from_loc`, `to_loc`, `assigned_to`, `picked_date`, `pick_status`, `jda_status`, `is_jda_sync`, `created_at`, `updated_at`) VALUES
	(1, 332543, '2017-10-12', 80011, 820, 34, '2017-10-12', 0, 'S', 1, '2017-10-12 14:25:27', '2017-10-12 16:00:21'),
	(2, 332548, '2017-10-12', 80011, 820, 18, '2017-10-12', 0, 'S', 1, '2017-10-12 14:45:28', '2017-10-12 16:02:28'),
	(3, 332549, '2017-10-12', 80011, 820, 34, '2017-10-12', 0, 'S', 1, '2017-10-12 14:55:21', '2017-10-12 16:04:21'),
	(5, 332552, '2017-10-12', 80011, 820, 34, '2017-10-12', 0, 'S', 1, '2017-10-12 15:14:33', '2017-10-12 16:11:17'),
	(6, 332553, '2017-10-12', 80011, 820, 18, '2017-10-12', 0, 'S', 1, '2017-10-12 15:14:33', '2017-10-12 16:13:19'),
	(8, 332555, '2017-10-12', 80011, 820, 18, '2017-10-12', 0, 'S', 1, '2017-10-12 15:44:27', '2017-10-12 16:19:12'),
	(9, 334653, '2017-11-27', 80011, 975, 22, '2017-11-27', 0, 'S', 1, '2017-11-27 10:37:30', '2017-11-27 11:05:18'),
	(10, 334654, '2017-11-27', 80011, 975, 22, '2017-11-27', 0, 'W', 1, '2017-11-27 10:37:30', '2017-11-27 11:07:14'),
	(12, 334683, '2017-11-27', 80011, 975, 22, '2017-11-27', 0, 'S', 1, '2017-11-27 14:40:32', '2017-11-27 15:02:20'),
	(13, 335027, '2017-12-04', 80011, 975, 10, '2017-12-04', 0, 'S', 1, '2017-12-04 11:20:04', '2017-12-04 11:48:18'),
	(29, 335211, '2017-12-08', 80011, 975, 9, '2017-12-08', 0, 'S', 1, '2017-12-08 09:00:50', '2017-12-08 10:17:25'),
	(30, 335212, '2017-12-08', 80011, 975, 9, '2017-12-08', 0, 'S', 1, '2017-12-08 09:14:13', '2017-12-08 14:14:12'),
	(32, 335214, '2017-12-08', 80011, 975, 9, '2017-12-08', 0, 'W', 1, '2017-12-08 10:37:23', '2017-12-08 14:15:26'),
	(39, 335378, '2017-12-11', 80011, 975, 9, NULL, 2, NULL, 0, '2017-12-11 08:27:48', '0000-00-00 00:00:00'),
	(40, 335379, '2017-12-11', 80011, 975, 9, NULL, 2, NULL, 0, '2017-12-11 09:28:57', '0000-00-00 00:00:00'),
	(41, 335773, '2017-12-18', 80011, 975, 9, '2017-12-18', 0, 'S', 1, '2017-12-18 09:09:59', '2017-12-18 10:22:14'),
	(42, 335775, '2017-12-18', 80011, 975, 9, '2017-12-18', 0, 'S', 1, '2017-12-18 09:25:16', '2017-12-18 10:56:23'),
	(43, 335776, '2017-12-18', 80011, 975, 16, NULL, 2, NULL, 0, '2017-12-18 09:35:43', '0000-00-00 00:00:00'),
	(44, 335793, '2017-12-18', 80011, 975, 10, NULL, 2, NULL, 0, '2017-12-18 11:34:21', '0000-00-00 00:00:00'),
	(45, 335794, '2017-12-18', 80011, 975, 10, NULL, 2, NULL, 0, '2017-12-18 11:34:21', '0000-00-00 00:00:00'),
	(48, 335813, '2017-12-18', 80011, 975, 10, NULL, 2, NULL, 0, '2017-12-18 14:14:55', '0000-00-00 00:00:00'),
	(49, 335822, '2017-12-18', 80011, 975, 10, NULL, 2, NULL, 0, '2017-12-18 14:25:55', '0000-00-00 00:00:00'),
	(50, 336041, '2017-12-21', 80011, 820, 34, '2017-12-21', 0, 'S', 1, '2017-12-21 14:05:59', '2017-12-21 17:27:14'),
	(51, 336042, '2017-12-21', 80011, 975, 9, '2017-12-21', 1, NULL, 0, '2017-12-21 14:05:59', '0000-00-00 00:00:00'),
	(57, 336049, '2017-12-21', 80011, 820, 34, '2017-12-21', 0, 'S', 1, '2017-12-21 14:38:53', '2017-12-21 17:27:28'),
	(63, 336050, '2017-12-21', 80011, 801, 18, '2017-12-21', 0, 'S', 1, '2017-12-21 14:47:11', '2017-12-21 17:09:20'),
	(65, 336051, '2017-12-21', 80011, 975, NULL, NULL, 3, NULL, 0, '2017-12-21 15:25:07', '0000-00-00 00:00:00'),
	(66, 336054, '2017-12-21', 80011, 975, NULL, NULL, 3, NULL, 0, '2017-12-21 15:25:07', '0000-00-00 00:00:00'),
	(67, 336058, '2017-12-21', 80011, 801, 18, '2017-12-21', 0, 'S', 1, '2017-12-21 15:25:07', '2017-12-21 17:09:38'),
	(68, 336063, '2017-12-21', 80011, 820, 34, '2017-12-21', 0, 'S', 1, '2017-12-21 15:43:41', '2017-12-21 17:27:45'),
	(69, 336068, '2017-12-21', 80011, 801, 18, '2017-12-21', 0, 'S', 1, '2017-12-21 16:54:42', '2017-12-21 17:10:08'),
	(70, 336069, '2017-12-21', 80011, 820, 34, '2017-12-21', 0, 'S', 1, '2017-12-21 16:54:42', '2017-12-21 17:28:06'),
	(76, 336109, '2017-12-22', 80011, 893, 16, '2017-12-22', 1, NULL, 0, '2017-12-22 07:17:10', '0000-00-00 00:00:00'),
	(77, 336110, '2017-12-22', 80011, 884, 16, NULL, 2, NULL, 0, '2017-12-22 07:33:06', '0000-00-00 00:00:00'),
	(78, 336111, '2017-12-22', 80011, 884, 16, '2017-12-22', 1, NULL, 0, '2017-12-22 07:33:06', '0000-00-00 00:00:00'),
	(80, 336114, '2017-12-22', 80011, 884, 13, NULL, 2, NULL, 0, '2017-12-22 08:18:39', '0000-00-00 00:00:00'),
	(81, 336115, '2017-12-22', 80011, 893, NULL, NULL, 3, NULL, 0, '2017-12-22 08:18:39', '0000-00-00 00:00:00'),
	(83, 336065, '2017-12-22', 80011, 893, 32, '2017-12-22', 1, NULL, 0, '2017-12-22 08:35:29', '0000-00-00 00:00:00'),
	(84, 336116, '2017-12-22', 80011, 884, 13, NULL, 2, NULL, 0, '2017-12-22 08:35:29', '0000-00-00 00:00:00'),
	(87, 336117, '2017-12-22', 80011, 893, 32, '2017-12-22', 1, NULL, 0, '2017-12-22 08:39:15', '0000-00-00 00:00:00'),
	(88, 336118, '2017-12-22', 80011, 884, 13, NULL, 2, NULL, 0, '2017-12-22 08:52:00', '0000-00-00 00:00:00'),
	(89, 336123, '2017-12-22', 80011, 975, 9, NULL, 2, NULL, 0, '2017-12-22 11:55:49', '0000-00-00 00:00:00'),
	(90, 336132, '2017-12-22', 80011, 884, NULL, NULL, 3, NULL, 0, '2017-12-22 14:11:27', '0000-00-00 00:00:00'),
	(91, 336133, '2017-12-22', 80011, 884, NULL, NULL, 3, NULL, 0, '2017-12-22 14:11:27', '0000-00-00 00:00:00'),
	(92, 336130, '2017-12-22', 80011, 884, NULL, NULL, 3, NULL, 0, '2018-01-24 16:06:54', '0000-00-00 00:00:00'),
	(93, 336288, '2017-12-27', 80011, 975, NULL, NULL, 3, NULL, 0, '2018-01-24 16:06:54', '0000-00-00 00:00:00'),
	(94, 336846, '2018-01-05', 80012, 80014, NULL, NULL, 3, NULL, 0, '2018-01-24 16:06:54', '0000-00-00 00:00:00'),
	(95, 338409, '2018-02-14', 80011, 975, 9, '2018-02-14', 1, NULL, 0, '2018-02-14 11:30:07', '0000-00-00 00:00:00'),
	(98, 339282, '2018-03-07', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 09:22:25', '2018-03-06 14:01:19'),
	(101, 339283, '2018-03-07', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 13:10:23', '2018-03-06 14:19:13'),
	(104, 339290, '2018-03-06', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 13:56:45', '2018-03-06 14:40:15'),
	(107, 339291, '2018-03-07', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 14:07:46', '2018-03-06 15:07:11'),
	(110, 339293, '2018-03-06', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 14:44:22', '2018-03-06 15:27:13'),
	(111, 339292, '2018-03-07', 80011, 975, 16, '2018-03-06', 0, 'S', 1, '2018-03-06 14:44:22', '2018-03-06 15:27:27');
/*!40000 ALTER TABLE `wms_subloc_pick_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_rcv_dtl
CREATE TABLE IF NOT EXISTS `wms_subloc_rcv_dtl` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(10) unsigned NOT NULL,
  `sku` bigint(20) unsigned NOT NULL,
  `upc` bigint(20) NOT NULL,
  `short_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_ord` smallint(5) unsigned NOT NULL,
  `qty_rcv` smallint(5) unsigned NOT NULL,
  `remarks` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subloc_rcv_dtl_tl_no_sku_upc_unique` (`tl_no`,`sku`,`upc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_rcv_dtl: ~4,393 rows (approximately)
DELETE FROM `wms_subloc_rcv_dtl`;
/*!40000 ALTER TABLE `wms_subloc_rcv_dtl` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_rcv_dtl` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_subloc_rcv_list
CREATE TABLE IF NOT EXISTS `wms_subloc_rcv_list` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tl_no` int(10) unsigned NOT NULL,
  `ship_date` date NOT NULL,
  `from_loc` int(10) unsigned NOT NULL,
  `to_loc` int(10) unsigned NOT NULL,
  `assigned_to` smallint(5) unsigned DEFAULT NULL,
  `tl_status` smallint(5) unsigned NOT NULL DEFAULT '3',
  `jda_status` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_jda_sync` smallint(5) unsigned NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `subloc_rcv_list_tl_no_ship_date_from_loc_to_loc_unique` (`tl_no`,`ship_date`,`from_loc`,`to_loc`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_subloc_rcv_list: ~182 rows (approximately)
DELETE FROM `wms_subloc_rcv_list`;
/*!40000 ALTER TABLE `wms_subloc_rcv_list` DISABLE KEYS */;
/*!40000 ALTER TABLE `wms_subloc_rcv_list` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_users
CREATE TABLE IF NOT EXISTS `wms_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `role_id` int(11) NOT NULL,
  `store_code` int(11) DEFAULT '0',
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `users_id_role_id_index` (`id`,`role_id`)
) ENGINE=InnoDB AUTO_INCREMENT=58 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_users: ~47 rows (approximately)
DELETE FROM `wms_users`;
/*!40000 ALTER TABLE `wms_users` DISABLE KEYS */;
INSERT INTO `wms_users` (`id`, `username`, `password`, `firstname`, `lastname`, `role_id`, `store_code`, `remember_token`, `created_at`, `updated_at`) VALUES
	(1, 'superadmin', '$2y$10$8s8sr1k3EpozBFW42hYTO.F.mkX2pSzYW1wXgTkYyWWDhOqmNkjUy', 'superadmin', 'superadmin', 1, 8001, 'L9mV4sR61OrMAlkVX42yU01TJaxWNaUOdMPRfpEHykcZ86FRYekN9uLzJZso', '2017-09-02 15:43:23', '2018-04-14 13:48:48'),
	(3, 'stockpiler', '$2y$10$nA8hhWhMLfeOuDghSAw7POIn9nav4UvCk6Ajip2VZs7kpbLDPAzOa', 'stock', 'piler', 1, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'whvisor', '$2y$10$zKL04WCaAY0XB4x8hF0/9OJUFkjXmMslEEqYzkbG8ncvjBM4.nAg.', 'warehouse', 'supervisor', 2, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'gnavila', '$2y$10$kmyHs7PG7iyjroERPpMZneNKLk/PnfD/IyIbXVehLX3.1EIP9SN..', 'greg', 'avila', 3, 8001, 'ArDwtWJjr7oH81mOkHP24ETavXA37xzV8dcZlvsdBC50Z9e4XS6SKDPfdKCz', '2017-08-09 04:11:47', '2018-04-14 08:11:05'),
	(6, 'yanbu', '$2y$10$2C.tcjtZ6NnLf0bwxStW1.TwJnIURYdK0NrROk15W.Urdzu.GKki.', 'yanbu', 'cutarra', 6, 8001, '', '0000-00-00 00:00:00', '2017-08-09 04:15:01'),
	(7, 'strhead', '$2y$10$6GiVRNm1EN67ZNWkSv7FG.fVlyi1/Dn7MpH9mlSjXiRQ0hWHBHt4e', 'store head ', 'sample', 3, 975, 'ON1BlXmeXbrg6KK2C27DBfoYThjFdENUJWehRK6ngZHKC1L7u00Lk2pDEsqT', '2017-08-13 14:17:04', '2017-10-04 10:02:46'),
	(8, 'strclerk', '$2y$10$y71Iu59BlpxUyRd70SJE..dnJG/qSPUEtt8aAu5OIMokJhYCz1DCm', 'clerk', 'sample', 4, 0, '', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(9, 'rpena', '$2y$10$Tq9n2COTd4Kt.TUNu5ojquGX8dnI7UBh4DBCP6WT7GlbXs6xiZxBe', 'rholemar', 'pena', 6, 8001, '', '0000-00-00 00:00:00', '2017-08-20 13:31:10'),
	(10, 'rcurimao', '$2y$10$gCVV2r6.b83wVfHF9rELke/09VA.3Z22vH1CIHP2otOVqDEdO.jxi', 'ryan', 'curimao', 6, 8001, '', '0000-00-00 00:00:00', '2017-08-20 13:33:11'),
	(13, 'lcawit', '$2y$10$aAtSi4uLoRGkNzG4WsmPUeeffzQn7FCTXS/XUzDbM134Nfiv5ig2.', 'leonil', 'cawit', 6, 8001, '', '0000-00-00 00:00:00', '2017-08-20 13:35:45'),
	(14, 'afsalvador', '$2y$10$MWN/Xeej0/P8z3Lc0g3GS.lSMengCCgMIFK0AJkTHSoFgu6UBTcg6', 'Adrian', 'Salvador', 4, 8001, 'W9mhBmTlXzX8S7IXsugWrMrDSGGCpxUguD3RMVWgqNrwT1xvl0yDUZN9iWwS', '2017-09-15 03:42:33', '2018-02-21 13:43:55'),
	(15, 'puhernando', '$2y$10$g4e.h3HvIAnFDL6HR4NdPOTWrWHXYvuFExyoXGQ7E.K2fuHtxLGaa', 'Pacifico', 'Hernando', 4, 8001, 'ZLqOQauZbF98YalV1Fr3MvN0cbxJe5erena4XKffYy9R01OacCpVZbOw0GNh', '2017-09-15 03:43:14', '2017-11-10 15:32:42'),
	(16, 'alizarda', '$2y$10$WTeODAhkVZkUVroxAfC5Y.Y7bxp6591/Fyr5eOaakG0CrPLa8Pg4e', 'Allan Carlo', 'Lizarda', 6, 8001, '', '0000-00-00 00:00:00', '2017-09-19 05:49:22'),
	(17, 'amontenegro', '$2y$10$VBKiNcyVGKfdwLAIWeNKBOtZaJbf5ikUkBXf1JIfnyLawPSO3.fPu', 'Arnel', 'Montenegro', 6, 8001, '', '0000-00-00 00:00:00', '2017-09-19 05:50:13'),
	(18, 'ggallenero', '$2y$10$aV6LBxN4HS.wOyKMNXqtzeNGyxK5KpTvuiSM0mQx7ye1g6rz6jW6W', 'Gil', 'Gallenero', 6, 8001, '', '0000-00-00 00:00:00', '2017-09-19 05:53:26'),
	(19, 'msd', '$2y$10$btb97ZgLitRNzwKYQwBoquzNpG2PvvxPwn2SVnatpD4Cnl/Lr/MSK', 'Management', 'SD', 1, 8001, 'Co1q8k2dXl7MNyVH6PZ54PqWz84dZKeHpw0yIvPTENegisCZPmF06BbTkTtc', '2017-09-21 02:36:07', '2017-12-20 08:15:17'),
	(20, 'aliguan', '$2y$10$FyYsJ4lWJPPndqhhH2K30OycbufkTHQXV6yZGUW9EcNff5o7bdY2O', 'Abigail', 'Liguan', 3, 8001, '', '0000-00-00 00:00:00', '2017-09-25 02:50:53'),
	(21, 'jnotarte', '$2y$10$Ro80g5jC0GVz.yiT0aJ2/OGS/3rQBUYR3yPNGun2GDFpGRc54Qdyi', 'Julius', 'Notarte ', 6, 8001, '', '0000-00-00 00:00:00', '2017-09-25 03:00:26'),
	(23, 'jgabasa', '$2y$10$vkLy7FeMvFUbUphzUDF3xOYWk.DQt4DlczjA3i7SXNn5zrmNw5paS', 'Joann ', 'Gabasa', 5, 8001, 'yelzjtJKraXQ5rL54d1HDFQQY5a7Iy6N70rNJAsnqN7QlUcNP6Ztk0aPZxTY', '2017-09-25 08:41:49', '2018-02-13 16:54:12'),
	(24, 'gmojica', '$2y$10$Y92oHJAXzcQhNq/c.IvWj.oMV8LEfNQjcYvqBDnKEx7ELMkWTht52', 'Gazelle', 'Mojica', 5, 8001, 'SrQaqKlk7Js4oIMDHtZ7QqzjIZCF4DlDQnBtD7TgD2de7Z0KfUHvbNFKJuba', '2017-09-25 08:42:48', '2017-10-26 19:18:19'),
	(25, 'alopez', '$2y$10$AfG1wdDgEWAEU.nwdJojnOLVIJjvlGQ2fhdB9Bz1Ar0SKeXXJemAS', 'Aurora', 'Lopez', 2, 8001, '', '0000-00-00 00:00:00', '2017-09-25 08:43:33'),
	(26, 'jmanreal', '$2y$10$Ca2tz9pLoXxPumlyAAsWl.sToPTzdpjYh8xxpyE9FWeDLyoF4Oej2', 'Jeffrey', 'Manreal', 6, 8001, 'osuvskOFUELeH8mljEf0w7VNARFF8Hfl7TtKJhAxoJ76Ki1nQhABqY6YR94O', '2017-09-26 08:21:10', '2017-10-25 18:33:14'),
	(27, 'oguerzon', '$2y$10$9fY8tYw4QyYcVhWIbSZ6We2j660apFNJbzCIzI23iNc.MqYuvLsWC', 'olan', 'guerzon', 2, 8001, 'fx15MbBNJN2cDJPDdHzq0SC9OoOgHi9YRUyAIm0avB5RQvX6G8HoAgYo0vms', '2017-09-27 05:38:51', '2017-10-05 09:35:33'),
	(28, 'debs storehead', '$2y$10$yT2qVL5wc/zMXfUFRR1/6eaY.gnMY4GcReRKwNT.wcHgGS8Lyggra', 'debs', 'shang', 7, 820, 'Ts5VOfIjIvYyut9LV9gCcGXP1tOtuhY0hP53gGUYZMBRM0S35kt5DkcnghHN', '2017-10-04 10:47:23', '2018-02-21 10:23:47'),
	(31, '1657', '$2y$10$n.I.p3jvN5vJSKH4Anuj6.052vk.CTarhKcN9VX59jp5x.zJP04xK', 'anabelle', 'egido', 8, 820, '', '0000-00-00 00:00:00', '2017-10-04 11:06:28'),
	(32, 'cpenamante', '$2y$10$QLFKyOTFruNBRDmgybaWr.j1NbWGVr4GjltrgwPbw5eXXoZLUXrxy', 'Charlie', 'Penamante', 6, 8001, '', '0000-00-00 00:00:00', '2017-10-09 13:34:38'),
	(34, 'ajquinia', '$2y$10$jsurCAepSRMjb1raGmZjVurIEI1ghyh7/Jd0kjo0usnBwaLwQHA9q', 'Armand John', 'Quinia', 6, 8001, '', '0000-00-00 00:00:00', '2017-10-09 15:19:44'),
	(35, '2019', '$2y$10$Gi3TcquwFzhl1WKOYkDOkO4e6g.JdGqO6WuJIABv2gOU2UQdNc4Le', 'Jennifer', 'Balacanao', 8, 820, NULL, '2017-11-27 12:50:56', '2017-11-27 12:50:56'),
	(36, '0104', '$2y$10$qPc/NZe1yB8kgU0YddYQYOXltpAkKrtxnyCLrE1vAJAwg4VfOU97i', 'jasmin', 'damil', 7, 820, 'kamzHaXiwkOf82TNS3o3oRHRdkS8QimN4sEuLo0z9n0t87PfNpOCYBh42J3F', '2017-11-27 13:12:12', '2018-03-05 12:11:48'),
	(37, '1016', '$2y$10$uB44RGhT4eL7Cedxhc25fOzkU5Xn.rCjQZ4kUYpInQwHjMG1Jh/V2', 'Lendle', 'Ngan', 8, 820, NULL, '2017-11-29 11:53:40', '2017-11-29 11:53:40'),
	(38, '01916', '$2y$10$lUDFTJIs5FQ192BUYKH7DuFLVIrZUo.IDRSGYio5bcG2Kr0LnWFQW', 'Chiara Janina', 'Del Castillo', 7, 820, '4HbuOKKS2KNub7q2J0tVh5ib2rMYXogdEWugltqqo6LH2jsTHGU85mvd32xF', '2017-11-29 13:48:04', '2017-12-11 13:49:48'),
	(39, '1218', '$2y$10$8NQK9UvXCiInZDoUvJ56sO9.b8mJhHpdS.0a1ZYFr99ciWV112I/y', 'Virgilio', 'Masangkay Jr.', 8, 820, NULL, '2017-12-04 12:47:12', '2017-12-04 12:47:12'),
	(40, '1748', '$2y$10$Bsw/zbYPbx/jjar7qeDlr.AYmvglgW0Ba2bSPGdNXplzYkzin7JoC', 'Angely', 'Yongzon', 8, 820, NULL, '2017-12-06 12:06:12', '2017-12-06 12:13:53'),
	(41, '1432', '$2y$10$7tdPYmkHJf7znsYC1gFJJeEZXZNIfo1wobv.uDNLjKVwherKv6msG', 'KHENLIE', 'APIL', 8, 820, NULL, '2017-12-08 13:23:01', '2017-12-08 13:23:01'),
	(42, '1982', '$2y$10$CZTQQwZfsRnd5ifWnTgCleHIWZzUB3ymLMMguYdtzNfXbQBX2l9/m', 'Jose Bryant', 'Correa', 7, 820, 'YD1nlNNZ2CUdPfvDS0mXCDDcrrZMFYiPfGPPyd1UNyWH7wwxQAmq9TDcxDhQ', '2017-12-10 18:19:48', '2018-04-14 14:31:18'),
	(44, '1732', '$2y$10$HsDWxOy8fyuvtiQ2xtXDxuBKYkc7Suz6ffr5SGoQav66Eixo9PPbS', 'Juancho Jr', 'Doroja', 8, 820, NULL, '2017-12-11 13:01:20', '2017-12-11 13:01:20'),
	(45, '1594', '$2y$10$osLERxDQEUtf8sc/t84.HukBk17gndrpxEuv4lIgp1dsLI3qzAEcC', 'mark', 'miole', 8, 820, NULL, '2017-12-13 14:06:01', '2017-12-13 14:06:01'),
	(46, '897', '$2y$10$3zaloz6aMMLvYoBFlbUBNuLo2NtgTtm6JFikGe23HibvaUh3IoOkG', 'Jersey', 'Vinculado', 8, 820, NULL, '2017-12-18 12:26:08', '2017-12-18 12:26:08'),
	(47, '0598', '$2y$10$1zulD83arQl3r27HPU14J.gml9U7VdbspLkVXPonXczRB22bvW4CC', 'Gelyn', 'Mallari', 8, 820, NULL, '2017-12-22 12:29:54', '2017-12-22 12:29:54'),
	(48, 'jbguangco', '$2y$10$ecbetwiB8kaGey3lBIRP9OuVDNi19eJ1W.DMi.pAmX63GBaEitfGq', 'Jessie', 'Guangco', 1, 820, NULL, '2017-12-22 16:19:22', '2017-12-22 16:19:22'),
	(49, '326', '$2y$10$6kvSe3JQjKj10zRyyJV2veJpmMKXcJOoQzsO/CIIaYWA2r8Lyllky', 'kheil', 'samson', 8, 820, NULL, '2017-12-29 13:56:28', '2017-12-29 13:56:28'),
	(50, '0217', '$2y$10$N88KWP4ifFOz.XqtbIoS5Oq3B9kP4epav7EED6I7X4R7psECgx.fW', 'JOANN', 'MARTINEZ', 8, 820, NULL, '2018-01-08 12:50:01', '2018-01-08 12:50:01'),
	(52, 'itprojects', '$2y$10$3bBJimcmEpSEDIjU9a2dZOimD9Rgu629g0rr9rCOjEqAm/EpEuuNi', 'IT', 'Projects', 1, 8001, 'E2PkZW2ZBNvt3dQzf6MXijsTG8T119f4TsBp1XynMDNkcrryhw4i1smruSEk', '2018-01-22 10:43:47', '2018-01-22 14:32:13'),
	(53, '1625', '$2y$10$GAms1GHSwaaqyOo5vT7ZdOnbUEEqDOai9eoiiiQkqkKrVuxH3b2RG', 'Michelle', 'Villarta', 8, 820, NULL, '2018-01-22 14:11:16', '2018-01-22 14:11:16'),
	(55, '1733', '$2y$10$geEa2WelD819kKOBbNt11ekPcZiJD0Yg0zFmRmNZn77sIiaUKDctS', 'allena', 'geronimo', 8, 820, NULL, '2018-01-31 12:56:32', '2018-01-31 12:56:32'),
	(56, '0455', '$2y$10$ljS5Hyhn.nFc9ndkY.Z0oe6D2xS/VrKf1mLSzTRBbl/GjngGOerGC', 'rea', 'muyo', 8, 820, NULL, '2018-02-14 14:35:30', '2018-02-14 14:35:30'),
	(57, 'rdistor', '$2y$10$Ox8rAwyxPMH68JtGC3kWFuSiwBfT29WjEaJK7uWtxPWjsA4WlAIce', 'Rhobbie', 'Distor', 6, 8001, NULL, '2018-02-19 08:27:09', '2018-02-19 08:27:09');
/*!40000 ALTER TABLE `wms_users` ENABLE KEYS */;

-- Dumping structure for table debs_ewms.wms_user_roles
CREATE TABLE IF NOT EXISTS `wms_user_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- Dumping data for table debs_ewms.wms_user_roles: ~8 rows (approximately)
DELETE FROM `wms_user_roles`;
/*!40000 ALTER TABLE `wms_user_roles` DISABLE KEYS */;
INSERT INTO `wms_user_roles` (`id`, `role_name`, `permissions`, `created_at`, `updated_at`) VALUES
	(1, 'Superadmin', 'canViewPurchaseOrder,CanAccessPacking,CanAccessShipping,CanAccessSublocReceiving,CanAccessSublocPicking,CanAccessSublocShipping,CanAccessReturnToWh,CanAccessStoreOrders,CanAccessProductMasterList,CanAccessStoreMasterList,CanAccessUsers,CanAccessUserRoles,CanAccessAuditTrail', '0000-00-00 00:00:00', '2018-01-22 10:41:47'),
	(2, 'Warehouse Manager', 'canViewPurchaseOrder,CanAccessPacking,CanAccessShipping,CanAccessSublocReceiving,CanAccessSublocPicking,CanAccessSublocShipping,CanAccessReturnToWh,CanAccessProductMasterList,CanAccessStoreMasterList,CanAccessUsers,CanAccessUserRoles,CanAccessAuditTrail', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(3, 'Warehouse Supervisor', 'canViewPurchaseOrder,CanAccessPacking,CanAccessShipping,CanAccessSublocReceiving,CanAccessSublocPicking,CanAccessSublocShipping,CanAccessReturnToWh,CanAccessProductMasterList,CanAccessStoreMasterList,CanAccessUsers,CanAccessAuditTrail', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(4, 'Inbound Logistics', 'canViewPurchaseOrder,CanAccessPacking,CanAccessSublocReceiving,CanAccessSublocPicking,CanAccessReturnToWh,CanAccessProductMasterList,CanAccessStoreMasterList', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(5, 'Outbound Logistics', 'CanAccessShipping,CanAccessSublocShipping', '0000-00-00 00:00:00', '2017-09-30 09:13:45'),
	(6, 'Warehouse Stockpiler', 'canViewPurchaseOrder,CanAccessPacking,CanInsertUserRoles', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
	(7, 'Store Head', 'CanAccessStoreOrders,CanAccessUsers', '0000-00-00 00:00:00', '2017-09-30 17:45:15'),
	(8, 'Store Clerk', 'CanAccessStoreOrders', '2017-09-30 17:58:01', '2017-09-30 17:58:01');
/*!40000 ALTER TABLE `wms_user_roles` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
