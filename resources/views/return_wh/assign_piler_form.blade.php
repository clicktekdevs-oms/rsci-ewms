@extends('layouts.main')
@section('content')

<div class="widget">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Assign to Stock Piler</h3>
    </div>


    <div class="widget-content">
   {!! Form::open(array('url'=>'returnwh/assign-to-piler', 'method' => "post", 'class'=>'form-horizontal', 'style' => 'margin: 0px;', 'role'=>'form')) !!}

		<div class="span3">&nbsp;</div>
		<div class="span7 add-piler-wrapper">
			<div class="control-group">
				<label class="control-label">MTS no. :</label>
				<div class="controls">
					<textarea style="resize: none" readonly rows="5">
						{!! $mts_no !!}
					</textarea>
					<input type="hidden" value="{!! $mts_no !!}" name="mts_no">
				</div> <!-- /controls -->
			</div> <!-- /control-group -->

			<div class="control-group">
				<label class="control-label"> Stock Piler :</label>
				<div class="controls">
					<select name="piler">
						<option hidden value="">Select :</option>
						@foreach( $pilers as $piler )
							<option value="{!! $piler->id !!}">{!! ucwords($piler->name) !!}</option>
						@endforeach
					</select>
				</div> <!-- /controls -->
			</div> <!-- /control-group -->

        </div>
        <!-- <div class="span10">&nbsp;</div> -->
        <div class="span3">&nbsp;</div>
        <div class="span7">
        	<div class="control-group">
				<label class="control-label" for=""></label>
				<div class="controls">
					<button type="submit" class="btn btn-info"> Assign </button>
					<a class="btn" href="#">Cancel</a>
				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		</div>

	{!! Form::close() !!}
       
	</div>
</div>
@endsection
