@extends('layouts.main')
@section('content')
<div class="control-group">
	<a href="{!! url('returnwh') !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i> Back to List</a>
</div>

<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #;">
             <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">

			      	<div class="span4">
			        	<div>
				        	<span class="search-po-left-pane"> MTS no.:</span>
				        	<span class="search-po-right-pane">
								<input type="text" readonly value="{!! $mts_list->mts_no !!}">
				        	</span>
				        </div>
			        	<div>
				        	<span class="search-po-left-pane"> Piler Name:</span>
				        	<span class="search-po-right-pane">
								<input type="text" readonly value="{!! ucwords($mts_list->assigned_to) !!}">
				        	</span>
				        </div>
			      	</div>
            </div>
          
          </div>
      	</div>

	</div> <!-- /controls -->
</div> <!-- /control-group -->


<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3> Return to Warehouse Details</h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>No.</th>
					 	 <th>SKU</th>
						<th>UPC</th>
						<th>Item Description</th>
						<th>QTY SHIP</th>
						<th>QTY RECEIVE</th>
						<th>Variance</th>
						<th>Remarks</th>
					</tr>
				</thead>
				@if( !CommonHelper::arrayHasValue( $mts_detail ) )
				<tr class="font-size-13">
					<td colspan="10" class="align-center" style="background-color:#f6f6f6">No records found!</td>
				</tr>
				@else
					@foreach( $mts_detail as $key => $detail )
						<tr style="{!! $detail->qty_rcv != $detail->qty_req ? 'background-color: #f2dede;' : '' !!}">
							<td>{!! $key+1 !!}</td>
							<td>{!! $detail->sku !!}</td>
							<td>{!! $detail->upc !!}</td>
							<td>{!! $detail->item_desc !!}</td>
							<td>{!! $detail->qty_req !!}</td>
							<td>
								@if($mts_list->mts_status == 1)
									{!! Form::open(array('url' => 'returnwh/update/qty', 'method' => 'post')) !!}
									<div class="input-group">
										<input type="text" value="{!! $detail->qty_rcv !!}" name="qty_rcv">
										<input type="hidden" name="upc" value="{!! $detail->upc !!}">
										<input type="hidden" name="mts_no" value="{!! $mts_list->mts_no !!}">
										<input type="hidden" name="sku" value="{!! $detail->sku !!}">
										<button type="submit" class="btn btn-primary" title="Update Quantity"><span class="icon-check"></span> </button>
									</div>
									{!! Form::close() !!}
								@else
									{!! $detail->qty_rcv !!}
								@endif
							</td>
							<td>{!! $detail->qty_rcv - $detail->qty_req !!}</td>
							<td>{!! $detail->remarks !!}</td>
						</tr>
					@endforeach
				@endif
					{{--@foreach( $reversecountdetail as $pd )--}}
					{{--<tr class="font-size-13"--}}
					{{--@if ( ($pd['quantity_to_pick'] - $pd['moved_qty']) > 0 )--}}
						{{--style="background-color:#F29F9F"--}}
					{{--@endif>--}}
						{{--<td>{{$counter++}}</td>--}}
				 {{----}}
						{{--<td>{{$pd['sku']}}</td>--}}
						{{--<td> {{$pd['upc']}}</td>--}}
						{{--<td>{{$pd['description']}}</td>--}}
				 		{{--<td>{{$pd['quantity_to_pick']}}</td>--}}
						{{--<td>{{$pd['moved_qty']}}</td>--}}
						{{--<td>{{$pd['moved_qty'] - $pd['quantity_to_pick']}}</td>--}}
						{{--<td>--}}
						{{--@if($pd['quantity_to_pick']  == '0' && $pd['sku'] == '')--}}
                                    {{--Not in PO and MasterList!--}}
                                {{----}}
                        {{--@elseif ($pd['quantity_to_pick']  == '0' )--}}
                                    {{--Not in PO--}}
                        {{--@elseif ( $pd['sku'] == '' )--}}
                                    {{--Not in MasterList!--}}
                        {{--@endif--}}
						{{--</td>--}}
					{{--</tr>--}}
					{{--@endforeach--}}
				{{--@endif--}}
			</table>
		</div>
	</div>

</div>

@endsection