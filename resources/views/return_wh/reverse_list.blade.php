@extends('layouts.main')
@section('content')
 <div class="control-group">
    <div class="controls">
        <div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #FFFFFF;">
           {!! Form::open(array('url'=>'returnwh/filter', 'class'=>'form-signin', 'id'=>'form-MTSReceiving', 'role'=>'form', 'method' => 'get')) !!}

            <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
                   
                    <div class="span4">
                     <div>
                            <span class="search-po-left-pane"> MTS no. :</span>
                            <span class="search-po-right-pane">
                               <input type="text" name="filter_doc_no" value="{!! old('filter_doc_no') !!}">
                            </span>
                    </div>
                  <div>
                            <span class="search-po-left-pane">Picker :</span>
                            <span class="search-po-right-pane">
                                <select name="filter_stock_piler">
                                    <option hidden value="">{!! old('filter_stock_piler') ? ucwords($picker) : 'Select :' !!}</option>
                                    @foreach( $users as $user)
                                        <option value="{!! $user->id !!}">{!! ucwords( $user->fullname ) !!}</option>
                                    @endforeach
                                </select>
                            </span>
                        </div>
                    </div>
                    <div class="span4">
                     <div>
                            <span class="search-po-left-pane"> Date Entry:</span>
                            <div class="search-po-right-pane input-append date">
                                <input type="text" name="filter_entry_date" id="filter_entry_date" value="{!! old('filter_entry_date') !!}" readonly>
                                <span class="add-on"><i class="icon-th"></i></span>
                            </div>
                        </div>
                        
                        <div>
                            <span class="search-po-left-pane">Store :</span>
                            <span class="search-po-right-pane">
                            <select name="filter_store">
                                <option hidden value="">{!! old('filter_store') ? $str_name : 'Select :' !!}</option>
                                @foreach( $stores as $store)
                                    <option value="{!! $store->store_num !!}">{!! $store->store_nam !!}</option>
                                @endforeach
                            </select>
                            </span>
                        </div>
                    </div>
                    
                   
                     <div class="span11 control-group collapse-border-top">
                        <button class="btn btn-success btn-darkblue" type="submit">Search</button>
                        <a class="btn" href="{!! url('returnwh') !!}">Clear</a>
                    </div>  
          </div>
              {!! Form::close() !!}
              
        </div>

    </div> <!-- /controls -->
</div> <!-- /control-group -->
{!! Form::open(array('url' => 'returnwh/assign', 'method' => 'post')) !!}
<div class="div-buttons btn-group">
  <button type="button" class="btn btn-info btn-darkblue " data-toggle="dropdown" href="#">Report
  <span class="caret"></span></button>
        <ul class="dropdown-menu">
            <li><button type="submit" name="returhwh_post" value="report" style="    display: block;
    padding: 3px 20px;
    clear: both;
    font-weight: 400;
    line-height: 1.42857143;
    color: #333;
    white-space: nowrap;
    background-color: #ddd;
    border-style: hidden;">Overage/Shortage Report</button></li>
        </ul>
</div>  
  
 <div class="div-buttons">
     <button class="btn btn-info btn-darkblue assignReverse" type="submit" name="returhwh_post"
     >Assign to Stock Piler</button>
     <a href="{!! url('returnwh/pulldata') !!}" class="btn btn-info btn-darkblue" style="margin-right: 5px">Pull Data from JDA</a>
 </div>
 
<div class="clear">
    <div class="div-paginate">
        @if( CommonHelper::arrayHasValue( $mts_list ) )
            <h6 class="paginate">
                <span>{!! $mts_list->appends(['filter_doc_no' => old('filter_doc_no'),
                                             'filter_stock_piler' => old('filter_stock_piler'),
                                             'filter_entry_date' => old('filter_entry_date'),
                                             'filter_store' => old('filter_store')])->render() !!}
                </span>
            </h6>
        @endif
    </div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3> Return to Warehouse list</h3>
      {{--<span class="pagination-totalItems">{{ $text_total }} {{ $picklist_count }}</span>--}}
    </div>
   
    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead >
                    
                    <th style="width: 20px;" class="align-center"><input type="checkbox" id="main-selected" /></th>
                     
                    <th>NO.</th>
                    <th style="width: 20px;">MTS no.</th>
                    <th>STORE</th>
                    <th>Piler Name</th>
                    <th>date created </th>
                    <th>
                        <select id="select_status">
                            <option hidden>
                                @if(old('filter_pick_status') == 3)
                                    OPEN
                                @elseif(old('filter_pick_status') == 2)
                                    ASSIGNED
                                @elseif(old('filter_pick_status') == 1)
                                    DONE
                                @elseif(old('filter_pick_status') == '0')
                                    POSTED
                                @else
                                    STATUS
                                @endif
                            </option>
                          <option data-id="{!! app('url')->full() !!}?page={!! $mts_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_pick_status=3" value="3">OPEN</option>
                          <option data-id="{!! app('url')->full() !!}?page={!! $mts_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_pick_status=2">ASSIGNED</option>
                          <option data-id="{!! app('url')->full() !!}?page={!! $mts_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_pick_status=1">DONE</option>
                          <option data-id="{!! app('url')->full() !!}?page={!! $mts_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_pick_status='0'">POSTED</option>
                        </select>
                    </th>
                    <th>ACTION</th>
                </thead>

                @if( !CommonHelper::arrayHasValue( $mts_list ) )
                <tr>
                    <td colspan="8">No records found!</td>
                </tr>
                @endif

                @foreach( $mts_list as $key => $list )
                    <tr >
                        <td>
                                <input type="checkbox" name="mts_no[]" value="{!! $list->mts_no !!}">
                        </td>
                        <td>{!! $key+1 !!}</td>
                        <td><a href="{!! url( 'returnwh/detail/'.$list->mts_no ) !!}">{!! $list->mts_no !!}</a></td>
                        <td>{!! $list->from_loc !!}</td>
                        <td>{!! ucwords( $list->fullname ) !!}</td>
                        <td>{!! date('F d, Y', strtotime( $list->created_at )) !!}</td>
                        <td><strong>{!! CommonHelper::getMtsStatus( $list->mts_status ) !!}</strong></td>
                        <td>
                            @if( $list->mts_status == 3)
                                <a href="#" class="btn btn-info" disabled>OPEN</a>
                            @elseif( $list->mts_status == 2 )
                                <a href="#" class="btn btn-warning" disabled>ASSIGNED</a>
                            @elseif( $list->mts_status == 1)
                                <a href="{!! url('returnwh/close/'.$list->mts_no ) !!}" ONCLICK="return confirm('Close MTS?')" class="btn btn-success btn-danger" >CLOSE</a>
                            @elseif( $list->mts_status == 0)
                                <a href="#" class="btn btn-success" disabled title="MTS already Posted">POSTED</a>
                            @endif
                        </td>
                    </tr>
                @endforeach

            </table>
        </div>
    </div>
    </div>
{!! Form::close() !!}
 </div>

<script type="text/javascript">
$(document).ready(function() {

    $('.date').datepicker({
      format: 'yyyy-mm-dd'
    });

});

$('#select_status').on('change', function () {
    location.href = $(this).find(":selected").attr('data-id');
});

</script>
 @endsection