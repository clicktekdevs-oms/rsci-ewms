@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">

			<div class="accordion-group search-panel">
				{!! Form::open(array('url'=>'masterlist/products/filter', 'class'=>'form-signin', 'id'=>'form-products', 'role'=>'form', 'method' => 'get')) !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span3">
						<div>
							<span class="search-po-left-pane">SKU :</span>
							<span class="search-po-right-pane">
								<input type="text" name="filter_prod_sku" value="{!! old('filter_prod_sku') !!}">
							</span>
						</div>
						<div>
							<span class="search-po-left-pane">UPC :</span>
							<span class="search-po-right-pane">
								<input type="text" name="filter_prod_upc" value="{!! old('filter_prod_upc') !!}">
							</span>
						</div>
					</div>




					<div class="span11 control-group collapse-border-top" style="margin-top: 6px;">
						<button class="btn btn-success btn-darkblue" type="submit">Search</button>
						<a class="btn" href="{!! url('masterlist/products') !!}">Clear Filters</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if( CommonHelper::arrayHasValue($products) )
		    <h6 class="paginate">
				<span>{!! $products->appends(['filter_prod_sku' => old('filter_prod_sku'), 'filter_prod_upc' => old('filter_prod_upc') ])->render() !!}&nbsp;</span>
			</h6>
		@else
			&nbsp;
		@endif
	</div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Product Masterlist</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th>No.</th>
						<th><a href="#" class="">SKU</a></th>
						<th><a href="#" class="">UPC</a></th>
						<th><a href="#" class="">SHORT NAME</a></th>
						<th><a href="#" class="">DESCRIPTION</a></th>
						<th><a href="#" class="">DEPT CODE</a></th>
					</tr>
				</thead>
				<tbody>
				@if( !CommonHelper::arrayHasValue($products) )
					<tr class="font-size-13">
						<td colspan="7" class="align-center">No records found!</td>
					</tr>
				@endif

				@foreach( $products as $key => $product )
					<tr>
						<td>{!! $key+1 !!}</td>
						<td>{!! $product->sku !!}</td>
						<td>{!! $product->upc !!}</td>
						<td>{!! $product->short_name !!}</td>
						<td>{!! $product->item_desc !!}</td>
						<td>{!! $product->dept_code !!}</td>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>

</div>
@endsection