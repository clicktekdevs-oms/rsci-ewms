@extends('layouts.main')
@section('content')
    {{--@if( $errors->all() )--}}
    {{--<div class="alert alert-error">--}}
    {{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    {{--{{ HTML::ul($errors->all()) }}--}}
    {{--</div>--}}
    {{--@endif--}}

    <div class="widget">
        <div class="widget-header"> <i class="icon-th-list"></i>
            <h3>Assign to Stock Piler</h3>
        </div>
        <!-- /widget-header -->

        <div class="widget-content">
        {!! Form::open(array('url'=>'subloc-rcv/assign-piler', 'id'=>"form-assign", 'class'=>'form-horizontal', 'style' => 'margin: 0px;', 'method' => 'post')) !!}
        <!-- <div class="span3">&nbsp;</div> -->
            <div class="span3">&nbsp;</div>
            <div class="span7 add-piler-wrapper">
                <div class="control-group">
                    <label class="control-label">MTS No. :</label>
                    <div class="controls">
                        <textarea readonly style="resize: none" rows="5">{!! $mts_no !!}</textarea>
                        <input type="hidden" name="mts_no" value="{!! $mts_no !!}">

                    </div> <!-- /controls -->
                </div> <!-- /control-group -->

                <div class="control-group">
                    <label class="control-label">Stock Piler. :</label>
                    <div class="controls">
                        <select name="piler">
                            <option value="" hidden>Please Select :</option>
                            @foreach( $pilers as $piler)
                                <option value="{!! $piler->id !!}">{!! ucwords( $piler->name ) !!}</option>
                            @endforeach

                        </select>

                    </div> <!-- /controls -->
                </div>


            </div>
            <!-- <div class="span10">&nbsp;</div> -->
            <div class="span3">&nbsp;</div>
            <div class="span7">
                <div class="control-group">
                    <label class="control-label" for=""></label>
                    <div class="controls">
                        <button class="btn btn-info" id="btn-assign">Assign </button>
                        <a class="btn" href="#">Cancel</a>
                    </div> <!-- /controls -->
                </div> <!-- /control-group -->
            </div>
            {!! Form::close() !!}
        </div>
    </div>

    {{--<script type="text/javascript">--}}
    {{--$(document).ready(function() {--}}
    {{--function removeStockPiler() {--}}
    {{--$('.remove-piler-btn').click(function(e) {--}}
    {{--console.log('ee');--}}
    {{--$(this).parent().parent().remove();--}}
    {{--});--}}
    {{--}--}}

    {{--removeStockPiler();--}}
    {{--// Submit Form--}}
    {{--$('.add-piler-btn').unbind('click').click(function(e) {--}}
    {{--// $('.piler-block').clone().appendTo(".add-piler-wrapper");--}}
    {{--var html = '';--}}
    {{--html += '<div class="control-group piler-block">'--}}
    {{--+ '<label class="control-label" for="stock_piler">{{ $entry_stock_piler }}</label>'--}}
    {{--+ '<div class="controls">'--}}
    {{--+ '{{ Form::select('stock_piler[]', $stock_piler_list, '') }}'--}}
    {{--+ '<a class="remove-piler-btn" style="margin-left: 3px;"><i class="icon-minus-sign" style="font-size: 1.5em; color:#CB1212;"></i></a>'--}}
    {{--+ '</div>'--}}
    {{--+ '</div>';--}}
    {{--$(".add-piler-wrapper").append(html);--}}

    {{--removeStockPiler()--}}

    {{--});--}}

    {{--// Submit Assign--}}
    {{--$('#btn-assign').click(function(e) {--}}
    {{--// stockpiler = $('select[name=\'stock_piler\']').val();--}}
    {{--var stockpiler = $('#stock_piler_select').val();--}}
    {{--console.log(stockpiler);--}}

    {{--if (stockpiler == '') {--}}
    {{--//alert('Alert Dialog' + );--}}
    {{--return false;--}}
    {{--} else {--}}
    {{--$('#form-assign').submit();--}}
    {{--}--}}
    {{--});--}}
    {{--});--}}
    {{--</script>--}}
@endsection