@extends('layouts.main')

@section('content')
    <div class="control-group">
        <div class="controls">
            <div class="accordion" id="accordion2">
                <div class="accordion-group" style="background-color: #FFFFFF;">
                    {!! Form::open(array('url'=>'subloc-rcv/filter', 'class'=>'form-signin', 'id'=>'form-pick-list', 'role'=>'form', 'method' => 'get')) !!}
                    <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">

                        <div class="span5">
                            <div>
                                <span class="search-po-left-pane">MTS No. :</span>
                                <span class="search-po-right-pane">
				        		<input type="text" value="{!! old('mts_no') !!}" name="mts_no">
							</span>
                            </div>
                            <div>
                                <span class="search-po-left-pane">Ship date :</span>
                                <span class="search-po-right-pane">
                                    <div class="search-po-right-pane input-append date">
                                        <input type="text" class="span2" value="{!! old('ship_date') !!}" name="ship_date" id="filter_entry_date" readonly>
                                        <span class="add-on"><i class="icon-th"></i></span>
				        	        </div>

				        	</span>
                            </div>
                        </div>

                        <div class="span5">


                            <div>
                                <span class="search-po-left-pane">From Store :</span>
                                <span class="search-po-right-pane">
				        		<select name="filter_from_store">
                                        <option value="">Please Select :</option>
                                    @foreach( $stores as $store )
                                        <option value="{!! $store->store_num !!}">{!! $store->store_nam !!}</option>
                                    @endforeach


								</select>
				        	</span>
                            </div>

                            <div>
                                <span class="search-po-left-pane">To Store :</span>
                                <span class="search-po-right-pane">
				        		<select name="filter_to_store">

                                     <option value="">Please Select :</option>
                                    @foreach( $stores as $store )
                                        <option value="{!! $store->store_num !!}">{!! $store->store_nam !!}</option>
                                    @endforeach

								</select>
				        	</span>
                            </div>

                        </div>



                        <div class="span11 control-group collapse-border-top">
                            <button class="btn btn-success btn-darkblue" id="submitForm">Search now</button>
                            <a class="btn" href="{!! url('subloc-rcv') !!}">Clear Filters</a>
                        </div>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- /controls -->
    </div>

    <div class="clear">
        <div class="div-paginate">
            @if(CommonHelper::arrayHasValue( $subloc_lists ) )
                <h6 class="paginate">
				<span>{!! $subloc_lists->appends(['mts_no' 	=> old('mts_no'),
				 							   'ship_date' 	=> old('ship_date'),
				 							   'filter_from_store'   => old('filter_from_store'),
				 							   'filter_to_store'     => old('filter_to_store'),
				 							   ])->render() !!} &nbsp;</span>
                </h6>
            @endif
        </div>
        {!! Form::open(array('url' => 'subloc-rcv/actionform', 'method' => 'post', 'id' => 'add_load_form')) !!}
        <div class="div-buttons">
            <button type="submit" class="btn btn-info btn-darkblue " title="Assign MTS to stockpiler">Assign Picker</button>
            <div class="div-buttons btn-group " style="margin-left: 5px">
                <button type="button" class="btn btn-info btn-darkblue " data-toggle="dropdown">Report <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href='#'>Shortage/Overage Report</a></li>
                </ul>
            </div>
            <a  href="{!! url('subloc-rcv/pulldata') !!}" class="btn btn-info btn-darkblue " title="Pull data from JDA.">Pull Data (JDA)</a>
        </div>

        <div class="widget widget-table action-table" style="z-index: 1 !important;">
            <div class="widget-header"> <i class="icon-th-list"></i>
                <h3>Subloc Receiving Lists</h3>
                {{--<span class="pagination-totalItems">{{ $text_total }} {{ $picklist_count }}</span>--}}
            </div>
            <!-- /widget-header -->
            <div class="widget-content">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered">
                        <thead >
                        <th><input type="checkbox"></th>
                        <th>MTS No.</th>
                        <th>SHIP DATE</th>
                        <th>FROM STORE</th>
                        <th>TO STORE</th>
                        <th>PILER NAME</th>
                        <th>
                            <select class="form-control input-sm" id="select_status">
                                <option hidden>
                                    @if(old('filter_tl_status') == 3)
                                        OPEN
                                    @elseif(old('filter_tl_status') == 2)
                                        ASSIGNED
                                    @elseif(old('filter_tl_status') == 1)
                                        DONE
                                    @elseif(old('filter_tl_status') == '0')
                                        POSTED
                                    @else
                                        STATUS
                                    @endif
                                </option>
                                <option data-id="{!! app('url')->full() !!}?page={!! $subloc_lists->currentPage() !!}&mts_no={!! old('mts_no') !!}&ship_date={!! old('ship_date') !!}&filter_to_store={!! old('filter_to_store') !!}&filter_from_store={!! old('filter_from_store') !!}&filter_tl_status=3">OPEN</option>
                                <option data-id="{!! app('url')->full() !!}?page={!! $subloc_lists->currentPage() !!}&mts_no={!! old('mts_no') !!}&ship_date={!! old('ship_date') !!}&filter_to_store={!! old('filter_to_store') !!}&filter_from_store={!! old('filter_from_store') !!}&filter_tl_status=2">ASSIGNED</option>
                                <option data-id="{!! app('url')->full() !!}?page={!! $subloc_lists->currentPage() !!}&mts_no={!! old('mts_no') !!}&ship_date={!! old('ship_date') !!}&filter_to_store={!! old('filter_to_store') !!}&filter_from_store={!! old('filter_from_store') !!}&filter_tl_status=1">DONE</option>
                                <option data-id="{!! app('url')->full() !!}?page={!! $subloc_lists->currentPage() !!}&mts_no={!! old('mts_no') !!}&ship_date={!! old('ship_date') !!}&filter_to_store={!! old('filter_to_store') !!}&filter_from_store={!! old('filter_from_store') !!}&filter_tl_status='0'">POSTED</option>
                            </select>
                        </th>
                        <th>ACTION</th>
                        </thead>

                        @if( !CommonHelper::arrayHasValue( $subloc_lists ) )
                            <tr class="font-size-13">
                                <td colspan="8" style="text-align: center;">No records found!</td>
                            </tr>
                        @endif

                        @foreach( $subloc_lists as $key => $list )
                            <tr>
                                <td><input type="checkbox" name="mts_no[]" value="{!! $list->tl_no !!}"></td>
                                <td><a href="{!! url('subloc-rcv-dtl/'. $list->tl_no ) !!}">{!! $list->tl_no !!}</a> </td>
                                <td>{!! date('F d, Y', strtotime($list->ship_date)) !!}</td>
                                <td>{!! $list->from_loc !!}</td>
                                <td>{!! $list->to_loc !!}</td>
                                <td>{!! ucwords( $list->assigned_to ) !!}</td>
                                <td><b>{!! CommonHelper::getPickStatus( $list->tl_status ) !!}</b></td>
                                <td>
                                    @if( $list->tl_status == 3 )
                                        <a href="#" disabled class="btn btn-primary">OPEN</a>
                                    @elseif( $list->tl_status == 2 )
                                        <a href="#" disabled class="btn btn-primary">ASSIGNED</a>
                                    @elseif( $list->tl_status == 1)
                                        <a href="{!! url('subloc-rcv/closemts/'.$list->tl_no ) !!}" onclick="return confirm('Close MTS ? ')"  class="btn btn-success">CLOSE MTS</a>
                                    @elseif( $list->tl_status == 0)
                                        <a href="#" disabled class="btn btn-primary">POSTED</a>
                                    @endif
                                </td>
                            </tr>
                        @endforeach

                    </table>
                </div>
            </div>
        </div>


        {!! Form::close() !!}

    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.date').datepicker({
                format: 'yyyy-mm-dd'
            });
        });

        $('#select_status').on('change', function () {
            location.href = $(this).find(":selected").attr('data-id');
        });

        function convertToUppercase(el) {
            if(!el || !el.value) return;
            el.value = el.value.toUpperCase();
        }
    </script>

@endsection