@extends('layouts.main')
@section('content')
    <div class="control-group">

        <a class="btn btn-info btn-darkblue" href="{!! redirect()->back()->getTargetUrl() !!}"><i class="icon-chevron-left"></i> Back to List</a>

    </div>

    <div class="control-group">
        <div class="controls">
            <div class="accordion" id="accordion2">
                <div class="accordion-group" style="background-color: #;">
                    <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
                        <div class="span3">
                            <div>
                                <span class="search-po-left-pane">MTS No. :</span>
                                <span class="search-po-right-pane">
								<input type="text" readonly value="{!! $subloc_list->tl_no !!}">
				        	</span>
                            </div>
                        </div>

                        <div class="span3">
                            <div>
                                <span class="search-po-left-pane"> Ship date:</span>
                                <span class="search-po-right-pane">
								<input type="text" readonly value="{!! date('F d, Y', strtotime($subloc_list->ship_date)) !!}">
				        	</span>
                            </div>
                        </div>

                        <div class="span4">
                            <div>
                                <span class="search-po-left-pane"> Piler Name:</span>
                                <span class="search-po-right-pane">
								<input type="text" readonly value="{!! ucwords( $subloc_list->assigned_to ) !!}">
				        	</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- /controls -->
    </div> <!-- /control-group -->



    <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
            <h3>Subloc Receiving Detail</h3>
            {{--<span class="pagination-totalItems">{{ $text_total }} {{ $picklist_detail_count }}</span>--}}
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
            <div class="table-responsive">
                <table class="table table-bordered">

                    <thead>
                    <tr>
                        <th>No.</th>
                        <th><a href="#">SKU</a></th>
                        <th><a href="#">UPC</a></th>
                        <th>SHORT DESCRIPTION</th>
                        <th>EXPECTED QTY</th>
                        <th>QUANTITY RECEIVED</th>
                        <th>VARIANCE</th>
                        <th>Remarks</th>
                    </tr>
                    </thead>

                    @if( !count( $subloc_details ) )
                        <tr>
                            <td colspan="7"> No Records Found!</td>
                        </tr>
                    @else

                        @foreach( $subloc_details as $key => $detail )
                            <tr style="{!! $detail->qty_ord != $detail->qty_rcv ? "background-color: #f2dede" : '' !!}">
                                <td>{!! $key+1 !!}</td>
                                <td>{!! $detail->sku !!}</td>
                                <td>{!! $detail->upc !!}</td>
                                <td>{!! $detail->short_name !!}</td>
                                <td>{!! $detail->qty_ord !!}</td>
                                <td>
                                    @if($subloc_list->tl_status == 1)
                                        {!! Form::open(array('url' => 'subloc-rcv/update/quantity', 'method' => 'post')) !!}
                                        <div class="input-group">
                                            <input type="text" value="{!! $detail->qty_rcv !!}" name="qty_rcv">
                                            <input type="hidden" name="upc" value="{!! $detail->upc !!}">
                                            <input type="hidden" name="tl_no" value="{!! $subloc_list->tl_no !!}">
                                            <input type="hidden" name="sku" value="{!! $detail->sku !!}">
                                            <button type="submit" class="btn btn-primary" title="Update Quantity"><span class="icon-check"></span> </button>
                                        </div>
                                        {!! Form::close() !!}
                                    @else
                                        {!! $detail->qty_rcv !!}
                                    @endif
                                </td>
                                <td>{!! $detail->qty_rcv - $detail->qty_ord !!}</td>
                                <td>{!! $detail->remarks !!}</td>
                            </tr>
                        @endforeach
                    @endif

                </table>
            </div>
        </div>


    </div>

@endsection