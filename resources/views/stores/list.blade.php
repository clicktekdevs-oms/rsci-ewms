@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
			<div class="accordion-group search-panel">
				{!! Form::open(array('url'=>'masterlist/stores/filter', 'class'=>'form-signin', 'id'=>'form-stores', 'role'=>'form', 'method' => 'get')) !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span4">
						<div>
							<span class="search-po-left-pane">Store name:</span>
							<span class="search-po-right-pane">
								<input type="text" name="filter_store_name" id="filter_store_name" value="{!! old('filter_store_name') !!}">
							</span>
						</div>
					</div>

					<div class="span4">
						<div>
							<span class="search-po-left-pane">Store Code:</span>
							<span class="search-po-right-pane">
								<input type="text" name="filter_store_code" value="{!! old('filter_store_code') !!}">
							</span>
						</div>
					</div>

					<div class="span11 control-group collapse-border-top" style="margin-top: 6px;">
						<button class="btn btn-success btn-darkblue" id="submitForm">Search Now</button>
						<a class="btn" href="{!! url('masterlist/stores') !!}">Clear Filters</a>
					</div>
				</div>

				{!! Form::close() !!}
			</div>
		</div>
	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if(CommonHelper::arrayHasValue($stores) )
		    <h6 class="paginate">
				<span>{!! $stores->appends(['filter_store_name' => old('filter_store_name'), 'filter_store_code' => old('filter_store_code') ]) !!}&nbsp;</span>
			</h6>
		@else
			&nbsp;
		@endif
	</div>
</div>

<div class="widget widget-table action-table">
	<div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Store Masterlist</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th><a href="#" class="">Store Code</a></th>
						<th><a href="#" class="">Store Name</a></th>
						<th>Store Address</th>
					</tr>
				</thead>
				<tbody>
				@if( !CommonHelper::arrayHasValue($stores) )
					<tr class="font-size-13">
						<td colspan="4" class="align-center">No records found!</td>
					</tr>
				@endif
					@foreach($stores as $key => $store)
					<tr class="font-size-13">
						<td>{!! $store->id !!}</td>
						<td>{!! $store->store_num !!}</td>
						<td>{!! $store->store_nam !!}</td>
						<td>{!! $store->store_add1.' '.$store->store_add2 !!}</td>
					</tr>
					@endforeach

				</tbody>
			</table>
		</div>
	</div>

</div>
@endsection