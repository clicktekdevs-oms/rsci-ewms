@extends('layouts.main')
@section('content')

<div class="widget">
    <div class="widget-header"> <i class="icon-key"></i>
    	<h3>Change Password</h3>
    </div>
    <!-- /widget-header -->
    
    <div class="widget-content">
			{!! Form::open(['url' => 'user/changepassword', 'method' => 'post', 'class' => 'form-horizontal']) !!}
		@foreach($errors->all() as $error)
			<div class="alert alert-danger">
				<ul>
					<li>{!! $error !!}</li>
				</ul>
			</div>
		@endforeach
		<div class="span3">&nbsp;</div>
		<div class="span7">

			<fieldset>

				<div class="control-group">											
					<label class="control-label" for="password">New Password :</label>
					<div class="controls">
						<input type="password" name="password" maxlength="10" required>
						<input type="hidden" name="user_id" value="{!! $user_id !!}">
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->
				
				<div class="control-group">											
					<label class="control-label" for="password_confirmation">Confirm Password :</label>
					<div class="controls">
						<input type="password" name="password_confirmation" maxlength="10" required>
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->
								
				<div class="control-group">											
					<label class="control-label" for=""></label>
					<div class="controls">
						<button class="btn btn-info" type="submit">Submit</button>
						<a class="btn" href="#">Cancel</a>
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->
			</fieldset>
        </div>
        <div class="span2">&nbsp;</div>
        {!! Form::close() !!}
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    // Submit Form
    $('#submitForm').click(function() {
    	$('#form-users').submit();
    });
    
    $('#form-users input').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#form-users').submit();
		}
	});
});	
</script>

@endsection
