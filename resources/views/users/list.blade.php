@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
			<div class="accordion-group search-panel">
				{!!  Form::open(array('url'=>'users/filter', 'class'=>'form-signin', 'id'=>'form-users', 'role'=>'form', 'method' => 'get')) !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span3">
						<div>
							<span class="search-po-left-pane">Username</span>
							<span class="search-po-right-pane">
								<input type="text" id='filter_username' value="{!! old('username') !!}" name="username">
							</span>
						</div>
					</div>


					<div class="span11 control-group collapse-border-top">
						<button class="btn btn-success  btn-darkblue" type="submit">Search Now</button>
						<a class="btn" href="{!! url('users') !!}" >Clear Filter</a>
					</div>
				</div>
			</div>
			{!! Form::close() !!}
		</div>
	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		<h6 class="paginate">
			<span>
				{!! $result->appends([])->render() !!}
			</span>
		</h6>
	</div>
	<div class="div-buttons">
		<a class="btn btn-info  btn-darkblue" href="{!! url('users/create') !!}">Add New User</a>
	</div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Users</h3>
      	<span class="pagination-totalItems"></span>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	<div class="table-responsive">
	    	{!! Form::open(array('url'=>'user/delete', 'class'=>'form-signin', 'id'=>'form', 'role'=>'form', 'method' => 'post', 'style' => 'margin-bottom: 0px;')) !!}
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						
						<th style="width: 20px;" class="align-center"><input type="checkbox" id="main-selected" /></th>
						<th>USER ID</th>
						<th><a href="" class="">USERNAME</a></th>
						<th><a href="" class="">NAME</a></th>
						<th><a href="" class="">ROLE</a></th>
						<th><a href="" class="">DATE CREATED</a></th>
						<th>LOCATION</th>
						<th style="width: 80px;" class="align-center">ACTION</th>
					</tr>
				</thead>
				<tbody>
				@if(count($result) <= 0)
					<tr>
						<td colspan="7"><div class="alert alert-info">No records found!</div> </td>
					</tr>
				@endif
					@foreach($result as $list)
						<tr>
							<td><input type="checkbox"></td>
							<td>{!! $list->id !!}</td>
							<td>{!! $list->username !!}</td>
							<td>{!! ucfirst($list->firstname) !!} {!! ucfirst($list->lastname) !!}</td>
							<td>{!! ucwords($list->role_id) !!}</td>
							<td>{!! date('F j, Y', strtotime($list->created_at)) !!}</td>
							<td>{!! $list->store_code == 8001 ? 'DEBENHAMS WAREHOUSE' : $list->store_code !!}</td>
							<td>
								<a href="{!! url('users/password/'. $list->id) !!}" title="Change Password"><i class="icon icon-lock"></i></a>
								<a href="{!! url('users/update/'. $list->id) !!}" title="edit"><i class="icon icon-edit"></i></a>
								<a onclick="return confirm('Are you sure you want to delete this user?')" href="{!! url('users/delete/'. $list->id) !!}" title="delete"><i class="icon-remove"></i></a>

							</td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{!! Form::close() !!}
		</div>
	</div>

</div>

@endsection