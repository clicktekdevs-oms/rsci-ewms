@extends('layouts.main')

@section('content')
@if( $errors->all() )
    <div class="alert alert-error">
    	<button class="close" data-dismiss="alert" type="button">&times;</button>
    	@foreach( $errors->all() as $error)
			<ul>
				<li>{!! $error !!}</li>
			</ul>
		@endforeach
    </div>
@endif

<div class="widget">
    <div class="widget-header"> <i class="icon-user"></i>
    	<h3>Update User</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	{!! Form::open(array('url'=>'user/update/data', 'class'=>'form-horizontal', 'id'=>'form-users', 'role'=>'form', 'method' => 'post')) !!}
		<div class="span3">&nbsp;</div>
		<div class="span7">
			<fieldset>
				<div class="control-group">
					<label class="control-label" for="username">Username :</label>
					<div class="controls">
						<input type="text" name="username" value="{!! $user->username !!}">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->

				<div class="control-group">
					<label class="control-label" for="firstname">Firstname :</label>
					<div class="controls">
						<input type="text" name="firstname" value="{!! $user->firstname !!}" maxlength="50">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->

				<div class="control-group">
					<label class="control-label" for="lastname">Lastname :</label>
					<div class="controls">
						<input type="text" name="lastname" value="{!! $user->lastname !!}" maxlength="50">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->

			 

				<div class="control-group">
					<label class="control-label" for="role_name">Role ID:</label>
					<div class="controls">
						<select name="role_id">
							<option hidden value="{!! $user->role_id !!}">{!! $user->role_name !!}</option>
							@if( Auth::user()->role_id == 7 )
								<option hidden value="{!! $user->role_id !!}">{!! $user->role_name !!}</option>
								<option value="7">Store Head</option>
								<option value="8">Store Clerk</option>
							@else
								<option hidden value="{!! $user->role_id !!}">{!! $user->role_name !!}</option>
								@foreach($roles as $role)
									<option value="{!! $role->id !!}">{!! $role->role_name !!}</option>
								@endforeach
							@endif
						</select>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->


				@if( Auth::user()->role_id !=2 && Auth::user()->role_id != 3 && Auth::user()->role_id != 7)
					<div class="control-group">											
					<label class="control-label" for="filter_store">Store :</label>
					<div class="controls">
						<select name="filter_store" id="filter_store">
							<option hidden value="{!! $user->store_code !!}">{!! $user->store_nam !!}</option>
							@foreach( $stores as $store )
								<option value="{!! $store->store_num !!}">{!! ucwords( $store->store_nam ) !!}</option>
							@endforeach
						</select>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
				@endif


				<input type="hidden" name="user_id" value="{!! $user->id !!}">

				<div class="control-group">
					<label class="control-label" for=""></label>
					<div class="controls">
						<a class="btn btn-info" id="submitForm">Submit</a>
						<a class="btn" href="#">Cancel</a>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
			</fieldset>
        </div>
        <div class="span2">&nbsp;</div>
	</div>
</div>

<script type="text/javascript">
$(document).ready(function() {
    // Submit Form
    $('#submitForm').click(function() {
    	$('#form-users').submit();
    });

    $('#form-users input').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#form-users').submit();
		}
	});

    toggleBarcodeElem();
	$('#user_role').click(function() {
    	toggleBarcodeElem();
    });

    function toggleBarcodeElem() {
    	var select_value = $('#user_role option:selected').text();
    	if(select_value == 'admin') $('#barcode-wrapper').hide();
    	else $('#barcode-wrapper').show();
    }
});
</script>

@endsection