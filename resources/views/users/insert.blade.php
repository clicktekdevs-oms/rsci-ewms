@extends('layouts.main')
@section('content')

	@if( $errors->all() )
		<div class="alert alert-error">
			<button class="close" data-dismiss="alert" type="button">&times;</button>
			@foreach($errors->all() as $error)
				<ul>
					<li>{!! $error !!}</li>
				</ul>
			@endforeach
		</div>
	@endif


<div class="widget">
    <div class="widget-header"> <i class="icon-user"></i>
    	<h3>New User</h3>
    </div>
    <!-- /widget-header -->
    
    <div class="widget-content">
    	{!! Form::open(array('url'=>'users/insert', 'class'=>'form-horizontal', 'autocomplete'=>'off', 'id'=>'form-users', 'role'=>'form', 'method' => 'post')) !!}
		<div class="span3">&nbsp;</div>
		<div class="span7">
			<fieldset>
				<div class="control-group">											
					<label class="control-label" for="username">Username :</label>
					<div class="controls">
						<input type="text" name="username" maxlength="50" value="{!! old('username') !!}">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
				
				<div class="control-group">											
					<label class="control-label" for="password">Password :</label>
					<div class="controls">
						<input type="password" name="password" maxlength="12">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
				
				<div class="control-group">											
					<label class="control-label" for="password_confirmation">Confirm Password</label>
					<div class="controls">
						<input type="password" name="password_confirmation" maxlength="12">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
				
				<div class="control-group">											
					<label class="control-label" for="firstname">Firstname :</label>
					<div class="controls">
						<input type="text" name="firstname" maxlength="50" value="{!! old('firstname') !!}">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
				
				<div class="control-group">											
					<label class="control-label" for="lastname">Lastname :</label>
					<div class="controls">
						<input type="text" name="lastname" maxlength="50" value="{!! old('lastname') !!}">
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
			 
				 
				
				<div class="control-group">											
					<label class="control-label" for="role_id">Role :</label>
					<div class="controls">
						<select name="role_id">
								<option hidden>Select :</option>
							@if( Auth::user()->role_id == 7 )
								<option value="7">Store Head</option>
								<option value="8">Store Clerk</option>
							@else
							@foreach($roles as $role)
								<option value="{!! $role->id !!}">{!! $role->role_name !!}</option>
							@endforeach
							@endif
						</select>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->

				@if( Auth::user()->role_id != 2 && Auth::user()->role_id != 3 && Auth::user()->role_id != 7 )
				<div class="control-group">											
					<label class="control-label" for="filter_store">Store :</label>
					<div class="controls">
						<select name="filter_store">
							<option hidden>Please Select :</option>
							@foreach( $stores as $store )
								<option value="{!! $store->store_num !!}">{!! ucwords( $store->store_nam ) !!}</option>
							@endforeach
						</select>
					</div> <!-- /controls -->
				</div> <!-- /control-group -->
			 	@endif


				<div class="control-group">											
					<label class="control-label" for=""></label>
					<div class="controls">
						<button class="btn btn-info" type="submit">Submit</button>
						<a class="btn" href="#">Cancel</a>
					</div> <!-- /controls -->				
				</div> <!-- /control-group -->
			</fieldset>
        </div>
		{!! Form::close() !!}
	</div>
</div>
@endsection