@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
			<div class="accordion-group search-panel">
				{!! Form::open(array('url'=>'masterlist/slots/filter', 'class'=>'form-signin', 'id'=>'form-slots', 'role'=>'form', 'method' => 'get')) !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span4">
						<div>
							<span class="search-po-left-pane">Slot Name:</span>
							<span class="search-po-right-pane">
								<input type="text" name="filter_slot_no" value="{!! old('filter_slot_no') !!}">
							</span>
						</div>
					</div>

					<div class="span11 control-group collapse-border-top" style="margin-top: 6px;">
						<button class="btn btn-success btn-darkblue" type="submit">Search</button>
						<a class="btn" href="{!! url('masterlist/slots') !!}">Clear Filters</a>
					</div>
				</div>


				{!! Form::close() !!}
			</div>
		</div>
	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if(CommonHelper::arrayHasValue($slots) )
		    <h6 class="paginate">
				<span>{!! $slots->appends(['filter_slot_no' => old('filter_slot_no')])->render() !!} &nbsp;</span>
			</h6>
		@else
			&nbsp;
		@endif
	</div>

</div>

<div class="widget widget-table action-table">
	<div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Slot Masterlist</h3>
     	{{--<span class="pagination-totalItems">{{ $text_total }} {{ $slots_count }}</span>--}}
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="10%">No.</th>
						<th><a href="#" class="">SLOT NO.</a></th>
						<th>SLOT ZONE</th>
					</tr>
				</thead>
				<tbody>
				@if( !CommonHelper::arrayHasValue($slots) )
					<tr class="font-size-13">
						<td colspan="3" class="align-center">No records found!</td>
					</tr>
				@endif

				@foreach( $slots as $key => $slot)
					<tr>
						<td>{!! $slot->id !!}</td>
						<td>{!! $slot->slot_name !!}</td>
						<td>{!! $slot->slot_zone !!}</td>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>


</div>
@endsection