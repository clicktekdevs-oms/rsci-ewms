@extends('layouts.main')
@section('content')
    <div class="control-group">

        <a class="btn btn-info btn-darkblue" href="{!! redirect()->back()->getTargetUrl() !!}"><i class="icon-chevron-left"></i> Back to List</a>

    </div>

    <div class="control-group">
        <div class="controls">
            <div class="accordion" id="accordion2">
                <div class="accordion-group" style="background-color: #;">
                    <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
                        <div class="span3">
                            <div>
                                <span class="search-po-left-pane">MTS No. :</span>
                                <span class="search-po-right-pane">
								<input type="text" readonly value="{!! $mts_detail->tl_no !!}">
				        	</span>
                            </div>
                        </div>


                        <div class="span4">
                            <span class="search-po-left-pane"> Ship date:</span>
                            <span class="search-po-right-pane input-append date">
							{!! Form::open(array('method' => 'post', 'url' => 'picking/updateshipdate')) !!}
                                <input type="hidden" name="mts_no" value="{!! $mts_detail->tl_no !!}">
							<input type="text"   name="ship_date" value="{!! $mts_detail->ship_date !!}" readonly>
							<span class="add-on"><i class="icon-th"></i></span>&nbsp;&nbsp;
							<button class="btn btn-primary btn-xs" type="submit" name="action_btn" style="margin-top: -10px">Save</button>
                                {!! Form::close() !!}
				        </span>
                        </div>

                        <div class="span4">
                            <div>
                                <span class="search-po-left-pane"> Piler Name:</span>
                                <span class="search-po-right-pane">
								<input type="text" readonly value="{!! ucwords($mts_detail->name) !!}">
				        	</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div> <!-- /controls -->
    </div> <!-- /control-group -->

    <div class="clear">
        <div class="div-paginate">
            {{--@if(CommonHelper::arrayHasValue($picklist_detail) )--}}
            {{--<h6 class="paginate">--}}
            {{--<span>{{ $picklist_detail->appends($arrFilters)->links() }}&nbsp;</span>--}}
            {{--</h6>--}}
            {{--@else--}}
            {{--&nbsp;--}}
            {{--@endif--}}
        </div>
    </div>

    <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
            <h3>Picklist Detail</h3>
            {{--<span class="pagination-totalItems">{{ $text_total }} {{ $picklist_detail_count }}</span>--}}
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                    <tr>
                        <th>No.</th>
                        <th><a href="#">SKU</a></th>
                        <th><a href="#">UPC</a></th>
                        <th>SHORT DESCRIPTION</th>
                        <th>EXPECTED QTY</th>
                        <th>QUANTITY PICKED</th>
                        <th>VARIANCE</th>
                    </tr>
                    </thead>

                    @if( !count($details) )
                        <tr>
                            <td colspan="7">No records found!</td>
                        </tr>
                    @endif

                    @foreach( $details as $key =>$detail )
                        <tr style="{!! $detail->qty_req != $detail->qty_rcv ? "background-color: #f2dede" : '' !!}">
                            <td>{!! $key+1 !!}</td>
                            <td>{!! $detail->sku !!}</td>
                            <td>{!! $detail->upc !!}</td>
                            <td>{!! $detail->short_desc !!}</td>
                            <td>{!! $detail->qty_req !!}</td>
                            <td>{!! $detail->qty_rcv !!}</td>
                            <td>{!! $detail->qty_rcv - $detail->qty_req !!}</td>
                        </tr>
                    @endforeach

                </table>
            </div>
        </div>


    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('.date').datepicker({
                format: 'yyyy-mm-dd'
            });


        });

        $('#select_status').on('change', function () {
            location.href = $(this).find(":selected").attr('data-id');
        });
    </script>

@endsection