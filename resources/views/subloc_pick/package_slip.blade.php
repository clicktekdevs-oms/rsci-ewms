<html>
<title>Package Slip</title>
<style>
    body {
        font-weight: 900;
        margin: 0px 0px 0px 20px;
        width: 100%;

    }
    .tl {
        font-size: 25px;
        margin-top: 25px;
        text-align: center;
    }
    .sub-label {
        font-size: 21px;
        width: 100% !important;
    }

    @page {
        width: 5in;
        height: 3in;
        page-break-after: always;
        direction: ltr;
    }

    @media print
    {
        @page
        {
            width: 5in;
            height: 3in;
            page-break-after: always;
            direction: ltr;
            float: left;

        }



        page
        {
            page-break-after: always;
        }
    }
    page {
        background: white;
        display: block;
        padding-top: 5%;
        margin: 0 auto;
        font-family: Arial;
        page-break-before: always;
    }
    .A4
    {
        width: 5in;
        height: 3in;
        font-family: "Helvetica Neue", Helvetica, Arial, sans-serif;
    }
    .initial
    {
        font-size: 13px;
        font-weight: 700;
    }
    .barcode
    {
        margin: 0;
    }

    /*page[size="A4"] {*/
    /*width: 5.5in;*/
    /*height: 3in;*/
    /*page-break-before: always;*/
    /*size: 5.5;*/
    /*}*/

    .cell {
        font-size: 18pt;
        padding: 18% 0 0 0 !important;
    }
    .rotated_vertical_td {
        height: 5in;
        width: 4in;
        text-transform:uppercase;
        margin:0;
        padding:0;
    }
    .rotated_vertical {
        -webkit-transform:rotate(90deg);
        -moz-transform:rotate(90deg);
        -ms-transform:rotate(90deg);
        -o-transform:rotate(90deg);
        transform:rotate(90deg);
        transform-origin: 100%;
        width: 100%;
    }

    table {

        font-size: 18px;
        width: 150%;
    }

    table , tr , td {
        font-size: 20px;
        vertical-align: middle;
    }



</style>
<body>
@foreach( $get_box as $box )
    <page class="A4 rotated_vertical cell">

        <table>

            <tr>
                <td colspan="2" id="header" style="margin-top: 20%;font-weight: 600">RSCI Package Slip (SUBLOC)</td>
            </tr>

            <tr>
                <td colspan="2" style="width: 100%"><b>From :{!! $box->from_loc !!}</b></td>
            </tr>

            <tr>
                <td colspan="2"><b>Ship Date : {!! $box->ship_date !!}</b> </td>
            </tr>

            <tr>
                <td colspan="2"><b>To : {!! $box->to_loc !!}</b></td>
            </tr>

            <tr>
                <td colspan="2"><b>Total Qty : {!! $box->total_qty !!}</b> <b colspan="1" style="font-size: 20px;margin-left: 60px">FRAGILE :</b> <input type="checkbox"><b>YES</b> <input type="checkbox"><b>NO</b></td>
            </tr>

            <tr>
                <td colspan="2"><b style="word-break: break-all">MTS No: {!! $box->tl_no !!}</b>  </td>


            </tr>

            <tr>
                <td style="">{!!  DNS1D::getBarcodeHTML($box->box_no, "C128", 2, 70) !!}</td>
                <td><b style="font-size: 30px;">{!! $box->box_no !!}</b></td>
            </tr>

        </table>
    </page>
@endforeach
</body>
</html>