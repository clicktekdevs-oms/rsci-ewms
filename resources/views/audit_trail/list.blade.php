@extends('layouts.main')
@section('content')
{{--@if( CommonHelper::arrayHasValue($error_date) )--}}
    {{--<div class="alert alert-error">--}}
    	{{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    	{{--{{ $error_date }}--}}
    {{--</div>--}}
{{--@endif--}}

<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
			<div class="accordion-group search-panel">
				{!! Form::open(array('url'=>'audit-trail/filter', 'class'=>'form-signin', 'id'=>'form-audit-trail', 'role'=>'form', 'method' => 'get'))  !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span4">
						<div>
							<span class="search-po-left-pane">Date From:</span>
							<div class="search-po-right-pane input-append date">
								<input type="text" name="filter_date_from" id="filter_date_from" value="{!! old('filter_date_from') !!}" readonly>
								<span class="add-on"><i class="icon-th"></i></span>
							</div>
						</div>
						<div>
							<span class="search-po-left-pane">Date To:</span>
							<div class="search-po-right-pane input-append date">
								<input type="text" name="filter_date_to" id="filter_date_to" value="{!! old('filter_date_to') !!}" readonly>
								<span class="add-on"><i class="icon-th"></i></span>
							</div>
						</div>
					</div>



					<div class="span11 control-group collapse-border-top">
						<a class="btn btn-success btn-darkblue" id="submitForm">Search</a>
						<a class="btn" href="{!! url('audit-trail') !!}">Clear Filters</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if(CommonHelper::arrayHasValue($result) )
		    <h6 class="paginate">
				<span>{!! $result->appends(['filter_date_from' => old('filter_date_from'), 'filter_date_to' => old('filter_date_to')])->render() !!}&nbsp;</span>
			</h6>
		@else
			&nbsp;
		@endif
	</div>
	{{--<div class="div-buttons">--}}
		{{--@if ( CommonHelper::valueInArray('CanExportAuditTrail', $permissions) )--}}
		{{--<a class="btn btn-info btn-darkblue" id="exportList">{{ $button_export }}</a>--}}
		{{--@endif--}}
		{{--@if ( CommonHelper::valueInArray('CanArchiveAuditTrail', $permissions) )--}}
		{{--<a class="btn btn-info btn-darkblue" href={{URL::to('audit_trail/archive_logs')}}>{{ $button_archive }}</a>--}}
		{{--@endif--}}
	{{--</div>--}}
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Audit Trail</h3>
      	{{--<span class="pagination-totalItems">{{ $text_total }} {{ $audit_trails_count }}</span>--}}
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered" style="table-layout: fixed;">
				<thead>
					<tr>
						<th style="width: 30px; max-width: 30px;">No.</th>
						<th style="width: 150px; max-width: 150px;"><a href="#" class="">Transaction Date</a></th>
						<th style="width: 100px; max-width: 100px;"><a href="#" class="">MODULE</a></th>
						<th><a href="#" class="">REFERENCE</a></th>
						<th style="width: 100px; max-width: 100px;"><a href="#" class="">USERNAME</a></th>
						<th style="width: 100px; max-width: 100px;"><a href="#" class="">ACTION DONE</a></th>
						<th><a href="#" class="">DETAILS</a></th>
					</tr>
				</thead>
				<tbody>
				@if( !CommonHelper::arrayHasValue($result) )
					<tr class="font-size-13">
						<td colspan="7" class="align-center">No records found!</td>
					</tr>
				@endif

				@foreach($result as $key => $item)
					<tr class="font-size-13">
						<td>{!! $key+1 !!}</td>
						<td>{!! date('F j, Y, g:i a', strtotime( $item->created_at )) !!}</td>
						<td style="word-wrap: break-word">{!! $item->module !!}</td>
						<td>{!! $item->reference !!}</td>
						<td>{!! $item->user_id !!}</td>
						<td>{!! $item->action !!}</td>
						<td style="word-wrap: break-word">{!! $item->data_after !!}</td>
					</tr>
				@endforeach

				</tbody>
			</table>
		</div>
	</div>

</div>

<script type="text/javascript">
$(document).ready(function() {
    $('.date').datepicker({
      format: 'yyyy-mm-dd'
    });

    // Submit Form
    $('#submitForm').click(function() {
    	$('#form-audit-trail').submit();
    });

    $('#form-audit-trail input').keydown(function(e) {
		if (e.keyCode == 13) {
			$('#form-audit-trail').submit();
		}
	});

    // Clear Form
    $('#clearForm').click(function() {
    	$('#filter_date_from').val('');
		$('#filter_date_to').val('');
		$('#filter_action').val('');
		$('#filter_reference').val('');

		$('select').val('');
		$('#form-audit-trail').submit();
    });

	// Export List

});
</script>
@endsection