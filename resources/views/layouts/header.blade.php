<!DOCTYPE Html>
<Html lang="en">
<head>
    <meta charset="utf-8">
    <title>RSCI-EWMS </title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="apple-mobile-web-app-capable" content="yes">
    <link rel="shortcut icon" href="{{asset('resources/img/deb.ico')}}" />
    <link>

{!!  Html::style('resources/css/bootstrap.min.css') !!}
{!!  Html::style('resources/css/bootstrap-responsive.min.css')!!}
<!--link href="http://fonts.googleapis.com/css?family=Open+Sans:400italic,600italic,400,600"
        rel="stylesheet"-->


    <!-- (((((((((((((imported bootstrap for subemenu))))))))))))) -->
{!! Html::style('resources/bootstrap-submenu-2.0.4/dist/css/bootstrap-submenu.css') !!}
{!! Html::style('resources/bootstrap-submenu-2.0.4/dist/css/bootstrap-submenu.css.map') !!}
{!! Html::style('resources/bootstrap-submenu-2.0.4/dist/css/bootstrap-submenu.min.css') !!}
<!-- (((((((((((((imported bootstrap for subemenu))))))))))))) -->


{!! Html::style('resources/css/font-awesome.css') !!}
{!! Html::style('resources/css/datepicker.css') !!}
{!! Html::style('resources/css/style.css') !!}
{!! Html::style('resources/css/pages/dashboard.css') !!}
{!! Html::style('resources/css/pages/signin.css') !!}
{!! Html::style('resources/css/pages/reports.css') !!}
{!! Html::script('resources/js/jquery-1.7.2.min.js') !!}
{!! Html::script('resources/js/jquery-1.7.2.min.js') !!}

<!-- Le Html5 shim, for IE6-8 support of Html5 elements -->
    <!--[if lt IE 9]>
    <script src="http://Html5shim.googlecode.com/svn/trunk/Html5.js"></script>
    <![endif]-->
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="navbar-inner">
        <div class="container">
            <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </a>
            <a class="navbar-brand"></a>
            <a class="brand">Rustan Specialty Concepts, Inc - eWMS @if(env('APP_ENV') == 'local') (TEST ENVIRONMENT) @endif</a>
            <div class="nav-collapse">
                <ul class="nav pull-right">
                    @if(Auth::check())
                        <li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-user"></i> {{Auth::user()->username}} <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li><a href="{{ URL::to('users/logout') }}">@lang('general.menu_logout')</a></li>
                            </ul>
                        </li>
                    @endif
                </ul>
            </div>
            <!--/.nav-collapse -->
        </div>
        <!-- /container -->
    </div>
    <!-- /navbar-inner -->
</div>
<!-- /navbar -->
@if(Auth::check())
    <div class="subnavbar">
        <div class="subnavbar-inner">
            <div class="container">
                <ul class="mainnav">

                    @if(Gate::check('role-access', 'canViewPurchaseOrder'))
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-shopping-cart"></i>
                                <span>WH Receiving</span>
                                <b class="caret"></b>
                            </a>

                            <ul class="dropdown-menu">
                                    <li><a href="{!! url('po') !!}">Purchase Orders</a></li>
                            </ul>
                        </li>
                    @endif


                        @if(Gate::check('role-access', 'CanAccessPacking') || Gate::check('role-access', 'CanAccessShipping'))
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-share"></i>
                                    <span>Transfers</span>
                                    <b class="caret"></b>
                                </a>

                                <ul class="dropdown-menu">
                                    @if(Gate::check('role-access', 'CanAccessPacking'))
                                        <li><a href="{!! url('picking') !!}">Picking / Packing</a></li>
                                    @endif
                                    @if(Gate::check('role-access', 'CanAccessShipping'))
                                        <li><a href="{!! url('loadship')!!}">Loading / Shipping </a></li>
                                    @endif
                                </ul>

                            </li>
                        @endif


                        @if(Gate::check('role-access', 'CanAccessSublocReceiving') || Gate::check('role-access', 'CanAccessSublocPicking') || Gate::check('role-access', 'CanAccessSublocShipping'))
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-inbox"></i>
                                <span>Subloc Transfer</span>
                                <b class="caret"></b>
                            </a>

                            <ul class="dropdown-menu">
                                @if(Gate::check('role-access', 'CanAccessSublocReceiving'))
                                    <li><a href="{!! url('subloc-rcv') !!}">MTS Receiving</a>
                                @endif
                                @if(Gate::check('role-access', 'CanAccessSublocPicking'))
                                    <li><a href="{!! url('subloc/picking') !!}">Picking / Packing</a></li>
                                @endif
                                @if(Gate::check('role-access', 'CanAccessSublocShipping'))
                                    <li><a href="{!! url('subloc/loadship') !!}">Loading / Shipping</a></li>
                                @endif
                            </ul>

                        </li>
                        @endif


                        @if(Gate::check('role-access', 'CanAccessReturnToWh'))
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-refresh"></i>
                                    <span> Return to Warehouse</span>
                                    <b class="caret"></b>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="{!! url('returnwh') !!}">Return to Warehouse</a> </li>
                                    </li>
                                </ul>
                            </li>
                        @endif

                    @if(Gate::check('role-access', 'CanAccessStoreOrders'))
                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-map-marker"></i>
                                <span> Store Receiving</span>
                                <b class="caret"></b>
                            </a>

                            <ul class="dropdown-menu">
                                <li><a tabindex="-1" href="{!! url('store-receiving') !!}">Store Orders</a> </li>
                                <li><a tabindex="-1" href="{!! url('store-subloc-receiving') !!}">Subloc Receiving</a> </li>
                                </li>

                            </ul>
                        </li>
                    @endif





                        @if(Gate::check('role-access', 'CanAccessStoreMasterList') || Gate::check('role-access', 'CanAccessProductMasterList'))

                        <li class="dropdown">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                <i class="icon-list-alt"></i>
                                <span>Masterlist</span>
                                <b class="caret"></b>
                            </a>

                            <ul class="dropdown-menu">
                                    <li><a href="{!! url('masterlist/products') !!}">Product Masterlist</a></li>
                                    <li><a href="{!! url('masterlist/slots') !!}">Slot Masterlist</a></li>
                                    <li><a href="{!! url('masterlist/stores') !!}">Store Masterlist</a></li>
                                     <li><a href="{!! url('audit-trail') !!}">Audit Trail</a></li>
                            </ul>
                        </li>

                        @endif


                        @if(Gate::check('role-access', 'CanAccessUsers') || Gate::check('role-access', 'CanAccessUserRoles'))
                            <li class="dropdown">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="icon-cogs"></i>
                                    <span>System</span>
                                    <b class="caret"></b>
                                </a>

                                <ul class="dropdown-menu">
                                        <li><a href="{!! url('users') !!}">User</a></li>
                                    @if( Gate::check('role-access', 'CanAccessUserRoles') )
                                        <li><a href="{!! url('users/role') !!}">User Roles</a></li>
                                    @endif
                                </ul>
                            </li>
                        @endif
                </ul>
            </div>
            <!-- /container -->
        </div>
        <!-- /subnavbar-inner -->
    </div>
@endif
<!-- /subnavbar -->
<div class="main">
    <div class="main-inner">
        <div class="container">
            <div class="row">
                <div class="span12">
                    @if(Session::has('message'))
                        <div class="alert alert-info">
                            <button class="close" data-dismiss="alert" type="button">&times;</button>
                            {{ Session::get('message') }}
                        </div>
@endif