</div>
<!-- /span12 -->
</div>
<!-- /row -->
</div>
<!-- /container -->
</div>
<!-- /main-inner -->
</div>
<!-- /main -->
<div class="footer collapse">
    <div class="footer-inner">
        <div class="container">
            <div class="row">
                <div class="span12"></div>
                <!-- /span12 -->
            </div>
            <!-- /row -->
        </div>
        <!-- /container -->
    </div>
    <!-- /footer-inner -->
</div>
<!-- /footer -->
<!-- Le javascript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
{!! HTML::script('resources/js/excanvas.min.js') !!}
{!! HTML::script('resources/js/chart.min.js') !!}
{!! HTML::script('resources/js/bootstrap.js') !!}
{!! HTML::script('resources/js/bootstrap-datepicker.js') !!}
{!! HTML::script('resources/js/full-calendar/fullcalendar.min.js') !!}

{!! HTML::script('resources/js/base.js') !!}

<script>
    $(document).ready(function(){
        $('.dropdown-submenu a.test').on("click", function(e){
            $(this).next('ul').toggle();
            e.stopPropagation();
            e.preventDefault();
        });
    });
</script>
</body>
</html>
