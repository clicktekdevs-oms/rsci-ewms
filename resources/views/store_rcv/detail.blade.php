@extends('layouts.main')
@section('content')
<div class="control-group">
	<a href="{!! url('store-receiving') !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i>Back to List</a>

<!--   -->

</div>
<!-- PO Detail -->
<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Pell Details</h3>
    </div>
    <!-- /widget-header -->
	<div class="widget-content">
	   <div class="row-fluid stats-box">
	      	<div class="span4">
	        	<div>
		        	<span class="left-pane">Pell No.</span>
		        	<span class="right-pane"><input type="text" value="{!! $pell !!}" readonly></span>
		        </div>
		       
	      	</div>

	      	<div class="span4">
	      		<div>
		        	<span class="left-pane">Ship Date</span>
		        	<span class="right-pane"> <input type="text" readonly value="{!! $ship_date !!}"></span>
		        </div>
		       
		      
		        
	      </div>
	   </div>
	 </div>
</div>
{!! Form::open(array('url' => 'store-receiving/assign', 'method' => 'post')) !!}
<div class="div-buttons">

	<button type="submit"  name="action_button" value="assign" class="btn btn-darkblue btn-info" style="margin-right: 5px;" id="assignClerk">Assign to Store Clerk</button>
	<div class="btn-group div-buttons">
		<button type="button" class="btn btn-info btn-darkblue " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Reports <span class="caret"></span>
		</button>
		<ul class="dropdown-menu">
			<li><button type="submit" style="display: block;
											clear: both;
											font-weight: 400;
											line-height: 1.42857143;
											color: #333;
											white-space: nowrap;
											background-color: #FFFFFF;
											border-style: hidden;" name="action_button" value="unlisted">Unlisted Report</button></li>
			<li><button type="submit" style="display: block;
											clear: both;
											font-weight: 400;
											line-height: 1.42857143;
											color: #333;
											white-space: nowrap;
											background-color: #FFFFFF;
											border-style: hidden;" name="action_button" value="discrepancy">Discrepancy Report</button></li>
			<li><button type="submit" style="display: block;
											clear: both;
											font-weight: 400;
											line-height: 1.42857143;
											color: #333;
											white-space: nowrap;
											background-color: #FFFFFF;
											border-style: hidden;" name="action_button" value="summary">Summary Report</button></li>
		</ul>

	</div>
</div>
  
  	<div class="clear">
	<div class="div-paginate">
		{{--@if(CommonHelper::arrayHasValue($store_orders) )--}}
		    {{--<h6 class="paginate">--}}
				{{--<span>{{ $store_orders->appends($arrFilters)->links() }}&nbsp;</span>--}}
			{{--</h6>--}}
		{{--@else--}}
			{{--&nbsp;--}}
		{{--@endif--}}
	</div>
 
</div>
<div class="widget widget-table action-table">


    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Box List</h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
					 
						<th style="width: 20px;" class="align-center"><input type="checkbox" id="box-all" /></th>
						<th>No.</th>
						<th>Box Number</th>
						<th>Mts</th>
						<th>Receiver</th>
						<th>Total Qty Shipped</th>
						<th>Total Qty Scanned</th>
						<th>Status</th>
				 
					</tr>
				</thead>
				@if( !CommonHelper::arrayHasValue($box_lists) )
				<tr class="font-size-13">
					<td colspan="4" class="align-center"No records found!</td>
				</tr>
				@else
					@foreach( $box_lists as $key => $list )
						<tr>
							<td><input type="checkbox" id="box_no" name="box_no[]" value="{!! $list->box_no !!}"></td>
							<td>{!! $key+1 !!}</td>
							<td ><a href="{!! url('store-receiving/box-detail/'.$list->box_no) !!}">{!! $list->box_no !!}</a></td>
							<td style="word-break: break-all">{!! $list->tl_no !!}</td>
							<td>{!! ucwords(  $list->store_clerk )!!}</td>
							<td>{!! $list->total_qty_rcv !!}</td>
							<td>{!! $list->total_qty_scan !!}</td>
							<td><b>{!! CommonHelper::getStoreStatus( $list->is_box_status ) !!}</b></td>
						</tr>
					@endforeach
				@endif

				<input type="hidden" name="pell" value="{!! $pell !!}">
				<input type="hidden" name="ship_date" value="{!! $ship_date !!}">


			</table>
		</div>
	</div>
</div>
{!! Form::close() !!}
<script>
    $('#assignClerk').click(function() {
        var sel = $('#box_no:checked').map(function(_, el) {
            return $(el).val();
        }).get();
        if(sel== "") {
            alert('Please select Box!');
            return false;
        }else
        {
            return confirm('Assign Box ?');
        }
        $('#po_no').val(sel);
    })

    $('#box-all').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });
</script>
@endsection