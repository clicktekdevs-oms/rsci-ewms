

 
<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3> 	Store Receiving Detail</h3>
    </div>
    
	<div class="widget-content">
	   <div class="row-fluid stats-box">
	      	<div class="span4">
	        	<div>
		        <span class="left-pane"> TL number : </span>
		        	<span class="left-pane"> {{ Form::text('so_no', $so_no, array('id' => 'so_no', 'readonly' => 'readonly')) }}</span>
		        </div>
		        <div>
		        	<span class="left-pane"> </span>
		        	<span class="right-pane"> </span>
		        </div>
	      	</div>

	      	<div class="span4">
	      		<div>
		        	<span class="left-pane">  </span>
		        	<span class="left-pane">  </span>
		        </div>
		        <div>
		        </div>
		        <div>
		        	<span class="left-pane"> </span>
		        	<span class="right-pane"> 
	      </div>

	      
	   </div>
	 </div>
</div>
 
<div class="clear">
	<div class="div-paginate">
		
		    <h6 class="paginate">
				<span> &nbsp;</span>
			</h6>
		
			 
		
	</div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3> Store Receiving content</h3>
      <span class="pagination-totalItems"> </span>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th> No. </th>
					 
						<th> sku   </th>
						<th> upc </th>
						<th> Short Name</th>
						<th> Ordered quantity </th>
						<th> Quantity Received </th>
						<!-- <th> Total Item</th> -->
					</tr>
				</thead>
			 
		 		@if( !CommonHelper::arrayHasValue($totalitem) )
				<tr class="font-size-13">
					<td colspan="9" style="text-align: center;"> No result found</td>
				</tr>
				@else
			 	@foreach($totalitem as $tir )
					<tr class="font-size-13">
						<td> {{$counter++}}</td>
						 
						<td>{{$tir->eeskeeyou}}</td>
						<td> {{$tir->eskeeyu}} </td>
						<td> {{$tir->desccc}}</td>
						<td> {{$tir->rcv_qty}}</td>
						<td> {{$tir->delv}}</td>
						 
					 </tr>
				 @endforeach
				 @endif
			</table>
		</div>
	</div>

	
    <h6 class="paginate">
		<span> </span>
	</h6>
	

</div>
 