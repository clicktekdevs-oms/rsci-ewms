@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
			<div class="accordion-group search-panel">
				{!! Form::open(array('url'=>'user/role/filter', 'class'=>'form-signin', 'id'=>'form-user-roles', 'role'=>'form', 'method' => 'get'))  !!}
				<div id="collapseOne" class="accordion-body collapse in search-panel-content">
					<div class="span4">
						<div>
							<span class="search-po-left-pane">Role :</span>
							<span class="search-po-right-pane">
								<input type="text" class="filter_role_name" id="filter_role_name" name="role_name" value="{!! old('role_name') !!}">
							</span>
						</div>
					</div>
					
					<div class="span11 control-group collapse-border-top">
						<button class="btn btn-success btn-darkblue" type="submit">Search</button>
						<a class="btn" href="{!! url('users/role') !!}">Clear Filters</a>
					</div>
				</div>
				{!! Form::close() !!}
			</div>
		</div>
	</div> <!-- /controls -->	
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-buttons">
		<a class="btn btn-info" href="{!! url('user/role/add') !!}">Add new User Role</a>
	</div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>User Roles</h3>
    </div>
    <!-- /widget-header -->
    
    <div class="widget-content">
    	<div class="table-responsive">
	    	{!! Form::open(array('url'=>'user_roles/delete', 'class'=>'form-signin', 'id'=>'form', 'role'=>'form', 'method' => 'post', 'style' => 'margin-bottom: 0px;'))  !!}
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th style="width: 20px;" class="align-center"><input type="checkbox" id="main-selected" /></th>
						<th>No.</th>
						<th><a href="#" class="">ROLE </a></th>
						<th style="width: 80px;" class="align-center">ACTION</th>
					</tr>
				</thead>
				<tbody>
				@if(count($roles) <=0)
					<tr>
						<td colspan="4">No records found!</td>
					</tr>
				@endif
					@foreach( $roles as $key => $role)
						<tr>
							<td><input type="checkbox"></td>
							<td>{!! $key+1 !!}</td>
							<td>{!! $role->role_name !!}</td>
							<td><a href="{!! url('user/role/edit/'.$role->id) !!}" title="Edit role"><i class="icon-edit"></i> </a> </td>
						</tr>
					@endforeach
				</tbody>
			</table>
			{!! Form::close() !!}
		</div>
	</div>
</div>
@endsection