@extends('layouts.main')
@section('content')
	@if( $errors->all() )
		<div class="alert alert-error">
			<button class="close" data-dismiss="alert" type="button">&times;</button>
			@foreach($errors->all() as $error)
				<ul>
					<li>{!! $error !!}</li>
				</ul>
			@endforeach
		</div>
	@endif
	<div class="widget">
		<div class="widget-header"> <i class="icon-group"></i>
			<h3>New User Role</h3>
		</div>
		<!-- /widget-header -->

		<div class="widget-content">
			{!! Form::open(array('url'=>'user/roles/update', 'class'=>'form-horizontal', 'id'=>'form-user-roles', 'role'=>'form', 'method' => 'post'))  !!}
			<div class="span11">
				<fieldset>
					<div class="control-group">
						<label class="control-label" for="role_name">Role Name :</label>
						<div class="controls">
							<input type="text" name="role_name" value="{!! $role_name !!}">
							<input type="hidden" name="role_id" value="{!! $role_id !!}">
						</div> <!-- /controls -->
					</div> <!-- /control-group -->

					<div class="control-group">
						<label class="control-label" for="group_access">Permissions :</label>

					</div> <!-- /control-group -->
				</fieldset>
			</div>
			<div class="span11">
				<table class="table table-condensed table-bordered" style="text-align: center;">
					<thead>
					<tr>
						<th class="align-center"> MODULE</th>
						<th class="align-center">ACCESS</th>
					</tr>
					</thead>
					<tbody>
					<tr>
						<td class="align-center font-12">Purchase Orders</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="canViewPurchaseOrder" @if(CommonHelper::valueInArray('canViewPurchaseOrder', $permission)) checked @endif/></td>
					</tr>

					<tr>
						<td class="align-center font-12">Picking / Packing</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessPacking" @if(CommonHelper::valueInArray('CanAccessPacking', $permission)) checked @endif /></td>
					</tr>

					<tr>
						<td class="align-center font-12">Loading / Shipping</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessShipping" @if(CommonHelper::valueInArray('CanAccessShipping', $permission)) checked @endif /></td>
					</tr>

					<tr>
						<td class="align-center font-12">Subloc Receiving</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessSublocReceiving" @if(CommonHelper::valueInArray('CanAccessSublocReceiving', $permission)) checked @endif  /></td>
					</tr>


					<tr>
						<td class="align-center font-12">Subloc Picking</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessSublocPicking" @if(CommonHelper::valueInArray('CanAccessSublocPicking', $permission)) checked @endif /></td>
					</tr>


					<tr>
						<td class="align-center font-12">Subloc Loading / Shipping</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessSublocShipping" @if(CommonHelper::valueInArray('CanAccessSublocShipping', $permission)) checked @endif  /></td>
					</tr>

					<tr>
						<td class="align-center font-12">Return To Warehouse</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessReturnToWh" @if(CommonHelper::valueInArray('CanAccessReturnToWh', $permission)) checked @endif /></td>
					</tr>

					<tr>
						<td class="align-center font-12">Store Receiving</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessStoreOrders" @if(CommonHelper::valueInArray('CanAccessStoreOrders', $permission)) checked @endif/></td>
					</tr>

					<tr>
						<td class="align-center font-12">Product Master List</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessProductMasterList" @if(CommonHelper::valueInArray('CanAccessProductMasterList', $permission)) checked @endif/></td>
					</tr>
					<tr>
						<td class="align-center font-12">Store Master List</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessStoreMasterList" @if(CommonHelper::valueInArray('CanAccessStoreMasterList', $permission)) checked @endif/></td>
					</tr>

					<tr>
						<td class="align-center font-12">Users</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessUsers" @if(CommonHelper::valueInArray('CanAccessUsers', $permission)) checked @endif/></td>
					</tr>
					<tr>
						<td class="align-center font-12">User Roles</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessUserRoles" @if(CommonHelper::valueInArray('CanAccessUserRoles', $permission)) checked @endif/></td>
					</tr>
					<tr>
						<td class="align-center font-12">Audit Trail</td>
						<td class="align-center"><input type="checkbox" class="checkbox" name="permissions[]" value="CanAccessAuditTrail" @if(CommonHelper::valueInArray('CanAccessAuditTrail', $permission)) checked @endif/></td>

					</tr>

					</tbody>
				</table>

				<button class="btn btn-info" type="submit">Submit</button>
				<a class="btn" href="#">Cancel</a>
			</div> <!-- /controls -->
			{!! Form::close() !!}
		</div>
	</div>
@endsection