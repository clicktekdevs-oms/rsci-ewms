@extends('layouts.print')
@section('content')
<style>
@media print {
    .soContainer {page-break-after: always; page-break-inside: avoid;}
    #actionButtons {display: none;}

}
@media screen {
    #mainContainer {width: 750px; }

}
body { font: normal 12px arial; margin: 0; counter-reset:pageNumber;}
table {padding: 0; border-collapse: collapse;}
h2 {margin-bottom: 1px;}
header {margin-bottom: 20px;}

.soContainer { border: solid 1px #000; padding: 10px;}
.soContainer:after { content:""; display:block; clear:both; }

.doctitle {text-align: center;}
.commonInfo {width: 100%; border: none;}
.commonInfo th, .commonInfo td  {text-align: left;}
.commonInfo th {width: 150px;}

.contents {margin-top: 5px; width: 100%;}
.contents th, .contents td {border: solid 1px #F0F0F0; margin: 0; }
.contents th {text-align: left;}
.contents th {background-color: #F0F0F0}

.contentasdfs {width: 100%; }
.contentasdfs th, .contentasdfs  td {border: solid 0px #F0F0F0; margin-right: 0px; }
  

.comments {width: 100%; margin-top: 15px;}
.comments hr{ margin-top:25px;}

.signatories {width: 100%; margin-top: 25px; line-height: 20px;  }
.signatories div {float: left; width: 30%; margin-right: 3%;}
.signatories hr{margin-top:25px;}

#actionButtons { top:0; left: 0; background-color: #DFF1F7; padding: 5px;}
#actionButtons a {display: inline-block; padding: 1em; background-color: #3199BE; text-decoration: none; font: bold 1em Verdana ; color: #FFF;}
#actionButtons a:hover {background-color: #1F6077 ;}

</style>

<div id="actionButtons">
	<a href="#" onclick="window.print();">PRINT THIS</a>
	<a href="{!! URL::previous() !!}">BACK TO LOAD LIST</a>

</div>
	<section class="soContainer">
	  			<div class="doctitle"> <h2>RSCI - eWMS<br/>PACKING, EQUIPMENT & LOADING LIST</h2></div>
				 <!-- <div style="text-align: center">Print Date: {{ date('m/d/y h:i A')}}</div> -->
			<hr>
	 
		<table class="commonInfo" style="font-size: 14px">
			<tr>
				<td>
					<table>
						<tr><th style="text-align: right">Origin :<td style="border: solid 0px #000;"><b>{!! ucwords($from_store_name[0]) !!}</b></td></tr></tr>
						<tr><th style="text-align: right";>Destination :<td style="border: solid 0px #000;"><b>{!! $store_name[0] !!}</b></td></tr>
						<tr><th style="text-align: right";>Seal #1 :<td><label type=""  value="" placeholder="" style="border: solid 0px #000;">{!! $load_list->seal_no !!}</label> </td></tr>
						<tr><th style="text-align: right";>Truck Van Plate no.:<td><label style="border: solid 0px #000;">{!! $load_list->plate_no !!}</label></td></tr>
					  	<tr><th style="text-align: right";>Delivery Date:<td><label>{!! $ship_date !!}</label></td></th></tr>
					</table>
				</td>
			<tr>
		</table>
		<table class="contents">
			<tr>
				<th colspan="3" style="text-align: center"><h3> Pell no. : {!! $load_code !!}</h3></th>
			</tr>
			 
            <tr>
				<th style="text-align: center">Box No.</th>
				<th style="text-align: center">MTS No.</th>
				<th style="text-align: center">QTY</th>
			</tr>
            <tr>
	<?php

	?>
	<?php
		$mov_qty = 0;
	?>

				@foreach( $details as $detail )
				<tr class="font-size-13 tblrow" >

					<td style="text-align: center">{!! $detail->box_no !!}</td>
					<td style="text-align: center">{!! $detail->tl_no !!}</td>
					<td style="text-align: center">{!! $detail->mov_qty !!}</td>
				<?php $mov_qty+=$detail->mov_qty?>
				</tr>
				@endforeach

			<tr>
				 
	 				<th style="text-align: right"> Total : </th> 
	 				<th style="text-align: center">{!! count( $details ) !!}</th>
	 				<th style="text-align: center"><?php echo $mov_qty ?></th>

			</tr>
		</table>
	 
<br>
	<table class="commonInfo" >
			<tr>
				<td>
					<table>
					   <tr><th style="text-align: right";> Counted/Loaded By/Date:
						   <td><label style="font-weight:700px;border: solid 0px #000;">{!! $piler !!}</label></td> </td>
					   </th></tr>
					   <tr><th style="text-align: right";> Delivery/Helper:
						   <td><label style="border: solid 0px #000;">{!! ucwords($load_list->delivery_helper) !!}</label></td> </td>
					   </th></tr>
					   <tr><th style="text-align: right";> Driver:
						   <td><label style="border: solid 0px #000;">{!! ucwords( $load_list->driver_name ) !!}</td></td> </td>
					   </th></tr>
					</table>
				</td>

				<td>
					<table>
					<tr><th style="text-align: right";> Witnessed/Sealed:
						<td><label placeholder=" (Sec. Guard)"  style="border: solid 0px #000;">{!! ucwords( $load_list->witnessed_sealed ) !!}</label></td> </td>
					   </th></tr>
					<tr><th style="text-align: right";> Received By/Date:
						<td><label style="border: solid 0px #000;"></label></td> </td>
					   </th></tr>
					</table>
				</td>
			 
			<tr>
		</table>
	</section>

	@endsection