@extends('layouts.main')
@section('content')
 <div class="control-group">
    <a href="{!! url('subloc/loadship') !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i> Back to List</a>
    
</div>
            <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Pell Number details</h3>
     </div>

    <!-- /widget-header -->
	<div class="widget-content">
        {!! Form::open(array('url' => 'subloc/loadship/add', 'method' => 'post')) !!}

        <div class="row-fluid">
	      	<div class="span4">
	        	<div>
		        	<span class="left-pane">Pell No. :</span>
		        	<span class="left-pane">
                        <input type="text" readonly value="{!! $load_code !!}" name="pell_no">
                    </span>

		        </div>

	   </div>
	   <div class="span4">
	        	
                 <div>
                    <span class="left-pane">Stock Piler :</span>
                    <span class="left-pane">
                        <input type="text" readonly value="{!! ucwords( $piler->fullname ) !!}">
                    </span>
                </div>
	   </div>
       </div>

           <div class="row-fluid">
               <div class="span4">
                   <div>
                       <span class="left-pane">Seal No. :</span>
                       <span class="left-pane">
                        <input type="text" {!! $piler->load_status == 0 ? 'disabled' : '' !!} value="{!! $piler->seal_no !!}" name="seal_no">
                    </span>
                   </div>
               </div>

               <div class="span4">
                   <div>
                       <span class="left-pane">Van/Plate No :</span>
                       <span class="left-pane">
                        <input type="text" {!! $piler->load_status == 0 ? 'disabled' : '' !!} value="{!! $piler->plate_no !!}" name="plate_no">
                    </span>
                   </div>
               </div>
           </div>

           <div class="row-fluid">
               <div class="span4">
                   <div>
                       <span class="left-pane">Delivery/Helper :</span>
                       <span class="left-pane">
                        <input type="text" {!! $piler->load_status == 0 ? 'disabled' : '' !!} value="{!! $piler->delivery_helper !!}" name="helper">
                    </span>
                   </div>
               </div>

               <div class="span4">
                   <div>
                       <span class="left-pane">Driver :</span>
                       <span class="left-pane">
                        <input type="text" {!! $piler->load_status == 0 ? 'disabled' : '' !!} value="{!! $piler->driver_name !!}" name="driver">
                    </span>
                   </div>
               </div>
           </div>

           <div class="row-fluid">
               <div class="span4">
                   <div>
                       <span class="left-pane">Witnessed/Sealed :</span>
                       <span class="left-pane">
                        <input type="text" {!! $piler->load_status == 0 ? 'disabled' : '' !!} value="{!! $piler->witnessed_sealed !!}" name="witness_sealed">
                    </span>
                   </div>
               </div>
           </div>

        <hr>
        <div class="row-fluid">
            <div class="span4">

                    <button class="btn btn-primary" type="submit">Submit</button>
                    <a class="btn btn-default">Clear Fields</a>

            </div>
        </div>
        {!! Form::close() !!}
     </div>

{!! Form::open(array('url' => 'subloc/loadship/removebox', 'method' => 'post')) !!}
          @if( $piler->load_status != 1 && $piler->load_status != 0 )
              <div class="div-buttons" style="margin-top: 5px">
                  <button type="submit" class="btn btn-info btn-darkblue removebutton" title="Its allow you to remove box no">Remove Box No.</button>
              </div>
          @endif

 

<div class="widget widget-table action-table" style="margin-top: 10px">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Pell number Content</h3>
       <span class="pagination-totalItems">  </span>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
                        @if( $piler->load_status != 1 && $piler->load_status != 0 )
                            <th style="width: 20px;" class="align-center"><input type="checkbox" id="selectall"></th>
                        @endif
				 	    <th>No.</th>
						<th>Box No.</th>
                        <th>MTS no.</th>
                        <th>From Store</th>
                        <th>Store Name</th>
                        <th>Store Address</th>
					</tr>
				</thead>
							
		 	    @if( !CommonHelper::arrayHasValue( $box_details )  )
					<tr class="font-size-13">
						<td colspan="7" style="text-align: center;">No Results Found</td>
					</tr>
			    @else
                    @foreach( $box_details as $key => $detail )
                        <tr class="tr_row">
                            @if( $piler->load_status != 1 && $piler->load_status != 0 )
                                <td>
                                    <input type="checkbox" name="box_no[]" value="{!! $detail->box_no !!}">
                                    <input type="hidden" name="load_code" value="{!! $load_code !!}">
                                    <input type="hidden" name="tl_no[]" value="{!! $detail->tl_no !!}">
                                </td>
                            @endif
                            <td>{!! $key+1 !!}</td>
                            <td>{!! $detail->box_no !!}</td>
                            <td>{!! $detail->tl_no !!}</td>
                            <td>{!! $detail->from_store_nam !!}</td>
                            <td>{!! $detail->store_num !!}</td>
                           <td>{!! $detail->store_add !!}</td>
                        </tr>
                    @endforeach
                @endif
			</table>
		</div>
	</div>
</div>
    {!! Form::close() !!}
    <script>
        $('.tr_row').on('click', function(e) {
            if ($(e.target).is(':checkbox')) {
                return;
            }
            var checkBox = $(this).find('input[type="checkbox"]');
            if (checkBox.is(':checked')) {
                checkBox.prop("checked", false);
            }else if(checkBox.attr("disabled")) {
                checkBox.prop("disabled", true);
            } else if (!checkBox.is(':checked')) {
                checkBox.prop("checked", true);
            }
        });


        $('#selectall').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });
    </script>
 @endsection
