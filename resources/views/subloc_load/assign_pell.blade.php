@extends('layouts.main')
@section('content')
<div class="control-group">
<div class="control-group">
    <a href="{!! url('subloc/loadship') !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i>Back to List</a>
    
</div>
    <div class="controls">
        <div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #FFFFFF;">
            <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Pell number Content</h3>
    </div>
            <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
                   
                    <div class="span3">
                     <div>
                             <span class="search-po-left-pane">Pell No :</span>
                            <span class="search-po-right-pane">
                                <input type="text" id="loadnumber" value="{!! $load_code !!}" readonly="">
                            </span>
                    </div>
                    </div>

                {!! Form::open(array('url' => "subloc/loadship/assignbox/$load_code/filter", 'method' => 'get')) !!}
                <div class="span4">
                    <div>
                        <input type="hidden" value="{!! $load_code !!}" name="load_code">
                        <span class="search-po-left-pane">Store :</span>
                        <span class="search-po-right-pane">
                                <select name="store">
                                <option hidden value="{!! old('store') !!}">{!! isset($store_name) ? $store_name : 'Select Store' !!}</option>
                                    @foreach( $stores as $store )
                                        <option value="{!! $store->store_num !!}">{!! $store->store_nam !!}</option>
                                    @endforeach
                            </select>
                            </span>

                    </div>
                </div>
                <button type="submit" style="margin-left: -20px;" class="btn btn-info" title="Search Stores"><i class="icon icon-search"></i> </button>
                <a href="{!! url('subloc/loadship/assignbox/'.$load_code ) !!}" class="btn btn-info" title="Clear filters"><i class="icon icon-eraser"></i> </a>
                {!! Form::close() !!}
          </div>
        </div>

    </div> <!-- /controls -->
</div> <!-- /control-group -->
{!! Form::open(array('url' => 'subloc/loadship/assign-to-pell', 'method' => 'post')) !!}
<div class="div-buttons">
            <button type="submit" class="btn btn-info btn-darkblue assignTLnumber" title="Assign To Pell number">Assign to Pell No.</button>
    </div>


<div class="clear">
    <div class="div-paginate">
   
          
    {{--@if( CommonHelper::arrayHasValue($picklist) )--}}
    {{--<h6 class="paginate">--}}
        {{--<span>{{ $picklist->appends($arrFilters)->links() }}</span>--}}
    {{--</h6>--}}
    {{--@endif--}}
       
    </div>
    <div class="div-buttons">
       
    </div>
</div>
<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
     <h3>Box number list</h3>
      <span class="pagination-totalItems"></span>
    </div>

    <div class="widget-content">
        <div class="table-responsive">
            <table class="table table-striped table-bordered">
                <thead>
                    <tr>
                        <th style="width: 20px;" class="align-center"><input type="checkbox" id="selectall"></th>
                        <th>No.</th>
                        <th>Box no.</th>
                        <th>MTS no.</th>
                        <th>From Store</th>
                        <th>To Store</th>
                        <th>Store Address</th>
                    </tr>
                </thead>

                    @if( !CommonHelper::arrayHasValue($box_list)  )
                    <tr class="font-size-13">
                        <td colspan="7" style="text-align: center;">No Results Found</td>
                    </tr>
                    @else

                        <?php $i = 0; ?>
                        @foreach( $box_list as $key => $list )

                            <tr class="tr_row">
                                <td>
                                    <input type="checkbox" value="{!! $i++ !!}" name="checkbox[]">
                                    <input type="hidden" value="{!! $list->box_no !!}" name="box_no[]">
                                    <input type="hidden" value="{!! $load_code !!}" name="load_code">
                                    <input type="hidden" value="{!! $list->tl_no !!}" name="tl_no[]">
                                    <input type="hidden" value="{!! $list->from_store_num !!}" name="from_store_num[]">
                                    <input type="hidden" value="{!! $list->store_num !!}" name="store_num[]">
                                </td>
                                <td>{!! $key+1 !!}</td>
                                <td>{!! $list->box_no !!}</td>
                                <td>{!! $list->tl_no !!}</td>
                                <td>{!! $list->from_store_nam !!}</td>
                                <td>{!! $list->store_nam !!}</td>
                                <td>{!! $list->store_add !!}</td>
                            </tr>
                        @endforeach
                    @endif

            </table>
        </div>
    </div>  
</div>
</div>

{!! Form::close() !!}
<script>
    $('.tr_row').on('click', function(e) {
        if ($(e.target).is(':checkbox')) {
            return;
        }
        var checkBox = $(this).find('input[type="checkbox"]');
        if (checkBox.is(':checked')) {
            checkBox.prop("checked", false);
        }else if(checkBox.attr("disabled")) {
            checkBox.prop("disabled", true);
        } else if (!checkBox.is(':checked')) {
            checkBox.prop("checked", true);
        }
    });


    $('#selectall').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });


</script>

@endsection
