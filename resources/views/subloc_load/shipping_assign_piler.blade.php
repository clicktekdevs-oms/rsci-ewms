@extends('layouts.main')
@section('content')
    <div class="widget">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Assign to Stock Piler</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	{!!  Form::open(array('url'=>'subloc/loadship/assigned_to', 'method'=>"post", 'class'=>'form-horizontal', 'style' => 'margin: 0px;', 'role'=>'form'))  !!}

            <div class="span3">&nbsp;</div>
            		<div class="span7 add-piler-wrapper">
            			<div class="control-group">
            				<label class="control-label">Pell number :</label>
            				<div class="controls">
                                <textarea READONLY rows="6" style="resize: none" name="pell">
                                    {!! $pell !!}
                                </textarea>
                                <input type="hidden" value="{!! $pell !!}" name="pell_list">
            				</div> <!-- /controls -->
            			</div> <!-- /control-group -->

                            <div class="control-group">
                                <label class="control-label">Stock Piler :</label>
                                <div class="controls">
                                   <select name="stockpiler">
                                        <option hidden>Select :</option>
                                        <option value="">Unassigned All</option>
                                       @foreach( $users as $user)
                                        <option value="{!! $user->id !!}">{!! ucwords( $user->fullname ) !!}</option>
                                       @endforeach
                                   </select>
                                </div>
                            </div>


                    </div>
                    <div class="span3">&nbsp;</div>
                    <div class="span7">
                        <div class="control-group">
                            <label class="control-label" for=""></label>
                            <div class="controls">
                                <button class="btn btn-info" type="submit">Assign</button>
                                <a class="btn" href="{!! url('subloc/loadship') !!}">Cancel</a>
                            </div> <!-- /controls -->
                        </div> <!-- /control-group -->
                    </div>

		{!! Form::close() !!}
	</div>

</div>
@endsection

