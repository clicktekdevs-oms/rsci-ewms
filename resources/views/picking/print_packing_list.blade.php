@extends('layouts.print')
@section('content')
<style>
@media print {
    .soContainer {page-break-after: always; page-break-inside: avoid;}
    #actionButtons {display: none;}

}
@media screen {
    #mainContainer {width: 750px; }

}
body { font: normal 12px arial; margin: 0; counter-reset:pageNumber;}
table {padding: 0; border-collapse: collapse;}
h1 {margin-bottom: 5px;}
header {margin-bottom: 20px;}

.soContainer { border: solid 1px #000; padding: 10px;}
.soContainer:after { content:""; display:block; clear:both; }

.doctitle {text-align: center;}
.commonInfo {width: 100%; border: none;}
.commonInfo th, .commonInfo td  {text-align: left;}
.commonInfo th {width: 150px;}

.contents {margin-top: 10px; width: 100%;}
.contents th, .contents td {border: solid 1px #F0F0F0; margin: 0; }
.contents th {text-align: left; padding: 5px;}
.contents th {background-color: #F0F0F0}

.contents2 {margin-top: 10px; width: 100%;}
.contents2 th, .contents td {margin: 0; }
.contents2 th {text-align: left; padding: 5px;}
.contents2 th {background-color: #F0F0F0}

.comments {width: 100%; margin-top: 15px;}
.comments hr{ margin-top:25px;}

.signatories {width: 100%; margin-top: 25px; line-height: 20px;  }
.signatories div {float: left; width: 30%; margin-right: 3%;}
.signatories hr{margin-top:25px;}
td.underline hr{ margin-top: 10px; border: none;border-bottom: solid 1px #000;}
td.underline {border-bottom: solid 1px #000;}
td.plain {border: 1px #F0F0F0; margin: 0;}
td.plainUnderline {border: 1px #F0F0F0; margin: 0;border-bottom: solid 1px #000;}

#actionButtons { top:0; left: 0; background-color: #DFF1F7; padding: 5px;}
#actionButtons a {display: inline-block; padding: 1em; background-color: #3199BE; text-decoration: none; font: bold 1em Verdana ; color: #FFF;}
#actionButtons a:hover {background-color: #1F6077 ;}

</style>

<div id="actionButtons">
	<a  onclick="window.print();">PRINT THIS</a>
	<a href="{!! URL::previous() !!}">BACK TO PICKING  LIST</a>

</div>
 
	<section class="soContainer">
		<header>
			<div class="doctitle">
				<h1>eWMS - RUSTAN SPECIALTY CONCEPTS, INC <br/>MTS REPORT</h1>
			 
			 
			</div>
		</header>
		<table class="commonInfo">
			<tr>
				<td>
					<table>
						<tr>
							<th>MTS no: </th>
							<td>{!! $lists->tl_no !!}</td>
						</tr>
						<tr>
						<th>From Location: </th>
						<td> 8001 - Warehouse </td>	
						</tr>
						<tr>
							<th> To Location :</th>
							<td>{!! $lists->to_loc !!} - {!! strtoupper( $lists->to_loc_desc ) !!}</td>
						</tr>
						<tr>
							<th> Division :</th>
							<td>{!! $lists->dept_code !!} - {!! $lists->dept_desc !!}</td>
						</tr>
					</table>
				</td>
				<td>
					<table>
						 
						 
						<tr>
							<th>Delivery Date : </th>
							<td class="underline">{!! date('F d, Y', strtotime( $lists->ship_date )) !!}</td>
						</tr>
						<tr>
							<th>MTS Date :</th>
							<td class="underline">{!! date('m/d/y h:i A') !!}</td>

						</tr>
						<tr>
							<th>User name :</th>
							<td class="underline">{!! strtoupper( Auth::user()->username ) !!}</td>
						</tr>
					</table>
				</td>
			<tr>
		</table>
		<table class="contents">
			<tr> <!-- 
				<th rowspan="2" style="text-align: center">Ref MTS No.</th> -->


				<th rowspan="2" style="text-align: center">Style</th>
				<th rowspan="2" style="text-align: center">SKU</th>
				<th rowspan="2" style="text-align: center">UPC</th>
				<th rowspan="2" style="text-align: center">Description</th>
				<th rowspan="2" style="text-align: center">Retail Price</th>
				<th colspan="2" style="text-align: center">Quantity</th>
			</tr>
            <tr>
				<th style="text-align: center">Issued</th>
				<th style="text-align: center">Received</th>
			</tr>



            <?php $qty_total_mov = 0; ?>

				@foreach( $details as $key =>  $detail)

					@foreach( $detail as $list )
						<tr>
							<td colspan="3" style="text-align: center;font-weight: 700">Box no : {!! $list->box_no !!}</td>
						</tr>
                    <?php $qty_rcv = 0; ?>

				@foreach( CommonHelper::getBoxDetails( $list->tl_no, $list->box_no ) as $value )

						<tr>
						<td style="text-align: center">{!! $value->short_desc !!}</td>
						<td style="text-align: center">{!! $value->sku !!}</td>
						<td style="text-align: center">{!! $value->upc !!}</td>
						<td style="text-align: center">{!! $value->item_desc !!}</td>
							<td style="text-align: center">{!! number_format($value->rtl_price, 2) !!}</td>
						<td style="text-align: center" class="underline">{!! $value->mov_qty !!}</td>
						<td class="underline"></td>
						<?php
						$qty_rcv+=$value->mov_qty;
						?>
						</tr>

					@endforeach

					<tr>
						<th></th>
						<th></th>
						<th style="text-align: right" colspan="2">Total:</td>
						<th style="text-align: center"><?php echo  $qty_rcv ?></td>
						<th></th>
					</tr>
                    <?php
                    $qty_total_mov+=$qty_rcv;
                    ?>
				@endforeach
				@endforeach
			<tr>
				<th></th>
				<th></th>
				<th style="text-align: right" colspan="2">Grand Total:</td>
				<th style="text-align: center"><?php echo  $qty_total_mov; ?></td>
				<th></th>

			</tr>

		 

			 <table class="contents2">
			<tr>
				<td colspan='3'>
				Comments:
				</td>
				<td></td>
				<td colspan='2'><!-- 
				Other Remarks: -->
				</td>
			</tr>
			<tr>
				<td colspan='3' class="underline"><br/></td>
				<td></td>
				<td colspan='5' class="underline"></td>
			</tr>
			<tr>
				<td colspan='4' >
					 
				</td>
			<tr>
			<tr>
				
				<td colspan='3' class="underline"><br/></td>
				<td></td>
				
				<td colspan='2' class="underline"><br/></td>
				<td></td>
				
				<td colspan='2' class="underline"><br/></td>
			</tr>
				<td colspan='3' class="underline"><br/></td>
				<td></td>
				<td colspan='3' class="underline"></td>
				<td></td>
				<td colspan='2' class="underline"></td>
			</tr>
			<!-- <tr>
				<td colspan='3'>
				Signature over Printed Name
				</td>
				<td></td>
				<td colspan='3'>
				Signature over Printed Name
				</td>
				<td></td>
				<td colspan='3'>
				Signature over Printed Name
				</td>
			</tr> -->
			<tr>
				<td colspan='3'>
					<br>
				</td>
			<tr>
			<tr>
				<td colspan='3'>
				Checked By:
				</td>
				<td></td>
				<td colspan='3'>
				Issued By/Date:
				</td>
				<td></td>
				<td colspan='3'>
				Delivered By/ Date:
				</td>
			</tr>
			<tr>
				<td colspan='3' class="underline"><br/></td>
				<td></td>
				<td colspan='3' class="underline"></td>
				<td></td>
				<td colspan='2' class="underline"></td>
			</tr>
			<tr>
			 
				<td colspan='3'>
				Issuance Validated By :
				</td>
				<td colspan='3'>
				Received By/ Date:
				</td>
				<td>
					
				</td>
				<td colspan='3'>
				Posted By/ Date :
				</td>
			</tr>
			<tr>
				<td colspan='3' class="underline"><br/></td>
				<td></td>
				<td colspan='3' class="underline"></td>
				<td></td>
				<td colspan='2' class="underline"></td>
			
			<tr>

		</table>
		 
	<!-- 	Copy 1 &  2 - WH-OS (for checking), then to WH-DC (for Posting) and then to WH-SCC (for IMS update) <br>   Copy 3 - WH-SCC (file copy upon release) -->
	</section>

@endsection