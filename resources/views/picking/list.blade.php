@extends('layouts.main')

@section('content')
{{--@if( $errors->first() )--}}
    {{--<div class="alert alert-error">--}}
    	{{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    	{{--{{ HTML::ul($errors->all()) }}--}}
    {{--</div>--}}
{{--@endif--}}


{{--@if( CommonHelper::arrayHasValue($success) )--}}
    {{--<div class="alert alert-success">--}}
    	{{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    	{{--{{ $success }}--}}
    {{--</div>--}}
{{--@endif--}}

<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #FFFFFF;">
            {!! Form::open(array('url'=>'picking/filter', 'class'=>'form-signin', 'id'=>'form-pick-list', 'role'=>'form', 'method' => 'get')) !!}
            <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">

			      	<div class="span5">
			      		<div>
				        	<span class="search-po-left-pane">MTS No. :</span>
				        	<span class="search-po-right-pane">
				        		<input type="text" value="{!! old('filter_doc_no') !!}" name="filter_doc_no">
							</span>
				        </div>
						<div>
							<span class="search-po-left-pane">Store Name :</span>
							<span class="search-po-right-pane">
				        	<select name="filter_store">
								<option hidden value="{!! empty(old('filter_store')) ? '' : old('filter_store') !!}">{!! empty(old('filter_store')) ? 'Please Select :' : $store_name !!}</option>
								@foreach( $store_list as $store)
									<option value="{!! $store->store_num !!}">{!! $store->store_nam !!}</option>
								@endforeach
							</select>
				        	</span>
						</div>
				    </div>

				    <div class="span5">
				       

				        <div>
				        	<span class="search-po-left-pane">Picker :</span>
				        	<span class="search-po-right-pane">
				        		<select name="filter_stock_piler">
									<option hidden value="{!! empty(old('filter_stock_piler')) ? '' : old('filter_stock_piler') !!}">{!! empty(old('filter_stock_piler')) ? 'Please Select :' : $picker !!}</option>
									@foreach( $user as $user)
										<option value="{!! $user->id !!}">{!! ucfirst( $user->name ) !!}</option>
									@endforeach
								</select>
				        	</span>
				        </div>

				    </div>

				<div class="span5">


					<div>
						<span class="search-po-left-pane">Ship by Date :</span>
						<span class="search-po-right-pane">
				        		<div class="search-po-right-pane input-append date">
									<input type="text" class="span2" value="{!! old('filter_entry_date') !!}" name="filter_entry_date" id="filter_entry_date" readonly>
									<span class="add-on"><i class="icon-th"></i></span>
				        		</div>
				        </span>
					</div>

				</div>


				    
			      	<div class="span11 control-group collapse-border-top">
			      		<button class="btn btn-success btn-darkblue" id="submitForm">Search now</button>
		      			<a class="btn" href="{!! url('picking') !!}">Clear Filters</a>
			      	</div>
            </div>
			  {!! Form::close() !!}
          </div>
      	</div>

	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if(CommonHelper::arrayHasValue( $pick_list ) )
		    <h6 class="paginate">
				<span>{!! $pick_list->appends(['filter_doc_no' 		=> old('filter_doc_no'),
				 							   'filter_store' 		=> old('filter_store'),
				 							   'filter_stock_piler' => old('filter_stock_piler'),
				 							   'filter_pick_status' => old('filter_pick_status'),
				 							   ])->render() !!} &nbsp;</span>
			</h6>
		@endif
	</div>
	{!! Form::open(array('url' => 'picking/assign-piler', 'method' => 'post', 'id' => 'add_load_form')) !!}
	<div class="div-buttons">
			<button type="submit" class="btn btn-info btn-darkblue " title="Assign MTS to stockpiler">Assign Picker</button>
		<a  href="{!! url('picking/pulldata') !!}" class="btn btn-info btn-darkblue " title="Pull data from JDA.">Pull from Data (JDA)</a>

	</div>


<div class="widget widget-table action-table" style="z-index: 1 !important;">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Picking Lists</h3>
      {{--<span class="pagination-totalItems">{{ $text_total }} {{ $picklist_count }}</span>--}}
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead >
					<th><input type="checkbox" id="picking-all"></th>
					<th>MTS No.</th>
					<th>SHIP DATE</th>
					<th>STORE</th>
					<th>PILER NAME</th>
					<th>PICKED DATE</th>
					<th>
						<select class="form-control input-sm" id="select_status">
							<option hidden>
								@if(old('filter_pick_status') == 3)
									OPEN
								@elseif(old('filter_pick_status') == 2)
									ASSIGNED
								@elseif(old('filter_pick_status') == 1)
									DONE
								@elseif(old('filter_pick_status') == '0')
									POSTED
								@else
									STATUS
								@endif
							</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $pick_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_pick_status=3">OPEN</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $pick_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_pick_status=2">ASSIGNED</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $pick_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_pick_status=1">DONE</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $pick_list->currentPage() !!}&filter_doc_no={!! old('filter_doc_no') !!}&filter_store={!! old('filter_store') !!}&filter_stock_piler={!! old('filter_stock_piler') !!}&filter_pick_status='0'">POSTED</option>
						</select>
					</th>
					<th>ACTION</th>
				</thead>
				@if( !CommonHelper::arrayHasValue( $pick_list ) )
					<tr class="font-size-13">
						<td colspan="7" style="text-align: center;">No records found!</td>
					</tr>
				@endif

				@foreach( $pick_list as $key => $list )
					<tr class="tr_row">
						@if( $list->pick_status == 3 || $list->pick_status == 2)
							<td>
								<input type="checkbox" name="mts_no[]" value="{!! $list->tl_no !!}">
							</td>
						@else
							<td></td>
						@endif
						<td><a href="{!! url('picking/detail/'. $list->tl_no ) !!}">{!! $list->tl_no !!}</a></td>
						<td style="vertical-align: middle">{!! date('F j, Y', strtotime( $list->ship_date )) !!}</td>
						<td>{!! $list->to_loc !!}</td>
						<td>{!! ucwords( $list->assigned_to ) !!}</td>
						<td>{!! $list->picked_date == null ? '' : date('F j, Y', strtotime($list->picked_date)) !!}</td>
						<td><strong>{!! CommonHelper::getPickStatus( $list->pick_status ) !!}</strong></td>
						<td>
							@if( $list->pick_status == 3)
								<a href="#" class="btn btn-primary" disabled>OPEN</a>
							@elseif( $list->pick_status == 2)
									<a href="{!! url('picking/close/') !!}" class="btn btn-primary" disabled>ASSIGNED</a>
							@elseif( $list->pick_status == 1)
								<a href="{!! url('picking/close/'.$list->tl_no) !!}" class="btn btn-success" onclick="return confirm('Are you sure you want to close this MTS?')">Close MTS</a>
							@elseif( $list->pick_status == '0')
								<div class="btn-group">
									<button type="button" class="btn btn-primary" disabled="">POSTED</button>
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" style="z-index: 99999999 !important;">
										<li><a href="{!! url('picking/packageslip/'.$list->tl_no) !!}" >Print Package Slip</a></li>
										<li><a href="{!! url('picking/mtslist/'.$list->tl_no ) !!}" >Print MTS List</a></li>
									</ul>
								</div>
							@endif
						</td>
					</tr>
				@endforeach

			</table>
		</div>
	</div>
</div>
</div>
	{!! Form::close() !!}

<script type="text/javascript">
    $(document).ready(function() {
        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });


    });

    $('#select_status').on('change', function () {
        location.href = $(this).find(":selected").attr('data-id');
    });

    $('.tr_row').on('click', function(e) {
        if ($(e.target).is(':checkbox')) {
            return;
        }
        var checkBox = $(this).find('input[type="checkbox"]');
        if (checkBox.is(':checked')) {
            checkBox.prop("checked", false);
        }else if(checkBox.attr("disabled")) {
            checkBox.prop("disabled", true);
        } else if (!checkBox.is(':checked')) {
            checkBox.prop("checked", true);
        }
    });

    $('#picking-all').click(function () {
        $('input:checkbox').prop('checked', this.checked);
    });


	{{--$.ajax({--}}
	{{----}}
	{{--url: "{{$url_generate_load_code}}",--}}
	{{--type: "POST",--}}
	{{--data: {'_token': token},--}}
	{{--success: function(response){--}}
	{{--response = JSON.parse(response);--}}
	{{--$('#load-code-created').html('You have generated ' + response.load_code);--}}
	{{--$('#add-load-modal').modal('show');--}}
	{{--$('#textfield-load-modal').modal('show');--}}
	{{--}--}}
	{{--});--}}
	{{--});--}}

</script>

@endsection


