@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #FFFFFF;">
            {!! Form::open(array('url'=> 'loadship/filter', 'class'=>'form-signin', 'id'=>'form-shipping', 'role'=>'form', 'method' => 'get')) !!}
            <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
	               
			      	<div class="span4">
			      		<div>
				        	<span class="search-po-left-pane">Pell No. :</span>
				        	<span class="search-po-right-pane">
								<input type="text" name="filter_load_code" value="{!! old('filter_load_code') !!}" class="login">
				        	</span>
				        </div>
			 
                    </div>
                    <div class="span4">
			      		<div>
				        	<span class="search-po-left-pane">Ship By Date:</span>
				        	<div class="search-po-right-pane input-append date">
								<input type="text" name="filter_entry_date" class="span2" value="{!! old('filter_entry_date') !!}" readonly="">
								<span class="add-on"><i class="icon-th"></i></span>
				        	</div>
				        </div>
			      	</div>

			      	<div class="span11 control-group collapse-border-top">
			      		<button class="btn btn-success btn-darkblue" type="submit">Search</button>
		      			<a class="btn" href="{!! url('loadship') !!}" id="clearForm">Clear</a>
			      	</div>
            </div>
          {!! Form::close() !!}
          </div>
      	</div>

	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if(CommonHelper::arrayHasValue($load_list) )
		    <h6 class="paginate">
				<span>{!!  $load_list->appends(['filter_load_code' => old('filter_load_code'), 'filter_entry_date' => old('filter_entry_date') ])->render() !!} &nbsp;</span>
			</h6>
		@else
			&nbsp;
		@endif
	</div>
	{!! Form::open(array('url' => 'loadship/assign', 'method' => 'post')) !!}
	<div class="div-buttons">
		{{-- @if ( CommonHelper::valueInArray('CanAccessBoxingLoading', $permissions) ) --}}
		<a  class="btn btn-info btn-darkblue" id="generate-load">Generate Load</a>
		<button type="submit" class="btn btn-info btn-darkblue assignPicklist" id="assignPiler" title="Assign To Picker">Assign To Stock Piler</button>
	{{-- @endif --}}

	</div>
	
</div>
<div class="widget widget-table action-table">
	<div class="widget-header"> <i class="icon-th-list"></i>
	 <h3>Pell number List</h3>
      {{--<span class="pagination-totalItems">{{$list_count}}</span>--}}
	</div>

	<div class="widget-content">
		<div class="table-responsive">
			<table class="table table-striped table-bordered">
				<thead>
					<tr>
						<th style="width: 20px;" class="align-center"><input type="checkbox" id="selectall"></th>
						<th>NO.</th>
						<th>PELL NO.</th>
						<th>Store</th>
						<th>Total Boxes</th>
						<th>Stock Piler</th>
						<th> Date Created </th>
						<th> Ship By Date </th>
						<th class="align-center">ACTION</th>
					</tr>
				</thead>
				@if( !CommonHelper::arrayHasValue($load_list)  )
					<tr class="font-size-13">
						<td colspan="7" style="text-align: center;">No Results Found</td>
					</tr>
				@else
					@foreach( $load_list as $key => $list)
						<tr class="tr_row">
							<td>
								@if( $list->load_status == 3 || $list->load_status == 2 )
								<input type="checkbox" name="pell[]" id="pell" value="{!! $list->load_code !!}">
								@endif
							</td>
							<td>{!! $key+1 !!}</td>
							<td><a href="{!! url('loadship/detail/'.$list->load_code) !!}">{!! $list->load_code !!}</a></td>
							<td>{!! $list->store_nam !!}</td>
							<td>{!! $list->total_box !!}</td>
							<td>{!! ucwords( $list->assigned_to ) !!}</td>
							<td>{!! date('F d, Y', strtotime($list->created_at)) !!}</td>
							<td>{!! is_null( $list->ship_date) ? '' : date('F d, Y', strtotime($list->ship_date)) !!}</td>
							<td>
								@if( $list->load_status == 1 )
									<a href="{!! url('loadship/ship/'.$list->load_code ) !!}" onclick="return confirm('Are you sure you want to ship this pell?')" class="btn btn-default">SHIP</a>
									<a href="{!! url( 'loadship/loadsheet/'.$list->load_code ) !!}" class="btn btn-warning">Print Load Sheet</a>
								@endif

								@if( $list->load_status == 3 || $list->load_status == 2 )
									<a href="{!! url('loadship/assignbox/'.$list->load_code) !!}" class="btn btn-primary" style="margin-bottom: 4px">Assign Box no.</a>
									@if( $list->total_box != 0 )
										 <a href="{!! url( 'loadship/loadsheet/'.$list->load_code ) !!}" class="btn btn-warning">Print Pre-Load Sheet</a>
									@endif
								@endif
									@if( $list->load_status == 0)
										<a href="#" class="btn btn-default" disabled>SHIPPED</a>
										<a href="{!! url( 'loadship/loadsheet/'.$list->load_code ) !!}" class="btn btn-warning">Print Load Sheet</a>
									@endif
							</td>
						</tr>
					@endforeach
				@endif

			</table>
		</div>
	</div>	
</div>
{!! Form::close() !!}
<div id="add-load-modal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="load-boxes-modal-label" aria-hidden="true">
	 <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title">New Load Code</h4>
      </div>
      <div class="modal-body">


{!! Form::open(array('role'=> 'form', "class"=> "form-horizontal", "id"	=> 'add-load-form', 'method' => 'post'))  !!}
		  {!! Form::close() !!}



 <span id="load-code-created"></span>

	</section>

      </div>
      <div class="modal-footer">
      	  <button type="button" class="btn btn-default" id="close-add-load" >OK</button>
      </div>
    </div><!-- /.modal-content -->
</div>
<script>

	$(document).ready(function () {
        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });

        $('#generate-load').click(function() {
            var token = $('#add-load-form input[name="_token"]').val();

            $.ajax({

                url: "{!!  url('loadship/new') !!}",
                type: "POST",
                data: {'_token': token},
				dataType : 'json',
                success: function(response){

                    $('#load-code-created').html('You have generated ' + response.result.load_code);
                    $('#add-load-modal').modal('show');
                    $('#textfield-load-modal').modal('show');
                }
            });
        });

        $('#close-add-load').click(function() {
            window.location.reload();
        });

        $('.tr_row').on('click', function(e) {
            if ($(e.target).is(':checkbox')) {
                return;
            }
            var checkBox = $(this).find('input[type="checkbox"]');
            if (checkBox.is(':checked')) {
                checkBox.prop("checked", false);
            }else if(checkBox.attr("disabled")) {
                checkBox.prop("disabled", true);
            } else if (!checkBox.is(':checked')) {
                checkBox.prop("checked", true);
            }
        });


        $('#selectall').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });

    });

    $('#assignPiler').click(function(){
        var aselect = $('#pell:checked').map(function(){
            return $(this).val();
        }).get();

        if(aselect == ""){
            alert('Please select Pell No.');
            return false;
        }
    });

</script>
@endsection

 

