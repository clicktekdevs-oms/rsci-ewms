<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<title> PO no</title>
<meta name="apple-mobile-web-app-capable" content="yes">

	<style type="text/css">



		html {

			font-family: sans-serif !important;
			font-size: 13px !important;
		}
		table{
			width: 100%;
			max-width: 100%;
			border-collapse: collapse;
		}

		table .thead th{
			padding: 5px;
		}


		table .thead td{
			border:1px solid;
			padding: 5px;
			text-align: center;
		}

		table .thead th {
			text-align: center;
			border: 1px solid ;
		}

		tr {

			page-break-inside: avoid;
		}




	</style>
</head>
<body>
<div class="table-responsive contnt2" >
			<div style="text-align: center;font-size: 14px !important;line-height: 1.3" >
				<b>RSCI - eWMS <br/> Shortage/Overage Report<br/></b>
				Printed By: {!! Auth::user()->username !!} <br>
				Print Date: {!!  date('m/d/y h:i A') !!}<br>
			</div>
	 <table class="contents2" style="font-size: 13px;margin-bottom: 15px" >
		<tr>
			<th colspan='3' style="text-align: left;">
				
				&nbsp;&nbsp;&nbsp;Shipment Reference : {!! $list->ship_ref_no !!}
			</th>
			<th colspan='3'>
				PO NO.: {!! $list->po_no !!}
		</tr>
		<tr>
			 
			<th colspan='5' style="text-align: left">
				&nbsp;&nbsp;&nbsp;Invoice No. : {!! $list->inv_no !!}
				
			</th>
			<th colspan='3'>
				RCR NO. : {!! $list->rcv_no !!}
				</th>
		</tr>
		<br>
		<br>
	</table> 
			 
				
			 
	 
	<table style="margin-bottom: 40px">
		<tr class="thead">
			<th rowspan="2">Division Code</th>
			<th rowspan="2">Style No.</th>
			<th rowspan="2">SKU</th>
			<th rowspan="2">UPC</th>
			<th colspan="2" style="text-align: center">Quantity</th>
			<th rowspan="2">Discrepancy</th>
			<th rowspan="2">Remarks</th>
		</tr>
		<tr class="thead">
			<th style="text-align: center" >Advised Per RA </th>
			<th style="text-align: center" > Actual Receipt</th>
		</tr>
        <?php
		$qtyvariance=0;
        $qtyord=0;
        $qtyrcv=0;


        ?>
		@foreach( $details as $detail )
			<tr class="thead">
				<td>{!! $detail->division_code !!}</td>
				<td>{!! $detail->short_name !!}</td>
				<td>{!! $detail->sku !!}</td>
				<td>{!! $detail->upc !!}</td>
				<td>{!! $detail->qty_ord !!}</td>
				<td>{!! $detail->qty_rcv !!}</td>
				<td>{!! $detail->qty_rcv - $detail->qty_ord !!}</td>
				<td>{!! $detail->remarks !!}</td>

				<?php
					$qtyord+=$detail->qty_ord;
					$qtyrcv+=$detail->qty_rcv;
					$qtyvariance+=( $detail->qty_rcv - $detail->qty_ord );
				?>
			</tr>
		@endforeach
		<tr class="thead">
			<td>Total Items :</td>
			<td colspan="3" style="text-align: left;font-weight: 600">{!! count($details) !!}</td>
			<td style="font-weight: 700"><?php echo $qtyord; ?></td>
			<td style="font-weight: 700"><?php echo $qtyrcv?></td>
			<td style="font-weight: 700"><?php echo $qtyvariance ?></td>
			<td></td>
		</tr>

		 
	</table>
	<table class="contents2" >
		<tr>
			<td colspan='3'>
				Generated By :
			</td>
			<td colspan='3'>
				Noted by/Date:
			</td>
		</tr>
		<tr>
			<td class="underline" colspan='2'><hr style="margin-top:20px;border-top: 1px solid black;"></td>
			<td></td>
			<td class="underline" colspan='2'><hr style="margin-top:20px;border-top: 1px solid black;"></td>
			<td></td>
		</tr>
	</table>
</div>

</body>
</html>