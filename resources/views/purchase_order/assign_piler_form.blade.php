@extends('layouts.main')
@section('content')
{{--@if( $errors->all() )--}}
    {{--<div class="alert alert-error">--}}
    	{{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    	{{--{{ HTML::ul($errors->all()) }}--}}
    {{--</div>--}}
{{--@endif--}}

<div class="widget">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Assign to Stock Piler</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	{!! Form::open(array('url'=>'po/assign-piler', 'method' => 'post', 'id'=>"form-assign", 'class'=>'form-horizontal', 'style' => 'margin: 0px;', 'role'=>'form'))  !!}
		<!-- <div class="span3">&nbsp;</div> -->
		<div class="span3">&nbsp;</div>
		<div class="span7 add-piler-wrapper">

			<div class="control-group">
				<label class="control-label">Receiver No :</label>
				<div class="controls">
					<input type="text" name="dept_code" readonly value="{!! $rcv_no !!}">

					<input type="hidden" name="receiver_num" value="{!! $rcv_no !!}">
				</div> <!-- /controls -->
			</div> <!-- /control-group -->

			<div class="control-group">
				<label class="control-label">Division :</label>
				<div class="controls">
					<input name="division" readonly value="{!! $division !!}"></input>

					<input type="hidden" name="dept_code" value="{!! $dpt_id !!}">
					<input type="hidden" name="division_code" value="{!! $div_id !!}">
				</div> <!-- /controls -->
			</div> <!-- /control-group -->

			<div class="control-group">
				<label class="control-label">Department :</label>
				<div class="controls">
					<textarea rows="5" style="resize: none;" readonly>
						{!! $department !!}
					</textarea>

				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		
			<div class="control-group">
				<label class="control-label">Stock Piler :</label>
				<div class="controls">
					<select name="stock_piler">
						<option hidden>Select :</option>
						@foreach( $pilers as $piler )
						<option value="{!! $piler->id !!}">{!! ucfirst($piler->firstname) !!} {!! ucfirst($piler->lastname) !!}</option>
						@endforeach
					</select>

				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		
        </div>
        <!-- <div class="span10">&nbsp;</div> -->
        <div class="span3">&nbsp;</div>
        <div class="span7">
        	<div class="control-group">
				<label class="control-label" for=""></label>
				<div class="controls">
					<button class="btn btn-info" type="submit">Assign</button>
					<a class="btn" href="javascript:history.back()">Cancel</a>
				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection