@extends('layouts.main')
@section('content')
<div class="control-group">
	<div class="controls">
		<div class="accordion" id="accordion2">
          <div class="accordion-group" style="background-color: #FFFFFF;">
            {!! Form::open(array('url'=>'po/filter', 'class'=>'form-signin', 'id'=>'form-purchase-order', 'role'=>'form', 'method' => 'get')) !!}
            <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
	                <div class="span4">
			        	<div>
				        	<span class="search-po-left-pane">PO No.:</span>
				        	<span class="search-po-right-pane">
								<input type="text" class="login" id="filter_po_no" name="filter_po_no" value="{!! old('filter_po_no') !!}">
				        	</span>
				        </div>

				        <div>
				        	<span class="search-po-left-pane">Shipment Ref No.</span>
				        	<span class="search-po-right-pane">
								<input type="text" class="login" id="filter_shipment_reference_no" value="{!! old('filter_shipment_reference_no') !!}" name="filter_shipment_reference_no">
				        	</span>
				        </div>
			      	</div>
			      	<div class="span4">
			      		<div>
				        	<span class="search-po-left-pane">Entry Date :</span>
				        	<div class="search-po-right-pane input-append date">
								<input type="text" class="span2" value="{!! old('filter_entry_date') !!}" name="filter_entry_date" id="filter_entry_date" readonly>
								<span class="add-on"><i class="icon-th"></i></span>
				        	</div>
				        </div>
			      		<div>
				        	<span class="search-po-left-pane"> Invoice No.</span>
				        	<span  class="search-po-right-pane">
								<input type="text" name="filter_invoice_no" class="login" id="filter_invoice_no" value="{!! old('filter_invoice_no') !!}">
								 </span>
				        </div>

			      	</div>


			      	<div class="span11 control-group collapse-border-top">
			      		<button class="btn btn-success btn-darkblue" type="submit">Search Now</button>
		      			<a href="{!! url('po') !!}" class="btn btn-default">Clear Filters</a>
			      	</div>
            </div>


            {!! Form::close() !!}
          </div>
      	</div>

	</div> <!-- /controls -->
</div> <!-- /control-group -->

<div class="clear">
	<div class="div-paginate">
		@if( CommonHelper::arrayHasValue($result) )
			<h6 class="paginate">
				<span>{!! $result->appends(['filter_po_no' => old('filter_po_no'),
				 							'filter_shipment_reference_no' => old('filter_shipment_reference_no'),
				 							'filter_entry_date' => old('filter_entry_date'),
				 							'filter_invoice_no' => old('filter_invoice_no')])->render() !!}&nbsp;
				</span>
			</h6>
		@endif
	</div>
	{!! Form::open(array('url' => 'po-list-post', 'method' => 'post')) !!}

	<div class="div-buttons">
		<table>
			<tr>
				<th>
					<div class="div-buttons ">

					</div>
				</th>
				<th>
			     </th>
			     <th>


					<div class="div-buttons">
						<button type="submit" name="submitButton" value="unlisted" class="btn btn-info btn-darkblue" >Unlisted Report</button>
						<a class="btn btn-info btn-darkblue" title="Pull item from JDA" href="{!! url('po/pulljda') !!} ">Pull from Data (JDA)</a>

					</div>

				</th>
			</tr>
		</table>
	</div>
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>

    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive align-center">
			<table class="table table-striped table-bordered " >
				<thead>
				<tr>
					<th><input type="checkbox"></th>
					<th>PO NO.</th>
					<th>Invoice No.</th>
					<th>Shipment Ref</th>
					<th>Total Qty</th>
					<th>Entry Date</th>
					<th>
						<select class="form-control" id="select_status">
							<option hidden>
								@if(old('filter_po_status') == 4)
									OPEN
								@elseif(old('filter_po_status') == 1)
									PARTIAL RECEIVED
								@elseif(old('filter_po_status') == '0')
									POSTED
								@else
									STATUS
								@endif
							</option>

							<option data-id="{!! app('url')->full() !!}?page={!! $result->currentPage() !!}&filter_po_no={!! old('filter_po_no') !!}&filter_shipment_reference_no={!! old('filter_shipment_reference_no') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_invoice_no={!! old('filter_invoice_no') !!}&filter_po_status=4">OPEN</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $result->currentPage() !!}&filter_po_no={!! old('filter_po_no') !!}&filter_shipment_reference_no={!! old('filter_shipment_reference_no') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_invoice_no={!! old('filter_invoice_no') !!}&filter_po_status=2">DONE</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $result->currentPage() !!}&filter_po_no={!! old('filter_po_no') !!}&filter_shipment_reference_no={!! old('filter_shipment_reference_no') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_invoice_no={!! old('filter_invoice_no') !!}&filter_po_status=1">PARTIAL RECEIVED</option>
							<option data-id="{!! app('url')->full() !!}?page={!! $result->currentPage() !!}&filter_po_no={!! old('filter_po_no') !!}&filter_shipment_reference_no={!! old('filter_shipment_reference_no') !!}&filter_entry_date={!! old('filter_entry_date') !!}&filter_invoice_no={!! old('filter_invoice_no') !!}&filter_po_status='0'">POSTED</option>
						</select>
					</th>
					<th>Action</th>
				</tr>
				</thead>

				@if(count($result) <= 0)
				<tr>
					<td colspan="9"><div class="alert alert-info">No records found!</div> </td>
				</tr>
				@endif
				@foreach($result as $key => $list)
					<tr>

						<td><input type="checkbox" name="rcvr_no[]" value="{!! $list->rcv_no !!}"></td>
						<td><a href="{!! url('po/division/'.$list->rcv_no) !!}">{!! $list->po_no !!}</a></td>
						<td>{!! $list->inv_no == 0 ? '' : $list->inv_no !!}</td>
						<td>{!! $list->ship_ref_no !!}</td>
						<td>{!! $list->total_qty !!}</td>
						<td>{!! date( 'F j, Y', strtotime( $list->entry_date ) ) !!}</td>
						<td><strong>{!! CommonHelper::getPOStatus( $list->po_status ) !!}</strong></td>
						<td>
							@if( $list->po_status  == 2 )
								<div class="btn-group">
									<button type="button" class="btn btn-primary" disabled="">OPTION :</button>
									<button type="button" class="btn btn-primary dropdown-toggle" data-toggle="dropdown">
										<span class="caret"></span>
									</button>
									<ul class="dropdown-menu" role="menu" style="z-index: 99999999 !important;">
										<li><a href="{!! url('po/partial/'.$list->po_no.'/'.$list->rcv_no) !!}" onclick="return confirm('Partial Received this PO?')" title="Partial Receive this PO to JDA">PARTIAL RECEIVE</a></a></li>
										<li><a href="{!! url('po/close/'.$list->po_no.'/'.$list->rcv_no) !!}" ONCLICK="return confirm('Close this PO?')" title="Close PO to JDA">CLOSED</a></li>
									</ul>
								</div>

							@elseif( $list->po_status == 0 )
								<a class="btn btn-success" disabled="">POSTED</a>
							@elseif( $list->po_status == 1 )
								<a class="btn btn-warning" disabled>PARTIAL RECEIVED</a>
							@else
								<a disabled="" class="btn btn-primary">OPEN</a>
							@endif
						</td>
					</tr>
				@endforeach
			</table>
			{!! Form::close() !!}

		</div>
	</div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        $('.date').datepicker({
            format: 'yyyy-mm-dd'
        });
    });

    $('#select_status').on('change', function () {
        location.href = $(this).find(":selected").attr('data-id');
    });

    function convertToUppercase(el) {
        if(!el || !el.value) return;
        el.value = el.value.toUpperCase();
    }
</script>
@endsection
