@extends('layouts.main')
@section('content')
{{--@if( $errors->first() )--}}
    {{--<div class="alert alert-error">--}}
        {{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
        {{--{{ HTML::ul($errors->all()) }}--}}
    {{--</div>--}}
{{--@endif--}}


{{--@if( CommonHelper::arrayHasValue($success) )--}}
    {{--<div class="alert alert-success">--}}
        {{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
        {{--{{ $success }}--}}
    {{--</div>--}}
{{--@endif--}}


<div class="control-group">
    
    <a class="btn btn-info btn-darkblue" href="{!! url('po/division/'. $po_detail->rcv_no) !!}"><i class="icon-chevron-left"></i> Back to List</a>
  
</div>


<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>PO Details</h3>
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
       <div class="row-fluid stats-box">
            <div class="span4">
                <div>
                    <span class="left-pane">PO No :</span>
                    <span class="right-pane">
                        <input type="text" name="filter_po_no" readonly value="{!! $po_detail->po_no !!}">
                    </span>
                </div>
                
            </div>

            <div class="span4">
               
                
          </div>

          <div class="span4">
                
            <div>
                    <span class="left-pane">PilerName :</span>
                    <span class="right-pane">
                        <input type="text" name="filter_stock_piler" readonly value="{!! ucwords($po_detail->assigned_to) !!}">
                    </span>
                </div>
                
          </div>

          <div class="span4">
                
               <div>
                    <span class="left-pane">Division :</span>
                    <span class="right-pane">
                        <input type="text" name="division" readonly value="{!! $po_detail->division !!}">
                    </span>
                </div>
          </div>
       </div>
     </div>
</div>

<div class="clear">
    {{--<div class="div-paginate">--}}
        {{--@if(CommonHelper::arrayHasValue($purchase_orders) )--}}
            {{--<h6 class="paginate">--}}
                {{--<span>{{ $purchase_orders->appends($arrFilters)->links() }}&nbsp;</span>--}}
            {{--</h6>--}}
        {{--@else--}}
            {{--&nbsp;--}}
        {{--@endif--}}
    {{--</div>--}}
</div>

<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>PO Contents</h3>
      {{--<span class="pagination-totalItems">{{ $text_total }} {{ $purchase_orders_count }}</span>--}}
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
        <div class="table-responsive" onkeypress="return isNumber(event)">
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>No.</th>
                        <th><a href="#" class="">SKU</a></th>
                        <th><a href="#" class="">UPC</a></th>
                        <th><a>SHORT NAME</a></th>
                        <th>EXPECTED QUANTITY</th>
                        <th>RECEIVED QUANTITY</th>
                        <th> VARIANCE </th>
                        <th> REMARKS </th>
                    </tr>
                </thead>

                @foreach( $po_details as $key => $detail)
                    <tr style="{!! $detail->qty_rcv != $detail->qty_ord ? 'background-color: #f2dede;' : '' !!}">
                        <td>{!! $key+1 !!}</td>
                        <td>{!! $detail->sku !!}</td>
                        <td>{!! $detail->upc !!}</td>
                        <td>{!! $detail->short_name !!}</td>
                        <td>{!! $detail->qty_ord !!}</td>
                        <td>

                                @if( $detail->po_status != 2 )
                                    {!! $detail->qty_rcv !!}
                                @else
                                    {!! Form::open(array('url' => 'po/detail/update', 'method' => 'post')) !!}
                                    <div class="input-group">
                                        <input type="text" style="margin-top: 10px" name="qty_rcv" required value="{!! $detail->qty_rcv !!}">
                                        <input type="hidden" name="upc" value="{!! $detail->upc !!}">
                                        <input type="hidden" name="rcv_no" value="{!! $detail->rcv_no !!}">
                                        <input type="hidden" name="sku" value="{!! $detail->sku !!}">
                                        <button type="submit" class="btn btn-primary" title="Update Quantity"><span class="icon-check"></span> </button>
                                    </div>
                                    {!! Form::close() !!}
                                @endif

                        </td>
                        <td>{!! $detail->qty_rcv - $detail->qty_ord !!}</td>
                        <td>{!! $detail->remarks !!}</td>
                    </tr>
                @endforeach



            </table>
        </div>
    </div>

    {{--@if( CommonHelper::arrayHasValue($purchase_orders) )--}}
    {{--<h6 class="paginate">--}}
        {{--<span>{{ $purchase_orders->appends($arrFilters)->links() }}</span>--}}
    {{--</h6>--}}
    {{--@endif--}}

  
    </div>

    <!--modal for close po-->
    {{--<div id="closePoModal" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="closePoModalLabel" aria-hidden="true">--}}
         {{--<div class="modal-content">--}}
          {{--<div class="modal-header">--}}
            {{--<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>--}}
            {{--<h4 class="modal-title">{{$entry_invoice}}</h4>--}}
          {{--</div>--}}
          {{--<div class="modal-body">--}}
            {{--{{ Form::open(array('role'=> 'form', "class"=> "form-horizontal"))}}--}}
            {{--<div class="form-group">--}}
                {{--{{ Form::label('invoice_no',$label_invoice_number, array("style" => "margin-right:10px","class" => "col-sm-2 control-label"))}}--}}
                {{--<div class="col-sm-10">--}}
                    {{--{{ Form::text('invoice_no', '', array('id'=>'invoiceNoInput','class'=> "form-control"))}}--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--<br/>--}}
            {{--<div class="form-group">--}}
                {{--{{ Form::label('invoice_amount',$label_invoice_amount, array("style" => "margin-right:10px","class" => "col-sm-2 control-label"))}}--}}
                {{--<div class="col-sm-10">--}}
                    {{--{{ Form::text('invoice_amount', '', array('required', 'id'=>'invoiceAmountInput','class'=> "form-control"))}}--}}
                {{--</div>--}}
            {{--</div>--}}
            {{--{{ Form::close()}}--}}
          {{--</div>--}}
          {{--<div class="modal-footer">--}}
            {{--<button type="button" class="btn btn-primary" id="closePOModalButton">Close PO</button>--}}
          {{--</div>--}}
        {{--</div><!-- /.modal-content -->--}}
    {{--</div>--}}

    <!-- end of modal for close po-->

    <!-- /widget-content -->
</div>

<script type="text/javascript">
$(document).ready(function() {

$('#form-purchase-order').keydown(function(e) {
        if (e.keyCode == 13) {
            $('#form-purchase-order').submit();
        }
    });
 
    // Submit Form
    $('#submitForm').click(function() {
        $('#form-purchase-order').submit();
    });

    });
</script>
@endsection
