@extends('layouts.main')

@section('content')
	<div class="control-group">
		<a href="{!! url('po') !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i> Back To List</a>
	</div>


	<div class="widget widget-table action-table">


		<div class="widget-header"> <i class="icon-th-list"></i>
			<h3>PO Division</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<div class="row-fluid stats-box">
				<div class="span4">
					<div>
						<span class="left-pane">PO No.</span>
						<span class="right-pane"><input type="text" value="{!! $list->po_no !!}" readonly="">
		        	</span>
					</div>

				{!! Form::open(['url' => 'po/update/shipref', 'method' => 'post']) !!}
					<div>
						<span class="left-pane">SHIPMENT REF :</span>
						<span class="right-pane"><input type="text" name="ship_ref" value="{!! $list->ship_ref_no !!}"><button class="btn btn-sm btn-primary" type="submit">Save</button>
		        	</span>
					</div>

					<input type="hidden" name="pono" value="{!! $list->po_no !!}">
					<input type="hidden" name="rcv_no" value="{!! $list->rcv_no !!}">

				{!! Form::close() !!}

				</div>

				<div class="span4">
					<div>
						<span class="left-pane">Entry Date :</span>
						<span class="right-pane"><input type="text" value="{!! date( 'F d, Y', strtotime($list->entry_date) ) !!}" readonly=""></span>
					</div>

				</div>

				<div class="span4">
					<div>
						<span class="left-pane">Total QTY :</span>
						<span class="right-pane"><input type="text" value="{!! $list->total_qty !!}" readonly=""></span>
					</div>

				<!-- <div>
		        	<span class="left-pane">Status :</span>
		        	<span class="right-pane">{{ Form::text('data_display','', array('readonly' => 'readonly')) }}</span>
		        </div>-->

				</div>

			</div>
		</div>
	</div>


	{!! Form::open(array('url' => 'po/assign-to-piler', 'method' => 'post')) !!}
	<div class="clear">
		<div class="div-paginate">

		</div>


		<div class="div-buttons">
			<div class="btn-group div-buttons">
				<button type="button" class="btn btn-info btn-darkblue " data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Report <span class="caret"></span>
				</button>
				<ul class="dropdown-menu">
					<li><a href="{!! url('po/rpt/shortage/pdf/'.$list->po_no.'/'.$list->rcv_no) !!}">Shipment Discrepancy</a></li>
					<li><a href="{!! url('po/rpt/upc/pdf/'.$list->po_no.'/'.$list->rcv_no) !!}">UPC Mismatch Report</a></li>
					<li><a href="{!! url('po/rpt/upc/csv/'.$list->po_no.'/'.$list->rcv_no) !!}">UPC Mismatch(CSV output)</a></li>
				</ul>
			</div>
		</div>

		<div class="pull-right" style="margin-right: 10px">
			<button type="submit" class="btn btn-info btn-darkblue assignPO" id="assignPO" title="Assign to stockpiler">Assign to Stock Piler</button>
		</div>
	</div>

	<div class="widget widget-table action-table">
		<div class="widget-header"> <i class="icon-th-list"></i>
			<h3>Purchase Order</h3>
		</div>
		<!-- /widget-header -->
		<div class="widget-content">
			<div class="table-responsive">
				<table class="table table-striped table-bordered">
					<thead>
					<tr>
						<th style="width: 20px;" class="align-center"><input type="checkbox" id="assign-all" /></th>
						<th>No.</th>
						<th>

							<select class="form-control" style="margin-bottom: -5px;" onchange="javascript:location.href = this.value;">

								<?php

									$a =url("/po/division/".$list->rcv_no);
								?>
								@foreach( $divisions as $division )
									<option hidden>{!! isset($division_name) ? $division_name : 'Please Select :' !!}</option>
									<option value="<?php echo  $a ?>/{!! $division->division_code !!}">{!! $division->division !!}</option>
								@endforeach

							</select>

						</th>
						<th><a href="" class="">Department</a></th>
						<th>ORDERED QTY</th>
						<th>RECEIVED QTY</th>
						<th>Stock Piler</th>
						<th>Status</th>
					</tr>
					</thead>

					<?php $i = 0;?>
					@foreach($result as $key => $value)
						<tr class="tr_row">
							<td>
								@if( $value->po_status == 3 || $value->po_status == 4 )
									<input type="checkbox" id="deptcode" name="checkbox[]" value="{!! $i++ !!}">
									<input type="hidden" name="division_code[]" value="{!! $value->division_code !!}">
									<input type="hidden" name="dept_code[]" value="{!! $value->dept_code !!}">
								@endif
							</td>
							<td>{!! $key+1 !!}</td>
							<td>{!! $value->division !!}</td>
							<td><a href="{!! url('po/division/detail/'. $value->rcv_no.'/'.$value->dept_code) !!}">{!! $value->department !!}</a></td>
							<td>{!! $value->qty_ord !!}</td>
							<td>{!! $value->qty_rcv !!}</td>
							<td>{!! ucwords($value->assigned_to) !!}</td>
							<td>{!! CommonHelper::getPOStatus( $value->po_status ) !!}</td>
						</tr>
						<input type="hidden" value="{!! $value->rcv_no !!}" name="rcv_no">
					@endforeach




				</table>
				{!! Form::close() !!}
			</div>
		</div>
	</div>
	<script>
        $('#select_status').on('change', function () {
            var type = document.getElementById('select_status').value;
            window.location.href = '{!! app('url')->full() !!}?division_code='+type;
            return true;
        });

        $('#assignPO').click(function() {
            var sel = $('#deptcode:checked').map(function(_, el) {
                return $(el).val();
            }).get();
            if(sel== "") {
                alert('Please select Department!');
                return false;
            }else
            {
                return confirm('Assign Department?');
            }
            $('#po_no').val(sel);
        })

        $('.tr_row').on('click', function(e) {
            if ($(e.target).is(':checkbox')) {
                return;
            }
            var checkBox = $(this).find('input[type="checkbox"]');
            if (checkBox.is(':checked')) {
                checkBox.prop("checked", false);
            }else if(checkBox.attr("disabled")) {
                checkBox.prop("disabled", true);
            } else if (!checkBox.is(':checked')) {
                checkBox.prop("checked", true);
            }
        });

        $('#assign-all').click(function () {
            $('input:checkbox').prop('checked', this.checked);
        });
	</script>
@endsection
