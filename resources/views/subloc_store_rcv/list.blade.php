@extends('layouts.main')
@section('content')


    <div class="control-group">
        <div class="controls">
            <div class="accordion" id="accordion2">
                <div class="accordion-group" style="background-color: #FFFFFF;">
                    {!! Form::open(array('url'=>'store-subloc-receiving/filter', 'class'=>'form-signin', 'id'=>'form-store-order', 'role'=>'form', 'method' => 'get')) !!}
                    <div id="collapseOne" class="accordion-body collapse in" style="padding-top: 20px;">
                        <div class="span4">
                            <div>
                                <span class="search-po-left-pane">Pell No. </span>
                                <span class="search-po-right-pane">
								<input type="text" placeholder="Type pell no." name="pell" value="{!! old('pell') !!}">
				        	</span>
                            </div>

                        </div>

                        <div class="span3">
                            <div>
                                <span class="search-po-left-pane">Ship Date :</span>
                                <div class="search-po-right-pane input-append date">
                                    <input type="text" name="ship_date" value="{!! old('ship_date') !!}" placeholder="type entry date" id="filter_entry_date">
                                    <span class="add-on"><i class="icon-th"></i></span>
                                </div>
                            </div>


                        </div>
                        <div class="span11 control-group collapse-border-top">
                            <a class="btn btn-success btn-darkblue" id="submitForm">Search</a>
                            <a class="btn" href="{!! url('store-subloc-receiving') !!}" id="clearForm">Clear Filters</a>
                        </div>
                    </div>

                    {!! Form::close() !!}
                </div>
            </div>

        </div> <!-- /controls -->
    </div> <!-- /control-group -->

    <div class="div-buttons">
        &nbsp;&nbsp;<a class="btn btn-info btn-darkblue" href={{URL::to('store-subloc-receiving/pulldata')}}> Pull JDA</a>
    </div>

    <div class="clear">



    </div>

    <div class="widget widget-table action-table">
        <div class="widget-header"> <i class="icon-th-list"></i>
            <h3>Subloc Store Receiving List</h3>
        </div>
        <!-- /widget-header -->
        <div class="widget-content">
            <div class="table-responsive">
                <table class="table table-striped table-bordered">
                    <thead>
                    <tr>

                        <!-- <th style="width: 20px;" class="align-center"><input type="checkbox" id="main-selected" /></th> -->
                        <th style="">No.</th>
                        <th>Pell No.</th>
                        <th>Ship Date</th>
                        <th>Status</th>
                        <th class="align-center">Action</th>
                    </tr>
                    </thead>

                    @if( !CommonHelper::arrayHasValue( $load_list ) )
                        <tr class="font-size-13">
                            <td colspan="9" style="text-align: center;">No records found! </td>
                        </tr>
                    @else
                        @foreach( $load_list as $key => $list )
                            <tr>
                                <td>{!! $key+1 !!}</td>
                                <td><a href="{!! url('store-subloc-receiving/'. $list->load_code ) !!}">{!! $list->load_code !!}</a> </td>
                                <td>{!! date('F j, Y', strtotime( $list->ship_date  ))!!}</td>
                                <td><b>{!! CommonHelper::getLoadStrStatus( $list->load_status_store ) !!}</b></td>
                                <td>
                                    @if( $list->load_status_store == 1)
                                        <a href="#{{ $list->load_code }}" class="btn btn-primary" data-toggle="modal">RECEIVE</a>
                                    @elseif( $list->load_status_store == 0)
                                        <a href="#" disabled class="btn btn-primary">RECEIVED</a>
                                    @elseif( $list->load_status_store == 2)
                                        <a href="#" disabled class="btn btn-primary">OPEN</a>
                                    @endif
                                </td>
                            </tr>

                            <div class="bs-example">
                                <!-- Modal HTML -->
                                <div id="{{ $list->load_code }}" class="modal fade">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                                <h4 class="modal-title">RECEIVE MTS</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p class="text-warning"><small>Please Login your Jda Credentials.</small></p>
                                                {!! Form::open(array('url' => 'store-subloc-receiving/receive', 'method' => 'post')) !!}
                                                <div class="form-group">
                                                    <label for="inputEmail" class="control-label col-xs-2"><b>JDA Username :</b></label>
                                                    <div class="col-xs-10">
                                                        <input type="text" required autocomplete="off" name="jda_username" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="form-group">
                                                    <label for="inputPassword" class="control-label col-xs-2"><b>Password :</b></label>
                                                    <div class="col-xs-10">
                                                        <input type="password" name="jda_password" autocomplete="off" required class="form-control">
                                                    </div>
                                                    <input type="hidden" name="pell_code" value="{!! $list->load_code !!}">
                                                </div>

                                                <div class="form-group">
                                                    <div class="col-xs-offset-2 col-xs-10">
                                                        <button type="submit" class="btn btn-primary">Login</button>
                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>

                                                    </div>
                                                </div>
                                                {!! Form::close() !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    @endif


                </table>
            </div>
        </div>

    </div>


    <script type="text/javascript">
        $(document).ready(function() {
            $('.date').datepicker({
                format: 'yyyy-mm-dd'
            });


            // Close SO


            // Submit Form
            $('#submitForm').click(function() {
                $('#form-store-order').submit();
            });

            $('#form-store-order input').keydown(function(e) {
                if (e.keyCode == 13) {
                    $('#form-store-order').submit();
                }
            });

            // Clear Form


            // Export List


            // Select
            $('.tblrow').click(function() {
                var rowid = $(this).data('id');

                if ($('#selected-' + rowid).length>0) {
                    if ($('#selected-' + rowid).is(':checked')) {
                        $('#selected-' + rowid).prop('checked', false);
                        $(this).children('td').removeClass('tblrow-active');
                    } else {
                        $('#selected-' + rowid).prop('checked', true);
                        $(this).children('td').addClass('tblrow-active');
                    }
                } else {
                    $(this).children('td').removeClass('tblrow-active');
                }
            });

            $('.item-selected').click(function() {
                var rowid = $(this).data('id');

                if ($(this).is(':checked')) {
                    $(this).prop('checked', false);
                    $(this).children('td').removeClass('tblrow-active');
                } else {
                    $(this).prop('checked', true);
                    $(this).children('td').addClass('tblrow-active');
                }
            });


            $('#main-selected').click(function() {
                if ($('#main-selected').is(':checked')) {
                    $('input[name*=\'selected\']').prop('checked', true);
                    $('.table tbody tr > td').addClass('tblrow-active');
                } else {
                    $('input[name*=\'selected\']').prop('checked', false);
                    $('.table tbody tr > td').removeClass('tblrow-active');
                }
            });
            $('#clearForm').click(function() {
                $('#filter_status, #filter_load_code, #filter_shipment_reference_no').val('');
                $('#filter_entry_date, #filter_back_order').val('');

                $('select').val('');
                $('#form-store-order').submit();
            });

        });
    </script>
@endsection