@extends('layouts.main')
@section('content')
<div class="control-group">
	<a href="{!! url('store-subloc-receiving/'.$box_info->load_code) !!}" class="btn btn-info btn-darkblue"> <i class="icon-chevron-left"></i> Back to List</a>

<!--   -->

</div>
<!-- PO Detail -->
<div class="widget widget-table action-table">
    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Box Details</h3>
    </div>
    <!-- /widget-header -->
	<div class="widget-content">
	   <div class="row-fluid stats-box">
	      	<div class="span4">
	        	  <div>
				        		<div>
		        	<span class="left-pane">Pell No.</span>
		        	<span class="right-pane"><input type="text" value="{!! $box_info->load_code !!}" readonly ></span>
		        </div>
				        </div>
		        <div>
		        	<span class="left-pane">Box No.</span>
		        	<span class="right-pane"><input type="text" value="{!! $box_info->box_no !!}" readonly></span>
		        </div>
	      	</div>
		        </div>
	      </div>
	</div>
	 </div>
</div>
	 
<div class="clear">
	<div class="div-paginate">
		{{--@if(CommonHelper::arrayHasValue($so_upc_details) )--}}
		    {{--<h6 class="paginate">--}}
				{{--<span>{{ $so_upc_details->appends($arrFilters)->links() }}&nbsp;</span>--}}
			{{--</h6>--}}
		{{--@else--}}
			{{--&nbsp;--}}
		{{--@endif--}}
	</div>
	
	 
</div>
<div class="widget widget-table action-table">


    <div class="widget-header"> <i class="icon-th-list"></i>
      <h3>Box Details</h3>
      {{--<span class="pagination-totalItems">{{ $text_total }} {{ $store_orders_count }}</span>--}}
    </div>
    <!-- /widget-header -->
    <div class="widget-content">
    	<div class="table-responsive">
			<table class="table table-bordered">
				<thead>
					<tr>
						<th>No.</th>
						<th>MTS No.</th>
						<th>SKU</th>
						<th>UPC</th>
						<th>Item Description</th>
						<th>Qty Order</th>
						<th>Qty Received</th>
						<th>Variance</th>

				 
					</tr>
				</thead>
			 	@if( !CommonHelper::arrayHasValue( $box_lists) )
				<tr class="font-size-13" >
					<td colspan="9" style="text-align: center;">No records found!</td>
				</tr>
				@else
					@foreach( $box_lists as $key => $list )

					<tr style="{!! $list->qty_rec != $list->qty_req ? 'background-color: #f2dede;' : '' !!}">
						<td>{!! $key+1 !!}</td>
						<td>{!! $list->tl_no !!}</td>
						<td>{!! $list->sku !!}</td>
						<td>{!! $list->upc !!}</td>
						<td>{!! $list->item_desc !!}</td>
						<td>{!! $list->qty_req !!}</td>
						<td>
							{!! Form::open(array('url' => 'store-receiving/updateqty', 'method' => 'post')) !!}
								<div class="col-xs-6">
									<div class="input-group">
										<input type="text" style="margin-top: 10px;" value="{!! $list->qty_rec !!}" name="qty_rec">


										<span class="input-group-btn">
											<button type="submit" class="btn btn-primary"><span class="icon icon-check"></span></button>
										</span>
									</div>
									<input type="hidden" value="{!! $list->tl_no !!}" name="tl_no">
									<input type="hidden" value="{!! $list->sku !!}" name="sku">
									<input type="hidden" value="{!! $list->upc !!}" name="upc">
								</div>
							{!! Form::close() !!}
						</td>
						<td>{!! $list->qty_rec - $list->qty_req !!}</td>
					</tr>
					@endforeach
				@endif
			</table>
		</div>
	</div>

 

</div>
@endsection
