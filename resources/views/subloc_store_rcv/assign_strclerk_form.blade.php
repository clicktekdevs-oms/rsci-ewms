@extends('layouts.main')
@section('content')
{{--@if( $errors->all() )--}}
    {{--<div class="alert alert-error">--}}
    	{{--<button class="close" data-dismiss="alert" type="button">&times;</button>--}}
    	{{--{{ HTML::ul($errors->all()) }}--}}
    {{--</div>--}}
{{--@endif--}}

<div class="widget">
    <div class="widget-header"> <i class="icon-th-list"></i>
    	<h3>Assign to Store Clerk</h3>
    </div>
    <!-- /widget-header -->

    <div class="widget-content">
    	{!! Form::open(array('url'=>'store-subloc-receiving/assign-clerk', 'method' => 'post', 'id'=>"form-assign", 'class'=>'form-horizontal', 'style' => 'margin: 0px;', 'role'=>'form'))  !!}
		<!-- <div class="span3">&nbsp;</div> -->
		<div class="span3">&nbsp;</div>
		<div class="span7 add-piler-wrapper">

			<div class="control-group">
				<label class="control-label">Pell :</label>
				<div class="controls">
					<input type="text" name="pell_no" readonly value="{!! $pell !!}">
				</div> <!-- /controls -->
			</div> <!-- /control-group -->

			<div class="control-group">
				<label class="control-label">Box No. :</label>
				<div class="controls">
					<textarea readonly style="resize: none" rows="5">
						{!! $box_no !!}
					</textarea>
					<input type="hidden" name="box_no" value="{!! $box_no !!}">
				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		
			<div class="control-group">
				<label class="control-label">Store Clerk :</label>
				<div class="controls">
					<select name="clerk">
						<option hidden>Select :</option>
						@foreach( $clerks as $clerk )
							<option value="{!! $clerk->id !!}">{!! ucwords( $clerk->name ) !!}</option>
						@endforeach
					</select>

				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		
        </div>
        <!-- <div class="span10">&nbsp;</div> -->
        <div class="span3">&nbsp;</div>
        <div class="span7">
        	<div class="control-group">
				<label class="control-label" for=""></label>
				<div class="controls">
					<button class="btn btn-info" type="submit">Assign</button>
					<a class="btn" href="{!! URL::previous() !!}">Cancel</a>
				</div> <!-- /controls -->
			</div> <!-- /control-group -->
		</div>

		{!! Form::close() !!}
	</div>
</div>
@endsection