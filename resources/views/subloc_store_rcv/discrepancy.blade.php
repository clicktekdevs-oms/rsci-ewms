<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Store Discrepancy Report</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<style type="text/css">

		html {

			font-family: sans-serif !important;
			font-size: 13px !important;
		}
		table{
			width: 100%;
			max-width: 100%;
			border-collapse: collapse;
		}


		tr {
			page-break-inside: avoid;
		}

		table tr {
			page-break-inside: avoid !important;
		}

		table .thead th{
			padding: 5px;
		}


		table .thead td{
			border:1px solid;
			padding: 5px;
			text-align: center;
		}

		table .thead th {
			text-align: center;
			border: 1px solid black;
		}

		@media print {
			.soContainer {page-break-after: always; page-break-inside: avoid;}
			#actionButtons {display: none;}

		}

		#actionButtons { margin-top: -10px; left: 0; background-color: #DFF1F7; padding: 5px;}
		#actionButtons a {display: inline-block; padding: 1em; background-color: #3199BE; text-decoration: none; font: bold 1em Verdana ; color: #FFF;}
		#actionButtons a:hover {background-color: #1F6077 ;}

	</style>
</head>
<body>
<div id="actionButtons">
	<a href="#" onclick="window.print();">PRINT THIS</a>
	<a href="{!! url('store-subloc-receiving/'.$pell) !!}"> BACK TO LIST</a>


</div>
<div class="table-responsive">
	<div style="text-align: center;font-size: 14px !important;line-height: 1.3;margin-bottom: 20px" >
		<a class="font-size-02" style="font-weight: 700"> Rustan Specialty Concepts Inc.<br/>Store Discrepancy Report<br/>{!! Auth::user()->store_code !!} - {!! ucwords( $store_name ) !!}</a>
	</div>


		<table class="contents2" style="font-size: 13px;margin-bottom: 15px" >
			<tr>
				<th colspan='3' style="text-align: left;">

					&nbsp;&nbsp;&nbsp;Pell No. : {!! $pell !!}
				</th>
				<th colspan='3'>
					Scanned By : {!! ucwords( $scanner ) !!}
			</tr>
			<tr>

				<th colspan='5' style="text-align: left">
					&nbsp;&nbsp;&nbsp;Print Date : {!! date('m/d/y h:i A') !!}

				</th>
				<th colspan='3'>

				</th>
			</tr>
			<br>
			<br>
		</table>

	<table class="table table-striped table-bordered">

		<tr class="thead">

			<td style="font-weight: bold">MTS No.</td>
			<td style="font-weight: bold">SKU</td>
			<td style="font-weight: bold">UPC</td>
			<td style="font-weight: bold">Item Description</td>
			<td style="font-weight: bold">Ordered Quantity</td>
			<td style="font-weight: bold">Received Quantity</td>
			<td style="font-weight: bold">Variance</td>
			<td style="font-weight: bold">Scanned Date</td>
		</tr>

		@if( !count( $discrepancies ) )
			<tr style="border: 1px solid">
				<td colspan="7" style="text-align:  center">No records found!</td>
			</tr>
		@endif


		<?php
			$total_qty_req = 0;
			$total_qty_rec = 0;
			$total_variance = 0;
		?>

		@foreach( $discrepancies as $discrepancy )

			<tr>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->tl_no !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->sku !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->upc !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->item_desc !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->qty_req !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->qty_rec !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->qty_rec - $discrepancy->qty_req !!}</td>
				<td style="text-align: center;border: 1px solid">{!! $discrepancy->updated_at !!}</td>
				<?php
					$total_qty_req += $discrepancy->qty_req;
					$total_qty_rec += $discrepancy->qty_rec;
					$total_variance += ($discrepancy->qty_rec - $discrepancy->qty_req)
				?>
			</tr>
		@endforeach

			<tr>
				<td colspan="4" style="text-align: right"><b>Grand Total :</b></td>
				<td style="text-align: right"><b><?php echo $total_qty_req ?></b></td>
				<td style="text-align: right"><b><?php echo $total_qty_rec ?></b></td>
				<td style="text-align: right"><b><?php echo $total_variance ?></b></td>
			</tr>



	</table>

	<table class="contents2" style="margin-top: 50px">
		<tr>
			<td colspan='3'>
				Generated By :
			</td>
			<td colspan='3'>
				Noted by/Date:
			</td>
		</tr>
		<tr>
			<td class="underline" colspan='2'><hr style="margin-top:20px;border-top: 1px solid black;"></td>
			<td></td>
			<td class="underline" colspan='2'><hr style="margin-top:20px;border-top: 1px solid black;"></td>
			<td></td>
		</tr>
	</table>
</div>

</body>
</html>