#! /bin/bash
find /var/log/ewms_log/po_rcv/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/picking/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/shipping/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/store_rcv/ -type f -mtime +6 -exec rm {} \;

find /var/log/ewms_log/return_wh/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/subloc_rcv/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/subloc_pick/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/subloc_load/ -type f -mtime +6 -exec rm {} \;
find /var/log/ewms_log/masterlist/ -type f -mtime +6 -exec rm {} \;
